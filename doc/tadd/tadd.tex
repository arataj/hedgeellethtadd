\documentclass[A4]{article}
\usepackage{epsfig}
\usepackage{color}

\input margins.tex
\input figures.tex

\newcommand{\code}[1]{\texttt{#1}}

\sloppy

\begin{document}
\begin{center}
\LARGE Conversion of internal assembler to TADD\\
\vspace{0.1in}
\large Artur Rataj\\
\vspace{0.3in}
\end{center}
\section{Introduction}
Once the assembler code is generated, two stages remain
to convert the assembler to TADDs: interpreting and generating
transitions.

\section{Interpreter}
The role of the interpreter is to initialize variables,
load needed classes, create objects and threads.

\subsection{Threads}
A main thread is created with the main method as the start one.
The interpreter is started with that single thread, and ends
once the main thread ends.
All other threads created by the interpreter when executing the
main thread, called here TADD threads, are the beginnings
of TADDs, one TADD per thread.

The main thread must end and must do it in a relatively short time.
Only assignments, allocations, calls and expressions that can be
statically determined are allowed in the main thread

\subsection{Library and annotations}
The interpreter uses a library that defines the needed Java classes,
like \verb|Object| or \verb|Thread|. In the library, some methods
are marked with special annotations beginning with double `@':
The annotations \verb|@@IGNORE|, \verb|@@MARKER|
and \verb|@@START_THREAD| are defined
as in the translator used by this TADD generator. The annotations
specific to TADDs are as follows:
\begin{itemize}
\item \verb|@@EMPTY_TRANSITION| -- generate an empty transition,
used with marker methods, the library does not use this annotation,
but there is an option to mark empty methods with this annotation.

\item \verb|@@JOIN| -- perform \verb|join()| on an object,
used with marker methods, usually only with \verb|Object.join()|.

%% \item \verb|@@NEW_THREAD| -- start a new thread, used with marker
%% methods, usually only with \verb|Thread.start()|.

\item \verb|@@NOTIFY| -- perform \verb|notify()| on an object,
used with marker methods, usually only with \verb|Object.notify()|.

\item \verb|@@NOTIFY_ALL| -- perform \verb|notifyAll()| on an object,
used with marker methods, usually only with \verb|Object.notifyAll()|.

\item \verb|@@RANDOM| -- generate a random number in the range
0 ... method's integer argument - 1. 
Used with marker methods, specifically in Java with 
\verb|Random.nextInt(int)|. Used for non--deterministic
choices.

\item \verb|@@DIST_UNI| and \verb|@@DIST_EXP| -- generate a random number in the range
\(\left<0 \ldots 1\right)\), using respectively an uniform ot a negative exponential
distribution.
Used with marker methods, specifically in Java the former is used with
\verb|Math.random()|, and translated into a probabilistic choice.

\item \verb|@@SLEEP| -- sleep by a given number of milliseconds.
Used with marker methods, usually only with
\verb|Thread.sleep(int)|.

\item \verb|@@WAIT| -- perform \verb|wait()| on an object,
used with marker methods, usually only with \verb|Object.wait()|.

\end{itemize}

Annotations \verb|@@IGNORE|, \verb|@@MARKER| and \verb|@@START_THREAD|
are used only by the interpreter. Annotations \verb|@@NOTIFY|,
\verb|@@NOTIFY_ALL|,
\verb|@@RANDOM|, \verb|@@SLEEP| and \verb|@@WAIT| are recognized
at the stage of generating transitions, where respective
special sets of transitions are generated out of them.

\subsection{Special annotations}
There are special annotations added to the operations that accompany
transitions:
\begin{itemize}
\item \verb|@@IN| -- the ``in'' transition that enters the critical
section, and the ``active'' transition of \verb|wait()|;

\item \verb|@@OUT| -- the ``out'' transition that leaves the critical
section, and the ``wait'' transition of \verb|wait()|;

\item \verb|@@NOTIFY_THREAD| -- the ``notify'' transitions
of \verb|wait()|, \verb|notify()| or \verb|notifyAll()|;

\item \verb|@@DO_NOTHING| -- the transition that does not notify
any thread in \verb|notify()|.
\end{itemize}

You may use these special annotations, just like any other annotations,
to selectively apply comment tags.

\subsection{Comment tags}
The tag \verb|@selected| is recognized. Source or target locations
of transitions, that have at least a
single accompanying operation that contain the tag, can be selected
depending on an additional annotation on the transition:
annotation:
\begin{itemize}
\item \verb|@@IN| --
target transition is chosen, that is, the one after the critical section
is entered;

\item \verb|@@OUT| --
target transition is chosen, that is, the one after the critical section
is left;

\item \verb|@@NOTIFY_THREAD| --
source transition is chosen, that is, the one before the thread is
notified;

\item \verb|@@DO_NOTHING| --
source transition is chosen;

\item \verb|@@TAIL| --
target transition is chosen.

\end{itemize}

In the case of any other tag and in the absence of any of the above tags,
the source location is chosen.

Effects of the selection are backend--dependent. Currently only the
\code{Verics} backend uses the selection to specify the valuation section.

Tags are ignored in the part of code that is only interpreted.

\medskip

\noindent Example:
\begin{verbatim}
  @selected(.inline infinite, .annotation @@WAIT, .annotation @@IN)
  put(packet);
\end{verbatim}
Apply the discussed tag to every operation \(o\)
that is within the flattened code of
the recursively inlined methods that replace the call
\verb|put()|, but only if \(o\)
contains the annotations \verb|@@WAIT| and
\verb|@@IN|. In effect, the tag is applied only
to operations that accompany the transitions ``active'' of
\verb|wait()| methods.

\section{Building TADDs}
This section described generating TADDs after interpreting
the main thread.

\subsection{Virtual call resolution}
The first step is virtual call resolution. In Java, a called
non--static method is determined by the runtime type of an object,
and not by the compile--type type by the object. After the
interpreter is run, some calls can be resolved thus, and if it
is not possible because of a null variable or a variable not
initialized at the interpreter stage, an error is reported.

\subsection{Code flattening}
Once the calls are resolved, it is possible to `flatten' the
code. To do so, the code is recursively inlined beginning at
a thread's start method,
so that there should be no any calls left in the `flattened' code.
This is because no stack is emulated in the generated TADDs,
and also because inlined code allows for more efficient optimizations
like reduction of variables and assignments.

\subsection{Optimization}
The next step is an optimization of the code like: value propagation,
optimizing branches with static conditions, compacting jump and
assignment chains, checking for dead code, removal of assignments
and locals that are unused, also unused because of the optimizations.

This step is performed because inlining methods allows for substantial
simplifications even if some code was previously optimized.

\subsection{Ambiguity check}
The TADD translator allows for assignments of reference variables by the
TADD threads, if the translator can determine that the assignments
do not cause a reference to be ambiguous. The translator checks only
local reference variables for ambiguity, thus, assignments to
non--local reference variables by TADD threads are forbidden.

Ambiguity means here that a variable can have more than a single
value. Such an ambiguity of a reference variable would make it impossible
to statically dereference the variable. Only dereferences that can be determined
statically are allowed, as TADDs do not support dereferences and some runtime
emulation of dereferences is not implemented.

For--like loops can be unrolled by the optimizer, changing their counter variables
into constants, and in this way dereferences of arrays may cease
to be ambiguous. The unrolling is forced for such loops, if
an indexing, using the loop counter, of an array of dereferences exists inside.

\subsection{Generating transitions}
Most operations are straightforwardly translated to TADDs:
assignments, jumps, binary and unary expressions and conditional
branches. There are some special cases, though.

\subsubsection{Merging operations}
There are some multiple assembler operations that are treated as
a whole, and as such transformed to respective TADD transitions:
\begin{itemize}
\item conditional expression + conditional branch as the next operation,
that uses the result of the conditional expression, is
transformed to two opposite conditional expressions on two transitions;

Example:
\begin{verbatim}
  0 c = a < 1
  1 c ? <null> : 3
\end{verbatim}
is translated to two conditional expressions: \(a < 1\) on a
transition from state 1 to state 2 and \(a \ge 1\) on a
transition from state 1 to state 3.

\item a marker method \verb|@RANDOM| followed by a marker method   
\verb|@SLEEP| that uses the result of the first marker method
is transformed to a respective clock condition.

Example:
\begin{verbatim}
  0 c = @@MARKER @@RANDOM (random 150)
  1 no op @@MARKER @@SLEEP (c)
\end{verbatim}  
is translated to series of parallel transitions, with
the following clock condition on each: \((x > t_{i})\), where
\(t_{i}\) is the transition number, \(0 \le t_{i} < 150\).

\item if an assignment of a non--deterministic value to a variable
is immediately followed by a tree of conditional choices over
that variable, the optimizer may merge respective
non--deterministic and conditional choices into a single
set of transitions, each containing both the variable's
update and the updates in the tree of conditional choices.
\end{itemize} 

\subsubsection{Locks and synchronization}
Annotation \verb|@WAIT| and synchronization
operations cause a new lock TADD 
to be created for the respective synchronization object if it does
not exist yet. Transitions are added to the lock as necessary.

As required by Java, within a single thread
synchronization operations on runtime object \(k\) are ignored if they
are nested within synchronization operations that are also on
the object \(k\).

If broadcast synchronization is absent, \code{notifyAll()} is implemented
using special trees, as shown in Fig.\ref{fig:notify-all}.
\pslatexfigure{notify-all}{A tree of transitions that emulates
\code{notifyAll()}, without broadcast and combined synchronization/variable
update. There are four threads 1, 2, 3 and 4, and the notifying thread
is 1. The variable that holds the number of waiting threads is named
\emph{c}. Meaning of symbols: \emph{x}! -- notify \emph{x}th thread,
-1 -- decrease \emph{c} by 1.}

\section{Choices}
Three kinds of choices are supported: conditional, probabilistic and
nondeterministic.

\subsection{Conditional choice}
A conditional choice is the translation of one or more constructs
like Java's \code{if}. The choice is translated into a state with
a number of transitions such that they have complementary guards, i.e.
always one and only one is true.

\psfigure{conditional-choice}{1.4in}{A conditional choice made
from a number of binary branches.}
For an example, see Fig.~\ref{fig:conditional-choice}.

As a volatile variable evaluated multiple times
might yield a different value each time, a care is taken,
that testing of such variables in the output graph match exactly the ones
in the source code. For non--volatile variables, though,
optimizations can be applied, which collapse several evaluations
into a single one.

\subsection{Probabilistic choice}
A probabilistic choice is translated from conditional expressions
that employ the call to a method
with the annotation \verb|@@DIST_UNI| and are of the form
\code{if(Math.random() < 0.4)}.
A choice of the type is translated into a state with two transitons,
each decorated with a probability value, and these two values sum to 1.

\psfigure{probabilistic-choice}{1.2in}{An example probabilistic choice.}
For example, the mentioned construct would be translated as seen
in Fig.~\ref{fig:probabilistic-choice}.

A probabilistic choice defined using a single \verb|if| has only two outgoing transitions.
To overcome that limitation, there is also available a balanced
\(n\)--ary probabilistic choice, that is translated into \(n\) outcoming
transitions, each labelled with a probability \(1/n\). It is defined
using an expression \verb|(int)(Math.random()*|\(n\)\verb|)|, which is
typically assigned to a variable.

\subsection{Nondeterministic choice}
A nondeterministic choice expresses a lack of knowledge about a model's
behaviour. It is expressed by a method annotated with \verb|@@RANDOM|.
In the case of Java,
the method is \verb|Random.nextInt(int |\(i\)\verb|)| is used, although the contract
of that method says, that the returned values are distributed
approximately uniformly. For the TADD backend, though, such a method's contract is,
that it returns some natural value from the range \(\left<0 \ldots i\right)\),
and there is nothing known about the distribution of the returned values.

A nondeterministic choice consists from a single state, from
which emerge \(i\) transitions, each assumes, that a different value
has been returned.
\psfigure{nondeterministic-choice}{1.8in}{An example nondeterministic choice.}
An example for \verb|i = (int)(Math.random()*3)| is shown in Fig.~\ref{fig:nondeterministic-choice}.
\end{document}
