\documentclass[A4, twocolumn]{article}
\usepackage{epsfig}
\usepackage{cellspace}
\usepackage[bindingoffset=0cm,hdivide={1.2cm,*,1.2cm},vdivide={-1.0cm,*,0.0cm}]{geometry}
\usepackage{realboxes}
\usepackage{amsmath}
\usepackage{lmodern}

\setlength{\columnsep}{0.0in}

\newcommand{\code}[1]{\texttt{#1}}

\newcommand{\footnoteremember}[2]{\footnote{#2}\newcounter{#1}\setcounter{#1}{\value{mpfootnote}}}
\newcommand{\footnoterecall}[1]{\footnotemark[\value{#1}]}

\sloppy

\begin{document}
\twocolumn[
  \begin{@twocolumnfalse}

\begin{center}
{\large\bf
	\texttt{\textbf{hc}} reference sheet
}
\vspace{0.1in}
\end{center}

  \end{@twocolumnfalse}
  ]

\thispagestyle{empty}
\hspace*{0.0in}\begin{minipage}{3.5in}
\renewcommand\thempfootnote{\scriptsize\arabic{mpfootnote}}
\begin{center}\large Subset of Java\end{center}\vspace{-0.05in}  
\centering
\begin{tabular}{|S{p{2.0in}}|Sc|Sc|}
\hline
Feature & Main & Automaton \\
\hline
\hline
access modifiers & + & + \\
\hline
allocation statements & + & - \\
\hline
arithmetic operators & + & +\footnote{exact subset is backend dependent, the
operators \code{+} \code{-} \code{*} \code{/} are supported by all backends} \\
\hline
arrays, including indexing & + & +\footnote{in the backends XML, Uppaal; emulated in PTA, Prism} \\
\hline
assertions & + & + \\
\hline
casts & + & + \\ % \footnote{if backend supports the operator \code{\%}} \\
\hline
call statements & + & +\footnote{non--recursive} \\
\hline
classes\footnote{no support for nested classes},
including interfaces & + & + \\
\hline
control flow statements: blocks,
\code{break}, \code{continue}, \code{do}--\code{while},
\code{if}, \code{for}, labels, \code{switch}, \code{while},
\code{return} & + & + \\
\hline
class inheritance, implementation & + & + \\
\hline
dereferences of
objects & + & +\footnoteremember{resolvable}{only if resolvable after the interpreting step} \\
\hline
exceptions & -\footnote{throw of \code{RuntimeException} is treated
as a failed assertion} & -\footnotemark[\value{mpfootnote}]\(^{,}\)\footnote{\code{try}--\code{catch} constructs are supported if the catch
blocks\\ ~\hspace{0.5in} are compiled to empty statements, automatons are generated assuming no
exceptions are actually thrown, except for \code{RuntimeException}; \code{throws} is allowed but meaningless} \\
\hline
final classes, methods and variables & + & + \\
\hline
initializers, including array initializers & + & + \\
\hline
\code{instanceof} & + & +\footnote{only if object is known after the interpreting step} \\
\hline
\code{Thread.interrupt()}, \code{Thread.isInterrupted()} &
+\footnote{only the main thread interrupts, only automaton threads check status} & +\footnotemark[\value{mpfootnote}] \\
\hline
\code{Math.random()} & - & +\footnote{translated to a probabilistic choice} \\
\hline
packages & + & + \\
\hline
\code{Random.nextInt(int)} & - & +\footnote{argument must be a constant; translated to a
non--deterministic choice} \\
\hline
ranges on arithmetic types
and expressions\footnote{defined within comment tags} & + & + \\
\hline
relational operators & + & +\footnote{object comparison only if objects
known after the interpreting step} \\
\hline
string operations & + & -\footnote{do not cause translation errors, but are removed} \\
\hline
synchronized blocks and methods & - & + \\
\hline
\code{System.out.print*()} & + & -\footnotemark[\value{mpfootnote}] \\
\hline
\code{Thread.sleep(int)} & - & +\footnote{argument must be either a constant or
\code{Random.nextInt(int)}} \\
\hline
\code{Thread.start()} & + & - \\
\hline
thread lock control: \code{join()}, \code{notify()}, \code{notifyAll()},
\code{wait()} & - & +\footnote{\code{notifyAll()} in the backends XML, Uppaal; emulated in PTA, Prism} \\
\hline
virtual methods & + & +\footnoterecall{resolvable} \\
\hline
\end{tabular}\par
\vspace{-0.75\skip\footins}
\renewcommand{\footnoterule}{}
\end{minipage}
% \end{center}

\vspace{1in}

\begin{center}
{\large Comment tags}
\end{center}
\vspace{-0.05in}
\Centerline{
\begin{minipage}{2.8in}
\begin{itemize}
\item variable, method and expression ranges\\
\hspace*{0.2in}\code{@(}min\code{,} max\code{)}

\item refrain from compilation of file/rest of
classes\\
\hspace*{0.2in}\code{@hcSkipRest()}

\end{itemize}
\end{minipage}
}

\vspace{0.0in}

\begin{center}
{\large Choices}
\end{center}
\vspace{-0.05in}
\Centerline{
\begin{minipage}{2.8in}
\begin{itemize}
\item probabilistic binary\\
\hspace*{0.2in}\(x\) \(<\)rel.~op\(>\) \code{Math.random()}

\item probabilistic \(n\)-ary, balanced\\
\hspace*{0.2in}\code{(int)(Math.random()*}\(n\)\code{)}

\item non--deterministic \(n\)-ary\\
\hspace*{0.2in}\code{Random.nextInt(}\(n\)\code{)}

\end{itemize}
\end{minipage}
}

\vspace{0.15in}

\noindent\Centerline{
\begin{minipage}{2.8in}
\renewcommand\thempfootnote{\scriptsize\arabic{mpfootnote}}
\begin{center}{\noindent\large~Delays\footnote{times for
an immediate non--overriding scheduler}}\vspace{-0.1in}
\end{center}
\begin{itemize}
\item constant, \(x = v\)\\  
\hspace*{0.2in}\code{Sleep.exact(}\(v\)\code{)}

\item probabilistic negative exponential\footnote{only in the backend PTA},\\
\(x \in (0, \infty)\)\\      
\hspace*{0.2in}\code{Sleep.exact(Dist.nxp(}\(\lambda\)\code{))}

\item probabilistic tabularised\footnotemark[\value{mpfootnote}], \(x \in (v_{1}, v_{N})\)\\
\hspace*{0.2in}\code{Sleep.exact(Dist.tab(}\(\mathbf{v}, \boldsymbol{\rho}\)\code{))}

\item non--deterministic, \(x = 0, 1, \ldots v - 1\)\\                    
\hspace*{0.2in}\code{Sleep.exact(Random.nextInt(}\(v\)\code{))}

\end{itemize}
Variants: 
\code{Sleep.min(}\(v\)\code{)}, \code{Sleep.max(}\(v\)\code{)},
\code{Thread.sleep(}\(1 \cdot 10^{-3} v\)\code{)}.
\end{minipage}
}

\vspace{0.1in}

\noindent\hspace*{1.0in}\begin{minipage}{1.9in}
\renewcommand\thempfootnote{\scriptsize\arabic{mpfootnote}}
\begin{center}
{\large Default ranges\footnote{the Verics backend allows only for \code{boolean}
and \code{int}}}\vspace{0.05in}

\hspace{-0.1in}\begin{tabular}{|Sc|Sc|Sc|}
\hline
type & min & max \\
\hline
\hline
\code{boolean} & \(0\) & \(1\) \\
\hline
\code{byte} & \(-2^{7}\) & \(2^{7} - 1\) \\
\hline
\code{char} & \(0\) & \(2^{16} - 1\) \\
\hline
\code{short} & \(-2^{15}\) & \(2^{15} - 1\) \\
\hline
\code{int} & \(-2^{15}\) & \(2^{15} - 1\) \\
\hline
\code{long} & \(-2^{31} + 1\) & \(2^{31} - 2\) \\
\hline
\end{tabular}
\end{center}
\par\vspace{-0.25in}
\renewcommand{\footnoterule}{}
\end{minipage}

\begin{center}
{\large Library}
\vskip 0.02in
\begin{tabular}{lp{2in}}
\code{Barrier} & synchronisation using conditional barrier \\
\code{\emph{BooleanClosure}} & abstract boolean closure \\
\code{MatrixIO} & matrix i/o of the main thread \\
\code{Model} & state formulas, custom naming, players,
model type, external constants, checked properties, simulation end \\
\code{Sleep} & sleep methods \\
\code{TuplesIO} & tuple i/o of the main thread \\
\code{TurnGame} & simplifies turn games\\
\code{TurnPlayer} & an agent within \code{TurnGame}\\
\end{tabular}
\end{center}

% A file compilable only by \code{javac}: use the tag
% \code{@hcSkipRest} before a package statement or the
% first class to ignore. A file compilable only by
% \code{hc}: use the extension \code{hc\_java}.
% \vspace{0.2in}

% \vspace{2.3in}~


\end{document}
