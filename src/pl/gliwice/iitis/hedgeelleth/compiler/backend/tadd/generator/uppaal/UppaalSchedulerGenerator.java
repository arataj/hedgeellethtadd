/*
 * UppaalSchedulerGenerator.java
 *
 * Created on Jul 22, 2009, 1:32:30 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.uppaal;

import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.AbstractSchedulerGenerator;

/**
 *
 * @author Artur Rataj
 */
public class UppaalSchedulerGenerator implements AbstractSchedulerGenerator {
    /**
     * Creates a new instance of UppaalSchedulerGenerator. 
     */
    public UppaalSchedulerGenerator() {
    }
}
