/*
 * FPath.java
 *
 * Created on Feb 19, 2010, 11:43:55 AM
 *
 * Copyright (c) 2010 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.properties;

import java.util.*;

/**
 * Path property called "eventually" or "future", for representing
 * reachability.
 *
 * @author Artur Rataj
 */
public class FPath extends AbstractPath {
    /**
     * An expression for this path.
     */
    public AbstractConditionalExpression sub;

    /**
     * Creates a new instance of FPath.
     *
     * @param sub                       an expression for this path
     */
    public FPath(AbstractConditionalExpression sub) {
        this.sub = sub;
    }
    @Override
    public String toString() {
        return "F " + sub.toString();
    }
}
