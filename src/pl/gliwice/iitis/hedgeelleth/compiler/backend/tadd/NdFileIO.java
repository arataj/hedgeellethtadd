package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

/*
 * NdFileIO.java
 *
 * Created on Jun 17, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */



import java.util.*;
import java.io.*;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.BackendPTACompilation;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.AbstractCodeOp;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.CodeOpNone;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.util.PWD;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.NdFile;
import pl.gliwice.iitis.hedgeelleth.pta.BranchId;
import pl.gliwice.iitis.hedgeelleth.pta.PTA;
import pl.gliwice.iitis.hedgeelleth.pta.PTATransition;
import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAVariable;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAScalarVariable;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAVirtualVariable;

/**
 * Writes/reads a non--deterministic data file.
 * 
 * @author Artur Rataj
 */
public class NdFileIO {
    /**
     * Default extension.
     */
    public static String EXTENSION = "nd";

    static class Fork {
        public List<PTATransition> xs = new LinkedList<>();
        public List<String> filters = new LinkedList<>();
        public List<List<Integer>> ints = new LinkedList<>();
    };
    /**
     * Analyzes the extract in order to find source code for each
     * argument.
     * 
     * @param extract extract to analyse
     * @param numArgs number of arguments, for error detection
     * @return subsequent source fragments
     */
    private static List<String> analyzeExtract(String extract, int numArgs)
            throws IOException {
         List<String> out = new LinkedList<>();
         if(extract.isEmpty() || !extract.endsWith(")"))
             throw new RuntimeException("expected a closing parenthesis after arguments");
         extract = extract.substring(0, extract.length() - 1);
         int pos = extract.lastIndexOf("(");
         if(pos == -1)
             throw new RuntimeException("expected an opening parenthesis before arguments");
         extract = extract.substring(pos + 1);
         final String ERROR = "arguments must be simple dereferences without arrays";
         Scanner sc = CompilerUtils.newScanner(extract).useDelimiter(",");
         if(!sc.hasNext())
             throw new RuntimeException("bound argument not found");
         sc.next();
         while(sc.hasNext()) {
             String arg = sc.next().trim();
             boolean first = true;
             for(char c : arg.toCharArray()) {
                 if(c == '.') {
                     first = true;
                     continue;
                 }
                 if(first) {
                     if(!Character.isJavaIdentifierStart(c))
                         throw new IOException(ERROR);
                 } else {
                     if(!Character.isJavaIdentifierPart(c))
                         throw new IOException(ERROR);
                 }
                 first = false;
             }
             out.add(arg);
         }
         if(out.size() != numArgs)
            throw new IOException(ERROR);
         return out;
    }
    /**
     * Writes nd data to a file from a compilation.
     * 
     * @param outFile file to create/overwrite
     * @param compilation compilation
     */
    public static void write(PWD topDirectory, File outFile,
            BackendPTACompilation compilation) throws IOException {
        PrintWriter out = new PrintWriter(outFile);
        try {
            SortedMap<BranchId, Fork> forks = new TreeMap<>();
            for(PTA pta : compilation.ptas.values()) {
                for(PTATransition x : pta.transitions) {
                    BranchId b = x.branchId;
                    if(b != null && b.type == BranchId.Type.NONDETERMINISTIC) {
                        if(!compilation.xStateFilter.keySet().contains(x))
                            throw new IOException("chosen backend does not provide information " +
                                    "necessary for a Nd file");
                        String filter = compilation.xStateFilter.get(x);
                        if(filter == null)
                            throw new IOException("the generated model has some additional " +
                                    "special guards incompatible with a Nd file");
                        if(x.ndInt == null)
                            throw new IOException("a non-deterministic branch not made using a " +
                                    "standard integer choice is not compatible with a Nd file: " +
                                    x.comments);
                        Fork fork;
                        if((fork = forks.get(b)) == null) {
                            fork = new Fork();
                            forks.put(b, fork);
                        }
                        fork.xs.add(x);
                        fork.filters.add(filter);
                        fork.ints.add(x.ndInt);
                        for(AbstractPTAVariable v : b.arguments) {
                            if(v instanceof PTAVirtualVariable) {
                                PTAVirtualVariable vv = (PTAVirtualVariable)v;
                                if(!compilation.allowedNdParameters.keySet().contains(vv.name))
                                    throw new IOException(vv.name + " used as an Nd parameter, " +
                                            "but is not a model constant");
                            }
                        }
                    }
                }
                for(BranchId b : forks.keySet()) {
                    Fork f = forks.get(b);
                    String extract = null;
                    StringBuilder xs = new StringBuilder();
                    Iterator<PTATransition> xI = f.xs.iterator();
                    Iterator<String> fI = f.filters.iterator();
                    Iterator<List<Integer>> iI = f.ints.iterator();
                    Set<String> filterUnique = new HashSet<>();
                    boolean first = true;
                    while(fI.hasNext()) {
                        PTATransition x = xI.next();
                        String filter = fI.next();
                        List<Integer> ndInt = iI.next();
                        if(x.backendOps.size() != 1)
                            throw new RuntimeException("expected a single op");
                        AbstractCodeOp op = x.backendOps.get(0);
                        if(!(op instanceof CodeOpNone))
                            throw new RuntimeException("expected a none op");
                        if(op.textRange == null)
                            throw new RuntimeException("expected a source text range");
                        if(first) {
                            out.println(op.textRange.encode(topDirectory));
                            extract = op.textRange.extract(topDirectory);
                        }
                        StringBuilder ndIntStr = new StringBuilder();
                        for(int i : ndInt) {
                            ndIntStr.append("#");
                            ndIntStr.append(i);
                        }
                        String filters = filter +"," + compilation.xTargetStateFilter.get(x);
                        if(filterUnique.contains(filters))
                            throw new IOException(op.getStreamPos() + ": " +
                                    "could not simplify a non--deterministic choice: formula too complex");
                        filterUnique.add(filters);
                        xs.append(ndIntStr.toString() + "," + filters + "\n");
                        first = false;
                    }
                    List<String> argStrings;
                    try {
                        argStrings = analyzeExtract(extract, b.arguments.size());
                    } catch(IOException e) {
                        throw new IOException(b.textRange.BEG.toString() + ": " +
                                e.getMessage());
                    }
                    Iterator<String> argStringsI = argStrings.iterator();
                    Set<String> uniqueName = new HashSet<>();
                    for(AbstractPTAVariable v : b.arguments) {
                        String name;
                        if(v instanceof PTAVirtualVariable) {
                            PTAVirtualVariable vv = (PTAVirtualVariable)v;
                            String value;
                            if(vv.floating)
                                value = vv.initValue + NdFile.DOUBLE_SUFFIX;
                            else
                                value = "" + (int)vv.initValue;
                            name = compilation.allowedNdParameters.get(vv.name) +
                                    "," + value;
                        } else if(v instanceof PTAScalarVariable) {
                            PTAScalarVariable sv = (PTAScalarVariable)v;
                            name = sv.name;
                        } else
                            throw new RuntimeException("unexpected");
                        if(uniqueName.contains(name))
                            throw new IOException(b.textRange.BEG.toString() + ": " +
                                    "duplicate argument to nd function: " + name);
                        uniqueName.add(name);
                        out.println("\"" + argStringsI.next() + "\"" + name);
                    }
                    out.print(xs.toString());
                }
            }
            if(out.checkError())
                throw new IOException("could not write to " + topDirectory.getRelativePath(
                    outFile.getAbsolutePath()));
        } catch(IOException e) {
            outFile.delete();
            throw e;
        } finally {
            out.close();
        }
    }
}
 