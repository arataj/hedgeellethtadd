/*
 * UppaalTADDGenerator.java
 *
 * Created on Jul 22, 2009, 1:29:49 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.uppaal;

import pl.gliwice.iitis.hedgeelleth.pta.PTAException;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.*;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeCompilation;

/**
 * Generates a TADD Uppaal--style.
 *
 * @author Artur Rataj
 */
public class UppaalTADDGenerator extends GraphTADDGenerator {
    /**
     * Creates a new instance of UppaalTADDGenerator.
     *
     * @param compilation               compilation
     * @param options                   standard types of options of this TADD
     *                                  generator
     */
    public UppaalTADDGenerator(CodeCompilation codeCompilation,
            TADDGeneratorOptions options) throws PTAException {
        super(codeCompilation, options,
                new UppaalSchedulerGenerator(),
                new UppaalSleepGenerator());
    }
}
