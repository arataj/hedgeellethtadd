/*
 * JavaTADDIO.java
 *
 * Created on Jul 17, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.xml;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.BackendPTACompilation;

import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.FileFormatOptions;
import pl.gliwice.iitis.hedgeelleth.compiler.util.ModelType;

/**
 * Generates a text representation of a TADD compilation in the
 * format of Uppaal. No loading is implemented.
 *
 * @author Artur Rataj
 */
public class UppaalIO extends XMLIO {
    /**
     * Creates a new generator of TADD compilation to file in the format
     * of Uppaal.
     * 
     * @param options TADD generation options
     * @param committedLocks            commited locations of locks
     */
    public UppaalIO(FileFormatOptions options, boolean committedLocks) {
        super(options, committedLocks);
    }
    @Override
    public List<ModelType> write(File file, BackendPTACompilation c) throws IOException {
        write(Profile.UPPAAL, file, c);
        c.type = ModelType.PTA;
        return Arrays.asList(new ModelType[] {c.type});
    }
}
