/*
 * TestAssemblyBuffer.java
 *
 * Created on Sep 23, 2009, 2:46:11 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

/**
 * A buffer with textual representation of assembly code, for testing
 * purposes only.
 *
 * @author Artur Rataj
 */
public class TestAssemblyBuffer {
    /**
     * Buffer of the last inline method, without runtime static analysis.
     */
    public String inline0;
    /**
     * Buffer of the last inline method, after runtime static analysis.
     */
    public String inline;
}
