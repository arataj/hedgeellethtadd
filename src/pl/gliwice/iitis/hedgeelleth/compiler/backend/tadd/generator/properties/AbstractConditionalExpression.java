/*
 * AbstractConditionalExpression.java
 *
 * Created on Feb 19, 2010, 1:23:59 PM
 *
 * Copyright (c) 2010 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.properties;

/**
 * An abstract conditional expression. Its subclasses are a path,
 * a relational expression, and a boolean expression.
 * 
 * @author Artur Rataj
 */
public class AbstractConditionalExpression {
    /**
     * Creates a new instance of AbstractConditionalExpression. 
     */
    public AbstractConditionalExpression() {
    }
}
