/*
 * UniMult.java
 *
 * Created on Oct 4, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator;

import pl.gliwice.iitis.hedgeelleth.compiler.CompilerAnnotation;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeConstant;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.CodeOpBinaryExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.pta.PTAException;

/**
 * A class for representing an equation of the form
 * <code>@DIST_UNI[()] * &lt;integer&gt;</code>.
 * 
 * @author Artur Rataj
 */
public class UniMult {
    /**
     * Multiplier of <code>@DIST_UNI[()]</code>.
     * Always &gt;= 1.
     */
    public int multiplier;
    
    /**
     * Creates a new instance of <code>UniMult</code>.
     * 
     * @param multiplier multiplier of <code>@DIST_UNI[()]</code>;
     * must be &gt;= 1
     */
    protected UniMult(int multiplier) {
        if(multiplier < 1)
            throw new RuntimeException("unexpected vaue");
        this.multiplier = multiplier;
    }
    /**
     * Tests, if a given expression is of the form
     * <code>@DIST_UNI[()] * &lt;integer&gt;</code>.
     * If yes, returns its description. Otherwise, returns null.
     * 
     * @param expr expression to test
     * @return a description or null
     */
    public static UniMult analyze(CodeOpBinaryExpression expr)
            throws PTAException {
        UniMult out = null;
        if(expr.left.value instanceof CodeConstant &&
                expr.right.value instanceof CodeConstant) {
            Literal left = ((CodeConstant)expr.left.value).value;
            Literal right = ((CodeConstant)expr.right.value).value;
            CompilerAnnotation annotation = null;
            long multiplier = 0;
            if(left.type.isOfIntegerTypes() && right.type.isAnnotation()) {
                annotation = right.type.getAnnotation();
                multiplier = left.getMaxPrecisionInteger();
            } else if(left.type.isAnnotation() && right.type.isOfIntegerTypes()) {
                annotation = left.type.getAnnotation();
                multiplier = right.getMaxPrecisionInteger();
            }
            if(annotation == CompilerAnnotation.DIST_UNI) {
                if(multiplier  > Integer.MAX_VALUE)
                    throw new PTAException(expr.getStreamPos(),
                            "multiplier of a uniform distribution does not fit into a 32--bit integer");
                if(multiplier  < 1)
                    throw new PTAException(expr.getStreamPos(),
                            "multiplier of a uniform distribution must be greater than 1");
                out = new UniMult((int)multiplier);
            }
        }
        return out;
    }
    @Override
    public String toString() {
        return "uni()*" + multiplier;
    }
}
