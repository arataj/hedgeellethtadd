/*
 * PrismIO.java
 *
 * Created on Jun 8, 2008
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.prism;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.*;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.AbstractTADDGenerator;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.properties.*;
import pl.gliwice.iitis.hedgeelleth.compiler.sa.RangeCodeStaticAnalysis.Proposal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.BinaryExpression.Op;
import pl.gliwice.iitis.hedgeelleth.compiler.util.ModelType;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.ToAlphanumeric;
import pl.gliwice.iitis.hedgeelleth.interpreter.ModelControl;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.AbstractRuntimeContainer;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.ArrayStore;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeObject;
import pl.gliwice.iitis.hedgeelleth.pta.PTAException;
import pl.gliwice.iitis.hedgeelleth.pta.values.*;
import pl.gliwice.iitis.hedgeelleth.pta.*;
import pl.gliwice.iitis.hedgeelleth.pta.io.GuardedExpression;
import pl.gliwice.iitis.hedgeelleth.pta.io.NameShortener;
import pl.gliwice.iitis.hedgeelleth.pta.io.PTAIO;
import pl.gliwice.iitis.hedgeelleth.pta.io.PTAIO.Context;
import pl.gliwice.iitis.hedgeelleth.pta.io.PTAIO.Flavour;
import pl.gliwice.iitis.hedgeelleth.pta.io.VariableDescription;

/**
 * Generates a text representation of a PTA compilation in the format
 * used by either the model checker Prism or Verics runtime.<br>
 * 
 * As opposed to <code>PTAIO.write()</code>, is compatible
 * with <code>BackendPTACompilation</code> and adapts the
 * compilation by performing operations like name shortening, or
 * generating invariants for range checking, or allowing arrays
 * indexed using constants by replacing them with scalars.
 * 
 * @author Artur Rataj
 */
public class PrismIO extends FileFormatIO {
    /**
     * Extension of a file with Prism model.
     */
    public static final String PRISM_MODEL_EXTENSION = "nm";
    /**
     * Extension of a file with Hedgeelleth model.
     */
    public static final String HEDGEELLETH_MODEL_EXTENSION = "nh";
    
    /**
     * File format to use.
     */
    Flavour flavour;
    /**
     * Counter of the generated TADDs. For making names of step and clock
     * variables unique.
     */
    int taddCount;
    
    /**
     * Creates a new generator of PTA compilation to file in the
     * format used by Prism or Verics.
     * 
     * @param flavour format of the generated model
     * @param options PTA generation options
     */
    public PrismIO(Flavour flavour, FileFormatOptions options) {
        super(options);
        this.flavour = flavour;
    }
    /**
     * Returns a comment describing a given list of operations.
     * 
     * @param ops                       list of operations
     * @param localVerbosityLevel       local verbosity level, meaning
     *                                  is the same as <code>verbosityLevel</code>
     * @return
     */
    protected static String getCodeOpListComment(List<AbstractCodeOp> ops,
            int localVerbosityLevel) {
        StringBuilder s = new StringBuilder();
        switch(localVerbosityLevel) {
            case 0:
            {
                break;
            }
            case 1:
            {
                if(!ops.isEmpty()) {
                    SortedSet<StreamPos> positions = new TreeSet<>();
                    for(AbstractCodeOp op : ops)
                        positions.add(op.getStreamPos());
                    for(StreamPos p : positions) {
                        s.append(PTAIO.INDENT_STRING + "// ");
                        s.append(p.toString() + "\n");
                    }
                }
                break;
            }
            case 2:
            {
                int count = 0;
                for(AbstractCodeOp op : ops) {
                    s.append(PTAIO.INDENT_STRING + "// " + count + " " +
                            op.toString() + " (" + op.getStreamPos() + ")\n");
                    ++count;
                }
                break;
            }
        }
        return s.toString();
    }
    /**
     * Returns a key, that represents the contents of a given transition
     * comment. Used to find repeating comments.
     *
     * @param states                    map of states ot a PTA
     * @param tt                        transition
     * @param localVerbosityLevel       local verbosity level, meaning
     *                                  is the same as <code>verbosityLevel</code>
     * @return                          key of the transition's comment
     */
    protected static String getTransitionCommentKey(HashMap<PTAState, Integer> states,
            PTATransition tt, int localVerbosityLevel) {
        return states.get(tt.targetState).toString() + " " +
            (tt.label == null ? "" : tt.label.toString()) + " " +
            getCodeOpListComment(tt.backendOps, localVerbosityLevel);
    }
    /*
     * Generates a comment for a given state. UNUSED.
     *
     * @param states                    map of states ot a PTA 
     * @param state                     state to comment in the PTA
     * @param localVerbosityLevel       local verbosity level, meaning
     *                                  is the same as <code>verbosityLevel</code>
     * @param compactTargets            compact transitions to different targets,
     *                                  used only for verbosity level 1
     * @return                          comment string
     */
    /*
    protected static String _commentState(HashMap<PTAState, Integer> states,
            PTAState state, int localVerbosityLevel, boolean compactTargets) {
        String s = "";
        if(state.getOutputsNum() == 0) {
            // a stop location inherites its comment from the preceding locations
            StringBuilder p = new StringBuilder();
            for(PTATransition tt : state.getFlatInput()) {
                PTAState prevState = tt.sourceState;
                p.append(_commentState(states, prevState, localVerbosityLevel, compactTargets));
            }
            s = p.toString();
        } else {
            switch(localVerbosityLevel) {
                case 0:
                {
                    break;
                }
                case 1:
                {
                    if(compactTargets) {
                        TreeSet<String> positions = new TreeSet<>();
                        for(Set<PTATransition> set : state.output.values())
                            for(PTATransition tt : set)
                                for(AbstractCodeOp op : tt.backendOps)
                                    positions.add("" + op.getStreamPos());
                        for(String p : positions)
                            s += " " + p;
                        if(!s.isEmpty())
                            s = "//" + s + "\n";
                        break;
                    }
                    // fallthrough
                }
                case 2:
                {
                    Map<String, Integer> repetitions = new HashMap<>();
                    for(Set<PTATransition> set : state.output.values())
                        for(PTATransition tt : set) {
                            String key = getTransitionCommentKey(states, tt,
                                    localVerbosityLevel);
                            int repeated;
                            if(repetitions.containsKey(key))
                                repeated = repetitions.get(key);
                            else
                                repeated = 0;
                            ++repeated;
                            repetitions.put(key, repeated);
                        }
                    for(Set<PTATransition> set : state.output.values())
                        for(PTATransition tt : set) {
                            String key = getTransitionCommentKey(states, tt,
                                    localVerbosityLevel);
                            int repeated;
                            if(repetitions.containsKey(key))
                                repeated = repetitions.get(key);
                            else
                                repeated = 0;
                            if(repeated != -1) {
                                repetitions.put(key, -1);
                                String repeatedStr;
                                if(repeated > 1)
                                    repeatedStr = " (repeated " + repeated + " times)";
                                else
                                    repeatedStr = "";
                                s += "// target " + states.get(tt.targetState) +
                                        repeatedStr + "\n";
                                if(tt.label != null)
                                    s += "// " + tt.label.toString() + "\n";
                                s += getCodeOpListComment(tt.backendOps,
                                        localVerbosityLevel);
                            }
                        }
                    break;
                }
            }
        }
        return s;
    }
     */
    /**
     * Returns the extension of the output model file, according to the flavour.
     * 
     * @return extension, without the leading dot
     */
    protected String getExtension() {
        switch(flavour) {
            case PRISM:
                return PRISM_MODEL_EXTENSION;
                
            case HEDGEELLETH:
                return HEDGEELLETH_MODEL_EXTENSION;

            default:
                throw new RuntimeException("unknown flavour");
        }
    }
    /**
     * Replaces <code>extension</code> extension with a
     * <code>.pctl</code> extension.
     * 
     * @param extension extension to replace, without the leading dot
     * @param fileName name of a file with the extension
     * @return respective name with a <code>.pctl</code> extension
     */
    public static String toPctlFilename(String extension, String fileName) {
        extension = "." + extension;
        int pos = fileName.lastIndexOf(extension);
        if(pos == fileName.length() - extension.length())
            fileName = fileName.substring(0, pos);
        fileName += ".pctl";
        return fileName;
    }
    protected void writeArrayFormulas(Context context, PrintWriter out) {
        for(String f : context.backendFormulas.keySet())
            out.println("\nformula " + f + " =\n" + PTAIO.INDENT_STRING +
                    context.backendFormulas.get(f) + ";");
    }
    /**
     * Returns a module name.
     * 
     * @param c compilation
     * @param pta pta, whose module name is returned
     * @param shortener name shortener
     * @param moduleNames names keyed with PTAs are
     * put into this map; within each call, an unique name is added
     * or <code>IOException</code> is thrown
     * @return module name to put into the output file
     */
    protected void getModuleName(BackendPTACompilation c, PTA pta,
            NameShortener shortener, Map<PTA, String> moduleNames) throws IOException {
        String moduleName;
        RuntimeObject thread = c.threadPtas.get(pta);
        if(c.modelControl.hasNamingConvention(thread)) {
            try {
                moduleName = c.modelControl.getElementName(
                        thread, thread.codeClass.getUnqualifiedName());
            } catch(CompilerException e) {
                throw new IOException(e.getMessage());
            }
        } else
            moduleName = shortener.getName(PTAIO.MODULE_PREFIX +
                pta.name, false, -1);
        if(moduleNames.values().contains(moduleName))
            throw new IOException("naming convention for " +
                    thread.getUniqueName() + " caused name conflict " +
                    "for module name " + moduleName);
        moduleNames.put(pta, moduleName);
    }
    /**
     * Returns a set of <code>GPath</code> for range checking.
     * If <code>v</code> is a scalar, then a single path is returned.
     * Otherwise, a path for each element is returned.
     * 
     * @param v checked variable, for which the statements are constructed
     * @param operator operator to compare the variable or its elements with
     * a constant
     * @param constant the constant to compare to
     * @return a set of PCTL G statements
     */
    protected Collection<GPath> getGPaths(AbstractPTAVariable v,
            Op operator, double constant) {
        Collection<GPath> out = new LinkedList<>();
        List<AbstractPTAVariable> variables = new LinkedList<>();
        if(v instanceof PTAScalarVariable)
            variables.add(v);
        else {
            PTAArrayVariable array = (PTAArrayVariable)v;
            for(int i = 0; i < array.length; ++i) {
                PTAElementVariable element = new PTAElementVariable(
                        array, new PTAConstant(i, array.floating), array.range,
                        array.floating);
                variables.add(element);
            }
        }
        for(AbstractPTAVariable w : variables) {
            GPath path = new GPath(new RelationalExpression(w,
                operator, constant));
            out.add(path);
        }
        return out;
    }
    /**
     * Writes a backend PTA compilation to a file.<br>
     * 
     * There is a similar method to <code>PTAIO.write</code>, that
     * writes a pure runtime PTA compilation.
     * 
     * @param file                      file to create
     * @param c                         PTA compilation
     * @return                           all model types generated
     */
    @Override
    public List<ModelType> write(File file, BackendPTACompilation c)
            throws IOException {
        try {
            PrintWriter out = new PrintWriter(new FileOutputStream(
                    file));
            SortedMap<String, AbstractBackendPTAVariable> sortedVariables =
                    new TreeMap<>();
            boolean clocksFound = false;
            Map<PTAArrayVariable, PTAScalarVariable[]> arrays;
            List<String> arrayNames;
            if(options.emulateArrays) {
                arrays = new HashMap<>();
                arrayNames = new LinkedList<>();
            } else {
                arrays = null;
                arrayNames = null;
            }
            Set<AbstractBackendPTAVariable> unknownConstants =
                    new HashSet<>();
            for(AbstractRuntimeContainer rc : c.variables.keySet()) {
                Map<CodeVariable, AbstractBackendPTAVariable> inContainer =
                        c.variables.get(rc);
                for(CodeVariable cv : inContainer.keySet()) {
                    AbstractBackendPTAVariable tv = inContainer.get(cv);
                    if(c.unknownConstants.contains(cv))
                        unknownConstants.add(tv);
                    if(options.emulateArrays && tv instanceof BackendPTAArrayVariable) {
                        BackendPTAArrayVariable a = (BackendPTAArrayVariable)tv;
                        ArrayStore store = (ArrayStore)a.container;
                        int length = store.getLength();
                        PTAScalarVariable[] array = new PTAScalarVariable[length];
                        for(int i = 0; i < length; ++i) {
                            // only elements, that are actually used
                            if(c.usedVariables.contains(new PTAElementVariable(
                                    (PTAArrayVariable)tv.v(), new PTAConstant(i, false),
                                    tv.v().range, tv.v().floating))) {
                                BackendPTAScalarVariable element =
                                        new BackendPTAScalarVariable(a.getStreamPos(),
                                            store, new PTAScalarVariable(tv.v().name + "::" + i,
                                                tv.v().range, tv.v().floating));
                                element.v().initValue = a.v().initValue[i];
                                element.proposal = new Proposal(tv.proposal);
                                //element.v().backendWriteRange = BackendPTACompilation.
                                //        getRangeFromProposal(tv); // ???????!!!!!!!!
                                element.v().backendWriteRange = tv.v().backendWriteRange;
                                array[i] = element.v();
                                sortedVariables.put(element.v().name, element);
                            }
                        }
                        arrays.put(a.v(), array);
                        arrayNames.add(a.v().name);
                    } else {
                        sortedVariables.put(tv.v().name, tv);
                        if(tv instanceof BackendPTAClock)
                            clocksFound = true;
                    }
                }
            }
            checkActualModelType(c);
            out.println(c.type.getHeader() + "\n");
            if(flavour == Flavour.HEDGEELLETH)
                out.print(PTAIO.getOptions(c));
            SortedMap<String, Integer> sLabelNames = new TreeMap<>();
            int nextFreeLabelNameNum = 0;
            SortedMap<String, PTA> sortedTADDs =
                    new TreeMap<>();
            int totalLocations = 0;
            for(PTA tadd : c.ptas.values()) {
                sortedTADDs.put(tadd.name, tadd);
                totalLocations += tadd.states.size();
            }
            Collection<String> allNames = new LinkedList<>();
            if(options.verboseTADDVariables) {
                for(String s : sortedVariables.keySet())
                    allNames.add(s);
                for(String t : sortedTADDs.keySet())
                    allNames.add(PTAIO.MODULE_PREFIX + t);
                for(String l : c.labels.keySet())
                    if(c.labels.get(l).type == PTALabel.Type.FREE)
                        allNames.add(l);
                    else
                        allNames.add(PTAIO.BACKEND_CHANNEL_PREFIX + l);
            } else
                throw new RuntimeException("required verbose " +
                        "names of variables");
            NameShortener shortener = new NameShortener(
                    options.verboseTADDVariables, options.compactInstances,
                    allNames, new PrismSubFilter());
            PCTLProperties pctl = new PCTLProperties();
            StringBuilder s = new StringBuilder();
            Map<AbstractPTAVariable, VariableDescription> variables =
                    new HashMap<>();
            PTAIO.Context context = new PTAIO.Context(this.flavour, variables,
                    shortener, options.emulateArrays, options.rangeChecking,
                    /* completed later */ null);
            context.backendArrays = arrays;
            if(arrayNames != null)
                context.backendArrayShortener = new NameShortener(true,
                    false, arrayNames, null);
            context.backendFormulas = new TreeMap<>();
            boolean fpSourceLevelFound = false;
            boolean fpInternalFound = false;
            boolean specialTransitions = PTAIO.isOutOfRangeIndexInNested(c.ptas.values());
            // assigned to the error variable, if a disabling guard is true
            long specialTransitionErrorValue = 0;
            // name of the error variable
            String specialTransitionErrorName = null;
            // sorts variables by their shortened names
            SortedMap<String, String> variableStrings = new TreeMap<>();
            int count = 0;
            for(AbstractBackendPTAVariable tv : sortedVariables.values()) {
                StreamPos pos = tv.pos;
                if(tv instanceof BackendPTAScalarVariable) {
                    // arrays are never internal variables
                    Variable.Category category = ((BackendPTAScalarVariable)tv).category;
                    if(category != null && category.isInternal())
                        // do not print positions of internal variables
                        pos = null;
                }
                if(this.flavour == Flavour.PRISM &&
                        !(tv instanceof BackendPTAClock) &&
                        !unknownConstants.contains(tv) && tv.v().floating) {
                    if(pos != null)
                        fpSourceLevelFound = true;
                    else
                        fpInternalFound = true;
                }
                if(tv instanceof BackendPTAClock) {
                    /* ignore -- clock variables are local in Prism */
                } else {
                    StringBuilder variableString = new StringBuilder();
                    // Prism does not support arrays, so they are emulated
                    if(flavour == Flavour.HEDGEELLETH || tv instanceof BackendPTAScalarVariable) {
                        /* do not display in globals -- will be displayed in the constant fields section */
                        double min;
                        double max;
                        if(unknownConstants.contains(tv)) {
                            if(this.flavour == Flavour.HEDGEELLETH)
                                throw new IOException("unknown constants not allowed");
                            min = Double.NaN;
                            max = Double.NaN;
                        } else {
                            if(options.verbosityLevel != 0) {
                                variableString.append("// " + tv.originalVariableName);
                                if(pos != null)
                                    variableString.append(" (" + pos.toString() + ")");
                                variableString.append("\n");
                            }
                            boolean errorVariable = tv.v().name.equals(
                                    AbstractTADDGenerator.ERROR_CODE_VARIABLE_QUALIFIED_NAME);
                            if(errorVariable) {
                                specialTransitionErrorValue = (long)tv.v().range.getMax() + 1;
                                try {
                                    tv.v().range = new PTARange(tv.v().range.backendPos,
                                            0, specialTransitionErrorValue);
                                } catch(PTAException e) {
                                    throw new RuntimeException("unexpected: " + e.toString());
                                }
                                specialTransitionErrorName = shortener.getName(tv.v().name,
                                        true, count);
                            }
                            min = tv.v().range.getMin();
                            max = tv.v().range.getMax();
                            if(min < Integer.MIN_VALUE || max > Integer.MAX_VALUE)
                                throw new RuntimeException("value does not fit into integer");
                            double pMin;
                            double pMax;
                            boolean knownWriteRange = tv.v().backendWriteRange != null;
                            if(knownWriteRange) {
                                pMin = tv.v().backendWriteRange.getMin();
                                pMax = tv.v().backendWriteRange.getMax();
                            } else {
                                // <code>knownWriteRange == false</code> will cause
                                // range checking anyway
                                pMin = 0;
                                pMax = 0;
                            }
                            String origRangeString = tv.v().range.toString();
                            if(options.rangeChecking && !tv.v().range.backendGuaranteed) {
                                ProbabilityOperator p = null;
                                List<AbstractPath> paths = new LinkedList<>();
                                if(errorVariable) {
                                    p = new ProbabilityOperator(
                                            1.0, BinaryExpression.Op.GREATER_OR_EQUAL);
                                    paths.addAll(getGPaths(tv.v(),
                                            BinaryExpression.Op.EQUAL,
                                            0));
                                } else {
                                    if(!knownWriteRange || pMin < min) {
                                        p = new ProbabilityOperator(
                                                1.0, BinaryExpression.Op.GREATER_OR_EQUAL);
                                        paths.addAll(getGPaths(tv.v(),
                                                BinaryExpression.Op.GREATER_OR_EQUAL,
                                                min));
                                        if(min == --min)
                                            /*throw new RuntimeException("absolute value too large")*/;
                                    }
                                    if(!knownWriteRange || pMax > max) {
                                        p = new ProbabilityOperator(
                                                1.0, BinaryExpression.Op.GREATER_OR_EQUAL);
                                        paths.addAll(getGPaths(tv.v(),
                                                BinaryExpression.Op.LESS_OR_EQUAL,
                                                max));
                                        if(max == ++max)
                                            /*throw new RuntimeException("absolute value too large")*/;
                                    }
                                }
                                for(AbstractPath path : paths) {
                                    AbstractProperty property = new SimpleProperty(
                                            p, path);
                                    pctl.properties.add(property);
                                }
                            }
                            variableString.append(PTAIO.getGlobalVariableDeclaration(tv.v(), count,
                                    min, max, shortener, flavour != Flavour.PRISM));
                            if(options.rangeChecking || !errorVariable)
                                variableString.append(" //");
                            if(options.rangeChecking)
                                variableString.append(" " + origRangeString);
                            if(!errorVariable) {
                                variableString.append(" O(:=) = ");
                                if(knownWriteRange) {
                                    if(tv.proposal.isEmpty())
                                        variableString.append("<empty>");
                                    else if(tv.v().floating)
                                        variableString.append("(" + pMin + " ... " + pMax + ")");
                                    else
                                        variableString.append("(" + (long)pMin + " ... " + (long)pMax + ")");
                                } else
                                    variableString.append("?");
                            }
                            variableString.append("\n");
                        }
                        try {
                            VariableDescription vd = new VariableDescription(
                                    count, new PTARange(null, min, max));
                            variables.put(tv.v(), vd);
                        } catch(PTAException e) {
                           throw new RuntimeException("unexpected exception: " +
                                e.toString());
                        }
                        variableStrings.put(shortener.getName(tv.v().name, true, count),
                                variableString.toString());
                        ++count;
                    } else if(tv instanceof BackendPTAArrayVariable) {
                         throw new IOException("unexpected array");
                    } else
                         throw new RuntimeException("unknown variable type");
                }
            }
            for(String variableString : variableStrings.values())
                s.append(variableString);
            boolean specialVariables = false;
            if(specialTransitions && specialTransitionErrorName == null) {
                s.append("global error : [0..1] init 0;\n");
                specialTransitionErrorName = "error";
                specialTransitionErrorValue = 1;
                specialVariables = true;
            }            
            if(this.flavour == Flavour.PRISM && variables.isEmpty() &&
                    !specialVariables &&
                    // no PTA has more than a single location
                    totalLocations == c.ptas.size())
                // Prism complains if there are no variables
                s.append("global fake : [0..1] init 0;\n");
            if(specialTransitions && specialTransitionErrorValue <= 0)
                throw new RuntimeException("invalid special transition's error value");
            ptaNameMap = new HashMap<>();
            for(PTA pta : sortedTADDs.values())
                getModuleName(c, pta, shortener, ptaNameMap);
            s.append(PTAIO.getPlayers(c.playerPTAs, c.playerLabels, c.playerList,
                    sortedTADDs.values(), c.labels.values(), ptaNameMap,
                    shortener));
            if(this.flavour == Flavour.HEDGEELLETH && !c.labels.isEmpty())
                s.append(PTAIO.getLabels(flavour, c.labels, shortener));
            out.print(s);
            taddCount = 0;
            for(PTA tadd : sortedTADDs.values()) {
                context.ptaIndices.put(tadd, taddCount);
                count = 0;
                PTAState startState = tadd.startState;
                context.stateIndices.put(startState, count);
                ++count;
                for(PTAState state : tadd.states.values())
                    if(state != startState) {
                        context.stateIndices.put(state, count);
                        ++count;
                    }
                ++taddCount;
            }
            context.backendWaitForSyncString = PTAIO.getWaitForSyncString(
                    context, c.ptas.values(), context.ptaIndices, context.stateIndices);
            taddCount = 0;
            for(PTA tadd : sortedTADDs.values()) {
                context.currPTAIndex = taddCount;
                out.println();
                if(options.verbosityLevel != 0)
                    out.println("// " + tadd.name);
                out.println("module " + ptaNameMap.get(tadd) + "\n");
                // this map stores serial numbers of states
                Map<PTAState, Integer> states =
                        new HashMap<>();
                Map<Integer, PTAState> statesSorted =
                        new HashMap<>();
                count = 0;
                PTAState startState = tadd.startState;
                states.put(startState, count);
                statesSorted.put(count, startState);
                ++count;
                for(PTAState state : tadd.states.values())
                    if(state != startState) {
                        states.put(state, count);
                        statesSorted.put(count, state);
                        ++count;
                    }
                int statesNum = count;
                PTAIO.writeModuleDeclarations(context, out, tadd, states);
                // prefix like comments and state guard
                Map<PTATransition, String> prefixes = new HashMap<>();
                // all guards but the probability and error ones -- the probability ones
                // are resolved in <code>printTransitions()</code>, which groups
                // probability branches into a single statement, and the error
                // guards are stored in <code>errorGuards</code>
                Map<PTATransition, String> npGuards = new HashMap<>();
                Map<PTATransition, String> updates = new HashMap<>();
                Map<PTATransition, GuardedExpression> errorGuards = new HashMap<>();
                // the keys are: backend op index, input state index, output state index
                SortedMap<Integer,
                        SortedMap<Integer,
                            SortedMap<Integer,
                                List<PTATransition>>>> sorted =
                    new TreeMap<>();
                for(PTAState tState : statesSorted.values())
                    for(Set<PTATransition> set : tState.output.values())
                        for(PTATransition tt : new TreeSet<>(set)) 
                            try {
    /*if(tt.sourceState.index == 40)
        tt = tt;*/
                                if(tt.backendOpIndex != -1 && sorted != null) {
                                    SortedMap<Integer, SortedMap<Integer, List<PTATransition>>> z =
                                            sorted.get(tt.backendOpIndex);
                                    if(z == null) {
                                        z = new TreeMap<>();
                                        sorted.put(tt.backendOpIndex, z);
                                    }
                                    SortedMap<Integer, List<PTATransition>> l = z.get(tt.sourceState.index);
                                    if(l == null) {
                                        l = new TreeMap<>();
                                        z.put(tt.sourceState.index, l);
                                    }
                                    List<PTATransition> m = l.get(tt.targetState.index);
                                    if(m == null) {
                                        m = new LinkedList<>();
                                        l.put(tt.targetState.index, m);
                                    }
                                    m.add(tt);
                                } else
                                    // lack of some indices, use the default order from the PTA
                                    sorted = null;
                                s = new StringBuilder();
                                int labelNum = -1;
                                if(tt.label != null) {
                                    if(sLabelNames.containsKey(tt.label.name))
                                        labelNum = sLabelNames.get(tt.label.name);
                                    else {
                                        labelNum = nextFreeLabelNameNum++;
                                        sLabelNames.put(tt.label.name, labelNum);
                                    }
                                }
                                if(!options.commentVariablesOnly) {
                                    if(tt.backendOpIndex != -1 && options.verbosityLevel != 0)
                                        s.append(PTAIO.INDENT_STRING + "// #" + tt.backendOpIndex + "\n");
                                    if(tt.label != null && options.verbosityLevel != 0)
                                        s.append(PTAIO.INDENT_STRING + "// " + tt.label.toString() + "\n");
                                    s.append(getCodeOpListComment(tt.backendOps,
                                            options.verbosityLevel));
                                }
                                s.append(PTAIO.INDENT_STRING + "[");
                                if(tt.label != null) {
                                    String labelName;
                                    if(tt.label.type == PTALabel.Type.FREE)
                                        labelName = tt.label.name;
                                    else
                                        labelName = shortener.getName(
                                            PTAIO.BACKEND_CHANNEL_PREFIX + tt.label.name, false,
                                            labelNum);
                                    switch(this.flavour) {
                                        case PRISM:
                                            // s.append("l" + labelNum);
                                            s.append(labelName);
                                            break;

                                        case HEDGEELLETH:
                                            s.append(labelName);
                                            s.append(tt.label.getPostfix());
                                            break;

                                        default:
                                            throw new RuntimeException("unknown flavour");
                                    }
                                }
                                s.append("] ");
                                String f = PTAIO.STEP_NAME + taddCount + "=" +
                                            states.get(tt.sourceState);
                                if(this.flavour == Flavour.PRISM && statesNum == 1)
                                    s.append("true");
                                else
                                    s.append("(" + f + ")");
                                String ndFilter = f;
                                prefixes.put(tt, s.toString());
                                GuardedExpression g = PTAIO.getNPGuards(context, tt);
    /*if(!g.getDisable().isEmpty())
        g = g;*/
                                s = new StringBuilder(g.getExpr());
                                if(s.length() != 0)
                                    ndFilter = null;
                                npGuards.put(tt, s.toString());
                                g.replaceExpr("");
                                errorGuards.put(tt, g);
                                c.xStateFilter.put(tt, ndFilter);
                                if(ndFilter != null) {
                                    String ndTargetFilter = PTAIO.STEP_NAME + taddCount + "=" +
                                            states.get(tt.targetState);
                                    c.xTargetStateFilter.put(tt, ndTargetFilter);
                                }
                                s = new StringBuilder();
                                if(this.flavour == Flavour.HEDGEELLETH || statesNum > 1) {
                                    if(this.flavour == Flavour.PRISM &&
                                            states.get(tt.targetState).intValue() == states.get(tt.sourceState).intValue() &&
                                            tt.update.isEmpty()) {
                                        // an explicit loop to itself, required by Prism games
                                        s.append("true");
                                    } else
                                        s.append("(" + PTAIO.STEP_NAME + taddCount + "'=" +
                                                states.get(tt.targetState) + ")");
                                }
/*if(tt.toString().indexOf("5 6") != -1)
    tt = tt;*/
                                GuardedExpression u = PTAIO.getUpdates(context, tt, tadd.states.size());
                                s.append(u.getExpr());
                                updates.put(tt, s.toString());
                                u.replaceExpr("");
                                errorGuards.get(tt).append(u);
                            } catch(IOException e) {
                                if(!tt.backendOps.isEmpty()) {
                                    e = new IOException(
                                            tt.backendOps.get(0).getStreamPos().toString() + ": " +
                                                    e.getMessage());
                                }
                                throw e;
                            }
                List<PTATransition> sortedFlat;
                if(sorted != null) {
                    sortedFlat = new LinkedList<>();
                    for(SortedMap<Integer, SortedMap<Integer, List<PTATransition>>> z : sorted.values()) {
                        List<PTATransition> xList = new LinkedList<>();
                        for(SortedMap<Integer, List<PTATransition>> l : z.values())
                            for(List<PTATransition> m : l.values())
                                for(PTATransition tt : m)
                                    xList.add(tt);
                        // reorder the states, if there are backward input/output dependencies
                        // between them
                        PTATransition[] x = xList.toArray(new PTATransition[xList.size()]);
                        int swaps = 0;
                        boolean rearranged;
                        do {
                            rearranged = false;
                            for(int i = 0; i < x.length; ++i) {
                                int to = states.get(x[i].targetState);
                                for(int j = 0; j < i; ++j) {
                                    int from = states.get(x[j].sourceState);
                                    if(to == from) {
                                        PTATransition t = x[i];
                                        x[i] = x[j];
                                        x[j] = t;
                                        ++swaps;
                                        rearranged = true;
                                    }
                                }
                            }
                        } while(rearranged &&
                                // to escape in the case of a circular dependency
                                swaps < x.length*x.length);
                        sortedFlat.addAll(Arrays.asList(x));
                    }
                } else
                    sortedFlat = null;
                PTAIO.printTransitions(context, out, statesSorted.values(),
                        options.verbosityLevel == 1, prefixes, npGuards, updates, errorGuards,
                        taddCount, states, sortedFlat, c.type,
                        "(" + specialTransitionErrorName + "'=" + specialTransitionErrorValue + ")");
                out.println("\nendmodule");
                ++taddCount;
            }
            if(context.flavour == PTAIO.Flavour.PRISM &&
                    context.backendWaitForSyncString != null)
                out.println("\nformula " + PTAIO.FORMULA_ENABLED_URGENT_NAME + " =\n" +
                        PTAIO.INDENT_STRING + context.backendWaitForSyncString + ";");
            writeArrayFormulas(context, out);
            PTAIO.generateStateFormulas(out, sortedTADDs.values(), context.stateIndices);
            String verbatim = c.modelControl.getVerbatimSuffix();
            if(!verbatim.isEmpty()) {
                switch(context.flavour) {
                    case PRISM:
                        out.print("\n" + verbatim);
                        break;
                        
                    case HEDGEELLETH:
                        System.out.println("warning: verbatim suffix ignored");
                        break;
                        
                    default:
                        throw new RuntimeException("unknown flavour");
                }
            }
            if(out.checkError())
                throw new IOException("write error to " + file.getPath());
            out.close();
            //
            // generate the PCTL file
            //
            String name = toPctlFilename(getExtension(), file.getAbsolutePath());
            file = new File(name);
            out = new PrintWriter(new FileOutputStream(file));
            SimplePropertyWriter writer = new SimplePropertyWriter(context);
            for(AbstractProperty property : pctl.properties) {
                if(property instanceof SimpleProperty)
                    out.println(writer.write((SimpleProperty)property));
                else
                    throw new IOException("only simple PCTL properties " +
                            "can be written");
            }
            if(options.generateModelControlProperties)
                for(ModelControl.Property p : c.modelControl.getProperties()) {
                    if(!p.name.isEmpty())
                        out.println("// " + p.name);
                    out.println(p.property);
                }
            if(out.checkError())
                throw new IOException("write error to " + file.getPath());
            out.close();
            if(fpSourceLevelFound)
                   throw new IOException("floating--point variables not supported by Prism format, " +
                           "output generated anyway for inspection");
            else if(fpInternalFound)
                   throw new IOException("floating--point variables not optimized out, but " +
                           "not supported by Prism format, output generated anyway for inspection");
            return Arrays.asList(new ModelType[] {c.type});
        } catch(IOException e) {
            throw new IOException("backend Prism, flavour " + flavour.toString() + ": " +
                    e.getMessage());
        }
    }
}
