/*
 * BarrierGenerator.java
 *
 * Created on Nov 22, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator;

import java.nio.IntBuffer;
import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CompilerAnnotation;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.BarrierLabels;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeConstant;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.AbstractCodeOp;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.CodeOpBinaryExpression;
import pl.gliwice.iitis.hedgeelleth.interpreter.op.BarrierParties;
import pl.gliwice.iitis.hedgeelleth.pta.*;
import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAVariable;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTABufferedConstant;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAConstant;

/**
 * Constructs transitions, which form different kinds of a barrier.
 * 
 * @author Artur Rataj
 */
public class BarrierGenerator {
    /**
     * Graph generator.
     */
    private final GraphTADDGenerator generator;
    /**
     * Context.
     */
    private final TADDGeneratorContext context;
    /**
     * Type of the barrier.
     */
    private final CompilerAnnotation type;
    /**
     * If the condition is required to be atomic.
     */
    private final boolean atomic;
    /**
     * Parties of this barrier.
     */
    private final BarrierParties parties;
    /**
     * Holds transitions of this barrier within all parties.
     */
    private final Map<String, BarrierLabels> bl;
    /**
     * Commented in the constructor's docs.
     */
    private final int userIndex;
    /**
     * Commented in the constructor's docs.
     */
    private final int producerIndex;
    /**
     * Commented in the constructor's docs.
     */
    private final int consumerIndex;
    /**
     * Commented in the constructor's docs.
     */
    private final int currOpIndex;
    /**
     * Commented in the constructor's docs.
     */
    private final int loopOpIndex;
    /**
     * Commented in the constructor's docs.
     */
    private final int passOpIndex;
    /**
     * If this barrier is conditional.
     */
    private final boolean conditional;
    /**
     * Direct condition, with a stop removed. Null if <code>conditional</code> is false.
     */
    private final AbstractPTAExpression conditionT;
    /**
     * Negated condition, with a stop removed. Null if <code>conditional</code> is false.
     */
    private final AbstractPTAExpression conditionF;
    /**
     * Like <code>conditionT</code>, but with a stop.
     */
    private final AbstractPTAExpression conditionTStopped;
//    /**
//     * Like <code>conditionF</code>, but with a stop.
//     */
//    private final AbstractPTAExpression conditionFStopped;
    /**
     * Commented in the constructor's docs.
     */
    CodeConstant producerValue;
    /**
     * Commented in the constructor's docs.
     */
    AbstractPTAVariable consumeTarget;
    /**
     * Source operations, from which derive this barrier
     */
    private final List<AbstractCodeOp> sourceOps;
    /**
     * Final transition or null if not yet known.
     */
    private PTATransition finalTransition;
    /**
     * All labels of all parties of this barrier have this common prefix.
     */
    private final String labelPrefix;
    
    /**
     * Creates a new barrier generator.
     * 
     * @param generator graph generator
     * @param context context
     * @param type type of the barrier
     * @param atomic if the condition is required to be atomic
     * @param parties description of this barrier's parties
     * @paam bl prefix of label names
     * @param userIndex index of this party in the general barrier's list; do
     * not use with asymmetric barriers, see <code>BarrierParties.collectedUsers</code>
     * for details
     * @param producerIndex index of this party in the producer barrier's list,
     * -1 for none
     * @param consumerIndex index of this party in the consumer barrier's list,
     * -1 for none
     * @param currOpIndex op index, to which to attach
     * @param loopOpIndex op index, to which to loop if the condition is false;
     * -1 for none; can not be -1 if <code>tCondition</code> is not null
     * @param passOpIndex op index, to which to loop if the condition is true;
     * -1 for some unknown next one
     * @param condition a condition; null if the party does not have a condition
     * on this barrier
     * @param producerValue integer value passed by the producer, null for none;
     * can be true only for a pair barrier, no condition and the producer
     * subautomaton
     * @param consumeTarget a variable, to which is assigned the return value
     * of consume(); null if none
     * @param sourceOps source operations, from which derive this barrier
     */
    public BarrierGenerator(GraphTADDGenerator generator,
            TADDGeneratorContext context, CompilerAnnotation type,
            boolean atomic, BarrierParties parties, String labelPrefix, int userIndex,
            int producerIndex, int consumerIndex, int currOpIndex, int loopOpIndex,
            int passOpIndex, AbstractPTAExpression condition, CodeConstant producerValue,
            AbstractPTAVariable consumeTarget, List<AbstractCodeOp> sourceOps)
                throws PTAException {
        this.generator = generator;
        this.type = type;
        this.atomic = atomic;
        this.context = context;
        this.parties = parties;
        this.bl = context.generator.labels.barriers;
        this.userIndex = userIndex;
        this.producerIndex = producerIndex;
        this.consumerIndex = consumerIndex;
        this.currOpIndex = currOpIndex;
        this.loopOpIndex = loopOpIndex;
        this.passOpIndex = passOpIndex;
        conditionTStopped = condition;
        conditional = conditionTStopped != null;
        if(conditional) {
            conditionT = conditionTStopped.clone();
            conditionT.backendRemoveStops();
            //conditionFStopped  = condition.newComplementary();
            //conditionFStopped.backendCopyStop(conditionTStopped);
            //conditionF = conditionFStopped.clone();
            conditionF = condition.newComplementary();
            conditionF.backendEnableStop();
        } else {
            conditionT = null;
            conditionF = null;
        }
        this.producerValue = producerValue;
        this.consumeTarget = consumeTarget;
        this.sourceOps = sourceOps;
        finalTransition = null;
        this.labelPrefix = labelPrefix;
        generate();
    }
//    /**
//     * Returns the length of the barrier suffix, including the index
//     * and the underscore. 0 if no barrier suffix is found.
//     * 
//     * @param name label name
//     * @return length in characters or 0
//     */
//    public static int getBarrierSuffixLength(String name) {
//        if(name.endsWith(SUFFIX)) {
//            int last = name.length() - SUFFIX.length() - 1;
//            for(int i = last; i > 0; --i)
//                if(!Character.isDigit(name.charAt(i)) &&
//                        // a real label has an `_'
//                        name.charAt(i) != '_' &&
//                        // a fictious label has an `F'
//                        name.charAt(i) != 'F') {
//                    if(name.charAt(i) != ':' || name.charAt(i - 1) != ':')
//                        // should really be classified as a missing suffix,
//                        // but a missing suffix and name which ends on 'barrier'
//                        // is so unlikely or even impossible, that let it be so for
//                        // tests
//                        throw new RuntimeException("invalid suffix");
//                    return name.length() - i - 1;
//                }
//            throw new RuntimeException("invalid suffix");
//        }
//        return 0;
//    }
    /**
     * <p>Creates a new transition. Connects the new transition, and adds it to
     * <code>bl</code>.</p>
     * 
     * // <p>If <code>toIndex<code> is -1, then the new transition is returned by
     * // <code>getFinalTransition()</code>.</p>
     * 
     * @param fromIndex source index
     * @param toIndex target index, -1 for unknown
     * @param label label, null for none
     * @param condition <code>conditionT</code>, <code>conditionF</code> or null
     * @param looping if in the case when this barrier requires an atomic condition, the
     * checking of atomicity should be performed on this transition,
     * @return the new transition
     */
    protected PTATransition create(int fromIndex, int toIndex, PTALabel label,
            AbstractPTAExpression condition, boolean looping) {
        PTATransition t = new PTATransition(label);
        if(condition != null)
            t.guard = condition.clone();
        t.connectTargetIndex = toIndex;
//        if(t.connectTargetIndex == -1) {
//            if(finalTransition != null)
//                throw new RuntimeException("final transition already specified");
//            finalTransition = t;
//        }
        if(looping)
            t.loopsToItself = atomic;
        try {
            context.factory.addTransition(fromIndex, t);
        } catch(PTAException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
        if(label != null) {
            BarrierLabels l = bl.get(label.name);
            if(l == null) {
                l = new BarrierLabels(label.name);
                bl.put(l.name, l);
            }
            l.transitions.add(t);
        }
        GraphTADDGenerator.addSourceInfo(t, context, sourceOps);
        return t;
    }
    /**
     * Returns the final transition, to which the incoming transitions may
     * attach, if <code>passOpIndex == -1</code>.
     * 
     * @param t a final transition
     */
    protected void setFinalTransition(PTATransition t) {
        finalTransition = t;
    }
    /**
     * Returns the final transition, to which the incoming transitions may
     * attach, if <code>passOpIndex == -1</code>.
     * 
     * @return a transition, created by this generator
     */
    public PTATransition getFinalTransition() {
        return finalTransition;
    }
    /**
     * Creates a new label, having a given index.
     * 
     * @param labelIndex index of the new label; labels with a common index also have
     * a common name
     * @return a newly created label
     */
    private PTALabel newLabel(int labelIndex) {
        try {
            return new PTALabel(labelPrefix + "::_" + labelIndex,
                    PTALabel.Type.BARRIER, false);
        } catch(PTAException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    /**
     * Creates a new location.
     * 
     * @return index of the new location
     */
    private int newLocation() {
        return context.factory.newStateIndex();
    }
    /**
     * A symmetric subautomaton of a barrier.
     */
    private void generateBarrierSymmetric() {
        setFinalTransition(
                create(currOpIndex, passOpIndex, newLabel(0), conditionTStopped, false));
        if(conditional)
            create(currOpIndex, loopOpIndex, newLabel(0), conditionF, true);
    }
    /**
     * A producer subautomaton of a barrier.
     */
    private void generateBarrierProduce() {
        setFinalTransition(
                create(currOpIndex, passOpIndex, newLabel(producerIndex), conditionTStopped, false));
        if(conditional)
            create(currOpIndex, loopOpIndex, newLabel(producerIndex), conditionF, true);
    }
    /**
     * Recursively creates a subtree for a directional barrier's consume
     * automaton.
     * 
     * @param rootLocationT index of the root location of this subtree for
     * a true condition
     * @param rootLocationF index of the root location of this subtree for
     * a false condition; if <code>rootLocationF == currOpIndex</code>,
     * then also <code>rootLocationF == rootLocationT</code>;
     * -1 if <code>condition</code> is false
     * @param synced indices of producers already synced
     * @param finalLocation if a single not synced producer is left,
     * do not create a new location, but instead connect to this
     * location
     */
    private void consumeTree(int rootLocationT, int rootLocationF,
            Set<Integer> synced, int finalLocation) {
        for(int i = 0; i < parties.numProducers; ++i)
            if(!synced.contains(i)) {
                boolean firstLayer = synced.isEmpty();
                boolean lastLayer = synced.size() == parties.numProducers - 1;
                if(lastLayer) {
                    AbstractPTAExpression c;
                    if(atomic || firstLayer)
                        c = conditionTStopped;
                    else
                        c = null;
                    create(rootLocationT, finalLocation, newLabel(i), c, false);
                    if(rootLocationF != -1) {
                        if(firstLayer || atomic)
                            create(rootLocationT, loopOpIndex, newLabel(i), conditionF, false);
                        if(rootLocationF != rootLocationT)
                            create(rootLocationF, loopOpIndex, newLabel(i), null, false);
                    }
                } else {
                    int targetLocationF;
                    if(rootLocationF != -1) {
                        targetLocationF = newLocation();
                        if(firstLayer || atomic)
                            create(rootLocationT, targetLocationF, newLabel(i), conditionF, false);
                        if(!firstLayer)
                            create(rootLocationF, targetLocationF, newLabel(i), null, false);
                    } else
                        targetLocationF = -1;
                    int targetLocationT = newLocation();
                    AbstractPTAExpression c;
                    if(atomic || firstLayer)
                        c = conditionT;
                    else
                        c = null;
                    create(rootLocationT, targetLocationT, newLabel(i), c, false);
                    Set<Integer> syncedNext = new HashSet<>(synced);
                    syncedNext.add(i);
                    consumeTree(targetLocationT, targetLocationF,
                            syncedNext, finalLocation);
                }
            }
    }
    /**
     * A consumer subautomaton of a barrier.
     */
    private void generateBarrierConsume() {
        if(atomic && conditional) {
            try {
                // to create a fictious loop, used to check
                // for atomicity
                PTALabel fictious = new PTALabel(labelPrefix + "::F" +
                        consumerIndex,
                        PTALabel.Type.BARRIER, false);
                create(currOpIndex, loopOpIndex, fictious, conditionF, true);
            } catch(PTAException e) {
                throw new RuntimeException("unexpected: " + e.toString());
            }
        }
        int finalLocation = newLocation();
        consumeTree(currOpIndex, conditional ? currOpIndex : -1,
                new HashSet<Integer>(), finalLocation);
        setFinalTransition(
                create(finalLocation, passOpIndex, null, null, false));
//        if(conditionT != null && parties.numProducers > 1) {
//            // a stop guard is needed, because a single expressions
//            // is copied to form a sequence of expressions
//            AbstractPTAExpression stopGuard = new PTAUnaryExpression(
//                    new PTAConstant(1, true), AbstractCodeOp.Op.NONE);
//            stopGuard.backendCopyStop(conditionT);
//            getFinalTransition().guard = stopGuard;
//            conditionT.backendRemoveStops();
//            conditionF.backendRemoveStops();
//        }
    }
    /**
     * A symmetric subautomaton of a pair barrier.
     */
    private void generatePairBarrierSymmetric() {
        int finalLocation = newLocation();
        for(int i = 0; i < parties.numUsers; ++i)
            if(i != userIndex) {
                PTALabel common = newLabel(i);
                create(currOpIndex, finalLocation, common, conditionTStopped, false);
                if(conditional)
                    create(currOpIndex, loopOpIndex, common, conditionF, true);
            }
        setFinalTransition(
                create(finalLocation, passOpIndex, null, null, false));
    }
    /**
     * A producer subautomaton of a pair barrier.
     */
    private void generatePairBarrierProduce() throws PTAException {
        generateBarrierProduce();
        if(producerValue != null) {
            int passed = producerValue.value.getInteger();
            if(producerIndex >= parties.producerValues.length)
                throw new PTAException(null,
                        "pair barrier declares " + parties.producerValues.length +
                        " producers, but has more");
            IntBuffer buffer = parties.producerValues[producerIndex];
            if(buffer.hasRemaining()) {
                int prevPassed = buffer.get();
                buffer.rewind();
                if(prevPassed != passed)
                    throw new PTAException(null,
                            "a producer can not pass different values at each barrier");
            } else {
                buffer.clear();
                buffer.put(passed);
                buffer.flip();
            }
        }
    }
    /**
     * A consumer subautomaton of a pair barrier.
     */
    private void generatePairBarrierConsume() throws PTAException {
        int finalLocation = newLocation();
        boolean producersFound = false;
        for(int i = 0; i < parties.numProducers; ++i) {
            IntBuffer buffer = parties.producerValues[i];
            PTALabel common = newLabel(i);
            PTATransition x = create(currOpIndex, finalLocation,
                    common, conditionTStopped, false);
            if(producerIndex != -1) {
                PTABinaryExpression guard = new PTABinaryExpression(
                                new PTABufferedConstant(buffer, -1),
                                new PTAConstant(producerIndex, false),
                                CodeOpBinaryExpression.Op.BINARY_EQUAL);
                generator.setStop(guard, context);
                if(x.guard != null)
                    throw new RuntimeException("condition already exists");
                x.guard = guard;
            }
            if(consumeTarget != null) {
                PTAAssignment a = new PTAAssignment(
                        consumeTarget, new PTABufferedConstant(
                            buffer, -1));
                generator.setStop(a, context);
                x.update.add(a);
            }
            if(conditional)
                create(currOpIndex, loopOpIndex, common, conditionF, true);
        }
        setFinalTransition(
                create(finalLocation, passOpIndex, null, null, false));
    }
    /**
     * Generates a sub--automaton for this barrier party.
     */
    private void generate() throws PTAException {
        int source = currOpIndex;
        switch(type) {
            case BARRIER_VOID:
            case BARRIER_CONDITIONAL:
                generateBarrierSymmetric();
                break;
                
            case BARRIER_PRODUCE_VOID:
            case BARRIER_PRODUCE_CONDITIONAL:
                generateBarrierProduce();
                break;
                
            case BARRIER_CONSUME_VOID:
            case BARRIER_CONSUME_CONDITIONAL:
                generateBarrierConsume();
                break;
                
            case PAIR_BARRIER_VOID:
            case PAIR_BARRIER_CONDITIONAL:
                generatePairBarrierSymmetric();
                break;
                
            case PAIR_BARRIER_PRODUCE_VOID:
            case PAIR_BARRIER_PRODUCE_CONDITIONAL:
                generatePairBarrierProduce();
                break;
                
            case PAIR_BARRIER_CONSUME_VOID:
            case PAIR_BARRIER_CONSUME_VOID_PRODUCER:
            case PAIR_BARRIER_CONSUME_CONDITIONAL:
                generatePairBarrierConsume();
                break;
                
            default:
                throw new RuntimeException("unknown barrier type");
        }
    }
}
