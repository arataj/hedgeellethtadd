/*
 * TADDLock.java
 *
 * Created on May 9, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.pta.PTAException;
import pl.gliwice.iitis.hedgeelleth.pta.PTALabel;
import pl.gliwice.iitis.hedgeelleth.pta.PTABinaryExpression;
import pl.gliwice.iitis.hedgeelleth.pta.PTARange;
import pl.gliwice.iitis.hedgeelleth.pta.PTATransition;
import pl.gliwice.iitis.hedgeelleth.pta.PTAState;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.TADDGeneratorContext;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAConstant;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.CodeOpBinaryExpression;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeObject;
import pl.gliwice.iitis.hedgeelleth.pta.AbstractPTAExpression;

/**
 * A PTA that is a lock.
 * 
 * @author Artur Rataj
 */
public class TADDLock extends BackendPTA {
    /**
     * Type of lock variables.
     */
    public static final Type LOCK_VARIABLE_TYPE =
            new Type(Type.PrimitiveOrVoid.LONG);
    /**
     * Prefix of wait count variables.
     */
    protected static final String WAIT_COUNT_VARIABLE_PREFIX = "wait_count_";
    /**
     * Prefix of the notify transitions.
     */
    protected static final String NOTIFY_LABEL_PREFIX = "notify_";
    /**
     * Prefix of the notify all transitions.
     */
    protected static final String NOTIFY_ALL_LABEL_PREFIX = "notify_all_";
    /*
     * Range of values allowed for a lock.
     */
    // protected static final IntegerRange LOCK_RANGE = new IntegerRange(
    //         new Type(Type.PrimitiveOrVoid.INT));

    
    /**
     * State for the open state.
     */
    public PTAState open;
    /**
     * State for the closed state.
     */
    public PTAState closed;
    /**
     * Unique index of a new state.
     */
    private int nextNewStateIndex;
    /**
     * Number of wait transitions in this lock.
     */
    public int waitTransitions;
    
    /**
     * Creates a new instance of PTA.
     * 
     * @param object a runtime object from which originates this PTA
     * @param name name of this PTA
     */
    public TADDLock(RuntimeObject ro, String name) {
        super(ro, name);
        open = new PTAState(1);
        closed = new PTAState(0);
        open.owner = this;
        closed.owner = this;
        states.put(open.index, open);
        states.put(closed.index, closed);
        nextNewStateIndex = 2;
        startState = open;
        waitTransitions = 0;
    }
    /**
     * Creates a new lock variable, with the initial value of 0,
     * and guaranteed range 0 ... <code>maxValue</code>.
     * 
     * @param name                      name of the variable
     * @param generator                 PTA generator
     * @param maxValue                  maximum value of the created
     *                                  variable
     * @return                          new runtime variable
     */
    AbstractBackendPTAVariable newVariable(String name, TADDGeneratorContext context,
            int maxValue) {
        CodeVariable v = context.generator.lockVariables.newVariable(
                name, LOCK_VARIABLE_TYPE);
        try {
            BackendPTARange range = new BackendPTARange(null,
                    new PTARange(null, 0, maxValue));
            range.r().backendGuaranteed = true;
            return context.generator.compilation.newVariable(
                    context.generator.lockVariables,
                    v, false, -1, 0.0, range,
                    null, false);
        } catch(PTAException e) {
            throw new RuntimeException("unexpected exception: " +
                    e.toString());
        }
    }
    /**
     * Returns a lock variable or null if none.
     * 
     * @param name                      name of the variable
     * @param generator                 PTA generator
     * @return                          runtime variable or null
     */
    AbstractBackendPTAVariable getVariable(String name, TADDGeneratorContext context) {
        CodeVariable v = context.generator.lockVariables.variablesByName.get(
                name);
        if(v == null)
            return null;
        else
            return context.generator.compilation.getVariable(
                    context.generator.lockVariables, v);
    }
    /**
     * Adds a synchronisation. If a respective transition already
     * exists, its guard is updated. Otherwise a new transition is
     * added to this lock.
     * 
     * @param label label
     * @param closes true for a closing synchronisation, false for
     * an opening synchronisation
     */
    protected PTATransition addSynchronisation(
            PTALabel label, boolean closes) {
        PTATransition tt;
        Set<PTATransition> tts = labeled.get(label);
        if(tts == null) {
            tt = new PTATransition(label);
            if(closes) {
                tt.sourceState = open;
                tt.targetState = closed;
            } else {
                tt.sourceState = closed;
                tt.targetState = open;
            }
            addTransition(tt);
        } else if(tts.size() == 1) {
            tt = tts.iterator().next();
            if(
                    (closes && tt.targetState != closed) ||
                    (!closes && tt.targetState != open))
                throw new RuntimeException("direction mismatch");
        } else
            throw new RuntimeException("more that one transition " +
                    "having the same label");
        return tt;
    }
    /**
     * Adds BEGIN transition, meaning begin synchronized section,
     * if the transition does not yet exist.
     * 
     * Returns the label of the transition.
     * 
     * @param threadName                name of thread
     * @return                          new begin transition
     */
    public PTATransition addBegin(String threadName) {
        PTALabel label;
        try {
            label = new PTALabel("in_" + name + "_" + threadName,
                PTALabel.Type.SENDER, false);
        } catch(PTAException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
        return addSynchronisation(label, true);
    }
    /**
     * Adds END transition, meaning end of synchronized section,
     * if the transition does not yet exist.
     * 
     * Returns the label of the transition.
     * 
     * @param threadName                name of thread
     * @return                          new end transition
     */
    public PTATransition addEnd(String threadName) {
        PTALabel label;
        try {
            label = new PTALabel("out_" + name + "_" + threadName,
                    PTALabel.Type.SENDER, false);
        } catch(PTAException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
        return addSynchronisation(label, false);
    }
    /**
     * Returns wait count variable. The variable is created if it does
     * not exist yet.<br>
     *
     * The variable's maximum value, being the number of served
     * threads, is not known at this time, so this methos sets
     * the maximum value to the bogus value of -1, which must be
     * modified later.
     * 
     * @param context                   PTA generator's context
     * @return                          wait count variable
     */
    public AbstractBackendPTAVariable getWaitCountVariable(
            TADDGeneratorContext context) {
        AbstractBackendPTAVariable countVariable = getVariable(
                getWaitCountVariableName(this), context);
        if(countVariable == null) {
            // the counter does not exist, create one
            //
            // number of served threads not known at this time,
            // thus the bogus maximum value
            countVariable = newVariable(getWaitCountVariableName(this),
                    context, 0);
        }
        return countVariable;
    }
    /**
     * Adds WAIT transition, if the transition does not yet exist,
     * and add respective count variable if it does not exist yet.<br>
     * 
     * Returns the label of the transition.<br>
     *
     * A wait count variable might be created, see
     * <code>getWaitCountVariable</code> for details.
     * 
     * @param threadName                name of thread
     * @param context                   PTA generator's context
     * @param rm                        current runtime method
     * @return                          new wait transition
     */
    public PTATransition addWait(String threadName, TADDGeneratorContext context) {
        PTALabel label;
        try {
            label = new PTALabel("wait_" + name + "_" + threadName,
                    PTALabel.Type.SENDER, false);
        } catch(PTAException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
        AbstractBackendPTAVariable countVariable = getWaitCountVariable(
                context);
        Set<PTATransition> tts = labeled.get(label);
        if(tts == null) {
            PTATransition tt = new PTATransition(label);
            PTAState mid;
            if(context.generator.options.labeledUpdates)
                mid = null;
            else {
                // new state to separate synchronizing label and
                // update
                mid = new PTAState(newStateIndex());
                states.put(mid.index, mid);
            }
            PTATransition updateTt;
            tt.sourceState = closed;
            if(mid == null) {
                tt.targetState = open;
                updateTt = tt;
            } else {
                tt.targetState = mid;
                // the update transition after synchronization,
                // thus, the wait count variable will mark that there is
                // a thread waiting just after the thread goes into
                // waiting state; notify in--between won't work, but
                // other threads operate fully asynchronously in--between
                // anyway, as the update is before the lock opens
                PTATransition midTt = new PTATransition(null);
                midTt.sourceState = mid;
                midTt.targetState = open;
                addTransition(midTt);
                updateTt = midTt;
            }
            AbstractPTAExpression expr = new PTABinaryExpression(countVariable.v(),
                    new PTAConstant(1, false), CodeOpBinaryExpression.Op.
                    BINARY_PLUS);
            // no check for stops in locks
            // expr.backendEnableStop();
            updateTt.update.add(expr);
            expr.target = countVariable.v();
            addTransition(tt);
            ++waitTransitions;
            return tt;
        } else if(tts.size() == 1) {
            PTATransition tt = tts.iterator().next();
            return tt;
        } else
            throw new RuntimeException("more than a single transition " +
                    "having the same label");
    }
    /**
     * Adds ACTIVE transition, if the transition does not yet exist.
     * 
     * Returns the label of the transition.
     * 
     * @param threadName                name of thread
     * @return                          transition label
     */
    public PTATransition addActive(String threadName) {
        PTALabel label;
        try {
            label = new PTALabel("active_" + name + "_" + threadName,
                    PTALabel.Type.SENDER, false);
        } catch(PTAException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
        return addSynchronisation(label, true);
    }
    /**
     * Returns a name of a wait count variable.
     * 
     * @param lock                      tested lock
     * @return                          variable name
     */
    public static String getWaitCountVariableName(TADDLock lock) {
        return WAIT_COUNT_VARIABLE_PREFIX + lock.name;
    }
    /**
     * Returns a label of a notify transition, in either NOTIFY or WAIT
     * operations.
     * 
     * @param lock                      lock on which a thread to notify
     *                                  is waiting
     * @param notifyingTreadName        name of the notifying thread
     * @param notifiedTreadName         name of the notifying thread
     * @param notify                    true if the label is for a NOTIFY operation,
     *                                  false if the label is for a WAIT operation
     * @return                          transition label
     */
    public static PTALabel getNotifyLabel(TADDLock lock, String notifyingThreadName,
            String notifiedThreadName, boolean notify) {
        try {
            return new PTALabel(NOTIFY_LABEL_PREFIX + lock.name + "_" +
                notifyingThreadName + "_" + notifiedThreadName,
                notify ? PTALabel.Type.SENDER : PTALabel.Type.RECEIVER, false);
        } catch(PTAException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    /**
     * Returns a broadcasting label of a notify all transition, in either NOTIFY or WAIT
     * operations.
     * 
     * @param lock                      lock on which a thread to notify
     *                                  is waiting
     * @param notifyingTreadName        name of the notifying thread
     * @param notify                    true if the label is for a NOTIFY operation,
     *                                  false if the label is for a WAIT operation
     * @return                          transition label
     */
    public static PTALabel getNotifyAllLabel(TADDLock lock, String notifyingThreadName,
            boolean notify) {
        PTALabel label;
        try {
            label = new PTALabel(
                    NOTIFY_ALL_LABEL_PREFIX + lock.name + "_" +
                    notifyingThreadName,
                    notify ? PTALabel.Type.SENDER : PTALabel.Type.RECEIVER, true);
        } catch(PTAException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
        return label;
    }
    /**
     * Returns an index of a new state, unique within this lock.
     * 
     * @return
     */
    public synchronized int newStateIndex() {
        return nextNewStateIndex++;
    }
}
