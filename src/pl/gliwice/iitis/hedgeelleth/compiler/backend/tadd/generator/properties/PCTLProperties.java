/*
 * PCTLProperties.java
 *
 * Created on Feb 19, 2010, 10:33:08 AM
 *
 * Copyright (c) 2010 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.properties;

import java.util.*;

/**
 * Holds properties of a set of TADDs, in the format being a subset of
 * PCTL.<br>
 *
 * The properties complete range checking and represent assertions.
 *
 * @author Artur Rataj
 */
public class PCTLProperties {
    /**
     * A collection of PCTL properties.
     */
    public Collection<AbstractProperty> properties;

    /**
     * Creates a new instance of PCTLProperties. 
     */
    public PCTLProperties() {
        properties = new ArrayList<>();
    }
}
