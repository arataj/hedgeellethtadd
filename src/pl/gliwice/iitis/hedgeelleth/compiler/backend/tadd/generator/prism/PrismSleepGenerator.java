/*
 * PrismSleepGenerator.java
 *
 * Created on Jul 22, 2009
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.prism;

import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.*;

/**
 * Sleep generator for the Prism output format.
 *
 * @author Artur Rataj
 */
public class PrismSleepGenerator extends ClockSleepGenerator {
    /**
     * Creates a new instance of PrismSleepGenerator.
     */
    public PrismSleepGenerator() {
    }
}
