/*
 * TADDOptions.java
 *
 * Created on Apr 29, 2011, 10:47:43 AM
 *
 * Copyright (c) 2011 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import pl.gliwice.iitis.hedgeelleth.pta.SchedulerType;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.CompilationOptions;
import pl.gliwice.iitis.hedgeelleth.modules.HedgeellethCompiler;

/**
 * TADD compilation options. Initially set to the default values.
 */
public class TADDOptions {
    /**
     * Available ouput formats.
     */
    public enum OutputFormat {
        VERICS_BMC,
        XML,
        UPPAAL,
        PRISM,
        HEDGEELLETH,
    };
    /**
     * Output format of an optional accompanying producer/consumer documentation.
     */
    public enum DocOutputFormat {
        // no output generation
        NONE,
        // AADL
        AADL,
    };
    public enum ArraySupport {
        /**
         * No support for arrays at all.
         */
        NONE,
        /**
         * Arrays are emulated.
         */
        EMULATE,
        /**
         * Arrays are directly supported.
         */
        INDEX,
    };

    /**
     * If locations of locks should be committed, used only by
     * XML and Uppaall backends.<br>
     *
     * Default is <code>false</code>.
     */
    public boolean committedLocks = false;
    /**
     * Valuation type to generate.<br>
     *
     * Default is <code>TADDGenerator.ValuationType.NONE</code>.
     */
    public ValuationType valuationType =
            new ValuationType(ValuationType.Type.SELECTED, null);
    /**
     * Format of the output file.<br>
     *
     * Default is <code>OutputFormat.XML</code>.
     */
    private OutputFormat outputFormat = OutputFormat.XML;
    /**
     * Format of an accompanying output file with a producer/consumer
     * documentation.<br>
     *
     * Default is <code>OutputFormat.NONE</code> which means that no such
     * file will be generated.
     */
    private DocOutputFormat docOutputFormat = DocOutputFormat.NONE;
    /**
     * If generator tags are enabled.<br>
     *
     * Default is <code>true</code>.
     */
    public boolean enableGeneratorTags = true;
    /**
     * Level of verbosity of comments in the ouput file,
     * 0 ... FileFormatIO.MAX_VERBOSITY_LEVEL.<br>
     *
     * Default is <code>FileFormatIO.DEFAULT_VERBOSITY_LEVEL</code>.
     */
    public int verbosityLevel = FileFormatOptions.DEFAULT_VERBOSITY_LEVEL;
    /*
     * It alters the translation somewhat and was requested by Andrzej.<br>
     *
     * Default is <code>false</code>.
     */
    // public boolean supportBackwardAnalysis = false;
    /**
     * If only variables should be commented in the output file.<br>
     *
     * Default is <code>false</code>.
     */
    public boolean commentVariablesOnly = false;
    /**
     * Experiment mode.
     *
     * Default is <code>ExperimentMode.FALSE</code>.
     */
    public HedgeellethCompiler.Options.ExperimentMode experimentMode =
            HedgeellethCompiler.Options.ExperimentMode.FALSE;
    /**
     * If the directory with experiment output files should be cleaned before
     * the files are generated.
     *
     * Default is <code>true</code>.
     */
    public boolean cleanExperimentDirectory = true;
    /**
     * If to generate sources with integer set expressions replaced with
     * constants, that are dependent on the experiment.
     *
     * Default is <code>false</code>.
     */
    public boolean generateExperimentSources = false;
    /**
        * Copied to <code>FileFormatOptions.verboseTADDVariables</code>.<br>
        * 
        * The default is true.
        */
    public boolean verboseTADDVariables = true;
    /**
     * If non--determinism caused by the synchronisation between directed
     * labels is allowed. Barrier labels do not exhibit non--determinism of
     * synchronisation.<br>
     * 
     * Important, when there is more than one listener label of a given name.
     * If this option is true, then a sender synchronises with a single, random
     * listener, as in UPPAAL. If false, a sender always synchronises with all listeners
     * at once, as in Prism.<br>
     *      * 
    * The default is true. The option should be false when generating to the Verics or
    * Prism format.
     */
    public boolean labelNondeterminism = true;
    /**
     * Eventually passed to <code>PTAOptimizer.removeFalseTransitions</code>.
     * Normally true, except for compiler tests.<br>
     * 
     * The default is true.
     */
    public boolean removeConstGuardsUsingRanges = true;
    /**
     * If to allow for floating--point variables in the PTAs.<br>
     * 
     * The default is true.  The option should be false when generating to the Prism format.
     */
    public boolean allowFpVariables = true;
    /**
     * If, beside normal output, also generate source code for a Java class, that returns
     * values of the state vector. Such a class can be used one the JVM side.<br>
     * 
     * The default is false.
     */
    public boolean generateStateClass = false;
    /**
     * If, beside normal output, also generate a file which describes non--determinism
     * in the model.<br>
     * 
     * The default is false.
     */
    public boolean generateNdFile = false;
    /**
     * How to handle arrays in the output model.
     * 
     * The default is EMULATE.
     */
    public ArraySupport arraySupport = ArraySupport.EMULATE;
    /**
     * Test options, null if outside compiler tests.
     */
    public CompilationOptions.Test test;
    /**
     * Scheduler for the PTAs. The default is a free scheduler.
     */
    public Scheduler scheduler = new Scheduler(SchedulerType.FREE,
            false, false, Double.NaN, Double.NaN, true, true, false);
    /**
     * If to disallow any move of field access to a neighbouring transition,
     * even if there are no explicit time constraints and no violation
     * of the order of field access.<br>
     * 
     * The default is false.
     */
    public boolean strictFieldAccess = false;
    /**
     * If checking of variable and primary ranges should be performed.
     * 
     * The default is true.
     */
    public boolean rangeChecking = true;
    /**
     * If to use smaller long ranges, that fit into 32 bits.
     * 
     * The default is true and must be true for a typical PTA generation.
     */
    public boolean reducedLongRange = true;
    /**
     * Specifies, what hc's library method
     * <code>Model.isStatistical()</code> returns.
     * 
     * The default is false.
     */
    public boolean isStatisticalHint = false;
    /**
     * If model control's properties should be generated.
     * 
     * The default is true.
     */
    public boolean generateModelControlProperties = true;
    /**
     * Fall back from MDP to DTMC if no non--determinism.
     */
    public boolean dtmcFallback = false;
    /**
     * Name shortener of variables replaces instances with their position in a sorted list.
     */
    public boolean compactInstances = true;
    
    /**
     * Sets the output format. Checks if <code>labelNondeterminism</code>
     * has a proper value for a given output format, otherwise throws a runtime
     * exception.
     * 
     * @param format format to set
     */
    public void setOutputFormat(OutputFormat format) {
        outputFormat = format;
        if(outputFormat == OutputFormat.PRISM) {
            if(labelNondeterminism)
                throw new RuntimeException("Prism format does not allow label nondeterminism");
            if(allowFpVariables)
                throw new RuntimeException("Prism format does not allow for floating--point variables");
        }
    }
    /**
     * Returns the output format.
     * 
     * @return output format
     */
    public OutputFormat getOutputFormat() {
        return outputFormat;
    }
    /**
     * Sets an output format of the optional accompanying producer/consumer
     * documentation file, <code>DocOutputFormat.NONE</code> for no generation
     * of such a file.
     * 
     * @param dof documentation output format
     */
    public void setDocOutputFormat(DocOutputFormat dof) {
        docOutputFormat = dof;
    }
    /**
     * Returns the accompanying producer/consumer documentation output format.
     * 
     * @return output format, <code>DocOutputFormat.NONE</code> for no
     * generation of the producer/consumer documentation
     */
    public DocOutputFormat getDocOutputFormat() {
        return docOutputFormat;
    }
};
