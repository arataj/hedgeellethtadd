/*
 * TADDBackend.java
 *
 * Created on Apr 14, 2009, 2:21:12 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.*;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.prism.PrismTADDGenerator;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.uppaal.UppaalTADDGenerator;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.prism.PrismIO;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.verics.VericsIO;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.xml.JavaTADDIO;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.xml.UppaalIO;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.mirela.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeCompilation;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.Optimizer;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.util.ModelType;
import pl.gliwice.iitis.hedgeelleth.compiler.util.PWD;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.StringAnalyze;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;
import pl.gliwice.iitis.hedgeelleth.modules.AbstractBackend;
import pl.gliwice.iitis.hedgeelleth.pta.PTA;
import pl.gliwice.iitis.hedgeelleth.pta.PTAException;
import pl.gliwice.iitis.hedgeelleth.pta.io.PTAIO;

/**
 * A backend for writing files by Java2TADD.<br>
 *
 * Semantic difference between Uppaal and XML output: for XML, it is assumed
 * that the broadcast label does not necessarily need to be non--blocking.
 *
 * @author Artur Rataj
 */
public class TADDBackend extends AbstractBackend {
    /**
     * This backend's options.
     */
    private TADDOptions options;
    /**
     * A buffer to fill with compilation assembly code to be used in
     * compiler tests, or null if not needed.
     */
    private TestAssemblyBuffer assemblyBuffer;
    /**
     * Default options for the optimizer. Modified only for testing.
     */
    public Optimizer.Tuning optimizerTuning;

    /**
     * Creates a new instance of TADDBackend.
     *
     * @param options                   the backend's options
     * @param assemblyBuffer            assembly buffer or null;
     *                                  only for testing
     */
    public TADDBackend(TADDOptions options, TestAssemblyBuffer assemblyBuffer) {
        super();
        this.options = options;
        this.assemblyBuffer = assemblyBuffer;
        optimizerTuning = new Optimizer.Tuning();
    }
    @Override
    public List<String> translate(CodeCompilation codeCompilation,
            PWD topDirectory, String outputFileNamePrefix,
            CompilerException translationErrors, OutputMode outputMode,
            Set<ModelType> modelTypes) {
        boolean experimentOutput;
        switch(outputMode) {
            case NORMAL:
                experimentOutput = false;
                break;

            case EXPERIMENT:
            case EXPERIMENT_CLEAN:
                experimentOutput = true;
                break;

            default:
                throw new RuntimeException("unknown output mode");
        }
        List<String> outputFileNames = new LinkedList<>();
        // System.out.println("Generating automatons...");
        AbstractTADDGenerator tg = null;
        AbstractDocBackend db = null;
        try {
            TADDGeneratorOptions tgOptions = new TADDGeneratorOptions(
                    options);
            switch(options.getOutputFormat()) {
                case VERICS_BMC:
                case XML:
                case UPPAAL:
                    tgOptions.labeledUpdates = true;
                    tgOptions.broadcastLabels =
                            options.getOutputFormat() != TADDOptions.OutputFormat.VERICS_BMC;
                    tgOptions.broadcastLabelBlocks =
                            options.getOutputFormat() != TADDOptions.OutputFormat.UPPAAL;
                    // tgOptions.allowDistributions =
                    //         options.getOutputFormat() == TADDOptions.OutputFormat.XML;
                    tg = new UppaalTADDGenerator(codeCompilation,
                            tgOptions);
                    break;

                case PRISM:
                    tgOptions.labeledUpdates = false;
                    tgOptions.broadcastLabels = false;
                    tgOptions.broadcastLabelBlocks = false;
                    tg = new PrismTADDGenerator(codeCompilation,
                            tgOptions);
                    break;

                case HEDGEELLETH:
                    tgOptions.labeledUpdates = true;
                    tgOptions.broadcastLabels = false;
                    tgOptions.broadcastLabelBlocks = false;
                    // tgOptions.allowDistributions = true;
                    tg = new PrismTADDGenerator(codeCompilation,
                            tgOptions);
                    break;

                default:
                    throw new RuntimeException("unknown output format");

            }
            // tgOptions.allowVariableDistributions = tgOptions.allowDistributions;
            RuntimeMethod rm = tg.generate(optimizerTuning);
            if(options.getDocOutputFormat() != TADDOptions.DocOutputFormat.NONE) {
                String name = tg.getMainClassName();
                name = name.substring(name.lastIndexOf('.') + 1);
                List<BackendPTA> ptas = new LinkedList<>();
                for(PTA pta : tg.compilation.ptas.values())
                    ptas.add((BackendPTA)pta);
                MirelaNetwork mn = new MirelaNetwork(name,
                        codeCompilation, ptas, tg.interpreter, rm);
                if(mn.nodes != null)
                    System.out.println("**\n" + mn.toString());
                switch(options.getDocOutputFormat()) {
                    case NONE:
                        db = null;
                        break;
                        
                    case AADL:
                        db = new AadlDocBackend(mn);
                        break;
                        
                    default:
                        throw new RuntimeException("unknown output format");
                }
            }
            tg.compilation.appendConstantFields(codeCompilation, tg.interpreter);
        } catch(PTAException e) {
            for(CompilerException.Report r : e.getReports())
                translationErrors.addReport(new CompilerException.Report(
                        r.pos, r.code,
                        "construction of PTA: " + r.message));
        }
        String stateFileName = null;
        String ndFileName = null;
        String docFileName = null;
        if(tg != null) {
            if(assemblyBuffer != null) {
                // used in compiler tests
                assemblyBuffer.inline0 = tg.lastInlineString0;
                assemblyBuffer.inline = tg.lastInlineString;
            }
            String experimentDirectoryName;
            switch(outputMode) {
                case NORMAL:
                    experimentDirectoryName = null;
                    break;

                case EXPERIMENT:
                case EXPERIMENT_CLEAN:
                    experimentDirectoryName = tg.getMainClassName();
                    if(options.test == null)
                        // a shortened name of the output directory
                        // if outside compiler tests
                        experimentDirectoryName = experimentDirectoryName.
                                substring(experimentDirectoryName.lastIndexOf('.') + 1);
                    break;

                default:
                    throw new RuntimeException("unknown output mode");
            }
            PWD outputDirectory;
            if(experimentOutput) {
                outputDirectory = new PWD(topDirectory);
                outputDirectory.appendPath(experimentDirectoryName);
                File d = outputDirectory.getFile("");
                if(d.exists()) {
                    if(outputMode == OutputMode.EXPERIMENT_CLEAN &&
                            !CompilerUtils.deleteDirectoryRecursively(d))
                        // directory exists, in this output mode delete it along with
                        // its contents
                        translationErrors.addReport(new CompilerException.Report(null,
                                CompilerException.Code.IO,
                                "could not remove directory " + experimentDirectoryName));
                }
                if(!d.exists() && !d.mkdir())
                    translationErrors.addReport(new CompilerException.Report(null,
                            CompilerException.Code.IO,
                            "could not create directory " + experimentDirectoryName));
            } else
                outputDirectory = topDirectory;
            if(!translationErrors.reportsExist()) {
                String extension = "";
                switch(options.getOutputFormat()) {
                    case VERICS_BMC:
                        extension = "tadd";
                        break;

                    case XML:
                        extension = "pxml";
                        break;

                    case UPPAAL:
                        extension = "xml";
                        break;

                    case PRISM:
                        extension = PrismIO.PRISM_MODEL_EXTENSION;
                        break;

                    case HEDGEELLETH:
                        extension = PrismIO.HEDGEELLETH_MODEL_EXTENSION;
                        break;

                    default:
                        throw new RuntimeException("unknown output format");
                }
                if(outputFileNamePrefix == null)
                    outputFileNamePrefix = "";
                String outputFileName;
                if(experimentOutput) {
                    outputFileName = outputFileNamePrefix +
                        "." + extension;
                } else {
                    String className = tg.getMainClassName();
                    outputFileName = outputFileNamePrefix +
                        className.substring(
                        className.lastIndexOf('.') + 1) +
                        "." + extension;
                }
                // System.out.println("ok.");
                FileFormatIO compilationWriter;
                FileFormatOptions generationOptions = new FileFormatOptions(
                        options.verbosityLevel,
                        options.commentVariablesOnly,
                        options.verboseTADDVariables,
                        options.rangeChecking,
                        options.generateModelControlProperties,
                        options.arraySupport == TADDOptions.ArraySupport.EMULATE,
                        options.dtmcFallback,
                        options.compactInstances);
                switch(options.getOutputFormat()) {
                    case VERICS_BMC:
                        compilationWriter = new VericsIO(generationOptions);
                        break;

                    case XML:
                        compilationWriter = new JavaTADDIO(generationOptions,
                                options.committedLocks);
                        break;

                    case UPPAAL:
                        compilationWriter = new UppaalIO(generationOptions,
                                options.committedLocks);
                        break;

                    case PRISM:
                        compilationWriter = new PrismIO(PTAIO.Flavour.PRISM,
                                generationOptions);
                        break;

                    case HEDGEELLETH:
                        compilationWriter = new PrismIO(PTAIO.Flavour.HEDGEELLETH,
                                generationOptions);
                        break;

                    default:
                        throw new RuntimeException("unknown output format " +
                                options.getOutputFormat());
                }
                try {
                    File f = outputDirectory.getFile(outputFileName);
                    modelTypes.addAll(
                            compilationWriter.write(f, tg.compilation));
                    if(options.generateStateClass)
                        try {
                            String classNameSuffix = outputFileNamePrefix.
                                    replace(',', '_');
                            stateFileName = StateClass.generate(
                                    outputDirectory, codeCompilation,
                                    tg.compilation, tg.interpreter,
                                    classNameSuffix);
                        } catch(PTAException e) {
                            throw new IOException(e.getMessage());
                        }
                    if(options.generateNdFile) {
                        File g = outputDirectory.getFile(StringAnalyze.replaceExtension(
                                outputFileName, NdFileIO.EXTENSION));
                        NdFileIO.write(outputDirectory, g, tg.compilation);
                        ndFileName = g.getName();
                    }
                    if(db != null) {
                        docFileName = StringAnalyze.replaceExtension(f.getPath(), "aadl");
                        db.generate(new File(docFileName), compilationWriter.ptaNameMap);
                    }
                    String fileName;
                    switch(outputMode) {
                        case NORMAL:
                            fileName = outputFileName;
                            break;

                        case EXPERIMENT:
                        case EXPERIMENT_CLEAN:
                            fileName = experimentDirectoryName +
                                    File.separatorChar + outputFileName;
                            break;

                        default:
                            throw new RuntimeException("unknown output mode");
                    }
                    outputFileNames.add(fileName);
                    switch(options.getOutputFormat()) {
                        case PRISM:
                        {
                            String pctlFileName = PrismIO.
                                    toPctlFilename(PrismIO.PRISM_MODEL_EXTENSION,
                                        fileName);
                            outputFileNames.add(pctlFileName);
                            break;
                        }
                        case HEDGEELLETH:
                        {
                            String pctlFileName = PrismIO.
                                    toPctlFilename(PrismIO.HEDGEELLETH_MODEL_EXTENSION,
                                        fileName);
                            outputFileNames.add(pctlFileName);
                            break;
                        }
                        default:
                            break;
                    }
                } catch(IOException e) {
                    translationErrors.addReport(new CompilerException.Report(
                            null, ParseException.Code.IO, e.getMessage()));
                }
            }
        }
        if(stateFileName != null)
            outputFileNames.add(stateFileName);
        if(ndFileName != null)
            outputFileNames.add(ndFileName);
        if(docFileName != null)
            outputFileNames.add(docFileName);
        return outputFileNames;
    }
}
