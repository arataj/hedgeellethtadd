/*
 * ValuationType.java
 *
 * Created on Jun 24, 2009, 4:29:53 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

/**
 * Valuation types: none, standard and where the tag
 * <code>selected</code> applies.
 */
public class ValuationType {
    public enum Type {
        NONE,
        FULL,
        SELECTED,
    };

    /**
     * Type of this valuation.
     */
    public Type type;
    /**
     * Label required, null for none.
     */
    public String label;

    /**
     * Creates a new instance of <code>ValuationType</code>.
     *
     * @param type                      type of the valuation
     * @param label                     label, null for none
     */
    public ValuationType(Type type, String label) {
        this.type = type;
        this.label = label;
    }
};
