/*
 * AbstractDocBackend.java
 *
 * Created on Mar 28, 2018
 *
 * Copyright (c) 2018  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.mirela;

import java.util.*;
import java.io.*;
import pl.gliwice.iitis.hedgeelleth.pta.PTA;
import pl.gliwice.iitis.hedgeelleth.pta.PTAException;

/**
 * An abstract backend for generating a a file the accompanying
 * producer/consumer architecture.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractDocBackend {
    /**
     * A Mirela network.
     */
    protected final MirelaNetwork NET;
    
    /**
     * Creates an abstract documentation backend.
     * 
     * @param net Mirela network
     */
    public AbstractDocBackend(MirelaNetwork net) {
        NET = net;
    }
    /**
     * Save the Mirela network to a file. Depending on the output format,
     * only selected information about the network will be saved.
     * 
     * @param outFile file to create
     * @param ptaNameMap an optional pta name mapping to be used in the
     * generated file, null for default PTA names
     */
    public abstract void generate(File outFile, Map<PTA, String> ptaNameMap) throws IOException;
}
