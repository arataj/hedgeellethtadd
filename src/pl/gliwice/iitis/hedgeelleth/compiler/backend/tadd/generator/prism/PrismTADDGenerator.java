/*
 * PrismTADDGenerator.java
 *
 * Created on Jul 22, 2009
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.prism;

import pl.gliwice.iitis.hedgeelleth.pta.PTAException;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeCompilation;

/**
 * Generates a TADD Prism--style.
 *
 * @author Artur Rataj
 */
public class PrismTADDGenerator extends GraphTADDGenerator {
    /**
     * Creates a new instance of PrismTADDGenerator.
     *
     * @param compilation               compilation
     * @param options                   standard types of options of this TADD
     *                                  generator
     */
    public PrismTADDGenerator(CodeCompilation codeCompilation,
            TADDGeneratorOptions options) throws PTAException {
        super(codeCompilation, options,
                new PrismSchedulerGenerator(),
                new PrismSleepGenerator());
    }
}
