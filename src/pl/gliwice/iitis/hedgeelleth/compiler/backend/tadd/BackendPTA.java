/*
 * BackendPTA.java
 *
 * Created on Jul 20, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.*;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.AbstractTADDGenerator;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeObject;
import pl.gliwice.iitis.hedgeelleth.pta.*;

/**
 * A PTA compilation plus functionality required by the backend.
 * 
 * @author Artur Rataj
 */
public class BackendPTA extends PTA {
    /**
     * A runtime object from which originates this PTA.
     */
    public final RuntimeObject RO;
    
    /**
     * Creates a new instance of BackendPTA.
     * 
     * @param object a runtime object from which originates this PTA
     * @param name name of this PTA
     */
    public BackendPTA(RuntimeObject ro, String name) {
        super(name);
        RO = ro;
    }
    /**
     * Removes a transition. The transition must have the source state field
     * already set.
     * 
     * The transition is removed from map of transitions of
     * the connecting states, if not already there.
     * 
     * @param tt                        transition
     */
    public void removeTransition(PTATransition tt) {
        removeFromLists(tt);
        for(SortedSet s : new LinkedList<>(tt.sourceState.output.values())) {
            s.remove(tt);
            if(s.isEmpty())
                tt.sourceState.output.values().remove(s);
        }
        if(tt.targetState != null)
            for(SortedSet s : new LinkedList<>(tt.targetState.input.values())) {
                s.remove(tt);
                if(s.isEmpty())
                    tt.targetState.input.values().remove(s);
            }
    }
    /**
     * Verifies if names of non--compiler tags are within a given set,
     * and if the required tags are present.<br>
     * 
     * Also throws an exception if a non--recognized tag name or an invalid modifier
     * prefix are found with <code>ParseException.Code.UNKNOWN</code>, and if
     * a non--recognized compiler modifier is found, throws a runtime
     * exception, as compiler tags should already be parsed by the compiler
     * before.
     * 
     * @param allowed                   set to verify the names against,
     *                                  key is tag name, value is allowed
     *                                  modifier's prefix
     * @param required                  set to verify the names against,
     *                                  key is tag name, value is required
     *                                  modifier's prefix, the tag names
     *                                  present in this map must also
     *                                  be present in <code>allowed</code>,
     *                                  but the opposite is not true,
     *                                  as <code>allowed</code> is enough
     *                                  to find out non--recognized tag names
     * @param unique                    set to verify the names against,
     *                                  key is tag name, value is unique
     *                                  modifier's prefix, that is, one
     *                                  that can occur either zero times or
     *                                  a single time, the tag names
     *                                  present in this map must also
     *                                  be present in <code>allowed</code>,
     *                                  but the opposite is not true,
     *                                  as <code>allowed</code> is enough
     *                                  to find out non--recognized tag names
     */
    public void verifyNonCompilerTagModifiers(
            Map<String, Set<String>> allowed,
            Map<String, Set<String>> required,
            Map<String, Set<String>> unique) throws ParseException {
        // System.out.println("tadd: " + this.toString());                
        if(allowed != null) {
            ParseException e = new ParseException();
            for(int index : states.keySet()) {
                PTAState state = states.get(index);
                for(int o : state.output.keySet())
                    for(PTATransition tt : state.output.get(o))
                        for(AbstractCodeOp op : tt.backendOps)
                            for(CommentTag ct : op.getTags()) {
                                // System.out.println("op: " + op.toString() + " ct: " + ct.toString());
                                /*if(op.toString().indexOf(" Eat") != -1)
                                    op = op;*/
                                Set<String> setAllowed = allowed.get(ct.name);
                                if(setAllowed == null)
                                    throw new ParseException(ct.getStreamPos(),
                                            ParseException.Code.UNKNOWN,
                                            "unrecognized name of comment tag " + ct.toString());
                                Set<String> setRequired = required.get(ct.name);
                                TreeSet<String> setRequiredCopy;
                                if(setRequired != null) 
                                    setRequiredCopy = new TreeSet<String>(
                                        setRequired);
                                else
                                    setRequiredCopy = new TreeSet<String>();
                                Set<String> setUnique = unique.get(ct.name);
                                TreeSet<String> setUniqueCopy;
                                if(setUnique != null) 
                                    setUniqueCopy = new TreeSet<String>(
                                        setUnique);
                                else
                                    setUniqueCopy = new TreeSet<String>();
                                Set<String> occured = new HashSet<String>();
                                for(String modifier : ct.modifiers) {
                                    String prefix = CommentTag.getModifierPrefix(modifier);
                                    if(CommentTag.getCompilerModifier(modifier) == null) {
                                        if(!setAllowed.contains(prefix))
                                            e.addReport(new ParseException.Report(ct.getStreamPos(),
                                                    ParseException.Code.UNKNOWN,
                                                    "unrecognized modifier " + modifier + " in comment tag " +
                                                    ct.toString()));
                                        setRequiredCopy.remove(prefix);
                                        if(setUniqueCopy.contains(prefix) &&
                                                occured.contains(prefix))
                                            e.addReport(
                                                    new ParseException.Report(ct.getStreamPos(),
                                                    ParseException.Code.MISSING,
                                                    "duplicate modifier named " + prefix + " not allowed " +
                                                    "in comment tag " + ct.toString()));
                                        occured.add(prefix);
                                    }
                                }
                                if(!setRequiredCopy.isEmpty()) {
                                    for(String prefix : setRequiredCopy)
                                        e.addReport(
                                                new ParseException.Report(ct.getStreamPos(),
                                                ParseException.Code.MISSING,
                                                "missing modifier named " + prefix + " in comment tag " +
                                                ct.toString()));
                                }
                            }
            }
            if(e.reportsExist())
                throw e;
        }
    }
    /**
     * Filters all tags, as defined in CodeOp.filterTags().
     * 
     * Even that <code>CodeCompilation</code> also contains a method
     * for filtering tags, the PTA backend can create its own operations.
     * This is why a PTA contains its own method for filtering tags.
     */
    void filterTags() {
        for(int index : states.keySet()) {
            PTAState state = states.get(index);
            for(int o : state.output.keySet())
                for(PTATransition tt : state.output.get(o))
                    for(AbstractCodeOp op : tt.backendOps)
                        op.filterTags();
        }
    }
    /**
     * Generates standard valuation, overriding any existing one.
     * 
     * @param indexBeg                  variable index from which to begin
     * @return                          next free variable index
     */
    int generateStandardValuation(int beginIndex) {
        valuation.clear();
        for(int index : states.keySet())
            valuation.put(states.get(index), beginIndex++);
        return beginIndex;
    }
    /**
     * Generates selected valuation, overriding any existing one. It
     * uses "selected" tags, so it should be called after the tags are filtered
     * by <code>filterTags()</code>.
     * 
     * @param label                     label required, null for none
     * @param indexBeg                  variable index from which to begin
     * @return                          next free variable index
     */
    int generateSelectedValuation(String label, int beginIndex) {
        valuation.clear();
        for(int index : states.keySet()) {
            PTAState state = states.get(index);
            for(int o : state.output.keySet())
                for(PTATransition tt : state.output.get(o))
                    NEXT_OP:
                    for(AbstractCodeOp op : tt.backendOps)
                        for(CommentTag s : op.getTags())
                            if(s.name.equals("selected")) {
                                if(label != null) {
                                    boolean found = false;
                                    for(CommentTag sLabel : op.getTags())
                                        if(sLabel.name.equals(label)) {
                                            found = true;
                                            break;
                                        }
                                    if(!found)
                                        continue NEXT_OP;
                                }
                                PTAState l;
                                if(op instanceof CodeOpNone) {
                                    CodeOpNone n = (CodeOpNone)op;
                                    if(n.annotations.contains(AbstractTADDGenerator.TA_IN_STRING) ||
                                            n.annotations.contains(AbstractTADDGenerator.TA_OUT_STRING) ||
                                            n.annotations.contains(CompilerAnnotation.TAIL_STRING))
                                        l = tt.targetState;
                                    else
                                        l = state;
                                } else
                                    l = state;
                                valuation.put(l, beginIndex++);
                                break;
                            }
        }
        return beginIndex;
    }
}

