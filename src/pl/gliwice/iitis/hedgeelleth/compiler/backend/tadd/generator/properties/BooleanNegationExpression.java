/*
 * BooleanNegationExpression.java
 *
 * Created on Feb 19, 2010, 1:29:41 PM
 *
 * Copyright (c) 2010 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.properties;

/**
 * A boolean negation expression.
 *
 * @author Artur Rataj
 */
public class BooleanNegationExpression extends AbstractConditionalExpression {
    /**
     * A subexpressio nof this expression.
     */
    public AbstractConditionalExpression sub;

    /**
     * Creates a new instance of BooleanNegationExpression.
     *
     * @param sub                       a subexpression of this
     *                                  expression
     */
    public BooleanNegationExpression(AbstractConditionalExpression sub) {
        super();
        this.sub = sub;
    }
}
