/*
 * SimplePropertyWriter.java
 *
 * Created on Feb 19, 2010, 1:35:41 PM
 *
 * Copyright (c) 2010 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.prism;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.properties.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.BinaryExpression;
import pl.gliwice.iitis.hedgeelleth.pta.io.PTAIO;
import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAVariable;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAConstant;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAElementVariable;

/**
 * Writes simple PCTL properties, so that they are compatible with a generated
 * Prism or PTA model.
 * 
 * @author Artur Rataj
 */
public class SimplePropertyWriter {
    /**
     * A PTA context.
     */
    PTAIO.Context context;

    /**
     * Creates a new instance of SimplePropertyWriter. 
     * 
     * @param context a PTA context
     */
    public SimplePropertyWriter(PTAIO.Context context) {
        this.context = context;
    }
    /**
     * If the number is integer, returns its textual form as it
     * would be integer, and not double. Otherwise, if the number
     * is not integer, returns directly the textual form of double.
     * 
     * @param d                         number
     * @return                          textual form of the number
     */
    protected String toString(double d) {
        if(d >= Integer.MIN_VALUE && d <= Integer.MAX_VALUE && Math.floor(d) == d)
            return "" + (int)d;
        else
            return "" + d;
    }
    /**
     * Converts a probability operator to Prism format.
     *
     * @param p                         probability operator
     * @return                          textual form
     */
    protected String toString(ProbabilityOperator p) {
        String out = "P " + p.r.toString() + " " + toString(p.p);
        return out;
    }
    /**
     * Converts a binary operator to Prism format.
     *
     * @param o                         binary operator
     * @return                          textual form
     */
    public String toString(BinaryExpression.Op o) {
        switch(o) {
            case CONDITIONAL_AND:
                return "&";

            case CONDITIONAL_OR:
                return "|";

            case EQUAL:
                switch(context.flavour) {
                    case PRISM:
                        return "=";
                        
                    case HEDGEELLETH:
                        return o.toString();
                        
                    default:
                        throw new RuntimeException("unknown flavour");
                }
            case INEQUAL:
            case LESS:
            case GREATER:
            case LESS_OR_EQUAL:
            case GREATER_OR_EQUAL:
                return o.toString();

            default:
                throw new RuntimeException("unknown operator");
        }
    }
    /**
     * Converts a relational expression to Prism or PTA format.
     *
     * @param e                         relational expression
     * @return                          textual form
     */
    protected String toString(RelationalExpression e) {
        String out;
        AbstractPTAVariable v;
        if(e.left instanceof PTAElementVariable) {
            PTAElementVariable element = (PTAElementVariable)e.left;
            v = element.array;
            // index is known to be an integer constant within
            // simple properties
            out = "[" + (int)((PTAConstant)element.index).value + "]";
        } else {
            v = e.left;
            out = "";
        }
        out = PTAIO.getName(context, v) + out + " " +
            toString(e.r) + " " +
            toString(e.right);
        return out;
    }
    /**
     * Converts a boolean binary expression to Prism format.
     *
     * @param e                         boolean binary expression
     * @return                          textual form
     */
    protected String toString(BooleanBinaryExpression e) {
        String out = "( " + toString(e.left) + " ) " +
                toString(e.b) +
                " ( " + toString(e.right) + " )";
        return out;
    }
    /**
     * Converts a boolean negation expression to Prism format.
     *
     * @param e                         boolean negation expression
     * @return                          textual form
     */
    protected String toString(BooleanNegationExpression e) {
        String out = "!( " + toString(e.sub) + " )";
        return out;
    }
    /**
     * Converts a conditonal expression to Prism format.
     *
     * @param e                         conditional expression
     * @return                          textual form
     */
    protected String toString(AbstractConditionalExpression e) {
        String out = "";
        if(e instanceof AbstractPath)
            out = toString((AbstractPath)e);
        else if(e instanceof RelationalExpression)
            out = toString((RelationalExpression)e);
        else if(e instanceof BooleanBinaryExpression)
            out = toString((BooleanBinaryExpression)e);
        else if(e instanceof BooleanNegationExpression)
            out = toString((BooleanNegationExpression)e);
        else
            throw new RuntimeException("unknown type of path");
        return out;
    }
    /**
     * Converts a path to Prism format.
     *
     * @param path                      path
     * @return                          textual form
     */
    protected String toString(AbstractPath path) {
        String out;
        if(path instanceof FPath) {
            FPath f = (FPath)path;
            out = "F " + toString(f.sub);
        } else if(path instanceof GPath) {
            GPath g = (GPath)path;
            out = "G " + toString(g.sub);
        } else
            throw new RuntimeException("unknown type of path");
        return out;
    }
    /**
     * Converts a simple property to Prism format.
     *
     * @param p                         property
     * @return                          textual form
     */
    public String write(SimpleProperty p) {
        String out = "";
        out += toString(p.p);
        out += " [ ";
        out += toString(p.path);
        out += " ]";
        return out;
    }
}
