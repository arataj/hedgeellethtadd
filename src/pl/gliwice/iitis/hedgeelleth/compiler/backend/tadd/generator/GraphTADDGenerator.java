/*
 * GraphTADDGenerator.java
 *
 * Created on Jul 20, 2009, 1:12:47 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CompilerAnnotation;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.AbstractCodeDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.ResolveReferences;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.interpreter.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.op.BarrierParties;
import pl.gliwice.iitis.hedgeelleth.interpreter.op.CodeArithmetics;
import pl.gliwice.iitis.hedgeelleth.pta.*;
import pl.gliwice.iitis.hedgeelleth.pta.values.*;

/**
 * A standard TADD generator -- implements methods that are likely to be
 * common by all TADD generators, that use a graph representation.<br>
 *
 * Needs generators of a scheduler and sleep, as
 * these are the most output format--dependent TADD elements.
 *
 * @author Artur Rataj
 */
public class GraphTADDGenerator extends AbstractAnnotationTADDGenerator {
    /**
     * Scheduler generator.
     */
    final AbstractSchedulerGenerator scheduler;
    /**
     * Sleep generator.
     */
    final AbstractSleepGenerator sleep;

    /**
     * Creates a new instance of GraphTADDGenerator. 
     *
     * @param codeCompilation               compilation
     * @param options                   standard types of options of this TADD
     *                                  generator
     * @param scheduler                 scheduler generator
     * @param sleep                     sleep generator
     */
    public GraphTADDGenerator(CodeCompilation codeCompilation,
            TADDGeneratorOptions options,
            AbstractSchedulerGenerator scheduler, AbstractSleepGenerator sleep)
            throws PTAException {
        super(codeCompilation, options);
        this.scheduler = scheduler;
        this.sleep = sleep;
    }
    /**
     * Checks for a possible deferring of this operation.
     * 
     * @param op operation to test for deferring until the next one
     * @param expr expression, that represents <code>op</code>
     * @param context context
     */
    protected void checkDeferring(AbstractCodeOp op, AbstractPTAExpression
            expr, TADDGeneratorContext context) throws PTAException {
        CodeOpBinaryExpression be;
        if(op instanceof CodeOpBinaryExpression)
            be = (CodeOpBinaryExpression)op;
        else
            be = null;
        // firstly, test if there is not an intervening target
        // of some jump
        CodeFieldDereference f;
        if(context.opIndex < context.method.code.ops.size() - 1 &&
                !context.method.code.isJumpTarget(context.opIndex + 1) &&
                op.getResult() instanceof CodeFieldDereference &&
                (f = (CodeFieldDereference)op.getResult()).isLocal()) {
            CodeOpNone n;
            if(resultUsedAtMostInNext(context, context.opIndex)) {
                // check if the operation is
                // an expression whose result is a local
                // variable that is the condition of a subsequent
                // branch
                CodeOpBranch b;
                CodeOpUnaryExpression u;
                if(context.method.code.ops.get(context.opIndex + 1) instanceof
                            CodeOpBranch &&
                        (b = (CodeOpBranch)context.method.code.ops.get(context.opIndex + 1)).
                            condition.value instanceof CodeFieldDereference &&
                        ((CodeFieldDereference)b.condition.value).isLocal()) {
                    CodeVariable next = ((CodeFieldDereference)b.condition.value).
                            variable;
                    if(f.variable == next) {
                        // yes -- so do not change this expression into
                        // a transition, cancel its assignment and defer
                        // the expression to be the condition of the
                        // next branch
                        context.deferredCondition = expr;
                        context.deferredOp = op;
                    }
                } else if(
                        // check if the operation is an expression of the form
                        // <code>@DIST_UNI[()] * &lt;integer&gt;</code> whose result is a local
                        // variable that is a next cast's source
                        be != null &&
                        context.method.code.ops.get(context.opIndex + 1) instanceof
                            CodeOpUnaryExpression &&
                        (u = (CodeOpUnaryExpression)context.method.code.ops.get(context.opIndex + 1)).
                            objectType != null && u.objectType.isOfIntegerTypes() &&
                        ((CodeFieldDereference)u.sub.value).isLocal()) {
                    UniMult um = UniMult.analyze(be);
                    if(um != null) {
                        CodeVariable next = ((CodeFieldDereference)u.sub.value).
                                variable;
                        if(f.isLocal() && f.variable == next) {
                            // yes -- so do not change this expression into
                            // a transition, but instead defer
                            // the expression to the following cast
                            context.deferredCondition = expr;
                            context.deferredOp = op;
                        }
                    }
                }
            } else if(
                    // check if the operation is
                    // an expression whose result is a local
                    // variable that is the condition of a
                    // subsequent barrier
                    context.method.code.ops.get(context.opIndex + 1) instanceof
                    CodeOpNone &&
                    (n = (CodeOpNone)context.method.code.ops.get(context.opIndex + 1)).
                        getBarrierType() != null && n.getBarrierType().isConditionalBarrier() &&
                    ((CodeFieldDereference)n.args.get(1).value).isLocal()) {
                // the value is expected to be transported to the barrier branch, but not
                // further
                if(context.transport.isTransported((context.opIndex + 1)*2 + 1, f) &&
                        !context.transport.isTransported((context.opIndex + 2)*2 + 1, f)) {
                    CodeVariable next = ((CodeFieldDereference)n.args.get(1).value).
                            variable;
                    if(f.variable == next) {
                        // yes -- so do not change this expression into
                        // a transition, cancel its assignment and defer
                        // the expression to be the condition of the
                        // next branch
                        context.deferredCondition = expr;
                        context.deferredOp = op;
                    }
                }
            }
        }
    }
    @Override
    void generate(CodeOpAssignment a, TADDGeneratorContext context) throws PTAException {
        if(a.getResult().getResultType().isJava()) {
            // assignments of java variables is done using trace in
            // <code>determineRuntimeJavaTypeLocals</code>
            // directly before a respective operation, that would
            // use these variables
            /* do nothing */
        } else {
            checkConnectPrevious(context);
            if(processPrimaryRange(a.rvalue, a, context))
                // not known, if this operation will have its
                // transitions generated now, unconnect the
                // range--checking code
                context.prevTransition.connectTargetIndex = -1;
            AbstractPTAVariable tLvalue = getVariable(a.getResult(), context.rm).v();
            AbstractPTAValue tRvalue = getValue(a.rvalue.value, context.rm);
            AbstractPTAExpression expr = new PTAAssignment(tLvalue, tRvalue);
            setStop(expr, context);
            checkDeferring(a, expr, context);
            if(context.deferredCondition == null) {
                PTATransition tt = new PTATransition(null);
                addSourceInfo(tt, context, a);
                tt.update.add(expr);
                context.factory.addTransition(context.currIndex, tt);
                context.prevTransition = tt;
            }
        }
    }
    @Override
    void generate(CodeOpJump j, TADDGeneratorContext context) throws PTAException {
        int targetIndex = j.gotoLabelRef.codeIndex;
        boolean jumpTarget = context.method.code.isJumpTarget(
                context.opIndex);
        boolean addJump = false;
        if(connectToNext(context.prevTransition)) {
            if(targetIndex == context.opIndex)
                // a loop
                addJump = true;
            else {
                // the jump is not a loop, add it to
                // the description of the previous operation
                addSourceInfo(context.prevTransition, context, j);
            }
            // forward connection of the previous operation
            // to the target of this jump
            context.prevTransition.connectTargetIndex = targetIndex;
        } else
            addJump = true;
        if(jumpTarget)
            addJump = true;
        if(addJump) {
            PTATransition tt = new PTATransition(null);
            addSourceInfo(tt, context, j);
            tt.connectTargetIndex = targetIndex;
            context.factory.addTransition(context.opIndex, tt);
            context.prevTransition = null;
        }
    }
    @Override
    void generateExpression(AbstractCodeOp e, TADDGeneratorContext context) throws PTAException {
        CodeOpBinaryExpression be;
        if(e instanceof CodeOpBinaryExpression)
            be = (CodeOpBinaryExpression)e;
        else
            be = null;
        CodeOpUnaryExpression ue;
        if(e instanceof CodeOpUnaryExpression)
            ue = (CodeOpUnaryExpression)e;
        else
            ue = null;
        // if not null, this operation is the cast within
        // <code>&lt;integer cast&gt;(@DIST_UNI[()] * &lt;integer&gt;)</code> 
        UniMult uniMult = null;
        List<AbstractCodeOp> sourceOps = new LinkedList<>();
        // if this operation required execution while building
        // the TADD and is already executed, so no further action
        // for the operation is needed
        boolean operationExecuted = false;
        // if the operation is known to be always true
        boolean alwaysFalse = false;
        // if the operation is known to be always false
        boolean alwaysTrue = false;
        if(ue != null) {
            // a unary expression. check special cases
            // requiring conversion
            switch(ue.op) {
                case UNARY_CAST:
                {
                    if(ue.objectType.numericPrecision() > 0) {
                        /*
                        // if a cast of arithmetic type, replace it with modulus,
                        // but only if there is a bound that guarantees, that
                        // the argument is not negative and its type has
                        // non--negative minimum value
                        TADDRange inIr  = new TADDRange(ue.sub.
                                value.getResultType());
                        if(inIr.min != 0)
                            throw new PTAException(ue.getStreamPos() +
                                ": to translate a primitive cast, argument type " +
                                "must have a minimum equal to 0");
                        be = new CodeOpBinaryExpression(
                                CodeOpBinaryExpression.Op.BINARY_MODULUS,
                                ue.getStreamPos());
                        be.left = ue.sub;
                        TADDRange outIr = new TADDRange(ue.objectType);
                        Literal max;
                        if(outIr.max < (1L << 31) - 2)
                            // max fits into int, use it instead of long
                            max = new Literal((int)(outIr.max + 1));
                        else
                            max = new Literal(outIr.max + 1);
                        be.right = new RangedCodeValue(
                                new CodeConstant(ue.getStreamPos(), max));
                        be.result = ue.getResult();
                        be.label = ue.label;
                        be.canBeDead = ue.canBeDead;
                        ue = null;
                        e = be;
                         */
                        boolean phrase = false;
                        if(context.deferredCondition != null) {
                            uniMult = UniMult.analyze(
                                    (CodeOpBinaryExpression)context.deferredOp);
                            if(uniMult == null)
                                throw new RuntimeException("an unknown condition " +
                                        "deferred to an arithmetic cast");
                            context.deferredCondition = null;
                            sourceOps.add(context.deferredOp);
                            context.deferredOp = null;
                            phrase = true;
                        }
                        if(!phrase)
                            // the ranges might differ in the main thread and in TADD threads,
                            // so better make the primitive type casts in the TADD threads illegal
                            throw new PTAException(ue.getStreamPos(),
                                "arithmetic casts within PTA threads are legal only in phrases");
                    } else {
                        // cast of object reference to one of another type
                        // like assignment, this currently makes sense only
                        // for local object references
                        if(ue.getResult().isVolatile())
                            throw new PTAException(ue.getStreamPos(),
                                    "assignment to volatile reference, " +
                                    "possible ambiguity could not be checked");
                        try {
                            InterpretingContext c = new InterpretingContext(interpreter,
                                    null, false, context.rm, InterpretingContext.UseVariables.NO);
                            // just to check for class cast errors, actual
                            // value setting is done using
                            // <code>determineRuntimeJavaTypeLocals</code>
                            RuntimeValue l = CodeArithmetics.computeValue(c, ue, null);
                        } catch(InterpreterException ie) {
                            ie.completePos(ue.getStreamPos());
                            throw new PTAException(ie);
                        }
                        // an assignment requiring no further transitions
                        operationExecuted = true;
                    }
                    break;
                }
                case UNARY_INSTANCE_OF:
                case UNARY_INSTANCE_OF_NOT_NULL:
                {
                    try {
                        InterpretingContext c = new InterpretingContext(interpreter,
                                null, false, context.rm, InterpretingContext.UseVariables.ALL);
                        Literal l = CodeArithmetics.computeValue(c, ue, null);
                        if(l.getBoolean())
                            alwaysTrue = true;
                        else
                            alwaysFalse = true;
                    } catch(InterpreterException ie) {
                        throw new PTAException(ue.getStreamPos(),
                                ie.getMessage());
                    }
                    break;
                }
            }
        } else {
            // a binary expression, check special cases
            // requiring conversion
            switch(be.op) {
                case BINARY_EQUAL:
                case BINARY_INEQUAL:
                    // not using <code>CodeArithmetics</code>, because
                    // it is not known if it is really object comparison
                    try {
                        // check if references to objects or null constants
                        boolean objectComparison =
                                (be.left.value.getResultType().isJava() || be.left.value.getResultType().isNull()) &&
                                (be.right.value.getResultType().isJava() || be.right.value.getResultType().isNull());
                        if(objectComparison) {
                            RuntimeObject left = interpreter.getReferencedOrNull(
                                    interpreter.getValue(be.left, context.rm));
                            RuntimeObject right = interpreter.getReferencedOrNull(
                                    interpreter.getValue(be.right, context.rm));
                            boolean equals = left == right;
                            if(be.op == CodeOpBinaryExpression.Op.BINARY_INEQUAL)
                                equals = !equals;
                            if(equals)
                                alwaysTrue = true;
                            else
                                alwaysFalse = true;
                        }
                    } catch(InterpreterException ie) {
                        throw new PTAException(be.getStreamPos(),
                                ie.getMessage());
                    }
                    break;

            }
        }
        if(!operationExecuted) {
            // build a TADD expression, without the result
            AbstractPTAExpression tE;
            if(alwaysFalse)
                tE = new PTAUnaryExpression(new PTAConstant(
                        booleanToInteger(false), false),
                        CodeOpUnaryExpression.Op.NONE);
            else if(alwaysTrue)
                tE = new PTAUnaryExpression(new PTAConstant(
                        booleanToInteger(true), false),
                        CodeOpUnaryExpression.Op.NONE);
            else {
                if(be != null) {
                    if(be.left.range != null || be.right.range != null) {
                        checkConnectPrevious(context);
                        boolean pLeft = processPrimaryRange(be.left, e, context);
                        boolean pRight = processPrimaryRange(be.right, e, context);
                        if(pLeft || pRight)
                            // not known, if this operation will have its
                            // transitions generated now, unconnect the
                            // range--checking code
                            context.prevTransition.connectTargetIndex = -1;
                    }
                    AbstractPTAValue tLeft = getValue(be.left.value, context.rm);
                    AbstractPTAValue tRight = getValue(be.right.value, context.rm);
                    tE = new PTABinaryExpression(tLeft, tRight, be.op);
                } else {
                    if(ue.sub.range != null) {
                        checkConnectPrevious(context);
                        if(processPrimaryRange(ue.sub, e, context))
                            // not known, if this operation will have its
                            // transitions generated now, unconnect the
                            // range--checking code
                            context.prevTransition.connectTargetIndex = -1;
                    }
                    AbstractPTAValue tSub = getValue(ue.sub.value, context.rm);
                    tE = new PTAUnaryExpression(tSub, ue.op);
                }
            }
            if(be != null)
                tE.singleEvaluationId = be.singleEvaluationId;
            setStop(tE, context);
            checkDeferring(e, tE, context);
            if(context.deferredCondition == null) {
                // make a transition out of this expression
                checkConnectPrevious(context);
                sourceOps.add(e);
                if(uniMult != null) {
                    int endIndex = context.factory.newStateIndex();
                    BranchId branchId = new BranchId(
                                BranchId.Type.PROBABILISTIC);
                    double branchProb = 1.0/uniMult.multiplier;
                    for(int i = 0; i < uniMult.multiplier; ++i) {
                        PTATransition tt = new PTATransition(null);
                        addSourceInfo(tt, context, sourceOps);
                        tt.branchId = branchId;
                        tt.guard = new PTABinaryExpression(
                                new PTADensity(PTADensity.ParameterType.UNI, new double[0]),
                                new PTAConstant(branchProb, true),
                                CodeOpBinaryExpression.Op.BINARY_LESS);
                        setStop(tt.guard, context);
                        PTAAssignment a = new PTAAssignment(
                                getVariable(e.getResult(), context.rm).v(),
                                new PTAConstant(i, false));
                        setStop(a, context);
                        tt.update.add(a);
                        context.factory.addTransition(context.currIndex, tt);
                        tt.connectTargetIndex = endIndex;
                    }
                    PTATransition tEnd = new PTATransition(null);
                    addSourceInfo(tEnd, context, sourceOps);
                    context.factory.addTransition(endIndex, tEnd);
                    context.prevTransition = tEnd;
                } else {
                    PTATransition tt = new PTATransition(null);
                    addSourceInfo(tt, context, sourceOps);
//                    // register and add the result
//                    List<AbstractCodeDereference> rv = new LinkedList<>();
//                    rv.add(e.getResult());
//                    registerVariables(rv, context.rm);
                    tE.target = getVariable(e.getResult(), context.rm).v();
                    tt.update.add(tE);
                    context.factory.addTransition(context.currIndex, tt);
                    context.prevTransition = tt;
                }
            }
        }
    }
    @Override
    void generate(CodeOpBranch b, TADDGeneratorContext context) throws PTAException {
        // barrier jumps are handled when a barrier is generated
        if(!b.barrier) { 
            checkConnectPrevious(context);
            AbstractPTAExpression tCondition = context.deferredCondition;
            List<AbstractCodeOp> sourceOps = new LinkedList<>();
            if(tCondition == null) {
                // the branch condition is never constant in an optimized
                // code
                processPrimaryRange(b.condition, b, context);
// if(b.condition.value instanceof CodeConstant)
//     b = b;
                AbstractCodeDereference condition = (AbstractCodeDereference)
                        b.condition.value;
                // an assignment to null
                tCondition = new PTAAssignment(null, getValue(
                        condition, context.rm));
                setStop(tCondition, context);
            } else {
                context.deferredCondition = null;
                sourceOps.add(context.deferredOp);
                context.deferredOp = null;
            }
            sourceOps.add(b);
            // determine if the condition is constant
            boolean alwaysFalse = false;
            boolean alwaysTrue = false;
            PTAConstant constant = tCondition.getConstant();
            if(constant != null) {
                int i = (int)constant.value;
                if(constant.value > 1 || constant.value < 0)
                    i = -1;
                switch(i) {
                    case 0:
                        alwaysFalse = true;
                        break;

                    case 1:
                        alwaysTrue = true;
                        break;

                    default:
                        throw new RuntimeException("invalid boolean value");
                }
            }
            // at most one branch label is absent,
            // its according transition is marked
            // as one to be connected to the next
            // state
            context.prevTransition = null;
            BranchId.Type branchType;
            if(tCondition.probability() == null)
                branchType = BranchId.Type.CONDITIONAL;
            else
                // probabilistic guards are not transformed to
                // probabilities before
                // <code>TADDFactory</code> produces a new PTA
                branchType = BranchId.Type.PROBABILISTIC;
            BranchId branchId = new BranchId(
                        branchType);
            if(!alwaysFalse) {
                PTATransition tTrue = new PTATransition(null);
                addSourceInfo(tTrue, context, sourceOps);
                if(!alwaysTrue)
                    tTrue.guard = tCondition;
                if(b.labelRefTrue != null)
                    tTrue.connectTargetIndex = b.labelRefTrue.codeIndex;
                else
                    context.prevTransition = tTrue;
                tTrue.branchId = branchId;
                context.factory.addTransition(context.currIndex, tTrue);
            }
            if(!alwaysTrue) {
                PTATransition tFalse = new PTATransition(null);
                addSourceInfo(tFalse, context, sourceOps);
                if(!alwaysFalse) {
                    tFalse.guard = tCondition.newComplementary();
                    tFalse.guard.backendCopyStop(tCondition);
                    tFalse.guard.singleEvaluationId = tCondition.singleEvaluationId;
                }
                if(b.labelRefFalse != null)
                    tFalse.connectTargetIndex = b.labelRefFalse.codeIndex;
                else
                    context.prevTransition = tFalse;
                tFalse.branchId = branchId;
                context.factory.addTransition(context.currIndex, tFalse);
            }
        }
    }
    @Override
    void generate(CodeOpSynchronize s, TADDGeneratorContext context) throws PTAException {
        RuntimeObject object;
        // denotes that a synchronization with an object k
        // should be skipped, because the synchronization
        // already exists
        boolean redundant;
        if(s.begin) {
            object  = interpreter.getValueUnchecked(
                    new RangedCodeValue(s.lock), context.rm).
                getReferencedObject();
            if(redundant = context.locks.contains(object)) {
                context.locks.push(null);
            } else
                context.locks.push(object);
        } else {
            redundant = (object = context.locks.pop()) == null;
        }
        if(!redundant) {
            checkConnectPrevious(context);
            TADDLock lock = compilation.getLock(object);
            CodeOpNone nSynchronize = new CodeOpNone(
                    s.getStreamPos());
            nSynchronize.appendTags(s, false, false);
            PTATransition tLock;
            if(s.begin) {
                tLock = lock.addBegin(context.name);
                nSynchronize.annotations.add(TA_IN_STRING);
            } else {
                tLock = lock.addEnd(context.name);
                nSynchronize.annotations.add(TA_OUT_STRING);
            }
            reportComplementaryLabel(tLock, null);
            PTALabel label = tLock.label;
            PTATransition tSynchronize = new PTATransition(label.newOpposite());
            // make nSynchronize envelop the synchronized block
            // it is done only for clarity of comments
            if(s.begin) {
                addSourceInfo(tSynchronize, context, nSynchronize);
            }
            addSourceInfo(tSynchronize, context, s);
            if(!s.begin) {
                addSourceInfo(tSynchronize, context, nSynchronize);
            }
            reportComplementaryLabel(tSynchronize, context.name);
            context.factory.addTransition(context.currIndex, tSynchronize);
            context.prevTransition = tSynchronize;
        }
    }
    @Override
    void generate(CodeOpReturn r, TADDGeneratorContext context) throws PTAException {
        checkConnectPrevious(context);
        if(context.threads.size() == 1)
            context.factory.addTransition(context.currIndex, null);
        else
            for(RuntimeThread t : context.threads)
                if(t != context.thread) {
                    PTATransition tJoin = new PTATransition(
                            getJoinLabel(context.thread, t, false));
                    addSourceInfo(tJoin, context, r);
                    tJoin.connectTargetIndex = context.opIndex;
                    context.factory.addTransition(context.currIndex, tJoin);
                    reportComplementaryLabel(tJoin, context.name);
                }
        context.prevTransition = null;
    }
    @Override
    void generate(CodeOpAllocation a, TADDGeneratorContext context) throws PTAException {
        if(a.constructor == null ||
                (!a.constructor.method.owner.name.equals(ResolveReferences.RANDOM_CLASS_NAME) &&
                    !a.getInstantiatedType().getJava().toString().equals(Type.SYSTEM_PACKAGE + "." +
                                Type.UNCHECKED_EXCEPTION_CLASS_NAME)))
            throw new PTAException(a.getStreamPos(),
                    "allocation in a PTA not resolved");
    }
    @Override
    void generate(CodeOpThrow t, TADDGeneratorContext context) throws PTAException {
        checkConnectPrevious(context);
        if(t.staticCheckError != null) {
            recoverableErrors.addAllReports(
                    new CompilerException(null, null, t.staticCheckError));
        } else {
            PTATransition ttError = new PTATransition(null);
            if(context.opIndex > 0 && !context.method.code.isJumpTarget(context.opIndex)) {
                AbstractCodeOp prev = context.method.code.ops.get(context.opIndex - 1);
                if(prev instanceof CodeOpAllocation) {
                    CodeOpAllocation alloc = (CodeOpAllocation)prev;
                    if(alloc.constructor != null &&
                            alloc.getInstantiatedType().getJava().toString().equals(Type.SYSTEM_PACKAGE + "." +
                                Type.UNCHECKED_EXCEPTION_CLASS_NAME)) {
                        ttError.comments = new LinkedList<>();
                        ttError.comments.add("#EXCEPTION:" + alloc.args.get(0));
                    }
                }
            }
            PTAAssignment a = new PTAAssignment(
                    compilation.errorVariable, new PTAConstant(
                    compilation.newErrorCode(), false));
            setStop(a, context);
            ttError.update.add(a);
            addSourceInfo(ttError, context, t);
            if(_SELFLOOPS) {
                // self--loop SELFLOOP
                ttError.connectTargetIndex = context.currIndex;
            } else {
                // an error state
                int errorIndex = context.factory.newStateIndex();
                ttError.connectTargetIndex = errorIndex;
                // add a state, which lacks output transitions
                context.factory.customStates.put(errorIndex, new PTAState(errorIndex));
            }
            context.factory.addTransition(context.currIndex, ttError);
        }
        context.prevTransition = null;
    }
    @Override
    void generateWaitAnnotation(CodeOpNone n, TADDGeneratorContext context)
            throws PTAException {
        checkConnectPrevious(context);
        RuntimeObject object = interpreter.getValueUnchecked(
                n.getThisArg(), context.rm).
                getReferencedObject();
        TADDLock lock = compilation.getLock(object);
        PTATransition tLockWait = lock.addWait(context.name, context);
        reportComplementaryLabel(tLockWait, null);
        PTALabel label = tLockWait.label;
        PTATransition tWait = new PTATransition(
                label.newOpposite());
        CodeOpNone nWait = (CodeOpNone)n.clone();
        nWait.annotations.add(TA_OUT_STRING);
        addSourceInfo(tWait, context, nWait);
        reportComplementaryLabel(tWait, context.name);
        context.factory.addTransition(context.currIndex, tWait);
        int notifyIndex = context.factory.newStateIndex();
        tWait.connectTargetIndex = notifyIndex;
        int activeIndex = context.factory.newStateIndex();
        for(RuntimeThread t : context.threads)
            if(t != context.thread) {
                // notify transition
                PTATransition tNotify = new PTATransition(
                        TADDLock.getNotifyLabel(lock, t.name, context.name,
                        false));
                CodeOpNone nNotify = (CodeOpNone)n.clone();
                nNotify.annotations.add(TA_NOTIFY_THREAD_STRING);
                addSourceInfo(tNotify, context, nNotify);
                reportComplementaryLabel(tNotify, context.name);
                context.factory.addTransition(notifyIndex, tNotify);
                tNotify.connectTargetIndex = activeIndex;
                if(context.generator.options.broadcastLabels) {
                    // notify all receiver, to listen to possible
                    // notify all broadcasts
                    PTATransition tNotifyAll = new PTATransition(
                            TADDLock.getNotifyAllLabel(lock, t.name,
                            false));
                    CodeOpNone nNotifyAll = (CodeOpNone)n.clone();
                    nNotifyAll.annotations.add(TA_NOTIFY_THREAD_STRING);
                    addSourceInfo(tNotifyAll, context, nNotifyAll);
                    reportComplementaryLabel(tNotifyAll, context.name);
                    context.factory.addTransition(notifyIndex, tNotifyAll);
                    tNotifyAll.connectTargetIndex = activeIndex;
                }
            }
        if(context.threads.size() == 1)
            // a "gap"
            context.factory.addTransition(notifyIndex, null);
        PTATransition tLockActive = lock.addActive(context.name);
        reportComplementaryLabel(tLockActive, null);
        label = tLockActive.label;
        PTATransition tActive = new PTATransition(
                label.newOpposite());
        CodeOpNone nActive = (CodeOpNone)n.clone();
        nActive.annotations.add(TA_IN_STRING);
        addSourceInfo(tActive, context, nActive);
        reportComplementaryLabel(tActive, context.name);
        context.factory.addTransition(activeIndex, tActive);
        context.prevTransition = tActive;
    }
    /**
     * Recursively builds a tree of notifyAll() without broadcasting,
     * or builds a set of branches of length 1 for notify().
     *
     * @param rootIndex                 root of the branch to build
     * @param sinkIndex                 where this branch's leaves should
     *                                  go
     * @param left                      threads that still need to be notified
     *                                  in the rest of this branch
     * @param lock                      lock
     * @param all                       true for notifyAll -- causes recursive
     *                                  descent, false for notify -- only
     *                                  branches of length 1 are constructed
     * @param n                         translated operation
     * @param context                   generator's context
     */
    private void buildNotificationTree(int rootIndex, int sinkIndex,
            List<RuntimeThread> left, TADDLock lock, boolean all,
            CodeOpNone n, TADDGeneratorContext context) throws PTAException {
        AbstractPTAVariable vCount = lock.getWaitCountVariable(
                context).v();
        for(RuntimeThread t : left) {
            PTALabel tNotifyLabel = TADDLock.getNotifyLabel(
                    lock, context.name, t.name,
                    true);
            PTATransition tNotify = new PTATransition(tNotifyLabel);
            CodeOpNone nNotify = (CodeOpNone)n.clone();
            nNotify.annotations.add(AbstractTADDGenerator.
                    TA_NOTIFY_THREAD_STRING);
            addSourceInfo(tNotify, context, nNotify);
            boolean continueBranch = all && left.size() > 1;
            int afterNotifyIndex;
            if(continueBranch)
                afterNotifyIndex = context.factory.newStateIndex();
            else
                afterNotifyIndex = sinkIndex;
            PTATransition tUpdate;
            if(context.generator.options.labeledUpdates) {
                tNotify.connectTargetIndex = afterNotifyIndex;
                tUpdate = tNotify;
            } else {
                int updateIndex = context.factory.newStateIndex();
                tNotify.connectTargetIndex = updateIndex;
                // update of wait count variable is delayed, but if another thread,
                // just because of that, wrongly assumes there are waiting threads,
                // the other threads' notify labels won't allow their respective
                // transitions to be execute anyway
                tUpdate = new PTATransition(null);
                addSourceInfo(tUpdate, context, nNotify);
                tUpdate.connectTargetIndex = afterNotifyIndex;
                context.factory.addTransition(updateIndex, tUpdate);
            }
            tNotify.guard = new PTABinaryExpression(
                    vCount, new PTAConstant(0, false),
                    CodeOpBinaryExpression.Op.BINARY_INEQUAL);
            setStop(tNotify.guard, context);
            AbstractPTAExpression expr = new PTABinaryExpression(vCount,
                    new PTAConstant(1, false), CodeOpBinaryExpression.Op.
                    BINARY_MINUS);
            setStop(expr, context);
            tUpdate.update.add(expr);
            expr.target = vCount;
            context.generator.reportComplementaryLabel(tNotify, context.name);
            context.factory.addTransition(rootIndex, tNotify);
            if(continueBranch) {
                // do not notify from within the branch
                PTATransition tDoNotNotify = new PTATransition(null);
                CodeOpNone nDoNotNotify = (CodeOpNone)n.clone();
                nDoNotNotify.annotations.add(AbstractTADDGenerator.
                        TA_DO_NOTHING_STRING);
                addSourceInfo(tDoNotNotify, context, nDoNotNotify);
                tDoNotNotify.guard = new PTABinaryExpression(
                        vCount, new PTAConstant(0, false),
                        CodeOpBinaryExpression.Op.BINARY_EQUAL);
                setStop(tDoNotNotify.guard, context);
                context.factory.addTransition(afterNotifyIndex, tDoNotNotify);
                tDoNotNotify.connectTargetIndex = sinkIndex;
                // continue the branch
                List<RuntimeThread> newLeft = new LinkedList<>();
                for(RuntimeThread t2 : left)
                    if(t2 != t)
                        newLeft.add(t2);
                buildNotificationTree(afterNotifyIndex, sinkIndex, newLeft,
                        lock, all, n, context);
            }
        }
    }
    @Override
    public void generateNotifyAnnotation(CodeOpNone n, boolean all, TADDGeneratorContext context)
            throws PTAException {
        checkConnectPrevious(context);
        RuntimeObject object = context.generator.interpreter.getValueUnchecked(
                n.getThisArg(), context.rm).
                getReferencedObject();
        TADDLock lock = context.generator.compilation.getLock(object);
        AbstractPTAVariable vCount = lock.getWaitCountVariable(
                context).v();
        int endIndex = context.factory.newStateIndex();
        if(all && context.generator.options.broadcastLabels) {
            PTALabel tNotifyAllLabel = TADDLock.getNotifyAllLabel(
                lock, context.name,
                true);
            PTATransition tNotifyAll = new PTATransition(tNotifyAllLabel);
            CodeOpNone nNotifyAll = (CodeOpNone)n.clone();
            nNotifyAll.annotations.add(AbstractTADDGenerator.
                    TA_NOTIFY_THREAD_STRING);
            addSourceInfo(tNotifyAll, context, nNotifyAll);
            reportComplementaryLabel(tNotifyAll, context.name);
            PTATransition tUpdate;
            if(context.generator.options.labeledUpdates) {
                tNotifyAll.connectTargetIndex = endIndex;
                tUpdate = tNotifyAll;
            } else {
                int updateIndex = context.factory.newStateIndex();
                tNotifyAll.connectTargetIndex = updateIndex;
                // update of wait count variable is delayed, but if another thread,
                // just because of that, wrongly assumes there are waiting threads,
                // the other threads' notify labels won't allow their respective
                // transitions to be execute anyway
                tUpdate = new PTATransition(null);
                tUpdate.connectTargetIndex = endIndex;
                context.factory.addTransition(updateIndex, tUpdate);
            }
            PTAAssignment a = new PTAAssignment(vCount,
                    new PTAConstant(0, false));
            // can not determine, where <code>vCount</code> is used
            setStop(a, context);
            tUpdate.update.add(a);
            context.factory.addTransition(context.currIndex, tNotifyAll);
        } else {
            List<RuntimeThread> left = new LinkedList<>();
            for(RuntimeThread t : context.threads)
                if(t != context.thread)
                    left.add(t);
            buildNotificationTree(context.currIndex, endIndex, left, lock, all,
                    n, context);
        }
        if(!all || !context.generator.options.broadcastLabels ||
                context.generator.options.broadcastLabelBlocks ||
                context.threads.size() == 1) {
            // notifyAll without a non--blocking broadcast label requires
            // "do not notify"
            if(context.threads.size() > 1) {
                PTATransition tDoNotNotify = new PTATransition(null);
                CodeOpNone nDoNotNotify = (CodeOpNone)n.clone();
                nDoNotNotify.annotations.add(AbstractTADDGenerator.
                        TA_DO_NOTHING_STRING);
                addSourceInfo(tDoNotNotify, context, nDoNotNotify);
                tDoNotNotify.guard = new PTABinaryExpression(
                        vCount, new PTAConstant(0, false),
                        CodeOpBinaryExpression.Op.BINARY_EQUAL);
                // contains no locals
                setStop(tDoNotNotify.guard, context);
                context.factory.addTransition(context.currIndex, tDoNotNotify);
                tDoNotNotify.connectTargetIndex = endIndex;
            } else {
                // no threads to notify, insert an empty transition
                PTATransition tDoNotNotify = new PTATransition(null);
                addSourceInfo(tDoNotNotify, context, n);
                context.factory.addTransition(context.currIndex, tDoNotNotify);
                tDoNotNotify.connectTargetIndex = endIndex;
            }
        }
        PTATransition tEnd = new PTATransition(null);
        addSourceInfo(tEnd, context, n);
        context.factory.addTransition(endIndex, tEnd);
        context.prevTransition = tEnd;
    }
    @Override
    void generateBarrierAnnotation(CodeOpNone n, TADDGeneratorContext context)
            throws PTAException {
        checkConnectPrevious(context);
        AbstractPTAExpression tCondition = context.deferredCondition;
        CodeConstant producerValue = null;
        // if this barrier has always a false condition
        boolean voidBarrier = false;
        if(n.args.size() == 2) {
            if(tCondition == null) {
                AbstractCodeValue arg1 = n.args.get(1).value;
if(!(n.args.get(1).value instanceof CodeConstant))
    n = n;
                if(arg1 instanceof CodeConstant &&
                        ((CodeConstant)arg1).getResultType().isBoolean()) {
                    // the condition has been optimized to a constant
                    CodeConstant c = (CodeConstant)arg1;
                    /*
                    tCondition = new PTAAssignment(null,
                            new PTAConstant(c.value.getBoolean() ? 1 : 0, false));
                    tCondition.backendEnableStop();
                     */
                    boolean condition = c.value.getBoolean();
                    if(condition) {
                        CompilerAnnotation replacementType;
                        switch(n.getBarrierType()) {
                            case BARRIER_CONDITIONAL:
                                replacementType = CompilerAnnotation.BARRIER_VOID;
                                break;

                            case BARRIER_PRODUCE_CONDITIONAL:
                                replacementType = CompilerAnnotation.BARRIER_PRODUCE_VOID;
                                break;

                            case BARRIER_CONSUME_CONDITIONAL:
                                replacementType = CompilerAnnotation.BARRIER_CONSUME_VOID;
                                break;

                            case PAIR_BARRIER_CONDITIONAL:
                                replacementType = CompilerAnnotation.PAIR_BARRIER_VOID;
                                break;

                            case PAIR_BARRIER_PRODUCE_CONDITIONAL:
                                replacementType = CompilerAnnotation.PAIR_BARRIER_PRODUCE_VOID;
                                break;

                            case PAIR_BARRIER_CONSUME_CONDITIONAL:
                                replacementType = CompilerAnnotation.PAIR_BARRIER_CONSUME_VOID;
                                break;

                            default:
                                throw new RuntimeException("unexpected barrier type");
                        }
                        n.setBarrierType(replacementType);
                    } else {
                        context.prevTransition = null;
                        voidBarrier = true;
                    }
                } else {
                    // the producer or consumer passes an integer
                    if(arg1 instanceof CodeConstant)
                        // the value passed has been optimized to a constant
                        producerValue = (CodeConstant)arg1;
                    else {
                        recoverableErrors.addReport(new PTAException.Report(
                            n.getStreamPos(), PTAException.Code.ILLEGAL,
                            "could not optimize producer value to a constant"));
                        context.prevTransition = null;
                        voidBarrier = true;
                    }
                }
            }
        }
        if(producerValue != null &&
                (n.getBarrierType() != CompilerAnnotation.PAIR_BARRIER_PRODUCE_VOID &&
                    n.getBarrierType() != CompilerAnnotation.PAIR_BARRIER_CONSUME_VOID_PRODUCER)) {
            throw new RuntimeException(
                    "producer value can be specified only in a pair barrier, " +
                    "and without an accompanying condition");
        }
        RuntimeObject object;
        try {
            object = interpreter.getValueUnchecked(
                    n.getThisArg(), context.rm).
                    getReferencedObject();
        } catch(InterpreterFatalException e) {
            recoverableErrors.addReport(new PTAException.Report(
                n.getStreamPos(), PTAException.Code.ILLEGAL,
                e.getMessage()));
            context.prevTransition = null;
            voidBarrier = true;
            object = null;
        }
        BarrierParties parties;
        int userIndex;
        if(object != null) {
            // register the party even if this barrier is void,
            // so that the number of users matches the
            // declared one
            parties = interpreter.barrierThreadsNum.get(object);
            if(parties == null) {
                recoverableErrors.addReport(new PTAException.Report(
                    n.getStreamPos(), PTAException.Code.ILLEGAL,
                    "barrier used but not activated"));
                voidBarrier= true;
                // unused
                userIndex = -1;
            } else
                userIndex = parties.addUser(context.rm);
        } else {
            parties = null;
            userIndex = -1;
        }
        if(!voidBarrier) {
            int producerIndex = -1;
            int consumerIndex = -1;
            PTAException.Report mismatchError = null;
            switch(n.getBarrierType()) {
                case BARRIER_PRODUCE_VOID:
                case BARRIER_PRODUCE_CONDITIONAL:
                case PAIR_BARRIER_PRODUCE_VOID:
                case PAIR_BARRIER_PRODUCE_CONDITIONAL:
                    if(!parties.directional)
                        mismatchError = new PTAException.Report(
                                n.getStreamPos(), PTAException.Code.ILLEGAL,
                                "a non-directional barrier has a producer");
                    else
                        producerIndex = parties.addProducer(context.rm);
                    break;

                case PAIR_BARRIER_CONSUME_VOID_PRODUCER:
                    producerIndex = producerValue.value.getInteger();
                    /* fallthrough */
                case BARRIER_CONSUME_VOID:
                case BARRIER_CONSUME_CONDITIONAL:
                case PAIR_BARRIER_CONSUME_VOID:
                case PAIR_BARRIER_CONSUME_CONDITIONAL:
                    if(!parties.directional)
                        mismatchError = new PTAException.Report(
                                n.getStreamPos(), PTAException.Code.ILLEGAL,
                                "a non-directional barrier has a consumer");
                    else
                        consumerIndex = parties.addConsumer(context.rm);
                    break;

                case BARRIER_VOID:
                case BARRIER_CONDITIONAL:
                case PAIR_BARRIER_VOID:
                case PAIR_BARRIER_CONDITIONAL:
                    if(parties.directional)
                        mismatchError = new PTAException.Report(
                                n.getStreamPos(), PTAException.Code.ILLEGAL,
                                "a directional barrier has a symmetric party");
                    break;

                default:
                    throw new RuntimeException("unknown barrier type");
            }
            if(mismatchError != null) {
                recoverableErrors.addReport(mismatchError);
                context.prevTransition = null;
                voidBarrier = true;
            }
            if(!voidBarrier) {
                boolean atomic = object.getValue(interpreter, context.rm,
                        object.codeClass.lookupField(ATOMIC_SYNC_FIELD_NAME)).getBoolean();
                PTATransition tBarrierPass = null;
                CodeOpNone nBarrier = (CodeOpNone)n.clone();
                nBarrier.annotations.add(TA_BARRIER_STRING);
                List<AbstractCodeOp> sourceOps = new LinkedList<>();
                sourceOps.add(nBarrier);
                int loopIndex = -1;
                int passIndex = -1;
                if(tCondition == null) {
                    // a barrier without a condition expected
                    if(n.getBarrierType() != CompilerAnnotation.BARRIER_VOID &&
                            n.getBarrierType() != CompilerAnnotation.BARRIER_PRODUCE_VOID &&
                            n.getBarrierType() != CompilerAnnotation.BARRIER_CONSUME_VOID &&
                            n.getBarrierType() != CompilerAnnotation.PAIR_BARRIER_VOID &&
                            n.getBarrierType() != CompilerAnnotation.PAIR_BARRIER_PRODUCE_VOID &&
                            n.getBarrierType() != CompilerAnnotation.PAIR_BARRIER_CONSUME_VOID &&
                            n.getBarrierType() != CompilerAnnotation.PAIR_BARRIER_CONSUME_VOID_PRODUCER)
                        throw new RuntimeException("barrier condition not found");
                } else {
                    // a barrier with a condition expected
                    if(n.getBarrierType() != CompilerAnnotation.BARRIER_CONDITIONAL &&
                            n.getBarrierType() != CompilerAnnotation.BARRIER_PRODUCE_CONDITIONAL &&
                            n.getBarrierType() != CompilerAnnotation.BARRIER_CONSUME_CONDITIONAL &&
                            n.getBarrierType() != CompilerAnnotation.PAIR_BARRIER_CONDITIONAL &&
                            n.getBarrierType() != CompilerAnnotation.PAIR_BARRIER_PRODUCE_CONDITIONAL &&
                            n.getBarrierType() != CompilerAnnotation.PAIR_BARRIER_CONSUME_CONDITIONAL)
                        throw new RuntimeException("unexpected barrier condition");
                    context.deferredCondition = null;
                    sourceOps.add(context.deferredOp);
                    context.deferredOp = null;
                    PTATransition tBarrierLoop = null;
                    CodeOpBranch branch = (CodeOpBranch)context.method.code.ops.get(
                            context.currIndex + 1);
                    sourceOps.add(branch);
                    loopIndex = branch.labelRefFalse.codeIndex;
                    if(branch.labelRefTrue != null)
                        passIndex = branch.labelRefTrue.codeIndex;
                }
                String labelPrefix = object.getUniqueName();
                AbstractPTAVariable result;
                if(n.getResult() != null)
                    result = getVariable(n.getResult(), context.rm).v();
                else
                    result = null;
                BarrierGenerator bg = new BarrierGenerator(this, context,
                        n.getBarrierType(), atomic, parties, labelPrefix,
                        userIndex, producerIndex, consumerIndex,
                        context.currIndex, loopIndex, passIndex,
                        tCondition, producerValue, result, sourceOps);
                context.prevTransition = bg.getFinalTransition();
            }
        }
    }
    @Override
    void generateSleepAnnotation(CodeOpNone n, TADDGeneratorContext context)
            throws PTAException {
        sleep.generateSleepAnnotation(n, context);
    }
    @Override
    void generateJoinAnnotation(CodeOpNone n, TADDGeneratorContext context)
            throws PTAException {
        RuntimeObject object = interpreter.getValueUnchecked(
                n.getThisArg(), context.rm).
                getReferencedObject();
        boolean found = false;
        for(RuntimeThread t : context.threads)
            if(t.object == object) {
                if(!found)
                    checkConnectPrevious(context);
                found = true;
                PTALabel label = getJoinLabel(t, context.thread, true);
                PTATransition tJoin = new PTATransition(label);
                addSourceInfo(tJoin, context, n);
                reportComplementaryLabel(tJoin, context.name);
                context.factory.addTransition(context.currIndex, tJoin);
                context.prevTransition = tJoin;
                break;
            }
        if(!found)
            throw new PTAException(n.getStreamPos(),
                    "object " + object.getUniqueName() + " does not have a thread");
    }
    /**
     * Returns, if the next operation after <code>n</code> is a sleep
     * operation.
     * 
     * @param context context, should point to <code>n</code> as the
     * current operation
     * @param n current operation
     * @return if the next operation is annotated with SLEEP
     */
    private boolean nextIsSleep(TADDGeneratorContext context, CodeOpNone n) {
        CodeOpNone n2;
        // check if the operation is
        // an expression whose result is a local
        // variable that is next sleep argument
        // and there is not an intervening target
        // of some jump, and the result is used
        // only in the next sleep argument
        if(context.opIndex < context.method.code.ops.size() - 1 &&
                context.method.code.ops.get(context.opIndex + 1) instanceof CodeOpNone &&
                (n2 = (CodeOpNone)context.method.code.ops.get(context.opIndex + 1)).
                annotations.contains(CompilerAnnotation.SLEEP_STRING) &&
                // @SLEEP is an annotation of a static method,
                // so the first argument is not "this"
                n2.args.get(1).value instanceof CodeFieldDereference &&
                ((CodeFieldDereference)n2.args.get(1).value).isLocal()) {
            CodeVariable next = ((CodeFieldDereference)n2.args.get(1).value).
                    variable;
            if(n.getResult() instanceof CodeFieldDereference) {
                CodeFieldDereference f = (CodeFieldDereference)n.getResult();
                if(f.isLocal() && f.variable == next &&
                        !context.method.code.isJumpTarget(context.opIndex + 1) &&
                        resultUsedAtMostInNext(context, context.opIndex))
                    return true;
            }
        }
        return false;
    }
    @Override
    void generateRandomAnnotation(CodeOpNone n, TADDGeneratorContext context)
            throws PTAException {
        // the method @RANDOM is non--static for Java, static or non--static for Verics
        boolean staticCall = n.getRandomBoundArgIndex() == 0;
        RangedCodeValue rv = n.args.get(n.getRandomBoundArgIndex());
        if(rv.range != null) {
            checkConnectPrevious(context);
            if(processPrimaryRange(rv, n, context))
                // not known, if this operation will have its
                // transitions generated now, unconnect the
                // range--checking code
                context.prevTransition.connectTargetIndex = -1;
        }
        String[] labels = null;
        try {
            RuntimeValue r;
            RuntimeObject ro;
            CodeVariable cv;
            try {
                if(staticCall ||
                        // this is an exception, any other object containing non--static
                        // @RANDOM must be initialised already
                        n.args.get(0).value.getResultType().getJava().toString().equals(
                                ResolveReferences.RANDOM_CLASS_NAME)) {
                    r = null;
                    ro = null;
                    cv = null;
                } else {
                    r = interpreter.getValue(n.args.get(0), context.rm);
                    ro = (RuntimeObject)r.value;
                    cv = ro.codeClass.fields.get("CHOICES");
                }
            } catch(InterpreterFatalException e) {
                throw new PTAException(n.getStreamPos(),
                        "contains @RANDOM and choice names but not initialized " +
                                "in the main thread");
            }
            if(cv != null) {
                // not a plan Random object, but has a field with choice names
                RuntimeValue names = ro.values.get(cv);
                // check if the names are used, or rather a constructor without name specification
                // was invoked
                if(!names.isNull()) {
                    if(!(names.getReferencedObject() instanceof ArrayStore))
                        throw new RuntimeException("contains @RANDOM and choice names but the " +
                                "latter is not an array");
                    ArrayStore array = (ArrayStore)names.getReferencedObject();
                    labels = new String[array.getLength()];
                    for(int i = 0; i < array.getLength(); ++i) {
                        RuntimeValue name = array.getElement(interpreter, context.rm, null, i);
                        labels[i] = (String)name.value;
                    }
                }
            }
        } catch(InterpreterException e) {
            throw new PTAException(n.getStreamPos(), e.getMessage());
        }
        AbstractPTAExpression tE = new PTAAssignment(null,
                getValue(rv.value, context.rm));
        setStop(tE, context);
        if(nextIsSleep(context, n)) {
            if(labels != null)
                throw new RuntimeException("names sleep not implemented");
            // yes -- so cancel the assignment of the random
            // value and defer the value to be the value of
            // next sleep
            context.deferredCondition = tE;
            context.deferredOp = n;
        }
        if(context.deferredCondition == null) {
            // make a transition out of this expression
            checkConnectPrevious(context);
            double num;
            AbstractPTAValue v = ((PTAAssignment)tE).sub;
            if(v instanceof PTAConstant) {
                num = ((PTAConstant)v).value;
                if(num <= 0)
                    throw new PTAException(n.getStreamPos(),
                            "argument of @RANDOM must " +
                            "be positive");
            } else
                throw new PTAException(n.getStreamPos(),
                        "range of random values " +
                        "returned by @RANDOM must have constant limits");
            int endIndex = context.factory.newStateIndex();
            AbstractCodeDereference result = n.getResult();
            if(result == null) {
                // nothing uses the random value
                // add just an empty bridge
                PTATransition tt = new PTATransition(null);
                addSourceInfo(tt, context, n);
                context.factory.addTransition(context.currIndex, tt);
                tt.connectTargetIndex = endIndex;
            } else {
                AbstractPTAVariable tLvalue = getVariable(result, context.rm).v();
                BranchId branchId = new BranchId(
                            BranchId.Type.NONDETERMINISTIC);
                branchId.textRange = n.textRange;
                branchId.arguments = NdUtils.getRandomNdArguments(n,
                    this, context.rm);
                for(int i = 0; i < Math.round(num); ++i) {
                    PTATransition tt = new PTATransition(null);
                    tt.branchId = branchId;
                    tt.ndInt = new LinkedList<>(Arrays.asList(i));
                    addSourceInfo(tt, context, n);
                    PTAAssignment a = new PTAAssignment(
                        tLvalue, new PTAConstant(i, false));
                    setStop(a, context);
                    tt.update.add(a);
                    context.factory.addTransition(context.currIndex, tt);
                    tt.connectTargetIndex = endIndex;
                    if(labels != null) {
                        if(i < labels.length)
                            tt.branchId.label.put(i,
                                    new PTALabel(labels[i], PTALabel.Type.FREE, false));
                        else
                            throw new PTAException(n.getStreamPos(),
                                    "out of names for non-deterministic choices: " +
                                    labels.length + " names but " +
                                    Math.round(num) + " choices to name");
                    }
                }
            }
            PTATransition tEnd = new PTATransition(null);
            addSourceInfo(tEnd, context, n);
            context.factory.addTransition(endIndex, tEnd);
            context.prevTransition = tEnd;
        }
    }
    @Override
    void generateIsStatisticalAnnotation(CodeOpNone n,
            TADDGeneratorContext context) throws PTAException {
        AbstractPTAExpression a = new PTAAssignment(
                getVariable(n.getResult(), context.rm).v(), new PTAConstant(
                    booleanToInteger(options.isStatisticalHint), false));
        setStop(a, context);
        checkDeferring(n, a, context);
        if(context.deferredCondition == null) {
            checkConnectPrevious(context);
            PTATransition tIsStatistical = new PTATransition(null);
            tIsStatistical.update.add(a);
            addSourceInfo(tIsStatistical, context, n);
            context.factory.addTransition(context.currIndex, tIsStatistical);
            context.prevTransition = tIsStatistical;
        }
    }
    @Override
    void generateEmptyTransitionAnnotation(CodeOpNone n,
            TADDGeneratorContext context) throws PTAException {
        checkConnectPrevious(context);
        PTATransition tEmpty = new PTATransition(null);
        CodeOpNone nEmpty = (CodeOpNone)n.clone();
        nEmpty.annotations.add(PTATransition.
                ANNOTATION_SPECIAL_EMPTY_TRANSITION_STRING);
        addSourceInfo(tEmpty, context, nEmpty);
        context.factory.addTransition(context.currIndex, tEmpty);
        context.prevTransition = tEmpty;
    }
    /**
     * Generates a tabbed distribution.
     * 
     * @param n current operation
     * @param lvalue assigned value
     * @param x parameter x
     * @param y parameter y
     * @param context context, should point to <code>n</code> as the
     * current operation
     */
    private void generateTabbedDist(CodeOpNone n, AbstractPTAVariable lvalue,
            PTAArrayVariable x, PTAArrayVariable y, TADDGeneratorContext context)
            throws PTAException {
        int num = x.length;
        int endIndex = context.factory.newStateIndex();
        BranchId branchId = new BranchId(BranchId.Type.PROBABILISTIC);
        branchId.textRange = n.textRange;
        for(int i = 0; i < num; ++i) {
            PTATransition tt = new PTATransition(null);
            tt.branchId = branchId;
            tt.probability = new PTAUnaryExpression(
                        new PTAElementVariable(x, new PTAConstant(i, false),
                            new PTARange(x.range), true),
                        AbstractCodeOp.Op.NONE);
            addSourceInfo(tt, context, n);
            PTAAssignment a = new PTAAssignment(
                lvalue, new PTAElementVariable(y, new PTAConstant(i, false),
                    new PTARange(y.range), true));
            setStop(a, context);
            tt.update.add(a);
            context.factory.addTransition(context.currIndex, tt);
            tt.connectTargetIndex = endIndex;
        }
        PTATransition tEnd = new PTATransition(null);
        addSourceInfo(tEnd, context, n);
        context.factory.addTransition(endIndex, tEnd);
        context.prevTransition = tEnd;
    }
    @Override
    void generateDistAssignment(CompilerAnnotation annotation,
            CodeOpNone n, TADDGeneratorContext context) throws PTAException {
        PTADensity.ParameterType type = PTADensity.getType(annotation);
        if(type == null)
            throw new RuntimeException("unknown distribution");
        int numArgs = PTADensity.getNumArgs(type);
        AbstractPTAValue[] params = new AbstractPTAValue[numArgs];
        for(int i = 0; i < numArgs; ++i) {
            RangedCodeValue rv = n.args.get(i);
            if(processPrimaryRange(rv, n, context))
                // not known, if this operation will have its
                // transitions generated now, unconnect the
                // range--checking code
                context.prevTransition.connectTargetIndex = -1;
            Type t = rv.value.getResultType();
            if(t.isArray(null))
                t = t.getElementType(null, 0);
            if(t.numericPrecision() == 0)
                throw new PTAException(rv.value.getStreamPos(),
                        "missing arithmetic value");
            params[i] = getValue(rv.value, context.rm);
        }
        AbstractPTAValue rvalue = new PTADensity(type,
                params);
        // check if this operation is of the form v = dist(w), and the
        // next one is sleep(v)
        if(nextIsSleep(context, n)) {
            // yes -- so cancel the assignment of the random
            // value and defer the value to be the value of
            // next sleep
            context.deferredCondition = new PTAUnaryExpression(rvalue,
                    AbstractCodeOp.Op.NONE);
            context.deferredOp = n;
            setStop(context.deferredCondition, context);
        }
        if(context.deferredCondition == null) {
            // a density function would explicitly occur in a PTA
            // if(!options.allowDistributions)
            //     throw new PTAException(n.getStreamPos(),
            //             "options forbid a density function");
            checkConnectPrevious(context);
            if(n.getResult() == null) {
                PTATransition tt = new PTATransition(null);
                addSourceInfo(tt, context, n);
                context.factory.addTransition(context.currIndex, tt);
                context.prevTransition = tt;
            } else {
                AbstractPTAVariable lvalue = getVariable(n.getResult(), context.rm).v();
                if(type == PTADensity.ParameterType.ARRAY)
                    generateTabbedDist(n, lvalue,
                            (PTAArrayVariable)params[0], (PTAArrayVariable)params[1],
                            context);
                else {
                    PTATransition t = new PTATransition(null);
                    PTAAssignment a = new PTAAssignment(lvalue, rvalue);
                    setStop(a, context);
                    t.update.add(a);
                    addSourceInfo(t, context, n);
                    context.factory.addTransition(context.currIndex, t);
                    context.prevTransition = t;
                }
            }
        }
    }
    @Override
    void generateMathUnary(CompilerAnnotation annotation,
            CodeOpNone n, TADDGeneratorContext context) throws PTAException {
        checkConnectPrevious(context);
        RangedCodeValue rv = n.args.get(0);
        processPrimaryRange(rv, n, context);
        PTATransition tUpdate = new PTATransition(null);
        AbstractPTAVariable tLvalue = getVariable(n.getResult(), context.rm).v();
        AbstractPTAValue tRvalue = getValue(rv.value, context.rm);
        AbstractCodeOp.Op op;
        switch(annotation) {
            case MATH_ABS:
                op = AbstractCodeOp.Op.UNARY_ABS;
                break;
                
            default:
                throw new RuntimeException("unknown annotation " +
                        annotation.toString());
        }
        PTAUnaryExpression expr = new PTAUnaryExpression(tRvalue, op);
        expr.target = tLvalue;
        setStop(expr, context);
        tUpdate.update.add(expr);
        addSourceInfo(tUpdate, context, n);
        context.factory.addTransition(context.currIndex, tUpdate);
        context.prevTransition = tUpdate;
    }
    @Override
    void addStateFormula(CodeOpNone n, boolean or, TADDGeneratorContext context)
            throws PTAException {
        try {
            // must directly use the interpreter, because
            // <code>PTAConstant</code> can not hold
            // strings
            context.factory.addNextToFormula(
                    interpreter.getValue(n.args.get(0), context.rm).toJavaString(), or, n);
        } catch(InterpreterException e) {
            throw new PTAException(n.getStreamPos(), e.getMessage());
        }
    }
    @Override
    void generateUnknownTransition(CodeOpNone n, String s, TADDGeneratorContext context)
                throws PTAException {
        if(!s.equals(CompilerAnnotation.MARKER_STRING))
            throw new PTAException(n.getStreamPos(),
                    "unknown annotation " + s);
    }
}
