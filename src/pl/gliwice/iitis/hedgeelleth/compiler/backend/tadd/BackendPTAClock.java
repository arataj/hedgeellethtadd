/*
 * BackendPTAClock.java
 *
 * Created on Jul 20, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import pl.gliwice.iitis.hedgeelleth.interpreter.container.AbstractRuntimeContainer;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAClock;

/**
 * A clock PTA variable, plus the functionality needed by the compiler.
 * 
 * @author Artur Rataj
 */
public class BackendPTAClock extends AbstractBackendPTAVariable {
    /**
     * Creates a wrapper for a clock PTA variable.
     * 
     * @param container container of this clock
     * @param v variable to wrap
     */
    public BackendPTAClock(AbstractRuntimeContainer container,
            PTAClock v) {
        super(null, container, v);
    }
    @Override
    public PTAClock v() {
        return (PTAClock)v;
    }
    @Override
    public void enlargeRangeToFitInitValues(boolean shouldAlreadyIncludeInit) {
        throw new RuntimeException("no initial value, method not applicable to clocks");
    }
}
