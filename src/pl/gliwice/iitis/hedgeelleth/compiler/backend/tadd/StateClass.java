/*
 * StateClass.java
 *
 * Created on Nov 28, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import java.util.*;
import java.io.*;
import java.lang.ref.WeakReference;
import java.util.Map.Entry;
import pl.gliwice.iitis.hedgeelleth.Hedgeelleth;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.util.PWD;
import pl.gliwice.iitis.hedgeelleth.interpreter.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.AbstractRuntimeContainer;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.ArrayStore;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeObject;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.pta.PTAException;
import pl.gliwice.iitis.hedgeelleth.pta.values.*;

/**
 * Generates a Java class, that can return values of state variables of
 * a model. To be used on the JVM side.
 * 
 * @author Artur Rataj
 */
public class StateClass {
    /**
     * Indent.
     */
    private static final String INDENT = "    ";
    /**
     * Indent before a regular code.
     */
    private static final String CODE_INDENT = INDENT + INDENT + INDENT;
    /**
     * Extension of a java source file.
     */
    private static final String JAVA_EXTENSION = "java";
    /**
     * Catch block.
     */
    private static final String CATCH_STRING = INDENT + INDENT +
                "} catch(ReflectiveOperationException e) {\n" +
                CODE_INDENT + "throw new RuntimeException(" +
                "\"unexpected: \" + e.toString());\n" +
                INDENT + INDENT + "}\n";
    /**
     * Method <code>getField()</code>.
     */
    private static final String GET_FIELD_STRING =
        "    private static Field getField(Class clazz, String fieldName)\n" +
        "            throws NoSuchFieldException {\n" +
        "        try {\n" +
        "            return clazz.getDeclaredField(fieldName);\n" +
        "        } catch (NoSuchFieldException e) {\n" +
        "            Class superClass = clazz.getSuperclass();\n" +
        "            if (superClass == null) {\n" +
        "                throw e;\n" +
        "            } else {\n" +
        "                return getField(superClass, fieldName);\n" +
        "            }\n" +
        "        }\n" +
        "    }\n";

    /**
     * Element of a path to a state variable.
     */
    private static class PathElement {
        /**
         * Dereferenced object.
         */
        public RuntimeObject object;
        /**
         * If <code>object</code> is a class, then a field within
         * that class. Otherwise null.
         */
        public CodeVariable field;
        /**
         * If <code>object</code> is an array, then an index within
         * that array. Otherwise -1.
         */
        int index;
        
        /**
         * Creates a reference to a class' field.
         * 
         * @param clazz class
         * @param field field
         */
        public PathElement(RuntimeObject clazz, CodeVariable field) {
            object = clazz;
            this.field = field;
            index = -1;
        }
        /**
         * Creates a reference to an array's element.
         * 
         * @param array array
         * @param index index
         */
        public PathElement(ArrayStore array, int index) {
            object = array;
            field = null;
            this.index = index;
        }
        /**
         * If this is a reference to a field.
         * 
         * @return true for a field, false for an array's element
         */
        public boolean isField() {
            return field != null;
        }
        @Override
        public String toString() {
            if(isField())
                return object.getUniqueName() + "::" + field.name;
            else
                return "[" + index + "]";
        }
    }
    /**
     * Finds paths to a given element, each path begins with a class, which serves
     * as a static starting point for looking up the variable.
     * 
     * @param c code compilation
     * @param i interpreter
     * @param pe element
     * @param excluded excluded objects, to disable recursion
     * @return a set of parallel paths to <code>pe</code>, including <code>pe</code>
     * itself at its tail
     */
    private static Set<List<PathElement>> findPath(CodeCompilation c, AbstractInterpreter i,
            PathElement pe, Set<RuntimeObject> excluded) {
        Set<List<PathElement>> paths = new HashSet<>();
        if(pe.object.context != Context.NON_STATIC) {
            // start
            List<PathElement> path = new LinkedList<>();
            paths.add(path);
        } else {
            // scan all fields for storage of <code>pe.object</code>
            for(WeakReference<RuntimeObject> r : i.getObjects()) {
                 RuntimeObject ro = r.get();
                 if(ro != null && !excluded.contains(ro)) {
                    Set<RuntimeObject> newExcluded = new HashSet<>(excluded);
                    newExcluded.add(pe.object);
/*if(ro.getVariables() == null)
    ro = ro;*/
                     for(CodeVariable source : ro.getVariables()) {
                         RuntimeValue sourceValue = ro.getValue(i, null, source);
                         if(sourceValue.type.isJava() &&
                                 sourceValue.getReferencedObject() == pe.object) {
                             paths.addAll(findPath(c, i, new PathElement(ro, source),
                                     newExcluded));
                         } else if(sourceValue.type.isArray(null)) {
                             ArrayStore as = (ArrayStore)sourceValue.getReferencedObject();
                             for(int index = 0; index < as.getLength(); ++index) {
                                 try {
                                    RuntimeValue elementValue = as.getElement(i, null, null, index);
                                    if(elementValue.type.isJava() &&
                                            elementValue.getReferencedObject() == pe.object) {
                                        paths.addAll(findPath(c, i, new PathElement(as, index),
                                                newExcluded));
                                    }
                                 } catch(InterpreterException e) {
                                     throw new RuntimeException("unexpected: " + i.toString());
                                 }
                             }
                         }
                     }
                 } else {
                     pe = pe;
                 }
            }
        }
        for(List<PathElement> path : paths)
            // next element
            path.add(pe);
        // if <code>path</code> is empty here, then <code>pe.object</code> is stored
        // nowhere within the fields
        return paths;
    }
    /**
     * Writes a method, that returns a state variable of a given index.
     * 
     * @param out print writer
     * @param maxNumericPrecision maximum numeric precision of the
     * state variables to put into this method; influences on this method's
     * name
     * @param names names of getters of variables having
     * subsequent indices
     * @param types return types of getters of variables having
     * subsequent indices
     */
    private static void writeIndexed(PrintWriter out, int maxNumericPrecision,
            List<String> names, List<Type> types) {
        String returnType;
        String suffix;
        if(maxNumericPrecision == new Type(Type.PrimitiveOrVoid.DOUBLE).
                numericPrecision()) {
            // includes all variables
            returnType = "double";
            suffix = "";
        } else if(maxNumericPrecision == new Type(Type.PrimitiveOrVoid.INT).
                numericPrecision()) {
            // includes only variables of type int or shorter
            returnType = "int";
            suffix = "Int";
        } else if(maxNumericPrecision == new Type(Type.PrimitiveOrVoid.LONG).
                numericPrecision()) {
            // includes all integer variables
            returnType = "long";
            suffix = "Long";
        } else
            throw new RuntimeException("precision not supported");
        out.println(INDENT + "public " + returnType + " get" + suffix + "(int index) {");
        if(!names.isEmpty()) {
            out.println(INDENT + INDENT + "switch(index) {");
            for(int index = 0; index < names.size(); ++index) {
                out.println(CODE_INDENT + "case " + index + ":");
                out.print(CODE_INDENT + INDENT);
                Type type = types.get(index);
                if(type.numericPrecision() <= maxNumericPrecision)
                    out.println("return " + names.get(index) + "();");
                else {
                    out.println("throw new RuntimeException(\"state variable \" +\n" +
                            CODE_INDENT + INDENT + INDENT + "\"at index " + index +
                            " has type " + type.toJavaString(returnType) + "\");");
                }
            }
            out.println(CODE_INDENT + "default:");
            out.println(CODE_INDENT + INDENT + "throw new RuntimeException(\"invalid index: \" +");
            out.println(CODE_INDENT + INDENT + INDENT + "index);");
            out.println(INDENT + INDENT + "}");
        } else {
            out.println(INDENT + INDENT + "throw new RuntimeException(\"no state variables\");");
        }
        out.println(INDENT + "}");
    }
    /**
     * Writes a method, that returns a state variable name for each index.
     * 
     * @param out print writer
     * @param names list of subsequent state variables
     */
    private static void writeGetName(PrintWriter out, List<String> names) {
        out.println(INDENT + "public String getName(int index) {");
        if(names.isEmpty())
            out.println(INDENT + INDENT + "throw new RuntimeException(\"no state variables\");");
        else {
            out.println(INDENT + INDENT + "switch(index) {");
            int count = 0;
            for(String s : names) {
                out.println(CODE_INDENT + "case " + count + ":");
                out.println(CODE_INDENT + INDENT + "return \"" + s + "\";");
                ++count;
            }
            out.println(CODE_INDENT + "default:");
            out.println(CODE_INDENT + INDENT + "throw new RuntimeException(\"invalid index: \" +");
            out.println(CODE_INDENT + INDENT + INDENT + "index);");
            out.println(INDENT + INDENT + "}");
        }
        out.println(INDENT + "}");
    }
    /**
     * Writes a state class to a file.
     * 
     * @param topDirectory top directory of the output files
     * @param name fully qualified name of the class to create
     * @param paths paths to subsequent state variables
     */
    protected static void write(PWD topDirectory, String name,
            SortedMap<AbstractBackendPTAVariable, List<List<PathElement>>> sortedPaths)
            throws PTAException {
        final String OBJECT = "o";
        final String FIELD = "f";
        int dotIndex = name.lastIndexOf('.');
        String packageName = name.substring(0, dotIndex);
        String className = name.substring(dotIndex + 1);
        PrintWriter out = null;
        try {
            out = new PrintWriter(topDirectory.getFile(className + "." + JAVA_EXTENSION));
            if(!packageName.equals(Hedgeelleth.DEFAULT_PACKAGE))
                out.println("package " + packageName + ";\n");
            out.println("import java.lang.reflect.Field;\n");
            out.println("public class " + className + " {");
            out.print(GET_FIELD_STRING);
            StringBuilder fields = new StringBuilder();
            StringBuilder constructor = new StringBuilder();
            List<Map<String, String>> getters = new LinkedList<>();
            List<String> getterNames = new LinkedList<>();
            List<Type> getterTypes = new LinkedList<>();
            constructor.append(INDENT + "public " + className + "() {\n");
            if(!sortedPaths.isEmpty())
                constructor.append(INDENT + INDENT + "try {\n");
            int variableCounter = 0;
            for(AbstractBackendPTAVariable v : sortedPaths.keySet())
                for(List<PathElement> path : sortedPaths.get(v)) {
                    // the path begins with a class
                    String prevObject = "null";
                    String prevField = null;
                    // to give names to arrays
                    String lastJavaFieldName = null;
                    int elementCounter = 0;
                    for(PathElement pe : path) {
                        // a field must be declared, if the next part of the path
                        // is no more dereferenced within the constructor
                        boolean declareObject = elementCounter == path.size() - 2;
                        boolean declareField = elementCounter == path.size() - 1;
                        Type objectType = pe.object.type;
                        String objectTypeString = objectType.toJavaString(null);
                        String currObject = null;
                        if(pe.isField()) {
                            Type fieldType = pe.field.type;
                            String fieldTypeString = fieldType.toJavaString(null);
                            String currField = FIELD + variableCounter;
                            if(declareField)
                                fields.append(INDENT  + "Field " + currField + ";\n");
                            constructor.append(CODE_INDENT +
                                    (declareField ? "" : "Field ") + currField + " = getField(" +
                                    objectTypeString + ".class, \"" + pe.field.name + "\");\n");
                            constructor.append(CODE_INDENT +
                                    currField + ".setAccessible(true);\n");
                            if(fieldType.isJava()) {
                                lastJavaFieldName = pe.field.name;
                                // references are stable; put this part of the path into the
                                // constructor
                                currObject = OBJECT + variableCounter;
                                if(declareObject)
                                    fields.append(INDENT + fieldTypeString + " " + currObject + ";\n");
                                constructor.append(CODE_INDENT +
                                        (declareObject ? "" : fieldTypeString + " ") + currObject +
                                        " = (" + fieldTypeString + ")" + currField +
                                        ".get(" + prevObject + ");\n");
                                prevField = currField;
                            } else {
                                String methodName = "get";
                                if(fieldType.isOfIntegerTypes())
                                    methodName += "Int";
                                else
                                    methodName += "Double";
                                Map<String, String> getter = new HashMap<>();
                                String n = v.v().name;
                                if(prevField == null)
                                    // a static field, no naming conventions
                                    // had been applied
                                    n = n.substring(n.lastIndexOf('.') + 1);
                                if(getterNames.contains(n))
                                    throw new PTAException(null, "name clash: " + n);
                                getterNames.add(n);
                                getterTypes.add(fieldType);
                                String s = currField + "." + methodName + "(" + prevObject + ")";
                                getter.put(fieldTypeString + " " + n, s);
                                getters.add(getter);
                            }
                            ++variableCounter;
                        } else {
                            currObject = OBJECT + variableCounter;
                            ++variableCounter;
                            Type elementType = objectType.getElementType(null, 0);
                            String elementTypeString = elementType.toJavaString(null);
                            if(elementType.isJava()) {
                                // references are stable; put this part of the path into the
                                // constructor
                                if(declareObject)
                                    fields.append(INDENT + elementTypeString + " " + currObject + ";\n");
                                constructor.append(CODE_INDENT +
                                        (declareObject ? "" : elementTypeString + " ") + currObject + " = " +
                                        prevObject + "[" + pe.index + "];\n");
                            } else {
                                Map<String, String> getter = new HashMap<>();
                                String n= v.v().name  + pe.index;
                                if(getterNames.contains(n))
                                    throw new PTAException(null, "name clash: " + n);
                                getterNames.add(n);
                                getterTypes.add(elementType);
                                getter.put(elementTypeString + " " + n,
                                        prevObject + "[" + pe.index + "]");
                                getters.add(getter);
                            }
                        }
                        prevObject = currObject;
                        ++elementCounter;
                    }
                }
            if(!sortedPaths.isEmpty())
                constructor.append(CATCH_STRING);
            constructor.append(INDENT + "}\n");
            out.print(fields.toString());
            out.print(constructor.toString());
            for(Map<String, String> getter : getters) {
                String key = getter.keySet().iterator().next();
                out.println(INDENT + key + "() {");
                String expr = getter.get(key);
                boolean usesReflection = expr.indexOf(".get") != -1;
                if(usesReflection)
                    out.println(INDENT + INDENT + "try {");
                out.println(INDENT + INDENT + (usesReflection ? INDENT : "") +
                        "return " + expr + ";");
                if(usesReflection)
                    out.print(CATCH_STRING);
                out.println(INDENT + "}");
            }
            writeIndexed(out, new Type(Type.PrimitiveOrVoid.DOUBLE).numericPrecision(),
                    getterNames, getterTypes);
            writeIndexed(out, new Type(Type.PrimitiveOrVoid.INT).numericPrecision(),
                    getterNames, getterTypes);
            writeIndexed(out, new Type(Type.PrimitiveOrVoid.LONG).numericPrecision(),
                    getterNames, getterTypes);
            out.println(INDENT + "public int size() {");
            out.println(INDENT + INDENT + "return " + getterNames.size() + ";");
            out.println(INDENT + "}");
            writeGetName(out, getterNames);
            out.println("}");
            if(out.checkError())
                throw new IOException("i/o error");
        } catch(IOException e) {
            throw new PTAException(null, "could not write to " + name + ": " +
                    e.getMessage());
        } finally {
            if(out != null)
                out.close();
        }
    }
    /**
     * Generates a Java class, that returns values of all state variables
     * that are fields.
     * 
     * @param topDirectory top directory of the output files
     * @param c code compilation
     * @param p PTA compilation
     * @param i interpreter, holding the t's values
     * @param experimentSuffix suffix to add to the class name in experiment
     * mode; empty string if not in experiment mode
     * @return name of the file generated
     */
    public static String generate(PWD topDirectory, CodeCompilation c, BackendPTACompilation p,
            AbstractInterpreter i, String experimentSuffix) throws PTAException {
        SortedMap<AbstractBackendPTAVariable, List<List<PathElement>>> sortedPaths =
                new TreeMap<>();
        for(Map.Entry<AbstractRuntimeContainer, Map<CodeVariable,
                AbstractBackendPTAVariable>> rc : p.variables.entrySet()) {
            for(Map.Entry<CodeVariable, AbstractBackendPTAVariable> cv : rc.getValue().entrySet()) {
                AbstractPTAVariable v = cv.getValue().v();
                if(p.usedVariables.contains(v) &&
                        cv.getValue().container instanceof RuntimeObject) {
                    RuntimeObject o = (RuntimeObject)cv.getValue().container;
                    if(o.context == Context.NON_STATIC && !(o instanceof ArrayStore) &&
                            !cv.getValue().namingConventionApplied)
                        throw new PTAException(cv.getValue().getStreamPos(),
                                "field " + v.name + " is both not static and has no naming convention, " +
                                "and thus state class can not be generated");
                    ArrayStore as;
                    Set<List<PathElement>> paths;
                    if(cv.getKey() == null) {
                        if(o instanceof ArrayStore) {
                            as = (ArrayStore)o;
                            // the index is meaningless for path searching
                            paths = findPath(c, i, new PathElement(as, -1),
                                    new HashSet<RuntimeObject>());
                        } else
                            throw new RuntimeException("only array stores are not represented " +
                                    "by variables");
                    } else {
                        if(o instanceof ArrayStore)
                            throw new RuntimeException("unexpected: array store represented by " +
                                    "a variable");
                        as = null;
if(o.context == Context.NON_STATIC && !o.type.isJava())
    o = o;
                        paths = findPath(c, i, new PathElement(o, cv.getKey()),
                                new HashSet<RuntimeObject>());
                    }
                    int minSize = Integer.MAX_VALUE;
                    for(List<PathElement> path : paths)
                        if(minSize > path.size())
                            minSize = path.size();
                    for(List<PathElement> path : new LinkedList<>(paths))
                        if(path.size() != minSize)
                            paths.remove(path);
                    // sort for repeatable compiler output
                    SortedMap<String, List<PathElement>> sorted = new TreeMap<>();
                    for(List<PathElement> path : paths) {
                        StringBuilder name = new StringBuilder();
                        for(PathElement pe : path) {
                            name.append(pe.toString());
                            name.append(" ");
                        }
                        sorted.put(name.toString(), path);
                    }
                    if(sorted.isEmpty())
                        throw new PTAException(cv.getValue().getStreamPos(),
                                "could not find a static hook-prefixed reference chain for " +
                                v.toString());
                    List<PathElement> path = sorted.values().iterator().next();
                    List<List<PathElement>> pathList = new LinkedList<>();
                    if(as != null) {
                        int length = as.getLength();
                        for(int index = 0; index < length; ++index) {
                            // only elements, that are actually used
                            if(p.usedVariables.contains(new PTAElementVariable(
                                    (PTAArrayVariable)v, new PTAConstant(index, false),
                                    v.range, v.floating))) {
                                if(!cv.getValue().namingConventionApplied)
                                    throw new PTAException(cv.getValue().getStreamPos(),
                                            "array " + v.name + " has no naming convention, " +
                                            "yet its elements are state variables, " +
                                            "and thus state class can not be generated");
                                List<PathElement> elementPath = new LinkedList<>(path);
                                int lastIndex = elementPath.size() - 1;
                                PathElement lastElement = elementPath.get(lastIndex);
                                elementPath.set(lastIndex, new PathElement(
                                        (ArrayStore)lastElement.object, index));
                                pathList.add(elementPath);
                            }
                        }
                    } else
                        pathList.add(path);
                    sortedPaths.put(cv.getValue(), pathList);
                }
            }
        }
//        for(AbstractPTAVariable v : sortedPaths.keySet()) {
//            System.out.println("variable = " + v.toString());
//            List<List<PathElement>> pathList = sortedPaths.get(v);
//            for(List<PathElement> r : pathList) {
//                System.out.print("\t");
//                for(PathElement pe : r)
//                    System.out.print(pe.toString() + " ");
//                System.out.println();
//            }
//            System.out.println();
//        }
        String suffix = "State" + experimentSuffix;
        write(topDirectory, p.mainClassName + suffix, sortedPaths);
        int pos = p.mainClassName.lastIndexOf(".");
        return p.mainClassName.substring(pos + 1) + suffix + "." + JAVA_EXTENSION;
    }
}
