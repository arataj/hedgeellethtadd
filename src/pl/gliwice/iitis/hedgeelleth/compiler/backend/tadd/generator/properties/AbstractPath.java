/*
 * AbstractPath.java
 *
 * Created on Feb 19, 2010, 11:27:20 AM
 *
 * Copyright (c) 2010 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.properties;

import java.util.*;

/**
 * An abstract  path, for a PCTL property.
 *
 * @author Artur Rataj
 */
public abstract class AbstractPath extends AbstractConditionalExpression {
    /**
     * Creates a new instance of Path. 
     */
    public AbstractPath() {
    }
}
