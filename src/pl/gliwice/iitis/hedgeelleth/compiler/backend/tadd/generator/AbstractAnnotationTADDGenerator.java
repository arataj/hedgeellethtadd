/*
 * AbstractAnnotationTADDGenerator.java
 *
 * Created on Jul 20, 2009, 2:17:36 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CompilerAnnotation;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeCompilation;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.CodeOpNone;
import pl.gliwice.iitis.hedgeelleth.pta.PTAException;
import pl.gliwice.iitis.hedgeelleth.pta.PTATransition;

/**
 * This differs from <code>AbstractTADDGenerator<code> in dispatching
 * <code>CodeOpNone<code> according to its annotations. The dispatching
 * takes into account several standard annotations, and calls respective
 * methods, abstract in this implementations.
 *
 * @author Artur Rataj
 */
public abstract class AbstractAnnotationTADDGenerator extends AbstractTADDGenerator {
    /**
     * Creates a new instance of AbstractAnnotationTADDGenerator. 
     *
     * @param compilation               compilation
     * @param options                   standard types of options of this TADD
     *                                  generator
     */
    public AbstractAnnotationTADDGenerator(CodeCompilation codeCompilation,
            TADDGeneratorOptions options) throws PTAException {
        super(codeCompilation, options);
    }
    @Override
    void generate(CodeOpNone n, TADDGeneratorContext context) throws PTAException {
        // the annotations can be modified within, however, any of the removed annotations
        // has already been iterated over, and any of the new annotations should not be
        // iterated over in this loop
        for(String s : new TreeSet<>(n.annotations)) {
            CompilerAnnotation a = CompilerAnnotation.parse(s);
            switch(a) {
                case WAIT:
                    generateWaitAnnotation(n, context);
                    break;

                case NOTIFY:
                case NOTIFY_ALL:
                    boolean all = CompilerAnnotation.parse(s) ==
                            CompilerAnnotation.NOTIFY_ALL;
                    generateNotifyAnnotation(n, all, context);
                    break;

                case BARRIER_VOID:
                case BARRIER_PRODUCE_VOID:
                case BARRIER_CONSUME_VOID:
                case BARRIER_CONDITIONAL:
                case BARRIER_PRODUCE_CONDITIONAL:
                case BARRIER_CONSUME_CONDITIONAL:
                case PAIR_BARRIER_VOID:
                case PAIR_BARRIER_PRODUCE_VOID:
                case PAIR_BARRIER_CONSUME_VOID:
                case PAIR_BARRIER_CONSUME_VOID_PRODUCER:
                case PAIR_BARRIER_CONDITIONAL:
                case PAIR_BARRIER_PRODUCE_CONDITIONAL:
                case PAIR_BARRIER_CONSUME_CONDITIONAL:
                    generateBarrierAnnotation(n, context);
                    break;

                case JOIN:
                    generateJoinAnnotation(n, context);
                    break;

                case RANDOM:
                    generateRandomAnnotation(n, context);
                    break;

                case SLEEP:
                    generateSleepAnnotation(n, context);
                    break;

                case HEAD:
                case TAIL:
                case STATE:
                    generateEmptyTransitionAnnotation(n, context);
                    break;

                case DIST_UNI:
                case DIST_NXP:
                case DIST_ARRAY:
                {
                    // throw new RuntimeException("ops with DIST_* annotations " +
                    //         "should have already be changed to constants");
                    generateDistAssignment(a, n, context);
                    break;
                }
                
                case MATH_ABS:
                case MATH_MAX:
                case MATH_MIN:
                    generateMathUnary(a, n, context);
                    break;

//                case ERROR:
//                    generateErrorTransition(n, context);
//                    break;
                    
                case MODEL_STATE_OR:
                    addStateFormula(n, true, context);
                    break;

                case MODEL_STATE_AND:
                    addStateFormula(n, false, context);
                    break;
                    
                case MODEL_IS_STATISTICAL:
                    generateIsStatisticalAnnotation(n, context);
                    break;
                    
                case UNKNOWN_ANNOTATION:
                default:
                    if(!s.equals(PTATransition.
                            ANNOTATION_SPECIAL_EMPTY_TRANSITION_STRING)) {
                        // call a method, which may generate transitions
                        // for such unrecognized annotations, but typically
                        // does nothing anyway
                        generateUnknownTransition(n, s, context);
                    } else
                        generateEmptyTransitionAnnotation(n, context);
                    break;
            }
        }
    }
    /**
     * Generates transition for <code>CodeOpNone</code> with a
     * <code>WAIT</code> annotation.<br>
     *
     * // A possible previous operation is not connected.
     *
     * @param n                         operation for which the transitions
     *                                  should be generated
     * @param context                   context
     */
    abstract void generateWaitAnnotation(CodeOpNone n, TADDGeneratorContext context)
            throws PTAException;
    /**
     * Generates transition for <code>CodeOpNone</code> with either
     * <code>NOTIFY</code> or <code>NOTIFY_ALL</code> annotation.<br>
     *
     * // A possible previous operation is not connected.
     *
     * @param n                         operation for which the transitions
     *                                  should be generated
     * @param all                       false for <code>NOTIFY</code>, true for
     *                                  <code>NOTIFY_ALL</code>
     * @param context                   context
     */
    abstract void generateNotifyAnnotation(CodeOpNone n, boolean all, TADDGeneratorContext context)
            throws PTAException;
    /**
     * Generates transition for <code>CodeOpNone</code> with a
     * <code>BARRIER_*</code> annotation.<br>
     *
     * @param n                         operation for which the transitions
     *                                  should be generated
     * @param context                   context
     */
    abstract void generateBarrierAnnotation(CodeOpNone n, TADDGeneratorContext context)
            throws PTAException;
    /**
     * Generates transition for <code>CodeOpNone</code> with a
     * <code>SLEEP</code> annotation.<br>
     *
     * // A possible previous operation is not connected.
     *
     * @param n                         operation for which the transitions
     *                                  should be generated
     * @param context                   context
     */
    abstract void generateSleepAnnotation(CodeOpNone n, TADDGeneratorContext context)
            throws PTAException;
    /**
     * Generates transition for <code>CodeOpNone</code> with a
     * <code>JOIN</code> annotation.<br>
     *
     * // A possible previous operation is not connected.
     *
     * @param n                         operation for which the transitions
     *                                  should be generated
     * @param context                   context
     */
    abstract void generateJoinAnnotation(CodeOpNone n, TADDGeneratorContext context)
            throws PTAException;
    /**
     * Generates transition for <code>CodeOpNone</code> with a
     * <code>RANDOM</code> annotation.<br>
     *
     * // A possible previous operation is not connected.
     *
     * @param n                         operation for which the transitions
     *                                  should be generated
     * @param context                   context
     */
    abstract void generateRandomAnnotation(CodeOpNone n, TADDGeneratorContext context)
            throws PTAException;
    /**
     * Generates transition for <code>CodeOpNone</code> with a
     * <code>IS_STATISTICAL</code> annotation.<br>
     *
     * @param n                         operation for which the transitions
     *                                  should be generated
     * @param context                   context
     */
    abstract void generateIsStatisticalAnnotation(CodeOpNone n, TADDGeneratorContext context)
            throws PTAException;
    /**
     * Generates transition for <code>CodeOpNone</code> with a
     * an annotation that specifies an empty transition. The annotations
     * in this class' implementation of
     * <code>generate(CodeOpNone, Context)</code> are <code>HEAD</code>,
     * <code>TAIL</code>, <code>STATE</code> or
     * <code>PTATransition.ANNOTATION_SPECIAL_EMPTY_TRANSITION_STRING</code>.<br>
     *
     * // A possible previous operation is not connected.
     *
     * @param n                         operation for which the transitions
     *                                  should be generated
     * @param context                   context
     */
   abstract void generateEmptyTransitionAnnotation(CodeOpNone n,
           TADDGeneratorContext context) throws PTAException;
    /*
     * Generates or defers an assignment of a distribution to a variable, out of
     * a <code>DIST_*</code> annotation, that could not be
     * optimized out into a constant.
     * 
     * @param annotation annotation
     * @param n source operation
     * @param context context
     */
    abstract void generateDistAssignment(CompilerAnnotation annotation,
            CodeOpNone n, TADDGeneratorContext context) throws PTAException;
    /*
     * Generates an assignment containing one of several unary math functions.
     * 
     * @param annotation annotation
     * @param n source operation
     * @param context context
     */
    abstract void generateMathUnary(CompilerAnnotation annotation,
            CodeOpNone n, TADDGeneratorContext context) throws PTAException;
//    /**
//     * Generates a self--loped transition for <code>CodeOpNone</code> with an
//     * <code>ERROR</code>annotation.<br>
//     *
//     * // A possible previous operation is not connected.
//     *
//     * @param n                         operation for which the transition
//     *                                  should be generated
//     * @param context                   context
//     */
//    abstract void generateErrorTransition(CodeOpNone n, TADDGeneratorContext context)
//            throws PTAException;
    /**
     * Generates transition for <code>CodeOpNone</code> with some unknown
     * annotation. Typical implementation is to ignore such annotation or
     * to throw <code>PTAException</code>.<br>
     *
     * // A possible previous operation is not connected.
     *
     * @param n                         operation for which the transitions
     *                                  should be generated
     * @param s                         string representing the annotation
     * @param context                   context
     */
    abstract void generateUnknownTransition(CodeOpNone n, String s,
            TADDGeneratorContext context) throws PTAException;
    /**
     * Adds a state formula.
     * 
     * @param n operation, that requested the state formula to be added
     * @param or true for OR formula, false for AND formula
     * @param context context
     */
    abstract void addStateFormula(CodeOpNone n, boolean or,
            TADDGeneratorContext context) throws PTAException;
}
