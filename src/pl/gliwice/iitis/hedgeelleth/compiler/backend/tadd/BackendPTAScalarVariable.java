/*
 * BackendPTAScalarVariable.java
 *
 * Created on Jul 20, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import pl.gliwice.iitis.hedgeelleth.pta.PTAException;
import pl.gliwice.iitis.hedgeelleth.compiler.sa.RangeCodeStaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable.Category;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.AbstractRuntimeContainer;
import pl.gliwice.iitis.hedgeelleth.pta.*;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAScalarVariable;

/**
 * A scalar PTA variable, plus the functionality needed by the compiler.
 * 
 * @author Artur Rataj
 */
public class BackendPTAScalarVariable extends AbstractBackendPTAVariable {
    /**
     * Category, if known.
     */
    public Category category;
    
    /**
     * Creates a wrapper for a scalar PTA variable.
     * 
     * @param pos position in the source stream, null for none
     * @param container container of this variable
     * @param v variable to wrap
     */
    public BackendPTAScalarVariable(StreamPos pos,
            AbstractRuntimeContainer container, PTAScalarVariable v) {
        super(pos, container, v);
    }
    @Override
    public PTAScalarVariable v() {
        return (PTAScalarVariable)v;
    }
    @Override
    public void proposeRange(RangeCodeStaticAnalysis.Proposal proposal)
            throws PTAException {
        if(proposal != null && proposal.rangeKnown() && proposal.isEmpty()) {
            // no viable assignments, set the range to the initial
            // value
            v().range.range.setMinF(v().initValue);
            v().range.range.setMaxF(v().initValue);
        } else
            super.proposeRange(proposal);
    }
    @Override
    public void enlargeRangeToFitInitValues(boolean shouldAlreadyIncludeInit) {
        if(PTASystem.getMin(v().range.range) > v().initValue) {
            if(shouldAlreadyIncludeInit)
                throw new RuntimeException("does not include an input value");
            v().range.range.setMinF(v().initValue);
        }
        if(PTASystem.getMax(v().range.range) < v().initValue) {
            if(shouldAlreadyIncludeInit)
                throw new RuntimeException("does not include an input value");
            v().range.range.setMaxF(v().initValue);
        }
    }
}
