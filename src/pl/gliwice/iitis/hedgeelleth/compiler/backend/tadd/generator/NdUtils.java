/*
 * NdUtils.java
 *
 * Created on Jun 18, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.AbstractBackendPTAVariable;
import pl.gliwice.iitis.hedgeelleth.compiler.code.AbstractCodeValue;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeConstant;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeFieldDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.CodeOpNone;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;
import pl.gliwice.iitis.hedgeelleth.pta.PTAException;
import pl.gliwice.iitis.hedgeelleth.pta.values.*;

/**
 * Some utility methods related to CopdeOpNone representing a @RANDOM method.
 * 
 * @author Artur Rataj
 */
public class NdUtils {
    /**
     * Returns a non--deterministic function's list of arguments/parameters.
     * 
     * @param n operation representing a call to @RANDOM
     * @param generator PTA generator
     * @param rm runtime method
     * @return non--deterministic function's list of arguments/parameters.
     */
    public static List<AbstractPTAVariable> getRandomNdArguments(
            CodeOpNone n, AbstractTADDGenerator generator, RuntimeMethod rm) throws PTAException {
        List<AbstractPTAVariable> arguments = new LinkedList<>();
        // start with the first argument after a bound
        for(int argIndex = n.getRandomBoundArgIndex() + 1; argIndex < n.args.size(); ++argIndex) {
            AbstractCodeValue arg = n.args.get(argIndex).value;
            AbstractCodeValue preArg = n.preArgs.get(argIndex);
            if(!(arg instanceof CodeFieldDereference) &&
                    preArg instanceof CodeFieldDereference) {
                // might be, the dereference has been optimised out
                if(!(arg instanceof CodeConstant))
                    throw new RuntimeException("variable replaced by a non--constant");
                CodeFieldDereference vv = (CodeFieldDereference)preArg;
                if(vv.variable.context == Context.NON_STATIC)
                    throw new PTAException(arg.getStreamPos(),
                            "parameter of a nd function can not be non--static");
                // a virtual variable which represents an optimised one
                arguments.add(new PTAVirtualVariable(vv.toNameString(),
                        vv.getResultType().isOfFloatingPointTypes(), (CodeConstant)arg));
            } else if(arg instanceof CodeFieldDereference) {
                AbstractBackendPTAVariable v = generator.getVariable((CodeFieldDereference)arg, rm);
                if(v.v() instanceof PTAScalarVariable) {
                    PTAScalarVariable s = (PTAScalarVariable)v.v();
                    arguments.add(s);
                } else
                    throw new PTAException(arg.getStreamPos(),
                            "expected a scalar variable or a constant");
            } else
                throw new PTAException(arg.getStreamPos(),
                        "expected a variable or a constant");
        }
        return arguments;
    }
}
