/*
 * AbstractBackendPTAValue.java
 *
 * Created on Jul 20, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.SourcePosition;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAValue;

/**
 * An abstract PTA variable, plus the functionality needed by the compiler.
 * 
 * @author Artur Rataj
 */
public class AbstractBackendPTAValue implements SourcePosition {
    /**
     * Position in the source stream, null for none.
     */
    public StreamPos pos;
    /**
     * Wrapped value.
     */
    protected AbstractPTAValue v;

    /**
     * Creates a wrapper for a PTA value.
     * 
     * @param pos position in the source stream, null for none
     * @param v value to wrap
     */
    AbstractBackendPTAValue(StreamPos pos,
            AbstractPTAValue v) {
        setStreamPos(pos);
        this.v = v;
    }
    /**
     * Returns the wrapped value.
     * 
     * @return a PTA value
     */
    public AbstractPTAValue v() {
        return v;
    }
    @Override
    public final void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public final StreamPos getStreamPos() {
        return pos;
    }
}
