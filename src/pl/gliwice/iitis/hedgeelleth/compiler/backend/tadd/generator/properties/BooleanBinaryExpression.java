/*
 * BooleanBinaryExpression.java
 *
 * Created on Feb 19, 2010, 1:26:02 PM
 *
 * Copyright (c) 2010 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.properties;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.BinaryExpression;

/**
 * A boolean binary expression.
 *
 * @author Artur Rataj
 */
public class BooleanBinaryExpression extends AbstractConditionalExpression {
    /**
     * The left side.
     */
    public AbstractConditionalExpression left;
    /**
     * Operator.
     */
    public BinaryExpression.Op b;
    /**
     * The right side.
     */
    public AbstractConditionalExpression right;

    /**
     * Creates a new instance of BooleanBinaryExpression.
     *
     * @param left                      left side subexpression
     * @param b                         boolean operator
     * @param right                     right side subexpression
     */
    public BooleanBinaryExpression(AbstractConditionalExpression left,
            BinaryExpression.Op b, AbstractConditionalExpression right) {
        super();
        this.left = left;
        if(!b.isBoolean())
            throw new RuntimeException("operator must be boolean");
        this.b = b;
        this.right = right;
    }
}
