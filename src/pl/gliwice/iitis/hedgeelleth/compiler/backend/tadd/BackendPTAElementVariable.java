/*
 * BackendPTAElementVariable.java
 *
 * Created on Jul 20, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.AbstractRuntimeContainer;
import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAVariable;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAElementVariable;

/**
 * An array element PTA variable, plus the functionality needed by the compiler.
 * 
 * @author Artur Rataj
 */
public class BackendPTAElementVariable extends AbstractBackendPTAVariable {
    /**
     * Creates a wrapper for an element PTA variable.
     * 
     * @param pos position in the source stream, null for none
     * @param container container of this variable
     * @param v variable to wrap
     */
    public BackendPTAElementVariable(StreamPos pos, AbstractRuntimeContainer container,
            PTAElementVariable v) {
        super(pos, container, v);
    }
    @Override
    public PTAElementVariable v() {
        return (PTAElementVariable)v;
    }
    @Override
    public void enlargeRangeToFitInitValues(boolean shouldAlreadyIncludeInit) {
        throw new RuntimeException("method not applicable to array elements, " +
                "but to arrays");
    }
}
