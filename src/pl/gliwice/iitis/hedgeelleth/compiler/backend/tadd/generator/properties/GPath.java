/*
 * GPath.java
 *
 * Created on Feb 19, 2010, 11:54 AM
 *
 * Copyright (c) 2010 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.properties;

import java.util.*;

/**
 * Path property called "globally", for representing invariance.
 *
 * @author Artur Rataj
 */
public class GPath extends AbstractPath {
    /**
     * An expression for this path.
     */
    public AbstractConditionalExpression sub;

    /**
     * Creates a new instance of GPath.
     *
     * @param sub                       an expression for this path
     */
    public GPath(AbstractConditionalExpression sub) {
        this.sub = sub;
    }
    @Override
    public String toString() {
        return "G " + sub.toString();
    }
}
