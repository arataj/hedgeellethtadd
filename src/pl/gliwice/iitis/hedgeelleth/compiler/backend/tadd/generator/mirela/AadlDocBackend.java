/*
 * AadlDocBackend.java
 *
 * Created on Mar 28, 2018
 *
 * Copyright (c) 2018  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.mirela;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.pta.PTA;

/**
 * A documentation backend for the AADL format.
 * 
 * @author Artur Rataj
 */
public class AadlDocBackend extends AbstractDocBackend {
    /**
     * Types associates with each size in bytes. A node has its type if its
     * <code>packetSize</code> is defined.
     */
    Map<Integer, String> types;
    /**
     * An optional pta name mapping, null for none.
     */
    private Map<PTA, String> ptaNameMap;
    /**
     * Creates an AADL documentation backend.
     * 
     * @param net Mirela network
     */
    public AadlDocBackend(MirelaNetwork net) {
        super(net);
        types = new HashMap<>();
    }
    /**
     * Returns a node's name, taking into accound the optional name map.
     * 
     * @param m node
     * @return the node's name
     */
    public String getName(MirelaNode m) {
        if(ptaNameMap == null)
            return m.name;
        else
            return ptaNameMap.get(m.pta);
    }
    @Override
    public void generate(File outFile, Map<PTA, String> ptaNameMap) throws IOException {
        this.ptaNameMap = ptaNameMap;
        try(PrintWriter out =  new PrintWriter(outFile)) {
            printHeader(out);
            printTypes(out);
            printNodes(out);
            printSystem(out);
            printFooter(out);
        }
    }
    /**
     * Prints the file's header information.
     * 
     * @param out print writer
     */
    protected void printHeader(PrintWriter out) throws IOException {
        out.println(
                "package " + NET.name + "\n\n" +
                "public\n\n" +
                "with base_types;\n" +
                "with Data_Model;\n");
    }
    /**
     * Prints types encountered in the network.
     * 
     * @param out print writer
     */
    protected void printTypes(PrintWriter out) throws IOException {
        for(MirelaNode n : NET.nodesList)
            if(n.packetSize != -1 && !types.containsKey(n.packetSize)) {
                String s = "t_" + getName(n);
                types.put(n.packetSize, s);
                out.print("data " + s + " ");
                switch(n.packetSize) {
                    case 1:
                    case 2:
                    case 4:
                    case 8:
                        out.print("extends base_types::Integer_" +
                                (n.packetSize*8));
                        break;
                        
                    default:
                        out.print("properties\n" +
		"\tData_Model::Data_Representation => Array;\n" +
                              "\tData_Model::Base_Type => (classifier (Base_Types::Integer_8));\n" +
		"\tData_Model::Dimension => (" + n.packetSize + ");");
                        break;
                }
                out.println("\nend " + s + ";\n\n" +
                        "data implementation " + s + ".i\n" +
                        "end " + s + ".i;\n");
            }
    }
    /**
     * Prints subsequent nodes.
     * 
     * @param out print writer
     */
    protected void printNodes(PrintWriter out) throws IOException {
        for(MirelaNode n : NET.nodesList) {
            switch(n.getClazz()) {
                case PERIODIC_THREAD:
                    printThread(out, n);
                    break;
                    
                case BUS:
                    printBus(out, n);
                    break;
                    
                case TIMER:
                    /* ignore */
                    break;
                    
                case UNKNOWN:
                    printUnknown(out, n);
                    break;
                    
                default:
                    throw new RuntimeException("unknown node class");
            }
        }
    }
    /**
     * Prints a thread node.
     * 
     * @param out print writer
     * @param n node to write
     */
    protected void printThread(PrintWriter out, MirelaNode n) throws IOException {
        out.println("process " + getName(n));
        boolean isProducer = NET.isProducer(n);
        boolean isConsumer = NET.isConsumer(n);
        StringBuilder f = new StringBuilder();
        if(isProducer || isConsumer) {
            f.append("features\n");
            if(n.packetSize == -1)
                throw new IOException("module " + n.name + ": AADL requires types for ports, none found");
            if(isProducer)
                f.append("\tdataOut : out data port " + types.get(n.packetSize) + ".i;\n");
            if(isConsumer)
                f.append("\tdataIn : in data port " + types.get(n.packetSize) + ".i;\n");
            f.append("flows\n");
            if(isProducer)
                f.append("\tfOut : flow source dataOut;\n");
            if(isConsumer)
                f.append("\tfIn : flow sink dataIn;\n");
            out.print(f.toString());
        }
        out.println("end " + getName(n) + ";\n\n" +
            "process implementation " + getName(n) + ".i\n" +
            "subcomponents\n" +
            "\tthr : thread " + getName(n) + "_thr.i;");
        if(isProducer || isConsumer) {
            out.println("connections");
            if(isProducer)
                out.println("\tcOut : port thr.dataOut -> dataOut;");
            if(isConsumer)
                out.println("\tcIn : port dataIn -> thr.dataIn;");
            out.println("flows");
            if(isProducer)
                out.println("\tfOut : flow source thr.fOut -> cOut -> dataOut;");
            if(isConsumer)
                out.println("\tfIn : flow sink dataIn -> cIn -> thr.fIn;");
            if(isProducer && isConsumer)
                out.println("\tfThru : flow dataIn -> cIn -> thr.fThru -> cOut -> dataOut;");
        }
        out.println("end " + getName(n) + ".i;\n\n" +
            "thread " + getName(n) + "_thr");
        out.print(f.toString());
        if(isProducer && isConsumer)
            out.print("\tfThru : dataIn -> dataOut;");
        if(!Double.isNaN(n.period)) {
            out.println("properties");
            int p = (int)Math.round(n.period);
            if(n.period != p)
                out.println(getWarning("rounding up a non-integer period"));
            out.println("\tperiod => " + p + " ms;\n" +
                    "\tdispatch_protocol => periodic;");
        }
        if(!Double.isNaN(n.xMin)) {
            out.println(getWarning("unexpected transmission time ignored"));
        }
        out.println("end " + getName(n) + "_thr;\n\n" +
                "thread implementation " + getName(n) + "_thr.i\n" +
                "end " + getName(n) + "_thr.i;\n");
    }
    /**
     * Prints a bus node.
     * 
     * @param out print writer
     * @param n node to write
     */
    protected void printBus(PrintWriter out, MirelaNode n) throws IOException {
        out.println("bus " + getName(n));
        boolean isProducer = NET.isProducer(n);
        boolean isConsumer = NET.isConsumer(n);
        if(!isConsumer)
            out.println(getWarning("bus input not connected"));
        if(!isProducer)
            out.println(getWarning("bus output not connected"));
        int minI = (int)Math.round(n.xMin);
        int maxI = (int)Math.round(n.xMax);
        if(minI != n.xMin || maxI != n.xMax)
            out.println(getWarning("rounding up a non-integer transmission time"));
        out.println("properties\n" +
                "\ttransmission_time => [ fixed => " +
                minI + " ms .. " + maxI + " ms; ];");
        if(!Double.isNaN(n.period)) {
            out.println(getWarning("unexpected period ignored"));
        }
        out.println("end " + getName(n) + ";\n");
    }
    /**
     * Prints a warning about an encounter of an unknown node.
     * 
     * @param out print writer
     * @param n node to write
     */
    protected void printUnknown(PrintWriter out, MirelaNode n) throws IOException {
        out.println(getWarning("unknown node " + getName(n)));
    }
    /**
     * Prints a system.
     * 
     * @param out print writer
     */
    protected void printSystem(PrintWriter out) throws IOException {
        out.println("system " + NET.name + "_sys");
        out.println("end " + NET.name + "_sys;\n");
        out.println("system implementation " + NET.name + "_sys.i");
        if(!NET.flows.isEmpty()) {
            StringBuilder componentsStr = new StringBuilder();
            StringBuilder connectionsStr = new StringBuilder();
            int connectionCount = 0;
            StringBuilder flowsStr = new StringBuilder();
            int flowCount = 0;
            for(MirelaFlow f : NET.flows) {
                flowsStr.append("\tf" + flowCount + " : end to end flow ");
                boolean expectsBus = false;
                List<MirelaNode> series = new LinkedList<>();
                int nodeCount = 0;
                boolean firstNode = true;
                boolean firstSeries = true;
                for(MirelaNode n : f.nodes) {
                    boolean lastNode = nodeCount == f.nodes.size() - 1;
                    if(n.getClazz() == MirelaNode.Clazz.BUS && !expectsBus)
                        throw new IOException(n.name +
                                ": buses required to connect to threads by AADL backend");
                    if(n.getClazz() != MirelaNode.Clazz.BUS && expectsBus)
                        throw new IOException(n.name +
                                ": threads can connect only to buses in AADL backend");
                    if(!expectsBus)
                        componentsStr.append(
                                "\t" + getName(n) + "_prs\t: process " + getName(n) + ".i;\n");
                    else
                        componentsStr.append(
                                "\t" + getName(n) + "_bus\t: bus " + getName(n) + ";\n");
                    series.add(n);
                    if(series.size() == 3) {
                        if(expectsBus)
                            throw new RuntimeException("expected thread -> bus -> thread");
                        String inName = getName(series.get(0));
                        String busName = getName(series.get(1));
                        String outName = getName(series.get(2));
                        connectionsStr.append("\tc" + connectionCount + ": " +
                                "port " + inName + "_prs.dataOut -> " + outName + "_prs.dataIn\n" +
                                "\t\t{actual_connection_binding => (reference(" + busName + "_bus));};\n");
                        flowsStr.append(inName + "_prs.f" +
                                (firstSeries ? "Out" : "Thru") + " -> c" + connectionCount + " -> ");
                        if(lastNode)
                            flowsStr.append(outName + "_prs.fIn;\n");
                        ++connectionCount;
                        MirelaNode last = series.get(series.size() - 1);
                        series.clear();
                        series.add(last);
                        firstSeries = false;
                    }
                    expectsBus = !expectsBus;
                    firstNode = false;
                    ++nodeCount;
                }
                ++flowCount;
            }
            if(componentsStr.length() != 0)
                out.print("subcomponents\n" + componentsStr.toString());
            if(connectionsStr.length() != 0)
                out.print("connections\n" + connectionsStr.toString());
            if(flowsStr.length() != 0)
                out.print("flows\n" + flowsStr.toString());
        }
        out.println("end " + NET.name + "_sys.i;\n");
    }
    /**
     * Prints the file's footer information.
     * 
     * @param out print writer
     */
    protected void printFooter(PrintWriter out) throws IOException {
        out.println("end " + NET.name + ";");
    }
    /**
     * Returns an AADL comment with a warning message.
     * 
     * @param text text to decorate with the warning
     * @return textual warning representation
     */
    protected String getWarning(String text) {
        return " --\n" +
                " -- WARNING: " + text + "\n" +
                " --";
    }
}
