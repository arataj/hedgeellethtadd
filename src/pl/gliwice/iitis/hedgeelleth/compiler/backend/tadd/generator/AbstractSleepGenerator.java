/*
 * AbstractSleepGenerator.java
 *
 * Created on Jul 22, 2009, 10:37:51 AM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator;

import pl.gliwice.iitis.hedgeelleth.pta.PTAException;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.CodeOpNone;

/**
 * An abstract new sleep interface.
 *
 * @author Artur Rataj
 */
public abstract interface AbstractSleepGenerator {
    /**
     * Generates transition for <code>CodeOpNone</code> with a
     * <code>SLEEP</code> annotation.<br>
     *
     * A possible previous operation is not connected.
     *
     * @param n                         operation for which the transitions
     *                                  should be generated
     * @param context                   context
     */
    abstract void generateSleepAnnotation(CodeOpNone n, TADDGeneratorContext context)
            throws PTAException;
}
