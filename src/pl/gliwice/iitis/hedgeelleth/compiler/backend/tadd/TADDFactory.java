/*
 * TADDFactory.java
 *
 * Created on May 8, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.op.AbstractCodeOp;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeObject;
import pl.gliwice.iitis.hedgeelleth.pta.*;
import pl.gliwice.iitis.hedgeelleth.pta.optimize.PTAOptimizer;
import pl.gliwice.iitis.hedgeelleth.pta.values.*;
import static pl.gliwice.iitis.hedgeelleth.pta.PTATransition.PROB_UNIQUENESS_PREFIX;

/**
 * Builds a PTA from the data from TADDGenerator.
 * 
 * @author Artur Rataj
 */
public class TADDFactory {
    /**
     * <p>Custom states.</p>
     * 
     * <p>Normally, all states with output transitions are added. If one misses
     * an output transition, it should be added here explicitly.</p>
     */
    public Map<Integer, PTAState> customStates;
    /**
     * Transitions keyed with operation or source indices.
     */
    public Map<Integer, PriorityQueue<PTATransition>> transitions;
    /**
     * Unique index of a new state. These are negative vaules &lt; -1,
     * because in transitions -1 means no index.
     */
    private int nextNewStateIndex;
    /**
     * Definition of sets of states, each set labelled with its common
     * name. States are represented within the set by their indices. A
     * source operation for putting a state into a given formula is mapped
     * to each state in the set. The operation can be null for none or unknown.<br>
     * 
     * Possibly translated into respective boolean formulas on the
     * state vector by the backend.
     */
    public SortedMap<String, Map<Integer, AbstractCodeOp>> stateFormulas;
    /**
     * Type of a state formula of a given name -- true for OR, false for AND.
     */
    public Map<String, Boolean> stateFormulasOr;
    /**
     * If not empty, the next state added should also be added to formulas
     * of given names. It is applied solely to the next state, so after that addition,
     * this list is emptied again.<br>
     * 
     * If a new PTA is to be produced and this field is still not null, then a respective
     * PTA exception is thrown about a pending formula.
     */
    public List<String> nextFormula;
    /**
     * Types of formulas added to <code>nextFormula</code>: true for
     * OR formulas, false for AND formulas. Should be of the same size as
     * <code>nextFormula</code>.
     */
    public List<Boolean> nextFormulaOr;
    /**
     * Operations, that added names to <code>nextFormula</code>, empty
     * element for none or unknown. Should be of the same size as
     * <code>nextFormula</code>.
     */
    public List<AbstractCodeOp> nextFormulaOp;
    
    /**
     * Creates a new instance of TADDFactory.
     */
    public TADDFactory() {
        customStates = new HashMap<>();
        transitions = new HashMap<>();
        nextNewStateIndex = -2;
        stateFormulas = new TreeMap<>();
        stateFormulasOr = new TreeMap<>();
        nextFormula = new LinkedList<>();
        nextFormulaOr = new LinkedList<>();
        nextFormulaOp = new LinkedList<>();
    }
//    /**
//     * Adds a state. A state does not need to be explicitly added in this method.
//     * If not added in this method, it is added at <code>index</code> by
//     * <code>addTransition</code>, but in such a case it is a default state.
//     * Use this method only if a state at a given index should have some custom
//     * properties.
//     * 
//     * @param index state index
//     * @return state to be customized
//     */
//    public PTAState addCustomState(int index) {
//        PTAState state = new PTAState(index);
//        customStates.put(index, state);
//        return state;
//    }
    /**
     * Adds the next added state, if any, to a formula of
     * a given name.
     * 
     * @param name name of a set of states
     * @param or true for OR formula, false for AND formula;
     * can not be mixed for a given <code>name</code>
     * @param op an operation, that specified, that a next
     * state should be within the formula; null for none or
     * unknown
     */
    public void addNextToFormula(String name, boolean or,
            AbstractCodeOp op) {
        nextFormula.add(name);
        nextFormulaOr.add(or);
        nextFormulaOp.add(op);
    }
    /**
     * Adds a transition. If <code>nextFormula</code>
     * is not null, then it is nulled, and the state at
     * <code>tt</code> is added to a respective formula.<br>
     * 
     * Can generate <code>PTAException</code> only if
     * <code>nextFormula</code> is not empty.
     * 
     * @param index                     operation or source index
     * @param tt                        transition, null for none,
     *                                  that is, to just add a
     *                                  default state
     */
    public void addTransition(int index, PTATransition tt) throws PTAException {
        /*
        if(index == -5)
            index = index;
            */
        if(tt != null) {
            tt.connectSourceIndex = index;
            for(int i = 0; i < nextFormula.size(); ++i) {
                String f = nextFormula.get(i);
                boolean or = nextFormulaOr.get(i);
                AbstractCodeOp op = nextFormulaOp.get(i);
                Map<Integer, AbstractCodeOp> map = stateFormulas.
                        get(f);
                Boolean formulaType = stateFormulasOr.get(f);
                if(map == null) {
                    map = new HashMap<>();
                    stateFormulas.put(f, map);
                    stateFormulasOr.put(f, or);
                } else if(formulaType != or)
                    throw new PTAException(op.getStreamPos(), "type of formula " +
                            f + " declared as both OR and AND");
                map.put(tt.connectSourceIndex, op);
            }
            nextFormula.clear();
            nextFormulaOr.clear();
            nextFormulaOp.clear();
        }
        PriorityQueue<PTATransition> set = transitions.get(index);
        if(set == null) {
            set = new PriorityQueue<>();
            transitions.put(index, set);
        }
        if(tt != null)
            set.add(tt);
    }
    /**
     * Returns an index of a new state, unique within the PTA
     * to build. The index is negative so it wil not conflict
     * with states that have indices after operation indices
     * in code methods.
     *
     * @return
     */
    public synchronized int newStateIndex() {
        return nextNewStateIndex--;
    }
    /**
     * Adds clock resets to transitions, that lead to
     * states with clock invariants or to states, that have
     * outcoming transitions with clock guards.
     * 
     * @param pta PTA to modify
     */
    public static void addResets(PTA pta) {
        for(PTAState tState : pta.states.values()) {
            PTAClock clock;
            if(tState.clockLimitHigh != null)
                clock = tState.clockLimitHigh.getClock();
            else
                clock = null;
            SCAN:
            for(Set<PTATransition> set : tState.output.values())
                for(PTATransition tt : set)
                    if(tt.clockLimitLow != null) {
                        if(clock == null)
                            clock = tt.clockLimitLow.getClock();
                        if(tt.clockLimitLow.getClock() != clock)
                            throw new RuntimeException("unexpected two different clocks");
                        break SCAN;
                    }
            if(clock != null)
                for(PTATransition tt : new LinkedList<>(pta.transitions))
                    if(tt.targetState == tState) {
                        PTAAssignment a = new PTAAssignment(clock,
                            new PTAConstant(0, false));
                        PTA.TransitionBag xbag = pta.keyBefore(tt);
                        tt.update.add(a);
                        a.backendEnableStop();
                        // an initial attempt; perhaps will be tried again after transitions
                        // are compacted
                        addStops(tt, a);
                        pta.keyAfter(xbag);
                    }
        }
    }
    /**
     * Try to add stops for a neutral update from the
     * update before it, or if none, from guards of its transition.<br>
     * 
     * Used by clock resets, as these have no source operations,
     * and thus their stops can not be defined by using
     * <code>AbstractTADDGenerator.setStop()</code>.
     * 
     * @param tt transition
     * @param a an update to add a stop; must not interfere the
     * flow, by for example being a clock reset, which does not
     * access any locals
     */
    public static void addStops(PTATransition tt, AbstractPTAExpression a) {
        boolean failed = true;
        int index = tt.update.indexOf(a);
        if(index == -1)
            throw new RuntimeException("update not found");
        for(--index; index >= 0; --index) {
            AbstractPTAExpression u = tt.update.get(index);
            if(u.backendStopEnabled()) {
                a.backendAddStops(u);
                failed = false;
                break;
            }
        }
        if(failed) {
            if(tt.guard != null)
                a.backendAddStops(tt.guard);
            else if(tt.probability != null)
                a.backendAddStops(tt.probability);
            else if(tt.clockLimitLow != null)
                a.backendAddStops(tt.clockLimitLow);
        }
    }
    /**
     * Sorts updates, removes redundant ones.
     */
    protected void sortUpdates() {
        for(PriorityQueue<PTATransition> set : transitions.values())
            for(PTATransition tt : set) {
                SortedMap<String, AbstractPTAExpression> unique = new TreeMap<>();
                for(AbstractPTAExpression expr : tt.update)
                    unique.put(expr.toString(), expr);
                tt.update.clear();
                tt.update.addAll(unique.values());
            }
    }
    /**
     * Converts probability guards into probabilities.
     * 
     * @param pta pta to modify
     */
    protected void findProbabilities(PTA pta) {
        for(PTATransition tt : new LinkedList<>(pta.transitions)) {
            AbstractPTAExpression p;
            if(tt.guard != null && (p = tt.guard.probability()) != null) {
                PTA.TransitionBag xbag = pta.keyBefore(tt);
                if(tt.probability == null) {
                    tt.probability = p;
                    tt.probability.backendCopyStop(tt.guard);
                } else
                    throw new RuntimeException("probability already assigned");
                tt.guard = null;
                pta.keyAfter(xbag);
            }
        }
    }
    /**
     * Used by <code>checkUniqueEvaluationIds</code> to add an evaluation
     * identifier.
     * 
     * @param ids map of states, keyed with the identifiers
     * @param state the state, from which outputs the transition that contains
     * the expression having <code>evaluationId</code>
     * @param evaluationId an identifier, not added if equal to -1
     */
    protected static void add(Map<Long, Set<PTAState>> ids,
            PTAState state, long evaluationId) {
        if(evaluationId != -1) {
            Set<PTAState> set = ids.get(evaluationId);
            if(set == null) {
                set = new HashSet<>();
                ids.put(evaluationId, set);
            }
            set.add(state);
        }
    }
    /**
     * Checks for unique <code>AbstractPTAExpression.singleEvaluationId</code>.
     * 
     * @param pta pta, across which the identifiers should be either -1 or unique
     */
    protected void checkUniqueEvaluationIds(PTA pta) {
        Map<Long, Set<PTAState>> ids = new HashMap<>();
        for(PTAState tState : pta.states.values()) {
            for(Set<PTATransition> set : tState.output.values())
                for(PTATransition tt : set) {
                    if(tt.guard != null)
                        add(ids, tState, tt.guard.singleEvaluationId);
                    for(AbstractPTAExpression expr : tt.update)
                        add(ids, tState, expr.singleEvaluationId);
                }
        }
        for(long id : ids.keySet()) {
            Set<PTAState> states = ids.get(id);
            if(states.size() > 1)
                throw new RuntimeException("a single evaluation identifier " +
                        "shared by transitions that come out of different states");
        }
    }
    /**
     * Checks if the connectivity between states is valid. Also updates
     * <code>PTATransition.sourceIndex</code> and
     * <code>PTATransition.targetIndex</code>, as 
     * <code>PTAOptimizer</code> is allowed to outdate these. Sets
     * an owner for each state.
     * 
     * @param pta pta, across which the identifiers should be either -1 or unique
     */
    public static void checkConnectivity(PTA pta) {
        Set<PTATransition> stray = new HashSet<>(pta.transitions);
        for(PTAState s : pta.states.values()) {
            for(int inputIndex : s.input.keySet()) {
                Set<PTATransition> set = s.input.get(inputIndex);
                if(set.isEmpty())
                    throw new RuntimeException("empty input set");
                for(PTATransition input : set) {
                    if(input.sourceState.index != inputIndex)
                        throw new RuntimeException("input mismatch");
                    if(!pta.transitions.contains(input))
                        throw new RuntimeException("input not listed");
                    if(pta.labeled.get(input.label) == null ||
                            !pta.labeled.get(input.label).contains(input))
                        throw new RuntimeException("input not listed");
                }
            }
            for(int outputIndex : s.output.keySet()) {
                Set<PTATransition> set = s.output.get(outputIndex);
                if(set.isEmpty())
                    throw new RuntimeException("empty output set");
                for(PTATransition output : set) {
                    if(output.targetState.index != outputIndex)
                        throw new RuntimeException("output mismatch");
                    stray.remove(output);
                    if(!pta.transitions.contains(output))
                        throw new RuntimeException("output not listed");
                    if(pta.labeled.get(output.label) == null ||
                            !pta.labeled.get(output.label).contains(output))
                        throw new RuntimeException("output not listed");
                }
            }
            s.owner = pta;
        }
        if(!stray.isEmpty())
            throw new RuntimeException("stray transitions");
        for(PTATransition t : pta.transitions) {
            t.connectSourceIndex = t.sourceState.index;
            t.connectTargetIndex = t.targetState.index;
        }
    }
    /**
     * Checks, if every atomic guard exist in a transition which loops
     * to itself, as it should.
     * 
     * @param pta pta to check
     */
    public static void checkAtomic(PTA pta) throws PTAException {
        PTAException error = null;
        for(PTAState s : pta.states.values())
            for(int outputIndex : s.output.keySet()) {
                Set<PTATransition> set = s.output.get(outputIndex);
                for(PTATransition t : set)
                    if(t.loopsToItself && t.targetState != s) {
                        if(error == null)
                            error = new PTAException();
                        StreamPos pos;
                        if(!t.backendOps.isEmpty())
                            pos = t.backendOps.get(0).getStreamPos();
                        else
                            pos = null;
                        error.addReport(new CompilerException.Report(pos,
                            CompilerException.Code.ILLEGAL,
                            t.guard.toString() + " is not in an atomic synchronisation, " +
                                    "but the barrier specifies otherwise"));
                    }
            }
        if(error != null)
            throw error;
    }
    /**
     * Updates <code>PTA.labeled</code>, which might have changed after
     * optimizations.<br>
     * 
     * This PTA modification is state formula--sensitive, see
     * <code>PTA.testFormulas()</code> for details.
     * 
     * @param pta pta to update its <code>labeled</code>
     */
    public static void updateLabeled(PTA pta) {
        pta.labeled.clear();
        for(PTATransition x : pta.transitions)
            /*if(x.label != null)*/ {
                Set<PTATransition> set = pta.labeled.get(x.label);
                if(set == null) {
                    set = new HashSet<>();
                    pta.labeled.put(x.label == null ? null : x.label.clone(), set);
                }
                set.add(x);
            }
    }
    /**
     * Completes clock reset stops, if possible. This method is
     * needed for <code>PTAOptimizer.closeStops()</code>
     * to work correctly.<br>
     * 
     * Should be one of the last operations on a constructed PTA,
     * as more compacted transitions make it more probable
     * to be sources of stops.<br>
     * 
     * This PTA modification is state formula--sensitive, see
     * <code>PTA.testFormulas()</code> for details.
     * 
     * @param pta pta, in which the clock resets should have the
     * stops completed
     */
    public static void completeClockResetStops(PTA pta) {
        for(PTATransition x : pta.transitions)
            for(AbstractPTAExpression u : x.update)
                if(u.target instanceof PTAClock) {
                    // a clock reset
                    addStops(x, u);
                }
    }
    /**
     * If two non--deterministic branches are identical, only one is left.
     * 
     * @param pta pta to transform
     */
    private void _removeRedundantNondeterminism(PTA pta) {
        if(false)
            return;
        Map<String, PTATransition> unique = new TreeMap<>();
        Set<PTATransition> removal = new TreeSet<>();
        for(PTATransition x : pta.transitions)  {
            String key = x.getKey();
            key = key.substring(0, key.indexOf(PROB_UNIQUENESS_PREFIX));
            if(unique.containsKey(key)) {
                unique.get(key).ndInt.addAll(x.ndInt);
                removal.add(x);
            } else
                unique.put(key, x);
        }
        for(PTATransition x : removal) {
            PTA.TransitionBag xbag = pta.keyBefore(x);
            // transition for removal -- no keyAfter()
        }
    }
    /**
     * Creates a PTA.
     * 
     * @param object a runtime object from which originates this PTA
     * @param name name of the new PTA
     * @param waitVariables a list of wait variables within locks
     * @param localVariables local variables for this PTA
     * @param removeConstGuardsUsingRanges passed to
     * <code>PTAOptimizer.removeFalseTransitions</code>;
     * should normally be true
     * @param implicitHidden passed to <code>PTAState.isHidden()</code>
     */
    public PTA newTADD(RuntimeObject ro, String name,
            List<AbstractBackendPTAVariable> waitVariables,
            Set<PTAScalarVariable> localVariables,
            boolean removeConstGuardsUsingRanges,
            boolean implicitHidden) throws PTAException {
        PTA tadd = new BackendPTA(ro, name);
        //
        // add transitions
        //
        tadd.add(transitions, customStates, null);
        //removeRedundantNondeterminism(tadd);
        //
        // check for the presence of operation indices,
        // and if this PTA is non--deterministic
        //
        for(PTATransition tt : tadd.transitions) {
            if(tt.backendOpIndex == -1)
                throw new RuntimeException("a transition's operation index is unspecified");
            if(tt.branchId != null && tt.branchId.type == BranchId.Type.NONDETERMINISTIC)
                tadd.nonDeterministic = true;
        }
        //
        // add state formulae
        //
        if(!nextFormula.isEmpty())
            throw new PTAException(nextFormulaOp.iterator().next().getStreamPos(),
                    "pending formula `" + nextFormula + "'");
        tadd.addFormulas(stateFormulas, stateFormulasOr);
        //
        // find the start state
        //
        int start = Integer.MAX_VALUE;
        for(PTAState tState : tadd.states.values())
            // minimum non--negative state is the start one
            if(tState.index >= 0 && start > tState.index) {
                start = tState.index;
                tadd.startState = tState;
            }
        // optimize
        PTAOptimizer.deleteRedundantStatesRecursive(tadd);
        //addResets(tadd);
        sortUpdates();
/*if(tadd.name.indexOf("Receiver") != -1)
    tadd = tadd;*/
        findProbabilities(tadd);
        PTAOptimizer.deleteDeadStates(tadd);
/*if(!tadd.states.isEmpty() && tadd.states.firstKey() == -6)
    tadd= tadd;*/
        if(PTAOptimizer.conditionalTrees(tadd))
            /*removeRedundant(tadd)*/;
        if(PTAOptimizer.nonDeterministic(tadd))
            /*removeRedundant(tadd)*/;
        if(PTAOptimizer.compactGuardUpdate(tadd,
                localVariables, implicitHidden, false))
            /*removeRedundant(tadd)*/;
        //removeRedundantNondeterminism(tadd);
        List<AbstractPTAVariable> w = new LinkedList<>();
        for(AbstractBackendPTAVariable v : waitVariables)
            w.add(v.v());
        if(PTAOptimizer.removeConstGuards(tadd, w,
                removeConstGuardsUsingRanges))
            PTAOptimizer.removeRedundant(tadd);
        if(PTAOptimizer.moveBackAssignment(tadd))
            PTAOptimizer.removeRedundant(tadd);
        ///removeRedundantNondeterminism(tadd);
        PTAOptimizer.compactProbabilisticBranches(tadd);
        PTAOptimizer.removeZeroProbabilisticBranches(tadd);
        checkUniqueEvaluationIds(tadd);
        updateLabeled(tadd);
        //
        // check for the validity of connections in the new PTA
        //
        checkConnectivity(tadd);
        return tadd;
    }
    /**
     * Nests expressions within a PTA, and then compacts series
     * of updates.<br>
     * 
     * This PTA modification is state formula--sensitive, see
     * <code>PTA.testFormulas()</code> for details.
     * 
     * @param pta pta to modify
     * @param writtenElements variables of elements that are
     * written to in the PTAs; null if the set is unknown or if
     * array variables are used by the backend
     * @param transport transport of values; used to set stops for transitions
     * @param strictFieldAccess defined as in
     * <code>TADDOptions.strictFieldAccess</code>
     * @param implicitHidden passed to <code>PTAState.isHidden()</code>
     * @param rangeChecking if range checking is enabled, so that updates
     * with source--level ranges can not be removed
     * @return if anything changed in the PTA
     */
    public static boolean nestAndCompactExpressions(PTA pta,
            Set<PTAElementVariable> writtenElements,
            PTAVariableTransport transport, boolean strictFieldAccess,
            boolean implicitHidden, boolean rangeChecking) throws PTAException {
        boolean stable = true;
        if(PTAOptimizer.nestExpressions(pta, transport.getTraced(),
                writtenElements, strictFieldAccess, implicitHidden,
                rangeChecking)) {
            PTAOptimizer.deleteRedundantStatesRecursive(pta);
            stable = false;
        }
        if(PTAOptimizer.compactUpdate(pta,
                transport.getTraced(), implicitHidden,
                false))
            stable = false;
        if(!stable) {
            updateLabeled(pta);
            checkConnectivity(pta);
        }
        return !stable;
    }
//    /**
//     * Checks, if any illega <code>CodeOpThrow</code> is left.
//     * 
//     * @param pta pta to test for validity
//     */
//    public static void checkStaticCheckThrows(PTA pta) {
//        
//    }
}
