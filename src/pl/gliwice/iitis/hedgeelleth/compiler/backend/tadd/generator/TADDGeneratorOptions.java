/*
 * TADDGeneratorOptions.java
 *
 * Created on Jul 22, 2009, 1:43:53 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator;

import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.Scheduler;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.TADDOptions;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.ValuationType;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.CompilationOptions;
import pl.gliwice.iitis.hedgeelleth.pta.PTAException;

/**
 * Standard types of options of the TADD generator.
 *
 * @author Artur Rataj
 */
public class TADDGeneratorOptions {
    /**
     * How to treat arrays: <code>INDEX</code>: allow indexing operators in the
     * PTA, <code>REPLACE<code>: replace arrays with conditional branches, 
     * <code>REPLACE_FORMULA</code>: replace writing as in <code>REPLACE</code>,
     * but use Prism formulas for reading.
     */
    public static enum Arrays {
        INDEX,
        REPLACE,
        REPLACE_FORMULA,
    };
    /**
     * Valuation type. Copied from <code>TADDOptions.valuationType</code>.
     */
    public ValuationType valuationType;
    /*
     * If to annotates methods with all operations optimized out or
     * removed with MARKER and EMPTY_TRANSITION.
     */
    /* public boolean annotateEmptyMethods; */
    /**
     * If updates of variables is allowed on transitions with synchronizing
     * labels.<br>
     * 
     * For example, the Prism backend makes variables global, and updates
     * of global variables in synchronized transitions is not allowed by Prism,
     * so this field is false for the backend.
     */
    public boolean labeledUpdates;
    /**
     * If broadcast labels can be used; broadcast labels are used for
     * simpler generation of notifyAll().
     */
    public boolean broadcastLabels;
    /**
     * True if the broadcast label can block or if both blocking
     * and not blocking behaviour should be supported; false if
     * non--blocking is required or if no broadcast labels
     * are supported.
     */
    public boolean broadcastLabelBlocks;
//    /**
//     * If to allow distributions.
//     */
//    public boolean allowDistributions;
//    /**
//     * If to allow variable distributions, i. e. ones, whose parameters are
//     * not constant.
//     */
//    public boolean allowVariableDistributions;
    /**
     * Nondeterminism of synchronisation. Copied from
     * <code>TADDOptions.labelNondeterminism</code>.
     */
    public boolean labelNondeterminism;
    /**
     * How to translate indexing of arrays.<br>
     */
    public Arrays replaceArrays;
    /**
     * Passed to <code>PTAOptimizer.removeFalseTransitions</code>.
     * Copied from <code>TADDOptions.removeFalseTransitions</code>.
     */
    public boolean removeConstGuardsUsingRanges;
    /**
     * If the backend allows for floating--point variables.
     * Copied from <code>TADDOptions.allowFpVariables</code>.
     */
    boolean allowFpVariables;
    /**
     * Test options, null if outside compiler tests.
     * Copied from <code>TADDOptions.test</code>.
     */
    public CompilationOptions.Test test;
    /**
     * If nested expressions are enabled. If yes, performs an optimization
     * that nests expressions, and also replaces all non--clock expressions
     * <code>PTANestedExpression</code>.
     */
    public boolean nestedExpressions;
    /**
     * Scheduler for the PTAs. Copied from
     * <code>TADDOptions.scheduler</code>.
     */
    public Scheduler scheduler;
    /**
     * If to disallow any move of field access to a neighbouring transition.
     * For details, see the field <code>TADDOptions.strictFieldAccess</code>.
     * Copied from that field.
     */
    public boolean strictFieldAccess;
    /**
     * If checking of variable and primary ranges should be performed.
     * Copied from <code>TADDOptions.rangeChecking</code>.
     */
    boolean rangeChecking;
    /**
     * If to use smaller long ranges, that fit into 32 bits.
     * 
     * The default is true and must be true for a typical PTA generation.
     */
    public boolean reducedLongRange = true;
    /**
     * Specifies, what hc's library method
     * <code>Model.isStatistical()</code> returns.
     * Copied from <code>TADDOptions.isStatisticalHint</code>.
     */
    public boolean isStatisticalHint = false;
    
    /**
     * Creates new TADD generator options. Some fields are copied
     * from <code>options</code>, as written in their docs.<br>
     * 
     * The field <code>replaceArrays</code> is set to false for
     * XML and UPPAAL as these support arrays, and to true for
     * VERICS BMC, PRISM and HEDGEELLETH.<br>
     * 
     * The field <code>nestedExpressions</code> is set to true
     * only for PRISM, as it supports it. For the rest of output formats,
     * the field is set to false.
     * 
     * @param options TADD options
     */
    public TADDGeneratorOptions(TADDOptions options) throws PTAException {
        valuationType = options.valuationType;
        labelNondeterminism = options.labelNondeterminism;
        switch(options.getOutputFormat()) {
            case XML:
            case UPPAAL:
                replaceArrays = Arrays.INDEX;
                nestedExpressions = false;
                break;

            case PRISM:
                if(options.arraySupport == TADDOptions.ArraySupport.INDEX)
                    throw new PTAException(null, "Prism format does not support non--emulated arrays");
                replaceArrays = Arrays.REPLACE_FORMULA;
                nestedExpressions = true;
                break;
                
            case VERICS_BMC:
                if(options.arraySupport != TADDOptions.ArraySupport.NONE)
                    throw new PTAException(null, "Verics format does not support arrays");
                // fallthrough
            case HEDGEELLETH:
                switch(options.arraySupport) {
                    case NONE:
                    case INDEX:
                        replaceArrays = Arrays.INDEX;
                        break;
                        
                    case EMULATE:
                        replaceArrays = Arrays.REPLACE;
                        break;
                }
                nestedExpressions = false;
                break;

            default:
                throw new RuntimeException("unknown format");
        }
        removeConstGuardsUsingRanges = options.removeConstGuardsUsingRanges;
        allowFpVariables = options.allowFpVariables;
        test = options.test;
        scheduler = options.scheduler;
        strictFieldAccess = options.strictFieldAccess;
        rangeChecking = options.rangeChecking;
        isStatisticalHint = options.isStatisticalHint;
        reducedLongRange = options.reducedLongRange;
    }
}
