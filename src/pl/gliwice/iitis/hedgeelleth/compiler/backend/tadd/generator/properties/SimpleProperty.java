/*
 * SimpleProperty.java
 *
 * Created on Feb 19, 2010, 11:28:50 AM
 *
 * Copyright (c) 2010 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.properties;

/**
 * A simple PCTL property, allows for only one path property.
 *
 * @author Artur Rataj
 */
public class SimpleProperty extends AbstractProperty {
    /**
     * Path for the probability operator.
     */
    public AbstractPath path;

    /**
     * Creates a new instance of SimpleProperty.
     *
     * @param p                         probability operator
     * @param path                      path property
     */
    public SimpleProperty(ProbabilityOperator p, AbstractPath path) {
        super(p);
        this.path = path;
    }
    @Override
    public String toString() {
        return p.toString() + " [ " + path.toString() + " ]";
    }
}
