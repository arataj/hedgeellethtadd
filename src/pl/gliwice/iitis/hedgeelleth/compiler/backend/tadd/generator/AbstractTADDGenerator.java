/*
 * TADDGenerator.java
 *
 * Created on Apr 20, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator;

import java.util.*;
import java.util.Map.Entry;

import pl.gliwice.iitis.hedgeelleth.compiler.CompilerAnnotation;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.AbstractCodeDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.Unroll.ConstantIndexingInfo;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.runtime.RuntimeStaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.runtime.SplitAmbiguous;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.CodePrimaryPrimitiveRange;
import pl.gliwice.iitis.hedgeelleth.compiler.sa.RangeCodeStaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.TypeRangeMap;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.op.ArithmeticInterpreter;
import pl.gliwice.iitis.hedgeelleth.pta.*;
import pl.gliwice.iitis.hedgeelleth.pta.optimize.ClockOptimizer;
import pl.gliwice.iitis.hedgeelleth.pta.optimize.PTAOptimizer;
import pl.gliwice.iitis.hedgeelleth.pta.values.*;

import hc.TuplesIO;

/**
 * An abstract translator from internal assembler to PTA.<br>
 *
 * Subclasses should implement methods for generating transitions
 * for each method.<br>
 *
 * Range checking: all range checking required by <code>RangedCodeValue</code>
 * should be implemented in the PTA. The range checking imposed by variable
 * types is in turn implemented by a backend, so that it can possibly be
 * explicit to the interpreter of the generated files, as opposed to being
 * implicitly checked by transitions. This can enable certain optimizations
 * within the interpreter. An example of this is <code>Prism</code>, which
 * used the functions <code>min()</code> and <code>max()</code> to find out
 * ranged of variable values.
 *
 * @author Artur Rataj
 */
public abstract class AbstractTADDGenerator {
    /**
     * If to be verbose.
     */
    boolean verbose = false;

    /**
     * Connections between complementary labels and barrier labels.
     */
    public class Labels {
        /**
         * Map of complementary labels, keyes with their names.<br>
         *
         * See <code>purgeNotComplemented</code> for details.
         */
        public final Map<String, ComplementaryLabel> complementary;
        /**
         * Barriers, keyed with a common name.
         */
        public final Map<String, BarrierLabels> barriers;
        
        public Labels() {
            complementary = new HashMap<>();
            barriers = new HashMap<>();
        }
        /**
         * Returns all members of a barrier, which synchronize with <code>x</code>,
         * thus, except for transitions, whose owner is the same as that of
         * <code>x</code>.
         * 
         * @param x transition, which belongs to a barrier
         * @return a list of transitions
         */
        public List<PTATransition> getBarrierRest(PTATransition x) {
            String name = x.label.name;
//            int length;
//            if((length = BarrierLabels.getBarrierSuffixLength(name)) > 0)
//                name = name.substring(0, name.length() - length);
//            else
//                throw new RuntimeException("barrier label has an invalid suffix");
            BarrierLabels bl = barriers.get(name);
            List<PTATransition> out = new LinkedList<>();
            for(PTATransition t : bl.transitions)
                if(!t.ownerName.equals(x.ownerName))
                    out.add(t);
            return out;
        }
    };
    /**
     * Annotation applied to the in transition of synchronized code and
     * to the active transition of a WAIT operation.
     */
    public static final String TA_IN_STRING =
            "@IN";
    /**
     * Annotation applied to the out transition of synchronized code and
     * to the wait transition of a WAIT operation.
     */
    public static final String TA_OUT_STRING =
            "@OUT";
    /**
     * Annotation applied to the notify transitions of NOTIFY and WAIT
     * operations.
     */
    public static final String TA_NOTIFY_THREAD_STRING =
            "@NOTIFY_THREAD";
    /**
     * Annotation applied to the barrier transition of a BARRIER operation.
     */
    public static final String TA_BARRIER_STRING =
            "@BARRIER";
    /**
     * Annotation applied to the do not notify transition of a NOTIFY
     * operation.
     */
    public static final String TA_DO_NOTHING_STRING =
            "@DO_NOTHING";
    /**
     * Unqualified name of the error code variable.
     */
    public static final String ERROR_CODE_VARIABLE_NAME = "#error";
    /**
     * Name of a container of the status variables.
     */
    public static final String STATUS_CONTAINER_NAME = "statusVariables";
    /**
     * Qualified name of the error code variable.
     */
    public static final String ERROR_CODE_VARIABLE_QUALIFIED_NAME =
            STATUS_CONTAINER_NAME + "::" + ERROR_CODE_VARIABLE_NAME;
    /**
     * Maximum possible number of operations in interpreted code.
     */
    protected static final int MAX_NUM_INTERPRETED_OPS = 1000000;
    /**
     * Name of the field in barrier classes, which indicates, that the condition
     * must be atomic.
     */
    protected static final String ATOMIC_SYNC_FIELD_NAME = "atomicSync";
    
    /**
     * A PTA compilation.
     */
    public BackendPTACompilation compilation;
    /**
     * A runtime container for holding lock PTA variables.
     */
    public InternalRuntimeContainer lockVariables;
    /**
     * A runtime container for holding clock PTA variables.
     */
    public InternalRuntimeContainer clockVariables;
    /**
     * A runtime container for holding status PTA variables.
     */
    public InternalRuntimeContainer statusVariables;
    /**
     * Information about connections between complementary and barrier labels.
     */
    Labels labels;
    /**
     * Map of ranges within TADDs for each PTA type.
     */
    TypeRangeMap typeRangeMap;
    /**
     * Interpreter.
     */
    public AbstractInterpreter interpreter;
//    /**
//     * Collects the number of users of each barrier.
//     * If a barrier has no users, it is missing in this map.<br>
//     * 
//     * The number of users can be obtained using
//     * <code>getBarrierCount()</code>.
//     */
//    final Map<RuntimeObject, BarrierParties> barrierCount;
    /*
     * If to support backward analysis.
     */
    // boolean supportBackwardAnalysis;
    /**
     * Code of the latest inlining, excluding runtime static analysis, or null.<br>
     *
     * For testing purposes only.
     */
    public String lastInlineString0;
    /**
     * Code of the latest inlining, including runtime static analysis, or null.<br>
     *
     * For testing purposes only.
     */
    public String lastInlineString;
    /**
     * The main method.
     */
    CodeClass mainMethodClass;
    /**
     * Standard types of options of this PTA generator.
     */
    public TADDGeneratorOptions options;
    /**
     * Enables self--loops. Deprecated because self loops produce
     * zeno conditions in PTAs.
     */
    boolean _SELFLOOPS = false;
    /**
     * If a generator, i.e. some routine called from within
     * <code>generateTransitions()</code>, can recover
     * from an error, then it can add a report to this exception,
     * instead of throwing a <code>PTAException</code>.
     */
    PTAException recoverableErrors = new PTAException();
    
    /**
     * Creates a new instance of TADDGenerator. To generate the PTA,
     * <code>generate</code> on this object must be called.
     *
     * @param compilation               compilation
     * @param options                   standard types of options of this PTA
     *                                  generator
     */
    public AbstractTADDGenerator(CodeCompilation codeCompilation,
            TADDGeneratorOptions options)
            throws PTAException {
        this.options = options;
        mainMethodClass = null;
        for(CodeClass c : codeCompilation.classes.values())
            if(c.mainMethod != null) {
                if(mainMethodClass == null)
                    mainMethodClass = c;
                else {
                    throw new PTAException(null, "more than one main method");
                }
            }
        if(mainMethodClass == null)
            throw new PTAException(null, "no main method");
        codeCompilation.modelControl.setStatisticalHint(
                options.isStatisticalHint);
        typeRangeMap = PTARange.getTADDDefaults(options.reducedLongRange);
        interpreter = new ArithmeticInterpreter(codeCompilation);
        interpreter.setOpCounterLimit(MAX_NUM_INTERPRETED_OPS);
        interpreter.typeRangeMap = typeRangeMap;
        interpreter.model = codeCompilation.modelControl;
        interpreter.model.clearProperties();
        // barrierCount = new HashMap<>();
    }
    public String getMainClassName() {
        return mainMethodClass.name;
    }
    /**
     * Generates the PTA, according to the constructor's arguments.<br>
     * 
     * This method imposes a restriction on the <code>state</code> tag,
     * that it must contain a single <code>name</code> modifier.
     * 
     * @param optimizerTuning options for the optimizer
     * @return the main runtime method
     */
    public RuntimeMethod generate(Optimizer.Tuning optimizerTuning) throws PTAException {
        try {
            RuntimeMethod rm;
            try {
                List<String> runtimeArgs = new LinkedList<>();
                rm = InterpreterUtils.interpret(interpreter, mainMethodClass,
                        runtimeArgs);
            } catch(CompilerException e) {
                throw new PTAException(e);
            }
            generateTADD(rm.pendingThreads, optimizerTuning);
            Map<String, Set<String>> allowedTagModifiers = new HashMap<>();
            Set<String> generateHeadSet = new HashSet<>();
            Set<String> generateTailSet = new HashSet<>();
            Set<String> selectedSet = new HashSet<>();
            selectedSet.add("name");
            Set<String> stateSet = new HashSet<>();
            stateSet.add("name");
            allowedTagModifiers.put("generateHead", generateHeadSet);
            allowedTagModifiers.put("generateTail", generateTailSet);
            allowedTagModifiers.put("selected", selectedSet);
            allowedTagModifiers.put("state", stateSet);
            Map<String, Set<String>> requiredTagModifiers = new HashMap<>();
            requiredTagModifiers.put("state", stateSet);
            Map<String, Set<String>> uniqueTagModifiers = new HashMap<>();
            uniqueTagModifiers.put("state", stateSet);
            try {
                compilation.verifyNonCompilerTagModifiers(allowedTagModifiers,
                        requiredTagModifiers, uniqueTagModifiers);
            } catch(ParseException e) {
                throw new PTAException(e);
            }
            compilation.filterTags();
            compilation.deleteEmptyLocks();
            switch(options.valuationType.type) {
                case NONE:
                    break;

               case FULL:
                    compilation.generateStandardValuation();
                    break;

                case SELECTED:
                    compilation.generateSelectedValuation(
                            options.valuationType.label);
                    break;
                    
                default:
                    throw new RuntimeException("unknown type of valuation");

            }
            return rm;
        } catch(InterpreterException e) {
            String trace = e.dumpStack(1);
            if(!trace.isEmpty())
                trace = "called from " + trace + ": ";
            throw new PTAException(e.getStreamPos(), "in interpreted code: " +
                    trace + e.getRawMessage());
        }
    }
    /**
     * Trims all operations that return java.lang.String and
     * locals used only by them.
     * 
     * @param method                    code method to modify
     */
    public void trimStringOps(CodeMethod method) {
        for(int index = 0; index < method.code.ops.size(); ++index) {
            AbstractCodeOp op = method.code.ops.get(index);
            AbstractCodeDereference result = op.getResult();
            if(result != null && result.getResultType().isString(null)) {
                CodeOpNone n = new CodeOpNone(
                        op.getStreamPos());
                method.code.replace(index, n, true);
            }
        }
        Optimizer.optimizeLabels(method.code);
        Optimizer.optimizeUnusedLocals(method);
        Optimizer.setLabelIndices(method);
    }
    /**
     * <p>Prefixes the method with a set of assignments of values of
     * arguments to the respective argument-- or argument proto--holding
     * locals.</p>
     * 
     * <p>These assignments inform various value--tracing methods
     * about argument values. Also, while it is redundant at this stage,
     * these assignments might be actually needed after merging of local
     * variables. If not, they will eventually be removed by optimisation
     * routines.</p>
     * 
     * @param method                    code method to modify
     */
    public void addArgumentAssignments(List<RuntimeValue> arguments,
            CodeMethod method) {
        Iterator<RuntimeValue> argumentsI = arguments.iterator();
        for(CodeVariable cv : method.args) {
            CodeVariable proto = method.argProtos.get(cv);
            if(proto != null)
                cv = proto;
            CodeOpAssignment a = new CodeOpAssignment(method.getStreamPos());
            a.rvalue = new RangedCodeValue(new CodeConstant(
                    method.getStreamPos(), argumentsI.next()));
            a.setResult(new CodeFieldDereference(cv));
            // method.code.ops.add(0, a);
        }
        Optimizer.setLabelIndices(method);
    }
    /**
     * Replaces <code>CodeOpNone</code> operations
     * decorated with annotations of method, that returns
     * a known value.
     * 
     * @param method code method to modify
     * @param constants method annotations mapped to the
     * respective constant retrun values
     */
    public void replaceConstantAnnotationCalls(CodeMethod method,
            Map<CompilerAnnotation, Literal> constants) {
        Iterator<AbstractCodeOp> iterator = method.code.ops.iterator();
        for(int index = 0; index < method.code.ops.size(); ++index) {
            AbstractCodeOp op = iterator.next();
            AbstractCodeDereference result = op.getResult();
            if(result != null && op instanceof CodeOpNone) {
                CodeOpNone n = (CodeOpNone)op;
                Literal c = null;
                for(String s : n.annotations) {
                    CompilerAnnotation annotation = CompilerAnnotation.parse(s);
                    if(constants.keySet().contains(annotation))
                        if(c == null)
                            c = constants.get(annotation);
                        else
                            throw new RuntimeException("only a single constant " +
                                    "per operation expected");
                }
                if(c != null) {
                    CodeOpAssignment a = new CodeOpAssignment(n.getStreamPos());
                    a.setResult(result);
                    a.rvalue = new RangedCodeValue(new CodeConstant(null,
                            c));
                    method.code.replace(index, a, true);
                }
            }
        }
    }
    /**
     * Trims all assignments of primary range variables.
     * 
     * @param method                    code method to modify
     */
    public void trimPrimaryRangeOps(CodeMethod method) {
        boolean stable = true;
        for(int index = 0; index < method.code.ops.size(); ++index) {
            AbstractCodeOp op = method.code.ops.get(index);
            AbstractCodeDereference result = op.getResult();
            if(result instanceof CodeFieldDereference) {
                CodeFieldDereference f = (CodeFieldDereference) result;
                if(f.isLocal() && f.variable.category.isPrimaryPRBound()) {
                    CodeOpNone n = new CodeOpNone(
                            op.getStreamPos());
                    method.code.replace(index, n, true);
                    stable = false;
                }
            }
        }
        if(!stable) {
            Optimizer.optimizeLabels(method.code);
            Optimizer.optimizeUnusedLocals(method);
            Optimizer.setLabelIndices(method);
        }
    }
    /**
     * Converts false to 0 and true to 1.
     * 
     * @param b                         boolean value
     * @return                          integer value
     */
    static int booleanToInteger(boolean b) {
        return b ? 1 : 0;
    }
    /**
     * Creates a new array PTA variable. This method should be called
     * before any possible assignment to the array occured in the
     * PTA part, so that the initial values as assigned in the
     * interpreted part are known.
     * 
     * @param store                     store of the array
     * @param range                     range of the array's elements
     * @param floating                  if elements of a floating--point type
     * @return                          the newly created PTA variable
     */
    protected AbstractBackendPTAVariable newTADDArray(ArrayStore store,
            boolean floating) throws PTAException {
        double[] initValues = new double[store.getLength()];
        for(int index = 0; index < store.getLength(); ++index)
            initValues[index] =
                    // no variable, thus no variable's primitive range
                    store.getElementUnchecked(null, null, null, index).getMaxPrecisionFloatingPoint();
        AbstractBackendPTAVariable tv = compilation.newVariable(store, null,
                true, store.getLength(), initValues, null,
                null, floating);
        return tv;
    }
    /**
     * Returns a PTA variable representing a runtime variable
     * referenced by a given dereference. The runtime variable
     * must be one of the integer types or a boolean, or a non--null
     * array of integer or boolean, otherwise, if the resulting
     * type of the dereference is a java class reference, null is returned,
     * any other type except for the above ones causes an exception
     * to be thrown.<br>
     * 
     * If the PTA variable is non--existent, it is created, and
     * its initial value is stored.<br>
     *
     * If the dereference of the respective code variable is an indexing
     * dereference, and the code variable has a variable primitive range,
     * then that primitive range is registered using
     * <code>BackendPTAArrayVariable.registerVariableRange()<code>.<br>
     * 
     * If <code>dereference</code> is not a scalar local
     * <code>CodeFieldDereference</code>, the interpreter is required
     * to be contain respective container values.
     * 
     * @param rv                        value holding the variable
     *                                  dereference and optional range;
     *                                  if the range is missing, type's default
     *                                  is used
     * @param rm                        current runtime method
     * @return                          PTA variable
     */
    public AbstractBackendPTAVariable getVariable(AbstractCodeDereference dereference,
            RuntimeMethod rm) throws PTAException {
        Type type = dereference.getResultType();
        // dereference variables are never registered, but if they
        // contain a reference to an array, the array is registered
        if(type.isJava() && !type.isArray(null))
            return null;
        AbstractBackendPTAVariable tv;
        if(type.isArray(null)) {
            // this variable represents an array
            // CodeVariable contained =  dereference.getTypeSourceVariable();
            // Type elementType = type.getElementType(null, 0);
            try {
                ArrayStore store = (ArrayStore)interpreter.getValueUnchecked(
                            new RangedCodeValue(dereference), rm).
                        getReferencedObject();
                if(store == null)
                    throw new InterpreterFatalException(
                            InterpreterFatalException.Category.UNCHECKED_NULL_DEREFERENCE,
                            "null array does not represent any PTA variable");
                else {
                    // a container of an array is the array's own store
                    tv = compilation.getVariable(store, null);
                    if(tv == null) {
                        // no PTA variable found, create a new one
                        //
                        // so the array was first found as a result of a dereference
                        tv = newTADDArray(store, type.getElementType(null, 0).
                                isOfFloatingPointTypes());
                    }
                    if(dereference instanceof CodeIndexDereference)
                        throw new RuntimeException("array within an array");
                    // register the range
                    BackendPTARange range = new BackendPTARange(
                            // <code>i.object</code> must either be a local or static
                            (CodeFieldDereference)dereference, interpreter, rm,
                            typeRangeMap);
                    BackendPTAArrayVariable atv = (BackendPTAArrayVariable)tv;
                    atv.registerVariableRange(range);
                    // register a proposal
                    if(((CodeFieldDereference)dereference).variable.proposal != null)
                        atv.registerVariableRangeProposal(((CodeFieldDereference)dereference).variable.proposal);
                }
            } catch(InterpreterFatalException e) {
                switch(e.category) {
                    case UNCHECKED_NULL_DEREFERENCE:
                    case UNINITIALIZED_VARIABLE:
                        // the variable does not contain a reference to any array
                        throw new PTAException(dereference.getStreamPos(),
                                e.getMessage());

                    default:
                        throw e;
                }
            }
        } else {
            if(!type.isOfIntegerTypes() && !type.isOfFloatingPointTypes() && !type.isBoolean())
                throw new PTAException(dereference.getStreamPos(),
                        "within primitive types only arithmetic and " +
                        "boolean are supported");
            AbstractRuntimeContainer container;
            try {
                container = interpreter.getContainer(dereference, rm);
            } catch(InterpreterException e) {
                throw new PTAException(e);
            }
            if(dereference instanceof CodeFieldDereference) {
                CodeFieldDereference f = (CodeFieldDereference)dereference;
                tv = compilation.getVariable(container, f.variable);
                if(tv == null) {
/*if(f.toString().indexOf("c5") != -1)
    f = f;*/
                    BackendPTARange range = new BackendPTARange(f, interpreter, rm,
                            typeRangeMap);
                    // no PTA variable found, create a new one
                    double v;
                    if(f.isLocal())
                        // locals are always initialized with 0 in TADDs
                        v = 0;
                    else if(type.isOfIntegerTypes() || type.isOfFloatingPointTypes()) {
                        // no need to check the range, as it is read from
                        // equivalent--type field
                        //
                        // allow for unknown constants, as their values are
                        // only initial values of these constant's variables,
                        // and these initial values are meaningless anyway
                        v = interpreter.getValueUnchecked(
                                new RangedCodeValue(dereference),
                                    rm, true).getMaxPrecisionFloatingPoint();
                    } else if(type.isOfBooleanType()) {
                        // no need to check the range, as it is read from
                        // equivalent--type field
                        //
                        // boolean variables can not be unknown constants
                        v = booleanToInteger(interpreter.getValueUnchecked(
                                    new RangedCodeValue(dereference), rm).
                                getBoolean());
                    } else
                        throw new RuntimeException("unknown type");
                    tv = compilation.newVariable(container, f.variable,
                            false, -1, v, range,
                            null, f.variable.type.isOfFloatingPointTypes());
                }
            } else {
                CodeIndexDereference i = (CodeIndexDereference)dereference;
                // find the array variable, or create it if not found
                try {
                    ArrayStore store = (ArrayStore)interpreter.getValueUnchecked(
                            new RangedCodeValue(new CodeFieldDereference(
                                i.object)), rm).getReferencedObject();
                    if(store == null)
                        throw new PTAException(dereference.getStreamPos(),
                                "null dereference of array " + i.object.toString());
                    // a container of an array is the array's own store
                    tv = compilation.getVariable(store, null);
                    if(tv == null) {
                        // no PTA variable found, create a new one
                        // so the array was first found as an indexed
                        // object
                        tv = newTADDArray(store, type.isOfFloatingPointTypes());
                    }
                } catch(InterpreterFatalException e) {
                    switch(e.category) {
                        case UNCHECKED_NULL_DEREFERENCE:
                        case UNINITIALIZED_VARIABLE:
                            // the variable does not contain a reference to any array
                            tv = null;
                            break;

                        default:
                            throw e;
                    }
                }
                // register the range
                BackendPTARange range = new BackendPTARange(
                        // <code>i.object</code> must either be a local or static
                        new CodeFieldDereference(i.object), interpreter, rm,
                        typeRangeMap);
                BackendPTAArrayVariable atv = (BackendPTAArrayVariable)tv;
                atv.registerVariableRange(range);
                // register a proposal
                if(i.object.proposal != null)
                    atv.registerVariableRangeProposal(i.object.proposal);
                // create the indexed element
                AbstractPTAValue index = getValue(i.index.value, rm);
                if(index instanceof PTAConstant) {
                    long v = (long)((PTAConstant)index).value;
                    if(v < 0 || v >= atv.v().length)
                        throw new PTAException(null,
                                "index out of range: " + v + ", length of array " +
                                atv.v().name + " is " + atv.v().length); 
                }
                tv = new BackendPTAElementVariable(atv.getStreamPos(), atv.container,
                        new PTAElementVariable(atv.v(), index,
                            tv.v().range, tv.v().floating));
            }
        }
        return tv;
    }
    /**
     * Removes a PTA variable. Called if the variable
     * is no longer needed.
     * 
     * @param v                         variable to remove
     */
    public void removeVariable(AbstractBackendPTAVariable v) {
        compilation.removeVariable(v);
    }
    /**
     * If the code value is a dereference, then a
     * respective PTA variable is returned.<br>
     * 
     * If the code value is a constant, then a respective new
     * PTA constant is generated. The constant must be one of
     * the integer types or boolean, otherwise a runtime
     * exception is thrown.<br>
     * 
     * If a PTA variable is non--existent, it is created. For
     * more information about getting PTA variables, see
     * getVariable().<br>
     * 
     * If the PTA variable can not be determined because of
     * a null value, PTA exception is thrown. 
     * 
     * @param value                     code value
     * @param rm                        current runtime method
     * @return                          PTA value
     * @see #getVariable
     */
    public AbstractPTAValue getValue(AbstractCodeValue value, RuntimeMethod rm)
            throws PTAException {
        if(value instanceof AbstractCodeDereference) {
            AbstractBackendPTAVariable v = getVariable((AbstractCodeDereference)value, rm);
            if(v == null)
                throw new PTAException(value.getStreamPos(), "null dereference");
            else
                return v.v();
        } else {
            CodeConstant c = (CodeConstant)value;
            Type type = c.value.type;
            if(type.isAnnotation()) {
                Literal.AnnotationValue v = c.value.getAnnotationValue();
                if(!v.constArgs)
                    throw new RuntimeException("literals with variable arguments " +
                            "not allowed, because optimizing treates them as constants");
                CompilerAnnotation annotation = type.getAnnotation();
                PTADensity.ParameterType densityType = PTADensity.getType(annotation);
                if(densityType == null)
                    throw new RuntimeException("unknown annotation constant");
                int numArgs = PTADensity.getNumArgs(densityType);
                if(v.args.size() != numArgs)
                    throw new PTAException(value.getStreamPos(),
                            "expected " + numArgs + " argument" +
                            (numArgs != 1 ? "s" : "") + ", found " +
                            v.args.size());
                PTARange range = new PTARange(c.getStreamPos(), v.range);
                if(v.constArgs) {
                    double[] params = new double[numArgs];
                    for(int i = 0; i < numArgs; ++i)
                        params[i] = (Double)v.args.get(i);
                    return new PTADensity(densityType, range, params);
                } else {
                    AbstractPTAValue[] params = new AbstractPTAValue[numArgs];
                    for(int i = 0; i < numArgs; ++i)
                        params[i] = (AbstractPTAValue)v.args.get(i);
                    return new PTADensity(densityType, range, params);
                }
            } else {
                double v;
                if(type.isOfIntegerTypes() || type.isOfFloatingPointTypes())
                    v = c.value.getMaxPrecisionFloatingPoint();
                else if(type.isBoolean())
                    v = booleanToInteger(c.value.getBoolean());
                else
                    throw new PTAException(value.getStreamPos(),
                            "can not create a constant " +
                            "from code value of the type " + type.toNameString());
                return new PTAConstant(v, type.isOfFloatingPointTypes());
            }
        }
    }
//    /**
//     * Registers runtime variables, if not already registered,
//     * including their initial values.<br>
//     * 
//     * The variable must be of a type allowed by getVariable(),
//     * otherwise an exception is thrown.
//     * 
//     * @param list                      list of references to
//     *                                  the variables
//     * @param rm                        current runtime method
//     */
//    public void registerVariables(List<AbstractCodeDereference> list,
//            RuntimeMethod rm) throws PTAException {
//        for(AbstractCodeDereference d : list)
//            getVariable(d, rm);
//    }
    /**
     * Registers runtime variables, if not already registered,
     * including their initial values.<br>
     * 
     * The variables must be of a type allowed by getVariable(),
     * otherwise an exception is thrown.
     * 
     * @param p PTA, whose variables to register
     * @param rm the runtime method, frm which <code>p</code> is
     * constructed
     */
//    public void registerVariables(PTA p, RuntimeMethod rm) throws PTAException {
//        List<AbstractCodeDereference> list = new LinkedList<>();
//        for(PTAState s : p.states.values()) {
//            s.clockLimitHigh.
//        }
//        for(AbstractCodeDereference d : list)
//            getVariable(d, rm);
//    }
    /**
     * Checks if there is a previous transition that should be connected
     * to the next one, and if yes, connects the previous transition to
     * the current one.<br>
     * 
     * This is a standard call before an operation is translated to
     * transitions, unless some special optimization is used, like it is in
     * <code>StandardTADDGenerator.generate(CodeOpJump, TADDGeneratorContext)</code>.
     * 
     * @param context                   context of the PTA generator
     */
    public void checkConnectPrevious(TADDGeneratorContext context) {
        if(connectToNext(context.prevTransition))
            context.prevTransition.connectTargetIndex = context.currIndex;
    }
    /**
     * Adds miscelanous information from he source, like the operation
     * and its index in the method translated to a PTA.
     * 
     * If the transition's index is already known, it is then not modified by this method.
     * 
     * @param tt transition to add the information to
     * @param context context
     * @param op operation, from which originates <code>tt</code>
     */
    public static void addSourceInfo(PTATransition tt,
            TADDGeneratorContext context, AbstractCodeOp op) {
        tt.backendOps.add(op);
        if(tt.backendOpIndex == -1)
            tt.backendOpIndex = context.opIndex;
    }
    /**
     * Adds miscelanous information from he source, like the operation
     * and its index in the method translated to a PTA.<br>
     * 
     * If the transition's index is already known, it is then not modified by this method.<br>
     * 
     * This is a convenience method.
     * 
     * @param tt transition to add the information to
     * @param context context
     * @param ops a list of operations, from which originates <code>tt</code>
     */
    public static void addSourceInfo(PTATransition tt,
            TADDGeneratorContext context, List<AbstractCodeOp> ops) {
        tt.backendOps.addAll(ops);
        if(tt.backendOpIndex == -1)
            tt.backendOpIndex = context.opIndex;
    }
    /**
     * This method  generates a primary range--checking sub--automaton, that begins at
     * <code>context.index</code>; if the range is null, this method does nothing.
     * This method should be called for every ranged value within an operation translated
     * to transitions.<br>
     *
     * Note: the previous operation should already be connected, see
     * <code>checkConnectPrevious()</code> for details.<br>
     *
     * Note: if this operation generates any states, <code>context.index</code>
     * is updated, so that the caller can use it normally as the beginning of the
     * sub--automaton it generates after the call to <code>generateRange</code>.
     * This operation ends at <code>context.index</code>. If it should be disconnected,
     * though, for example as there is not code going to be generated at
     * <code>context.index</code> by the caller,
     * then set <code>context.prevTransition.targetIndex = -1</code> after
     * this method exits.
     *
     * @param rv                        ranged code value
     * @param op                        operation for which the range is
     *                                  generated
     * @param context                   generator's context
     * @return                          if any range checking sub--automaton
     *                                  has been generated
     */
    public boolean processPrimaryRange(RangedCodeValue rv, AbstractCodeOp op,
            TADDGeneratorContext context) throws PTAException {
        boolean generated = false;
        CodePrimaryPrimitiveRange range = rv.range;
        if(context.generator.options.rangeChecking && range != null) {
            // an error state
            int errorIndex = context.factory.newStateIndex();
            //
            // test min
            //
            int minOkIndex = context.factory.newStateIndex();
            PTATransition ttErrorMin = new PTATransition(null);
            PTATransition ttOkMin = new PTATransition(null);
            addSourceInfo(ttErrorMin, context, op);
            addSourceInfo(ttOkMin, context, op);
            ttErrorMin.guard = new PTABinaryExpression(
                    getValue(rv.value, context.rm),
                    getValue(range.min, context.rm),
                    CodeOpBinaryExpression.Op.BINARY_LESS);
            setStop(ttErrorMin.guard, context);
            if(!_SELFLOOPS) {
                PTAAssignment aMin = new PTAAssignment(
                        compilation.errorVariable, new PTAConstant(
                        compilation.newErrorCode(), false));
                context.generator.setStop(aMin, context);
                ttErrorMin.update.add(aMin);
            }
            ttErrorMin.connectTargetIndex = errorIndex;
            ttOkMin.guard = new PTABinaryExpression(
                    getValue(range.min, context.rm),
                    getValue(rv.value, context.rm),
                    CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL);
            setStop(ttOkMin.guard, context);
            ttOkMin.connectTargetIndex = minOkIndex;
            context.factory.addTransition(context.currIndex, ttErrorMin);
            context.factory.addTransition(context.currIndex, ttOkMin);
            //
            // test max
            //
            int rangeOkIndex = context.factory.newStateIndex();
            PTATransition ttErrorMax = new PTATransition(null);
            PTATransition ttOkMax = new PTATransition(null);
            addSourceInfo(ttErrorMax, context, op);
            addSourceInfo(ttOkMax, context, op);
            ttErrorMax.guard = new PTABinaryExpression(
                    getValue(range.max, context.rm),
                    getValue(rv.value, context.rm),
                    CodeOpBinaryExpression.Op.BINARY_LESS);
            setStop(ttErrorMax.guard, context);
            if(!_SELFLOOPS) {
                PTAAssignment aMax = new PTAAssignment(
                        compilation.errorVariable, new PTAConstant(
                        compilation.newErrorCode(), false));
                context.generator.setStop(aMax, context);
                ttErrorMax.update.add(aMax);
            }
            ttErrorMax.connectTargetIndex = errorIndex;
            ttOkMax.guard = new PTABinaryExpression(
                    getValue(rv.value, context.rm),
                    getValue(range.max, context.rm),
                    CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL);
            setStop(ttOkMax.guard, context);
            ttOkMax.connectTargetIndex = rangeOkIndex;
            context.factory.addTransition(minOkIndex, ttErrorMax);
            context.factory.addTransition(minOkIndex, ttOkMax);
            if(_SELFLOOPS) {
                //
                // set error code
                //
                PTATransition ttError = new PTATransition(null);
                PTAAssignment a = new PTAAssignment(
                        compilation.errorVariable, new PTAConstant(
                        compilation.newErrorCode(), false));
                context.generator.setStop(a, context);
                ttError.update.add(a);
                addSourceInfo(ttError, context, op);
                // self--loop SELFLOOP
                ttError.connectTargetIndex = errorIndex;
                context.factory.addTransition(errorIndex, ttError);
            } else
                // add a state, which lacks output transitions
                context.factory.customStates.put(errorIndex, new PTAState(errorIndex));

            // all connected, not needed
            context.prevTransition = ttOkMax;
            context.currIndex = rangeOkIndex;
            generated = true;
        }
        return generated;
    }
    /**
     * If the parameter is a transition with undefined target
     * index, what means that the transition should be connected to
     * a next transition. For a null parameter this method
     * returns false.
     * 
     * @param t                         transition, can be null
     * @return                          if should be connected
     *                                  to the next transition
     */
    public boolean connectToNext(PTATransition t) {
        return t != null && t.connectTargetIndex == -1;
    }
    /**
     * If result of this operation is only used at most in the next
     * operation. Because of simplified analysis directed at temporary
     * variables of conditions, it can give false negatives, but
     * always true positives.
     * 
     * @param context translation context
     * @param index                     index of the operation whose
     *                                  result is analyzed, the result
     *                                  must be local or false is returned
     * @return                          if used at most in the next operation,
     *                                  can give false negatives
     */
    protected boolean resultUsedAtMostInNext(TADDGeneratorContext context, int index) {
        AbstractCodeDereference result = context.method.code.ops.
                get(index).getResult();
        if(result.isVolatile())
            return false;
        else {
            // result is not volatile, so it is just a local variable
            //
            // possibly a false positive, if the next operation assigns to <code>result</code>
            // as well
            return !context.transport.isTransported((index + 1)*2 + 1,
                    (CodeFieldDereference)result);
        }
    }
    /**
     * Reports an instance of complementary label.<br>
     *
     * // NotifyAll without broadcasts build special trees of transitions,
     * // which need to be complete, thus, this case is not reported.
     * 
     * @param tt                        transition containing the label
     *                                  instance to report
     * @param ownerName                 name of the transitions owner;
     *                                  null if <code>tt.ownerName</code>
     *                                  should be left as it is
     * 
     * @see ComplementaryLabel
     */
    public void reportComplementaryLabel(PTATransition tt, String ownerName)
            throws PTAException {
        if(ownerName != null) {
            if(tt.ownerName != null && !tt.ownerName.equals(ownerName))
                throw new RuntimeException("name mismatch: `" + tt.ownerName +
                        "' `" + ownerName + "'");
            tt.ownerName = ownerName;
        }
        PTALabel label = tt.label;
        ComplementaryLabel complementary =
                labels.complementary.get(label.name);
        if(complementary == null) {
            complementary = new ComplementaryLabel(label, options);
            labels.complementary.put(label.name, complementary);
        }
        switch(label.type) {
            case DIRECTIONAL:
                throw new RuntimeException("can not be called on label declaration");

            case SENDER:
                complementary.reportActive(tt);
                break;

            case RECEIVER:
                complementary.reportPassive(tt);
                break;

            case BARRIER:
            case FREE:
                throw new RuntimeException("label not directional");

            default:
                throw new RuntimeException("unknown type");
        }
    }
    /**
     * Purges transitions of complementary labels that are not
     * complemented, and also solitary barrier labels.
     * Then, dead states are deleted, as these are likely to
     * appear after the purge.<br>
     *
     * Only complementary labels registered using
     * <code>reportComplementaryLabel</code>
     * are purged. See that method's docs for details on which labels are
     * registered.
     *
     * Called after all TADDs are generated.
     */
    @SuppressWarnings("empty-statement")
    void purgeNotComplemented() throws PTAException {
        for(ComplementaryLabel c : labels.complementary.values())
            if(!c.isComplemented() /* &&
                    !(c.broadcast && !options.broadcastLabelBlocks) */)
                for(PTATransition tt : c.transitions)
                    for(PTA tadd : compilation.ptas.values())
                        if(tadd.transitions.contains(tt)) {
                            ((BackendPTA)tadd).removeTransition(tt);
                            break;
                        }
        for(PTA tadd : compilation.ptas.values())
            if(PTAOptimizer.deleteDeadStates(tadd))
                ;
    }
    /**
     * Purges transitions of solitary barrier labels.
     * Then, dead states are deleted, as these are likely to
     * appear after the purge. <br>
     * 
     * Called after all TADDs are generated. Atomicity of
     * barriers must be checked before this method is
     * called.
     */
    @SuppressWarnings("empty-statement")
    void purgeLoneBarriers() throws PTAException {
        for(Entry<String, BarrierLabels> e : labels.barriers.entrySet()) {
            Set<String> existsIn = new HashSet<>();
            for(PTATransition t : e.getValue().transitions)
                existsIn.add(t.ownerName);
            if(existsIn.size() == 1) {
                // the barrier label has been found only in a single PTA
                PTA tadd = compilation.ptas.get(existsIn.iterator().next());
                for(PTATransition t : e.getValue().transitions)
                    if(tadd.transitions.contains(t))
                        ((BackendPTA)tadd).removeTransition(t);
            }
        }
        for(PTA tadd : compilation.ptas.values())
            if(PTAOptimizer.deleteDeadStates(tadd))
                ;
    }
    /**
     * Adds all variables in <code>expr</code>, and
     * variables required by these variables, to <code>used</code>.
     * 
     * @param used set of variables to complete
     * @param expr expression to search for variables, null for none
     * @param includeWrittenElements when an element is written to, then add not
     * only the element's array, but also one element variables that
     * possibly are written to
     */
    void addUsed(Set<AbstractPTAVariable> used,
            AbstractPTAExpression expr, boolean includeWrittenElements) {
        if(expr != null) {
            List<AbstractPTAVariable> list = new LinkedList<>();
            list.addAll(expr.getVariables());
            for(AbstractPTAVariable v : list) {
                // element variables should be added only when written to;
                // otherwise, only their arrays are added
                if(!(v instanceof PTAElementVariable))
                    used.add(v);
                used.addAll(v.getKnownSources());
            }
            for(AbstractPTAVariable v : list)
                if(v instanceof PTAElementVariable) {
                    PTAElementVariable e = (PTAElementVariable)v;
                    used.add(e.array);
                    if(includeWrittenElements && e.equals(expr.target)) {
                        int indexLow;
                        int indexHigh;
                        if(e.index instanceof PTAConstant) {
                            indexHigh = indexLow = (int)((PTAConstant)e.index).value;
                        } else if(e.index instanceof AbstractPTAVariable){
                            AbstractPTAVariable i = (AbstractPTAVariable)e.index;
                            // variable index of an element variable is added earlier
                            // by <code>used.addAll(v.getSources())</code>
                            indexLow = Math.max(0, (int)i.range.getMin());
                            indexHigh = Math.min(e.array.length - 1, (int)i.range.getMax());
                        } else
                            throw new RuntimeException("unexpected index type");
                        for(int index = indexLow; index <= indexHigh; ++index)
                            used.add(new PTAElementVariable(e.array, new PTAConstant(index, false),
                                    new PTARange(e.range), e.floating));
                    }
                }
        }
    }
    /**
     * Returns used variables.
     * 
     * @param includeWrittenElements if to add element variables which are written to;
     * declarations of these variables are not added by this generator, as it uses array
     * variables instead, and thus, for <code>removeUnusedVariables</code>, this
     * parameter can be false
     * @return a set of used variables
     */
    public Set<AbstractPTAVariable> getUsedVariables(boolean includeWrittenElements) {
        Set<AbstractPTAVariable> used = new HashSet<>();
        for(PTA p : compilation.ptas.values()) {
            for(PTAState s : p.states.values()) {
                addUsed(used, s.clockLimitHigh, includeWrittenElements);
            }
            for(PTATransition t : p.transitions) {
                addUsed(used, t.clockLimitLow, includeWrittenElements);
                addUsed(used, t.guard, includeWrittenElements);
                addUsed(used, t.probability, includeWrittenElements);
                for(AbstractPTAExpression u : t.update)
                    addUsed(used, u, includeWrittenElements);
            }
        }
        return used;
    }
    /**
     * Removes variables from the compilation, that are not used
     * by the PTAs within that compilation.
     */
    void removeUnusedVariables() {
        Set<AbstractPTAVariable> used = getUsedVariables(false);
        Map<AbstractRuntimeContainer, Map<CodeVariable,AbstractBackendPTAVariable>>
                newVariables = new HashMap<>();
        for(AbstractRuntimeContainer c : compilation.variables.keySet()) {
            Map<CodeVariable, AbstractBackendPTAVariable> set =
                    compilation.variables.get(c);
            Map<CodeVariable, AbstractBackendPTAVariable> newSet =
                    newVariables.get(c);
            for(CodeVariable cv : set.keySet()) {
                AbstractBackendPTAVariable v = set.get(cv);
                if(used.contains(v.v())) {
                    if(newSet == null) {
                        newSet = new HashMap<>();
                        newVariables.put(c, newSet);
                    }
                    newSet.put(cv, v);
                }
            }
        }
        compilation.variables = newVariables;
    }
    /**
     * Returns the join label, for either JOIN operation or an end of
     * thread.
     * 
     * @param endThread                 thread that ends
     * @param joinThread                thread that calls join()
     * @param join                      true if the label is for JOIN operation,
     *                                  false if it is for an end of a thread
     */
    public PTALabel getJoinLabel(RuntimeThread endThread, RuntimeThread joinThread,
            boolean join) {
        try {
            return new PTALabel("join_" + endThread.name + "_" + joinThread.name,
                    join ? PTALabel.Type.SENDER : PTALabel.Type.RECEIVER, false);
        } catch(PTAException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    /**
     * Returns all locks' wait variables.
     * 
     * @return a list of variables, empty for none
     */
    public List<AbstractBackendPTAVariable> getLockWaitVariables() {
        List<AbstractBackendPTAVariable> v = new LinkedList<>();
        for(TADDLock lock : compilation.locks.values()) {
            AbstractBackendPTAVariable w = compilation.getVariable(
                    lockVariables, lockVariables.variablesByName.get(TADDLock.
                    getWaitCountVariableName(lock)));
            if(w != null)
                v.add(w);
        }
        return v;
    }
    /**
     * Sets ranges of wait count variables, once the locks are constructed.
     */
    protected void setLockVariablesRanges() {
        for(TADDLock lock : compilation.locks.values()) {
            AbstractBackendPTAVariable v = compilation.getVariable(
                    lockVariables, lockVariables.variablesByName.get(TADDLock.
                    getWaitCountVariableName(lock)));
            if(v != null)
                BackendPTACompilation.setMax(v.v().range.range, lock.waitTransitions);
        }
    }
    /**
     * Determines <code>AbstractPTAExpression.backendStop</code>. Must
     * be called on each expressions, that ends up in the PTA.
     * 
     * @param expr expression to set the transfer stop; scalar variables within the
     * expression must step from a code compilation, so e. g. not the special error
     * variable
     * @param context context
     */
    protected void setStop(AbstractPTAExpression expr,
            TADDGeneratorContext context) throws PTAException {
        expr.backendEnableStop();
        //
        // get all local variables
        //
        List<AbstractPTAVariable> variables = new LinkedList<>(
                context.localScalarVariables);
        //
        // write to the stop only the not transported ones
        //
        for(AbstractPTAVariable v : variables)
            if(v instanceof PTAScalarVariable) {
                PTAScalarVariable s = (PTAScalarVariable)v;
                CodeVariable c = compilation.codeScalars.get(s);
                if(c == null)
                    throw new RuntimeException("no code scalar found");
                if(c.isLocal() &&
                        !context.transport.isTransported(context.opIndex*2 + 1,
                        new CodeFieldDereference(c)))
                    expr.backendAddStop(v);
            }
    }
    /**
     * Generates transitions for the assignment operation.
     *
     * @param a                         operation for which the transitions
     *                                  should be generated
     * @param context                   context
     */
    abstract void generate(CodeOpAssignment a, TADDGeneratorContext context) throws PTAException;
    /**
     * Generates transitions for the jump operation.
     *
     * @param j                         operation for which the transitions
     *                                  should be generated
     * @param context                   context
     */
    abstract void generate(CodeOpJump j, TADDGeneratorContext context) throws PTAException;
    /**
     * Generates transitions for the unary or binary expression operations.
     *
     * @param e                         operation for which the transitions
     *                                  should be generated, either
     *                                  <code>CodeOpUnaryExpression</code> or
     *                                  <code>CodeOpBinaryExpression</code>
     * @param context                   context
     */
    abstract void generateExpression(AbstractCodeOp e, TADDGeneratorContext context)
            throws PTAException;
    /**
     * Generates transitions for the branch operation.
     *
     * @param j                         operation for which the transitions
     *                                  should be generated
     * @param context                   context
     */
    abstract void generate(CodeOpBranch j, TADDGeneratorContext context) throws PTAException;
    /**
     * Generates transitions for the synchronize operation.
     *
     * @param s                         operation for which the transitions
     *                                  should be generated
     * @param context                   context
     */
    abstract void generate(CodeOpSynchronize s, TADDGeneratorContext context) throws PTAException;
    /**
     * Generates transitions for the none operation.
     *
     * @param n                         operation for which the transitions
     *                                  should be generated
     * @param context                   context
     */
    abstract void generate(CodeOpNone n, TADDGeneratorContext context) throws PTAException;
    /**
     * Generates transitions for the return operation.
     *
     * @param r                         operation for which the transitions
     *                                  should be generated
     * @param context                   context
     */
    abstract void generate(CodeOpReturn r, TADDGeneratorContext context) throws PTAException;
    /**
     * Generates transitions for the allocation operation.
     *
     * @param a                         operation for which the transitions
     *                                  should be generated
     * @param context                   context
     */
    abstract void generate(CodeOpAllocation a, TADDGeneratorContext context) throws PTAException;
    /**
     * Generates transitions for the throw operation.
     *
     * @param t                         operation for which the transitions
     *                                  should be generated
     * @param context                   context
     */
    abstract void generate(CodeOpThrow t, TADDGeneratorContext context) throws PTAException;
    /*
     * Finds all field dereferences in the operation, then tries to find actual
     * runtime fields. If not possible, reports an error, otherwise, modifies
     * respective values of variables from <code>object</code> parts of the field
     * dereferences so that they point to correct runtime fields.<br>
     *
     * Note that static fields have the <code>object</code> part equal to null,
     * and so this method does not  need to do anything to them.<br>
     *
     * @param op                        operation, in which to search for
     *                                  runtime fields
     * @param sa                        runtime static analysis, for
     *                                  finding unambiguous runtime fields
     *                                  pointed by dereferences or for
     *                                  reporting ambiguous dereferences
     */
    /*
    protected void determineRuntimeFields(AbstractCodeOp op, RuntimeStaticAnalysis sa,
            TADDGeneratorContext context) throws PTAException {
        Collection<CodeFieldDereference> all = op.getFieldSources();
        all.addAll(op.getFieldTargets());
        for(CodeFieldDereference d : all)
            if(d.object != null) {
                // non--static field, needs runtime value of <code>object<code>
if(d.toString().equals("#default.B i#0#default.B_test__#this::int sh"))
    d = d;
                RuntimeField rf = sa.getRuntimeField(context.rm,
                        context.index, d);
if(rf == null)
    rf = rf;
                if(rf.object != null) {
                    try {
                        interpreter.setValue(new CodeFieldDereference(d.object),
                                rf.object, context.rm);
                    } catch(InterpreterException e) {
                        throw new RuntimeException("unexpected exception " + e.toString());
                    }
                } else
                    throw new PTAException("ambiguous dereference of non--static field " +
                            d.toNameString() + " at " + op.getStreamPos().toString());
        }
     }
     */
    /**
     * Finds all source local java type dereferences in the operation, then tries
     * to find actual values of these sources. If not possible, reports an error,
     * otherwise, modifies respective values of the locals.<br>
     *
     * This way, any non--ambiguous object reference read in the operation
     * should be resolvable, as
     * its object parts should either be a Java type field, thus assumed
     * constant by PTA threads, or a local, with proper value written to in
     * this method.
     *
     * @param op                        operation, in which to search for
     *                                  source locals
     * @param sa                        runtime static analysis, for
     *                                  finding values of locals
     * @param context                   context, it is sufficient if it contains
     *                                  only <code>rm</code> and <code>opIndex</code>
     */
    protected void determineRuntimeJavaTypeLocals(
            RuntimeMethod rm, AbstractCodeOp op, RuntimeStaticAnalysis sa,
            TADDGeneratorContext context) throws PTAException {
        Collection<CodeVariable> sources = op.getLocalSources();
        for(CodeVariable local : sources)
            // a local has an unknown runtime type when this method is called,
            // so try to trace it to a field
            if(local.type.isJava()) {
                //if(interpreter.getCodeClass(local.type).isEqualToOrExtending(superClass)
                AbstractCodeValue value = null;
                CodeFieldDereference d = new CodeFieldDereference(local);
                if(op instanceof CodeOpNone) {
                    CodeOpNone n = (CodeOpNone)op;
                    if(n.calledMethod.annotations.contains(CompilerAnnotation.RANDOM_STRING)) {
                        Set<AbstractCodeValue> alt = sa.getTrace(context.rm).getValuesM1(
                                context.opIndex - 1, d);
                        if(alt.size() == 1 && alt.iterator().next() instanceof CodeConstant)
                            value = alt.iterator().next();
                        else {
//                           try {
                                // this annotation marks a special method whose owner is unimportant, and
                                // thus possible ambiguity is not a problem
                                continue;
//                                n.args.add(
//                                        new RangedCodeValue(new CodeConstant(n.getStreamPos(), 
//                                                interpreter.getValue(n.getThisArg(), context.rm))));
//                                n.args.remove(0);
//                           } catch(InterpreterException e) {
//                           }
                        }
                    }
                }
                if(value == null) {
                    value = sa.getTrace(context.rm).getSingleM1(
                            context.opIndex - 1, d);
                }
                /*
                while(value instanceof CodeFieldDereference) {
                    value = interpreter.getValue(new RangedCodeValue((CodeFieldDereference)value),
                        context.rm);
                }
                 */
                if(value instanceof CodeConstant) {
                    CodeConstant c = (CodeConstant)value;
                    try {
                        // store
                        interpreter.setValue(d, new RuntimeValue(c.value), context.rm);
                    } catch(InterpreterException e) {
                        throw new RuntimeException("unexpected exception " + e.toString());
                    }
                } else if(value instanceof CodeIndexDereference) {
                    // trace to an array's element
                    CodeIndexDereference i = (CodeIndexDereference)value;
                    try {
                        CodeFieldDereference array = new CodeFieldDereference(i.object);
                        AbstractCodeValue object = sa.getConstant(rm,
                                context.opIndex, array);
                        if(object instanceof CodeConstant) {
                            ArrayStore store = (ArrayStore)((RuntimeValue)((CodeConstant)object).value).
                                getReferencedObject();
                            if(store == null)
                                throw new PTAException(op.getStreamPos(),
                                        "dereference on a null array");
                            if(i.index.value instanceof CodeConstant) {
                                CodeConstant c = (CodeConstant)i.index.value;
                                int index = CompilerUtils.toInt(
                                        c.value.getMaxPrecisionInteger());
                                RuntimeValue element = store.getElement(
                                        interpreter, rm, null, index);
                                try {
                                    // store
                                    interpreter.setValue(d, element, context.rm);
                                } catch(InterpreterException e) {
                                    throw new RuntimeException("unexpected exception " + e.toString());
                                }
                            } else
                                throw new PTAException(op.getStreamPos(),
                                        "an array of references indexed with a variable");
                        } else
                            throw new PTAException(op.getStreamPos(),
                                    "can not determine an array instance");
                    } catch(InterpreterException e) {
                        throw new PTAException(op.getStreamPos(),
                                "in interpreter: " + e.getMessage());
                    }
                } else if(op instanceof CodeOpThrow) {
                    // the reference is not necessary for generation of the throw code;
                    // if easily traceable, then the code is decorated with respective
                    // information in <code>GraphTADDGenerator.generate(CodeOpThrow,
                    // TADDGeneratorContext)</code>
                    /* ignore */
                } else
                    // do not use reference's name, as value propagation could
                    // replace the original reference
                    throw new PTAException(op.getStreamPos(),
                            "ambiguous dereference when called from " +
                            context.method.owner.name + "." +
                            context.method.getDeclarationDescription(true));
            }
    }
    /**
     * Returns all element variables from a set of different variables.
     * 
     * @param all an input set of variables
     * @return a subset containing only element variables
     */
    Set<PTAElementVariable> selectElementVariables(Set<AbstractPTAVariable> all) {
        Set<PTAElementVariable> out = new HashSet<>();
        for(AbstractPTAVariable v : all)
            if(v instanceof PTAElementVariable)
                out.add((PTAElementVariable)v);
        return out;
    }
    /**
     * <p>Creates a new PTA and generates its transitions on basis of
     * a method. Can also create or extend lock TADDs if needed.</p>
     * 
     * <p>The method must have inlined any required called method. Any
     * call and allocation operations in the method cause an error.</p>
     * 
     * <p>This method also detects the null pointer and invalid class cast detections,
     * which were previously ignored, when object instance--related control flow was
     * yet in place, and thus it was not sure, if such exceptions would actually happen
     * in a real flow of control.</p>
     * 
     * @param threads                   threads of all non--lock
     *                                  TADDs
     * @param thread                    thread whose PTA to create
     * @param method                    code method
     * @param sa                        runtime static analysis, for
     *                                  finding unambiguous runtime fields
     *                                  pointed by dereferences or for
     *                                  reporting ambiguous dereferences
     * @param transport                 transport for variables in
     *                                  <code>method</code>
     * @param scheduler                 scheduler
     * @return                          the PTA generated
     */
    PTA generateTransitions(List<RuntimeThread> threads,
            RuntimeThread thread, RuntimeMethod rm,
            RuntimeStaticAnalysis sa, CodeVariableTransport transport)
                throws PTAException {
// if(rm.method.owner.name.contains("Link"))
//     rm = rm;
        TADDGeneratorContext context = new TADDGeneratorContext();
        context.generator = this;
        context.threads = threads;
        context.thread = thread;
        context.name = thread.name;
        context.method = rm.method;
        context.transport = transport;
        context.rm = rm;
        context.factory = new TADDFactory();
        context.clock = null;
        if(options.scheduler.usesClock())
            context.createClock();
        boolean implicitHidden = options.scheduler.type ==
                SchedulerType.HIDDEN && options.scheduler.implicit;
        context.locks = new Stack<>();
        context.localScalarVariables = new LinkedList<>();
        PTAException errors = new PTAException();
        for(CodeVariable cv : context.rm.method.variables.values())
            if(cv.type.isPrimitive()) {
                try {
                    // a local and a scalar -- no setting of runtime values
                    // in the interpreter required
                    AbstractBackendPTAVariable bv = getVariable(
                            new CodeFieldDereference(cv), context.rm);
                    if(bv instanceof BackendPTAScalarVariable)
                        context.localScalarVariables.add(bv.v());
                } catch(PTAException e) {
                    errors.addAllReports(e);
                }
            }
        if(errors.reportsExist())
            throw errors;
        // reports of recoverable errors with a completed position
        Set<PTAException.Report> reportsCompleted = new HashSet<>();
        SCAN:
        for(int index = 0; index < context.method.code.ops.size(); ++index) {
            context.opIndex = index;
            context.currIndex = context.opIndex;
            AbstractCodeOp op = context.method.code.ops.get(index);
            try {
/*if(op.toString().indexOf("#this = #1002 -> #default.Fork#10") != -1)
    op = op;*/
/*if(index == 34 && op instanceof CodeOpAssignment)
    index = index;*/
                determineRuntimeJavaTypeLocals(rm, op, sa, context);
                //determineRuntimeFields(op, sa, context);
                if(op instanceof CodeOpAssignment) {
                    CodeOpAssignment a = (CodeOpAssignment)op;
                    generate(a, context);
                } else if(op instanceof CodeOpJump) {
                    CodeOpJump j = (CodeOpJump)op;
                    generate(j, context);
                } else if(op instanceof CodeOpBinaryExpression ||
                        op instanceof CodeOpUnaryExpression) {
                    generateExpression(op, context);
                } else if(op instanceof CodeOpBranch) {
                    CodeOpBranch b = (CodeOpBranch)op;
                    generate(b, context);
                } else if(op instanceof CodeOpSynchronize) {
                    CodeOpSynchronize s = (CodeOpSynchronize)op;
                    generate(s, context);
                } else if(op instanceof CodeOpNone) {
                    CodeOpNone n = (CodeOpNone)op;
                    generate(n, context);
                } else if(op instanceof CodeOpReturn) {
                    CodeOpReturn r = (CodeOpReturn)op;
                    generate(r, context);
                } else if(op instanceof CodeOpAllocation) {
                    CodeOpAllocation a = (CodeOpAllocation)op;
                    generate(a, context);
                } else if(op instanceof CodeOpThrow) {
                    CodeOpThrow t = (CodeOpThrow)op;
                    generate(t, context);
                } else
                    throw new PTAException(op.getStreamPos(),
                            "illegal operation in the TADD part: " + op.toString());
                if(reportsCompleted.size() < recoverableErrors.getReports().size()) {
                    for(PTAException.Report e : recoverableErrors.getReports())
                        if(!reportsCompleted.contains(e)) {
                            e.completePos(op.getStreamPos());
                            reportsCompleted.add(e);
                        }
                }
            } catch(PTAException e) {
                e.completePos(op.getStreamPos());
                recoverableErrors.addAllReports(e);
                // no recoverable error, break the scan
                break SCAN;
            } catch(InterpreterFatalException e) {
                // the message prefix means, that the PTA threads are not really
                // executed, and thus local values are unknown
                PTAException f = new PTAException(op.getStreamPos(),
                        "run of the main thread: " +
                        e.getMessage());
                recoverableErrors.addAllReports(f);
                // no recoverable error, break the scan
                break SCAN;
            }
        }
        if(recoverableErrors.reportsExist()) {
            for(CompilerException.Report r : recoverableErrors.getReports()) {
                // remove needless parts of the message of
                // <code>InterpreterFatalException</code>
                r.message = r.message.replace(
                        "unexpected error: pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException: ",
                        "");
                // add thread information
                r.message = "in thread " + context.name + ": " + r.message;
            }
            throw recoverableErrors;
        }
        PTA tadd = context.factory.newTADD(thread.object, context.name,
                getLockWaitVariables(), new PTAVariableTransport(
                    this, rm, transport).getTraced(),
                options.removeConstGuardsUsingRanges,
                implicitHidden);
        for(PTA pta : compilation.ptas.values())
            if(pta instanceof TADDLock)
                // locks can be modified by other automatons, update their labels
                context.factory.updateLabeled(pta);
        tadd.clock = context.clock;
        compilation.ptas.put(context.name, tadd);
        if(verbose)
            log(4, tadd.toString());
        return tadd;
    }
    /**
     * Replaces <code>CodeOpNone</code> with certain annotations and
     * optionally all arguments evaluated to constants. These ops are replaced
     * with assignments by literals with respective annotation constants, in order to allow more
     * streamlining being made by the following optimizations.<br>
     * 
     * The rest of this backend is aware of these specific annotation constants. The
     * constants currently are of the types <code>@DIST_*</code>. It is assumed
     * that return ranges of the marker method are always respected, so these
     * ranges are copied to literals and otherwise lost, thus, can be further used only
     * to create proposals.
     * 
     * @param rm runtime of the method to process
     * @param requireConstArgs if to require constant arguments for operations
     * to be changed into constants; if false, then
     * the replacement constants are not really constants, and thus no
     * optimizations are further allowed, that assume constants are relocatable;
     * normally, this argument should always be true
     */
    protected void replaceDistCalls(RuntimeStaticAnalysis sa, RuntimeMethod rm,
            boolean requireConstArgs) throws PTAException {
        int index = 0;
        CodeMethod method = rm.method;
        try {
            List<AbstractCodeOp> code = new LinkedList<>();
            for(AbstractCodeOp op : method.code.ops) {
                CodeOpAssignment replacement = null;
                if(op instanceof CodeOpNone) {
                    CodeOpNone n = (CodeOpNone)op;
                    CompilerAnnotation a = null;
                    SCAN:
                    for(String s : n.annotations) {
                        a = CompilerAnnotation.parse(s);
                        switch(a) {
                            case DIST_UNI:
                            case DIST_NXP:
                            case DIST_ARRAY:
                                break SCAN;

                            default:
                                a = null;
                        }
                    }
                    if(a != null) {
                        List<Double> args = new LinkedList<>();
                        List<Object> variableArgs = new LinkedList<>();
                        boolean constArgs = true;
                        for(RangedCodeValue r : n.args) {
                            if(r.value instanceof CodeConstant &&
                                    r.value.getResultType().numericPrecision() != 0)
                                args.add(((CodeConstant)r.value).value.getMaxPrecisionFloatingPoint());
                            else {
                                constArgs = false;
                                if(requireConstArgs)
                                    break;
                            }
                            AbstractPTAValue value = getValue(r.value, rm);
                            variableArgs.add(value);
                        }
                        if(constArgs || !requireConstArgs) {
                            Type.TypeRange range;
                            if(n.calledMethod.range != null) {
                                Literal min = n.calledMethod.range.evaluateMin(sa, rm, index,
                                        method.getThisArg());
                                Literal max = n.calledMethod.range.evaluateMax(sa, rm, index,
                                        method.getThisArg());
                                Type type = n.getResult().getResultType();
                                if(!type.isOfIntegerTypes() && !type.isOfFloatingPointTypes())
                                    throw new RuntimeException("literal-replaceable marker method " +
                                            "expected to return an arithmetic value");
                                if(min == null || max == null)
                                    throw new RuntimeException("can not evaluate range " +
                                            "of a literal-replaceable marker method");
                                if(min.type.numericPrecision() == 0 ||
                                        max.type.numericPrecision() == 0)
                                    throw new RuntimeException("can not evaluate range " +
                                            "of a literal-replaceable marker method to " +
                                            "arithmetic constants");
                                if(type.isOfIntegerTypes())
                                    range = new Type.TypeRange(
                                        min.getMaxPrecisionInteger(),
                                        max.getMaxPrecisionInteger());
                                else
                                    range = new Type.TypeRange(
                                        min.getMaxPrecisionFloatingPoint(),
                                        max.getMaxPrecisionFloatingPoint());
                            } else
                                range = null;
                            Literal l;
                            if(constArgs) {
                                Literal.AnnotationValue av = new Literal.AnnotationValue(
                                        args, range);
                                l = new Literal(a, av);
                            } else {
                                Literal.AnnotationValue av = new Literal.AnnotationValue(
                                        variableArgs, range, 0);
                                l = new Literal(a, av);
                            }
                            replacement = new CodeOpAssignment(n.getStreamPos());
                            replacement.label = n.label;
                            replacement.result = n.result;
                            replacement.addTags(n.getTags());
                            replacement.rvalue = new RangedCodeValue(
                                    new CodeConstant(n.getStreamPos(), l),
                                    // the return range could alternately be converted to a primary
                                    // range, but it is assumed to be correct, thus no checking
                                    // automaton required, see docs of this method for details
                                    null);
                        }
                    }
                }
                if(replacement != null)
                    code.add(replacement);
                else
                    code.add(op);
                ++index;
            }
            method.code.ops = code;
        } catch(InterpreterException | PTAException e) {
            e.completePos(method.code.ops.get(index).getStreamPos());
            if(e instanceof PTAException)
                // omit merging of reports
                throw (PTAException)e;
            else
                throw new PTAException(e);
        }
    }
//    /**
//     * Returns the number of users for each barrier.
//     * 
//     * @param object the barrier's object
//     * @return number of usages, 0 for no usages
//     */
//    public int getBarrierCount(RuntimeObject object) {
//        if(barrierCount.containsKey(object))
//            return barrierCount.get(object).size();
//        else
//            return 0;
//    }
    /**
     * Checks for validity of declaration of the number of users per barrier,
     * made using a method marked by the anotation BARRIER_ACTIVATE.
     */
    public void checkBarrierUsage() throws PTAException {
        for(RuntimeObject barrier : interpreter.barrierThreadsNum.keySet()) {
            String errorStr = interpreter.barrierThreadsNum.get(barrier).countMatches();
            if(errorStr != null)
                throw new PTAException(null, "barrier " + barrier.getUniqueName() + " "
                        + errorStr);
        }
    }
    /**
     * Finds used variables, removes unused ones.
     */
    void analyseVariableUsage() {
        removeUnusedVariables();
        compilation.usedVariables = getUsedVariables(options.replaceArrays
                != TADDGeneratorOptions.Arrays.INDEX);
    }
    /**
     * Unrolls loops.
     * 
     * @param maxValues passed to <code>Unroll()</code>
     * @param maxBodyUnrolled passed to <code>Unroll()</code>
     * @param rmList list of runtime methods to unroll
     * @param ciInfo information about constant indexing, null for none
     * @param sa runtime static analysis
     * @param arguments 
     * @return if any loop has been unrolled
     */
    private boolean unroll(int maxValues, int maxBodyUnrolled, List<RuntimeMethod> rmList,
            Map<RuntimeMethod, ConstantIndexingInfo> ciInfo,
            RuntimeStaticAnalysis sa) throws InterpreterException {
        boolean unrolled = false;
        Map<CodeVariable, AbstractCodeValue> arguments = new HashMap<>();
        // unrolling a variable range declaration may make it look constant,
        // while it is not
        Unroll unroll = new Unroll(maxValues, maxBodyUnrolled, true, false, true);
        for(RuntimeMethod rm : rmList)
            if(unroll.transform(rm.method, ciInfo.get(rm))) {
                unrolled = true;
                // no more needed, as contant indexing is now after
                // unrolling, and these optimizations were to streamline
                // the patterns, the indexing introduces
                if(false) {
                    // shorten the code using cheap optimizations
                    // System.out.print("u");System.out.flush();
                    boolean[] changed = new boolean[1];
                    TraceValues.trimDeadCode(rm.method, true, changed,
                            // no need for a complete trace
                            TraceValues.traceValuesWithCasts(
                                rm.method, arguments, new TraceValues.Options(
                                        interpreter, true)));
                    if(changed[0])
                        Optimizer.setLabelIndices(rm.method);
                    // optimizes out easy cases, produced by the unrolling,
                    // so that <code>optimizeAssignments</code>
                    // is faster
                    Optimizer.optimizeAssignmentsCheap(rm.method.code);
                    Optimizer.optimizeAssignments(rm.method.code);
                    Optimizer.optimizeJumpChains(rm.method.code);
                    Optimizer.setLabelIndices(rm.method);
                }
                //
                sa.traceRuntimeValues(rm);
                // propagate the now--constant index
// if(rm.getUniqueName().contains("Periodic.run"))
//     rm = rm;
                if(TraceValues.propagateValues(rm.method, arguments,
                        sa.getTrace(rm), null, interpreter, rm))
                    sa.traceRuntimeValues(rm);
                sa.analyse(rm);
// rm = rm;
            }
        return unrolled;
    }
    /**
     * Trims dead code.
     * 
     * @param method method to optimize
     * @return if anything has been optimized
     */
    private boolean trimDeadCode(CodeMethod method) {
        boolean[] changed = new boolean[1];
        TraceValues.trimDeadCode(method, true, changed,
                // no need for a complete trace
                TraceValues.traceValuesWithCasts(
                    method, new HashMap<CodeVariable, AbstractCodeValue>(),
                        new TraceValues.Options(interpreter, true)));
        if(changed[0]) {
            Optimizer.setLabelIndices(method);
            return true;
        } else
            return false;
    }
    /**
     * Inline.
     * 
     * @param optimizerTuning tuning options of the optimizer
     * @param threads threads to optimize
     * @param inlinedList list of methods inlined so far, for testing
     * recursion
     * @param instantiated passed to the constructor of <code>RuntimeCallResolution</code>
     * @return list of inlined runtime methods
     */
    private List<RuntimeMethod> inline(Optimizer.Tuning optimizerTuning,
            List<RuntimeThread> threads) throws PTAException {
        Set<CodeClass> instantiated =null;
        List<CodeMethod> inlinedList = new ArrayList<>();
        Map<CodeVariable, AbstractCodeValue> arguments = new HashMap<>();
        Map<RuntimeThread, RuntimeMethod> rmMap = new HashMap<>();
        for(RuntimeThread t : threads) {
            if(verbose)
                log(0, "thread " + t.toString());
            CodeMethod m = t.startMethod;
            log(1, "runtime call resolution");
            RuntimeCallResolution rca = new RuntimeCallResolution(interpreter, m,
                    t.startMethodArgs.get(0), instantiated, optimizerTuning.traceAllocInCallResolution);
            CodeMethod ra = rca.getMethod();
            log(1, "\n");
            log(1, "inlining code");
            CodeMethod inlined = Inline.inline(ra, true, false);
//testSeekOpDuplicates(threads, inlined);
            AbstractCodeOp recursiveCall;
            if((recursiveCall = Inline.containsCalls(inlined.code)) != null)
                throw new PTAException(recursiveCall.getStreamPos(),
                        "direct or indirect recursive call");
            trimStringOps(inlined);
            addArgumentAssignments(t.startMethodArgs, inlined);
            Map<CompilerAnnotation, Literal> constants = new HashMap<>();
            constants.put(CompilerAnnotation.MODEL_IS_STATISTICAL,
                    new Literal(options.isStatisticalHint));
            replaceConstantAnnotationCalls(inlined, constants);
            RuntimeMethod rm = new RuntimeMethod(inlined);
            rmMap.put(t, rm);
            interpreter.setValueUnchecked(new CodeFieldDereference(
                    inlined.getThisArg()), t.startMethodArgs.get(0), rm);
            boolean stable;
            do {
                stable = true;
                ResolveReferences rr = new ResolveReferences(t.startMethodArgs.get(0), rm, interpreter);
                CompilerException resolutionError = new CompilerException();
                if(rr.resolve(resolutionError)) {
                    inlined = (new RuntimeCallResolution(interpreter, inlined,
                            t.startMethodArgs.get(0), instantiated,
                            optimizerTuning.traceAllocInCallResolution)).getMethod();
                    inlined = Inline.inline(inlined, true, false);
                    rm.method = inlined;
                    rr.localize(rm);
                    stable = false;
                }
                if(resolutionError.reportsExist())
                    throw new PTAException(resolutionError);
            } while(!stable);
            TraceValues.propagateValues(inlined,
                    new HashMap<CodeVariable, AbstractCodeValue>(), null, null,
                    interpreter, null);
            boolean propagateValuesAgain = false;
            if(Optimizer.optimizeBasic(inlined, true, optimizerTuning))
                propagateValuesAgain = true;
            boolean[] changed = new boolean[1];
            // trim dead code before constant indexing and unroll, as these two
            // could make some of that code very large
            TraceValues.trimDeadCode(inlined, true, changed,
                    // no need for a complete trace
                    TraceValues.traceValuesWithCasts(
                        inlined, arguments, new TraceValues.Options(
                                interpreter, true)));
            if(changed[0]) {
                Optimizer.setLabelIndices(inlined);
                propagateValuesAgain = true;
            }
            if(propagateValuesAgain)
                TraceValues.propagateValues(inlined,
                    new HashMap<CodeVariable, AbstractCodeValue>(), null, null,
                    interpreter, null);
            if(verbose || options.test != null)
                log(2, lastInlineString0 = inlined.toString());
            log(1, "\n");
            inlinedList.add(inlined);
        }
        Iterator<CodeMethod> inlinedIterator = inlinedList.iterator();
        List<RuntimeMethod> rmList = new ArrayList<>();
        for(RuntimeThread t : threads) {
            CodeMethod inlined = inlinedIterator.next();
            RuntimeMethod rm = rmMap.get(t);
            // set the runtime value of "this" of the inline
            // to "this" of the thread's run method
//            interpreter.setValueUnchecked(new CodeFieldDereference(
//                    inlined.getThisArg()), t.startMethodArgs.get(0), rm);
            rmList.add(rm);
        }
        return rmList;
    }
    /**
     * Unrolls loops and splits ambiguous dereferences into
     * unambiguous, if possible.
     * 
     * @param sa runtime static analysis data
     * @param rmList list of runtime methods to process
     * @param loopComplexityMultiplier multiplies the complexity threshold
     * of a loop, which is allowed to be unrolled
     * @return updated <code>sa</code> if anything has been
     * optimised, null if nothing changed
     */
    private RuntimeStaticAnalysis unrollAndSplit(RuntimeStaticAnalysis sa,
            List<RuntimeMethod> rmList, int loopComplexityMultiplier) throws PTAException {
        boolean changed = false;
        Map<RuntimeField,Literal> replaced = sa.getReplacedFields();
        boolean saUpToDate = true;
        try {
            boolean unrolled;
            do {
                if(!saUpToDate) {
                    sa = new RuntimeStaticAnalysis(interpreter, rmList);
                    sa.setReplacedFields(replaced);
                    saUpToDate = true;
                }
                ConstantIndexing ci = new ConstantIndexing(sa,
                        // index dereferences must be resolved, even
                        // if the backend supports array per se
                        options.replaceArrays == TADDGeneratorOptions.Arrays.INDEX,
                        options.replaceArrays == TADDGeneratorOptions.Arrays.REPLACE_FORMULA);
                Map<RuntimeMethod, ConstantIndexingInfo> ciInfo = new HashMap<>();
                // a dry run to supply hints to <code>unroll()</code>
                for(RuntimeMethod rm : rmList) {
                    ciInfo.put(rm, new ConstantIndexingInfo());
                    if(ci.transform(rm, ciInfo.get(rm), true)) {
                        saUpToDate = false;
                        changed = true;
                    }
                }
                if(!saUpToDate) {
                    sa = new RuntimeStaticAnalysis(interpreter, rmList);
                    sa.setReplacedFields(replaced);
                    saUpToDate = true;
                }
                
                if(unrolled = unroll(
                        6*loopComplexityMultiplier,
                        120*loopComplexityMultiplier,
                        rmList, ciInfo, sa)) {
                    changed = true;
                    saUpToDate = false;
                }
                // actual constant indexing, after loops are unrolled and thus some
                // indexing has already become constant
                for(RuntimeMethod rm : rmList)
                    if(ci.transform(rm, null, false)) {
                        changed = true;
                        saUpToDate = false;
                    }
            } while(unrolled);
            if(!saUpToDate) {
                sa = new RuntimeStaticAnalysis(interpreter, rmList);
                sa.setReplacedFields(replaced);
                saUpToDate = true;
            }
        } catch(InterpreterException e) {
            throw new PTAException(e.getStreamPos(),
                    "in constant indexing: " + e.getRawMessage());
        }
        log(1, "splitting ambiguous dereferences");
        try {
//testSeekOpDuplicates(rmList);
            if(!options.allowFpVariables)
                // removing fp variables may require condition/branch combos to be
                // splitted, but that in turn may be disallowed by jumps into the
                // branches; try to reduce such jumps before an attempt at the
                // splitting
                for(RuntimeMethod rm : rmList)
                    if(TraceValues.forwardJumpsWithBranches(rm.method, null, sa.getTrace(rm))) {
                        sa.traceRuntimeValues(rm);
                        changed = true;
                    }
            // unrolling a variable range declaration may make it look constant,
            // while it is not
            SplitAmbiguous split = new SplitAmbiguous(sa,
                    true, false, false);
            boolean stable = true;
            for(RuntimeMethod rm : rmList)
                if(split.transform(rm)) {
                    stable = false;
                    changed = true;
                    saUpToDate = false;
                }
            if(!saUpToDate) {
                sa = new RuntimeStaticAnalysis(interpreter, rmList);
                sa.setReplacedFields(replaced);
                saUpToDate = true;
            }
        } catch(InterpreterException e) {
            throw new PTAException(e.getStreamPos(),
                    "splitting ambiguous dereferences: " + e.getRawMessage());
        }
        if(changed)
            return sa;
        else
            return null;
    }
    /**
     * Static analysis.
     * 
     * @param optimizerTuning tuning options of the optimizer
     * @param threads threads to optimize
     * @param sa runtime static analysis data
     * @param rmList list of runtime methods
     * @param requiresSaUpdate list of methods, whoch require
     * update of their runtime static analysis data
     * @return if any of the static--analysis related optimisations
     * modified the code
     */
    private boolean staticAnalysis(Optimizer.Tuning optimizerTuning,
            List<RuntimeThread> threads, RuntimeStaticAnalysis sa,
            List<RuntimeMethod> rmList, Set<RuntimeMethod> requiresSaUpdate)
            throws PTAException {
        boolean changed = false;
        Map<CodeVariable, AbstractCodeValue> arguments = new HashMap<>();
        try {
            log(1, "static analysis");
            // static analysis does not accept dead code
            for(RuntimeMethod rm : rmList) {
                Optimizer.optimizeBasic(rm.method, true, optimizerTuning);
                trimDeadCode(rm.method);
                requiresSaUpdate.add(rm);
            }
//testSeekOpDuplicates(rmList);
            boolean first = true;
            boolean stable;
            do {
                stable = true;
                for(RuntimeMethod rm : requiresSaUpdate)
                    sa.traceRuntimeValues(rm);
                requiresSaUpdate.clear();
                boolean constFound;
                if(options.test == null || !options.test.ignoreConstantElements)
                    constFound = CodeStaticAnalysis.findConstantElements(sa, rmList);
                else
                    constFound = false;
                if(!options.allowFpVariables) {
                    try {
                        // removing fp variables may require condition/branch combos to be
                        // splitted, but that in turn may be disallowed by jumps into the
                        // branches; try to reduce such jumps befor an attempt at the
                        // splitting
                        for(RuntimeMethod rm : rmList)
                            if(TraceValues.forwardJumpsWithBranches(rm.method, null, sa.getTrace(rm))) {
                                sa.traceRuntimeValues(rm);
                                stable = false;
                            }
                        // unrolling a variable range declaration may make it look constant,
                        // while it is not
                        SplitAmbiguous split = new SplitAmbiguous(sa,
                                false, true, false);
                        for(RuntimeMethod rm : rmList)
                            if(split.transform(rm))
                                stable = false;
                    } catch(InterpreterException e) {
                        throw new PTAException(e.getStreamPos(),
                                "removing floating--point variables: " + e.getRawMessage());
                    }
                }
                // the condition stems from the fact, that the following optimizations will
                // be repeated later anyway and that they might be slow; the optimizations
                // are run now only because of a possible loop with 
                // <code>CodeStaticAnalysis.findConstantElements</code>
                if(first || constFound || !stable)
                    for(RuntimeMethod rm : rmList) {
                        do {
                            InterpretingContext c = new InterpretingContext(interpreter,
                                    null, false, rm, InterpretingContext.UseVariables.NO);
                            // because of the loop, it might be faster to use a partial trace, instead of computing
                            // now a full one
                            boolean repeat = CodeStaticAnalysis.staticAnalysisLooped(
                                    c, rm.method);
                            if(TraceValues.propagateValues(rm.method, arguments, null, null,
                                    interpreter, rm))
                                repeat = true;
                            if(repeat) {
                                requiresSaUpdate.add(rm);
                                stable = false;
                            } else
                                break;
                        } while(true);
                    }
                first = false;
                if(!stable)
                    changed = true;
            } while(!stable);
        } catch(InterpreterException e) {
            throw new PTAException(e.getStreamPos(),
                    "static analysis: " + e.getRawMessage());
        }
        //
        // optimize code after the runtime static analysis
        //
        Iterator<RuntimeMethod> rmIterator = rmList.iterator();
        for(RuntimeThread t : threads) {
            RuntimeMethod rm = rmIterator.next();
            if(Optimizer.optimizeAssignments(rm.method.code)) {
                CodeLabel.setIndices(rm.method.code);
                requiresSaUpdate.add(rm);
                changed = true;
            }
        }
        return changed;
    }
    /**
     * Replaces special calls.
     * 
     * @param optimizerTuning tuning options of the optimizer
     * @param threads threads to optimize
     * @param sa runtime static analysis data
     * @param rmList list of runtime methods
     */
    private void replaceSpecialCalls(Optimizer.Tuning optimizerTuning,
            List<RuntimeThread> threads, RuntimeStaticAnalysis sa,
            List<RuntimeMethod> rmList) throws PTAException {
        Iterator<RuntimeMethod> rmIterator = rmList.iterator();
        for(RuntimeThread t : threads) {
            RuntimeMethod rm = rmIterator.next();
            replaceDistCalls(sa, rm, true);
            Optimizer.optimizeBasic(rm.method, true, optimizerTuning);
            if(!options.rangeChecking)
                // trim range ops after variable ranges are determined, but
                // before merging of locals
                trimPrimaryRangeOps(rm.method);
            // no need for a complete trace
        }
    }
    /**
     * General optimisation.
     * 
     * @param optimizerTuning tuning options of the optimizer
     * @param threads threads to optimize
     * @param sa runtime static analysis data
     * @param rmList list of runtime methods
     * @return if anything has been optimised
     */
    private boolean generalOptimisation(Optimizer.Tuning optimizerTuning,
            List<RuntimeThread> threads, RuntimeStaticAnalysis sa,
            List<RuntimeMethod> rmList) throws PTAException {
        boolean changed = false;
        Map<CodeVariable, AbstractCodeValue> arguments = new HashMap<>();
        Iterator<RuntimeMethod> rmIterator = rmList.iterator();
        for(RuntimeThread t : threads) {
            RuntimeMethod rm = rmIterator.next();
            if(Optimizer.optimizeBasic(rm.method, true, optimizerTuning))
                changed = true;
            CodeValuePossibility possibility = TraceValues.traceValuesWithoutCasts(
                rm.method, arguments, true);
            boolean stable;
            // System.out.print("o");System.out.flush();
            do {
                stable = true;
                // try to decrease the number of locals, after proposals
                // are computed
                //
                // the merging excludes array references, as these will later
                // be replaced with direct arrays anyway, and thus such merging might
                // only loose primitive ranges of merged array references
                boolean done = false;
                while(!done &&
                        Optimizer.optimizeDeadAndLocals(rm.method, true, true,
                            optimizerTuning.tuneToTADDs, possibility)) {
                    if(!Optimizer.optimizeBasic(rm.method, true, optimizerTuning))
                        done = true;
                    // no need for a complete trace
                    possibility = TraceValues.traceValuesWithoutCasts(rm.method, arguments, true);
                    stable = false;
                }
                done = false;
                while(!done &&
                        TraceValues.propagateValues(rm.method, arguments, possibility,
                            null, interpreter, rm)) {
                    if(!Optimizer.optimizeBasic(rm.method, true, optimizerTuning))
                        done = true;
                    // no need for a complete trace
                    possibility = TraceValues.traceValuesWithoutCasts(rm.method, arguments, true);
                    stable = false;
                }
                // some other final optimizations
                if(TraceValues.removeRedundantAssignments(rm.method, null, possibility,
                        optimizerTuning.assignmentTransportCheck)) {
                     // no need for a complete trace
                     possibility = TraceValues.traceValuesWithoutCasts(rm.method, arguments, true);
                     stable = false;
                }
                boolean updatePossibility = false;
                if(TraceValues.forwardJumpsWithBranches(rm.method, null, possibility)) {
                    updatePossibility = true;
                    stable = false;
                }
                CodeVariableTransport tt = TraceValues.traceTransport(rm.method);
                if(TraceValues.invertBranches(rm.method, tt)) {
                    updatePossibility = true;
                    tt = null;
                }
                if(updatePossibility) {
                    // no need for a complete trace
                    possibility = TraceValues.traceValuesWithoutCasts(rm.method, arguments, true);
                }
                if(!options.strictFieldAccess)
                    if(tt == null)
                        tt = TraceValues.traceTransport(rm.method);
                    if(TraceValues.propagateValues(rm.method, arguments, possibility,
                            tt, interpreter, rm)) {
                        stable = false;
                        Optimizer.optimizeBasic(rm.method, true, optimizerTuning);                        
                        // no need for a complete trace
                        possibility = TraceValues.traceValuesWithoutCasts(rm.method, arguments, true);
                    }
                try {
                    ++couunter;
if(couunter == 1)
    couunter = couunter;
                    if(options.test == null || options.test.extraOptimizations) {
                        sa.clearChanged();
                        sa.analyse(rm);
                        if(sa.isChanged()) {
                            Optimizer.optimizeBasic(rm.method, true, optimizerTuning);
                            // no need for a complete trace
                            possibility = TraceValues.traceValuesWithoutCasts(rm.method, arguments, true);
                            stable = false;
                        }
                    }
                } catch(InterpreterException e) {
                    throw new PTAException(e.getStreamPos(),
                            "in runtime code analysis: " + e.getRawMessage());
                }
                if(!stable)
                    changed = true;
            } while(!stable);
            log(1, "\n");
        }
        return changed;
    }
    /**
     * Updates data about selected runtime methods within a runtime static
     * analysis.
     * 
     * @param sa runtime static analysis
     * @param updateList collection of runtime methods
     */
    public static void updateSa(RuntimeStaticAnalysis sa, Collection<RuntimeMethod> updateList) {
        for(RuntimeMethod rm : updateList)
            sa.traceRuntimeValues(rm);
    }
    /**
     *  Restores PTA defaults, after an unsuccesfull stage, and before a next stage.
     * 
     */
    private void setPTADefaults() throws PTAException {
        compilation = new BackendPTACompilation(
                interpreter.compilation.modelControl,
                options.scheduler.implicit ? options.scheduler.type : null);
        compilation.mainClassName = mainMethodClass.name;
        labels = new Labels();        
        lockVariables = new InternalRuntimeContainer("lockVariables");
        clockVariables = new InternalRuntimeContainer("clocks");
        statusVariables = new InternalRuntimeContainer(STATUS_CONTAINER_NAME);
        if(compilation.errorCodeVariable == null) {
            compilation.errorCodeVariable = new CodeVariable(null, null,
                    ERROR_CODE_VARIABLE_NAME,
                    new Type(Type.PrimitiveOrVoid.BOOLEAN),  Context.NON_STATIC,
                    false, false, false, Variable.Category.INTERNAL_NOT_RANGE);
            compilation.errorVariable = compilation.newVariable(statusVariables,
                    compilation.errorCodeVariable, false, -1,
                    new Double(0), null, ERROR_CODE_VARIABLE_NAME, false).
                        v();
        }
        compilation.codeScalars.clear();
        compilation.codeScalars.put((PTAScalarVariable)compilation.errorVariable,
                compilation.errorCodeVariable);
        compilation.unknownConstants = interpreter.unknownConstants;
    }
//    /**
//     * Clears ranges of all variables.
//     */
//    private void clearRanges() {
//        int count = 0;
//        if(count == 0)
//            return;
//        for(AbstractRuntimeContainer c : compilation.variables.keySet()) {
//            Map<CodeVariable, AbstractBackendPTAVariable> set =
//                    compilation.variables.get(c);
//            for(CodeVariable cv : set.keySet()) {
//                AbstractBackendPTAVariable v = set.get(cv);
//                v.v().range = null;
//                ++count;
//                System.out.println("*" + v.v().toString());
//            }
//        }
//        System.out.println("found " + count + " variables");
//    }
    /**
     * Generate TADDs.
     * 
     * @param threads                   list of threads of
     *                                  non--lock TADDs
     * @param optimizerTuning           options for the optimizer
     */
    void generateTADD(List<RuntimeThread> threads,
            Optimizer.Tuning optimizerTuning) throws PTAException {
        //
        // transform the code
        //
        List<RuntimeMethod> rmList = inline(optimizerTuning,
                threads);
//testSeekOpDuplicates(rmList);
        RuntimeStaticAnalysis sa = null;
        boolean implicitHidden = options.scheduler.type ==
                SchedulerType.HIDDEN && options.scheduler.implicit;
        Map<PTA, PTAVariableTransport> transport =
                new HashMap<>();
        // ModelControl initialModelControl = new ModelControl(interpreter.model);
        try {
            sa = new RuntimeStaticAnalysis(interpreter, rmList /*, arr*/);
        } catch(InterpreterException e) {
            throw new PTAException(e.getStreamPos(),
                    "in runtime code analysis: " + e.getRawMessage());
        }
        final int MAX_STAGE = 1;
        STAGES:
        for(int stage = 0; stage <= MAX_STAGE; ++stage) {
            // interpreter.compilation.modelControl = initialModelControl;
            // interpreter.resetPTA();
            int loopComplexityMultiplier;
            switch(stage) {
                case 0:
                    loopComplexityMultiplier = 1;
                    break;
                    
                case 1:
                    loopComplexityMultiplier = 10;
                    break;
                    
                default:
                    throw new RuntimeException("unknown stage");
            }
            log(1, "unrolling and constant indexing");
            int optimisationCount = 0;
            boolean majorChanged;
            do {
                majorChanged = false;
                RuntimeStaticAnalysis updatedSa;
                if((updatedSa = unrollAndSplit(sa, rmList, loopComplexityMultiplier)) != null) {
                    majorChanged = true;
                    sa = updatedSa;
                }
                Set<RuntimeMethod> requiresSaUpdate = new HashSet<>();
    if(optimisationCount == 1)
        sa = sa;
                if(staticAnalysis(optimizerTuning, threads, sa, rmList,
                        requiresSaUpdate))
                    majorChanged = true;
                updateSa(sa, requiresSaUpdate);
                //
                // optimize variable primitive ranges
                //
                sa.clearChanged();
    if(optimisationCount > 1)
        sa = sa;
                new RangeCodeStaticAnalysis(sa, true);
                if(sa.isChanged()) {
                    updateSa(sa, rmList);
                    majorChanged = true;
                }
                replaceSpecialCalls(optimizerTuning, threads, sa, rmList);
                updateSa(sa, rmList);
                if(generalOptimisation(optimizerTuning, threads, sa, rmList)) {
                    majorChanged = true;
                    updateSa(sa, rmList);
                }
                ++optimisationCount;
            } while(majorChanged);
    if(optimisationCount > 2) {
        System.out.println("\n\n*\n*\n*\n********" + optimisationCount + "*******************\n*\n*\n\n");
        sa = sa;
    }
            try {
                //
                // generate transitions
                //
                setPTADefaults();
// clearRanges();
                Iterator<RuntimeMethod> rmIterator = rmList.iterator();
                for(RuntimeThread t : threads) {
                    RuntimeMethod rm = rmIterator.next();
                    // a complete trace is needed
                    sa.traceRuntimeValues(rm);
                    // Optimizer.optimizeUnusedLocals(rm.method);
                    lastInlineString = rm.method.toString();
                    CodeVariableTransport r = TraceValues.traceTransport(rm.method);
// if(rm.method.getKey().contains(""))
//     rm = rm;
                    PTA pta = generateTransitions(threads, t, rm, sa, r);
        //TraceValues.propagateValues(rm.method, arguments, null,
        //    null, interpreter, rm);
                    transport.put(pta, new PTAVariableTransport(this, rm, r));
                    compilation.threadPtas.put(pta, t.object);
                }
                break STAGES;
            } catch(PTAException e) {
                if(stage == MAX_STAGE)
                    throw e;
                else {
                    // try one more time, with more aggressive settings
                    recoverableErrors.clearReports();
                    // compilation.threadPtas.clear();
                    transport.clear();
                    // clearRanges();
                }
            }
        }
        checkBarrierUsage();
        compilation.registerReplacements(sa.getReplacedFields(),
                interpreter);
        compilation.computeFinalRanges(true);
        setLockVariablesRanges();
        purgeNotComplemented();
        // applied to all PTAs, including locks
        //for(PTA pta : transport.keySet()) {
        Scheduler.LocationManager lm = new Scheduler.LocationManager();
        for(PTA pta : compilation.ptas.values()) {
            if(pta.replaceBufferedConstants()) {
                // buffered constants may indicate a barrier with
                // parallel transitions, which are producer--selective,
                // or set a value tested directly after the barrier
                //
                // some guards might have become constant after
                // the replacement
                PTAOptimizer.removeConstGuards(pta, null, true);
                // just before applying invariants, as these might prevent
                // this optimisation from happening
                PTAOptimizer.conditionalTrees(pta);
                PTAOptimizer.nonDeterministic(pta);
            }
//            PTAOptimizer.conditionalTrees(pta);
            if(!options.scheduler.implicit) {
                options.scheduler.applyInvariants(pta, labels,
                        options.labelNondeterminism,
                        compilation, clockVariables, lm);
                // a schedule may add empty transitions
                PTAOptimizer.deleteRedundantStatesRecursive(pta);
            }
            // sets location owners, required by the location manager
            TADDFactory.checkConnectivity(pta);
        }
        lm.complete();
        for(PTA pta : compilation.ptas.values()) {
            ClockOptimizer.transformClockConstraints(pta);
            TADDFactory.addResets(pta);            
        }
        Set<PTA> transported = transport.keySet();
        analyseVariableUsage();
        boolean stable = true;
        for(PTA pta : transported)
            if(PTAOptimizer.removeUnusedLocalUpdates(pta, transport.get(pta).getTraced())) {
                // removed updates may cause needless series of transitions
                //// not needed, may cause more comment blur
                //// PTAOptimizer.compactUpdate(pta, transport.get(pta).getTraced(), implicitHidden);
                PTAOptimizer.removeRedundant(pta);
                if(PTAOptimizer.compactGuardUpdate(pta, transport.get(pta).getTraced(),
                        implicitHidden, false))
                    /*TADDFactory.removeRedundant(pta)*/;
                if(PTAOptimizer.removeZeroProbabilisticBranches(pta))
                    PTAOptimizer.removeRedundant(pta);
                stable = false;
            }
        if(options.nestedExpressions) {
            // nest expressions only in the thread PTA's, as opposed
            // to lock PTAs
            for(PTA pta : transported) {
                if(TADDFactory.nestAndCompactExpressions(pta,
                        options.replaceArrays != TADDGeneratorOptions.Arrays.INDEX ?
                            selectElementVariables(compilation.usedVariables) : null,
                        transport.get(pta), options.strictFieldAccess, implicitHidden,
                        options.rangeChecking))
                    stable = false;
//                if(false && PTAOptimizer.compactEmptySyncLocal(pta))
//                    stable = false;
                TADDFactory.completeClockResetStops(pta);
                // it is supposed, that closing stops is likely to be important
                // when options requests nested expressions, as both may
                // reduce the number of states
                if(PTAOptimizer.closeStops(pta, transport.get(pta).getTraced()))
                    stable = false;
            }
        }
        // integrate empty transitions in series
        for(PTA pta : transported) {
            removeSpuriousSleepAnnotations(pta, implicitHidden);
            if(PTAOptimizer.compactSpecial(pta, implicitHidden))
                stable = false;
            if(PTAOptimizer.compactGuardUpdate(pta, new HashSet<PTAScalarVariable>(), implicitHidden, false))
                stable = false;
            if(PTAOptimizer.compactUpdate(pta, new HashSet<PTAScalarVariable>(), implicitHidden, true))
                stable = false;
        }
        if(!stable)
            analyseVariableUsage();            
        compilation.setLabels();
        compilation.applyModelControl();
        PTAException error = new PTAException();
        for(PTA pta : transported) {
            TADDFactory.checkConnectivity(pta);
            options.scheduler.markWaitForSync(pta);
            TADDFactory.checkConnectivity(pta);
            try {
                TADDFactory.checkAtomic(pta);
            } catch(PTAException e) {
                error.addAllReports(e);
            }
        }
        if(error.reportsExist())
            throw error;
        purgeLoneBarriers();
        for(PTA pta : transported)
            pta.testFormulas();
    }
    /**
     * Remove the leftover 
     * 
     * @param pta pta
     * @param implicitHidden implicit hidden
     */
    private void removeSpuriousSleepAnnotations(PTA pta, boolean implicitHidden) {
        for(PTATransition x : pta.transitions)
            if(x.isSpecial() && x.clockLimitLow == null && x.sourceState.clockLimitHigh == null)
                for(AbstractCodeOp op : new LinkedList<>(x.backendOps))
                    if(op instanceof CodeOpNone) {
                        CodeOpNone n = (CodeOpNone)op;
                        for(String a : new TreeSet<>(n.annotations))
                            if(a.equals(CompilerAnnotation.SLEEP_STRING)) {
                                x.backendOps.remove(op);
                            }
                    }
    }
    private void testSeekOpDuplicates(List<RuntimeThread> rmList, CodeMethod inlined) {
        for(AbstractCodeOp op : inlined.code.ops) {
            for(RuntimeThread rm2 : rmList)
                if(inlined != rm2.startMethod)
                    for(AbstractCodeOp op2 : rm2.startMethod.code.ops)
                        if(op == op2)
                            throw new RuntimeException("duplicate");
        }
    }
    private void testSeekOpDuplicates(List<RuntimeMethod> rmList) {
        for(RuntimeMethod rm : rmList)
            for(AbstractCodeOp op : rm.method.code.ops) {
                for(RuntimeMethod rm2 : rmList)
                    if(rm != rm2)
                        for(AbstractCodeOp op2 : rm2.method.code.ops)
                            if(op == op2)
                                throw new RuntimeException("duplicate");
            }
    }
    void log(int indentLevel, String s) {
        if(verbose)
            CompilerUtils.log("", indentLevel, s);
    }
static int couunter = 0;
}
