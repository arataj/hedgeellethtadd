/*
 * VericsIO.java
 *
 * Created on May 17, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.verics;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.ModelType;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.AbstractRuntimeContainer;
import pl.gliwice.iitis.hedgeelleth.pta.values.*;
import pl.gliwice.iitis.hedgeelleth.pta.*;

/**
 * Generates a text representation of a PTA compilation in the format
 * used by the Verics group.
 * 
 * @author Artur Rataj
 */
public class VericsIO extends FileFormatIO {
    final static String INDENT_STRING = "";

    /**
     * Creates a new generator of PTA compilation to file in the
     * format used by the Verics group.
     * 
     * @param options PTA generation options
     */
    public VericsIO(FileFormatOptions options) {
        super(options);
        if(options.emulateArrays)
            throw new RuntimeException("emulation of arrays not supported");
    }
    /**
     * Produces a text representation of a value.
     * 
     * @param v                         value
     * @param variables                 variables
     * @param clockNum                  current clock number for clock--related
     *                                  expressions, -1 for other expressions
     * @return                          text description of the value
     */
    protected String parse(AbstractPTAValue e,
            Map<AbstractPTAVariable, Integer> variables,
            int clockNum) {
        if(e instanceof PTAConstant) {
            PTAConstant c = (PTAConstant)e;
            String s;
            if(c.floating)
                s = "" + c.value;
            else
                s = "" + (int)c.value;
            if(clockNum == -1)
                return "c" + s;
            else
                return s;
        } else if(e instanceof PTAClock) {
            return "x" + clockNum;
        } else {
            AbstractPTAVariable v = (AbstractPTAVariable)e;
            return "z" + variables.get(v);
        }
    }
    /**
     * Produces a text representation of an expression.
     * 
     * If clock number is -1, only assignments to non--clock variables are produced.
     * Otherwise, only assignments to clock variables are produced.
     * 
     * 
     * @param e                         expression
     * @param variables                 variables
     * @param clockNum                  current clock number for clock--related
     *                                  expressions, -1 for other expressions
     * @return                          text description of the
     *                                  expression
     */
    protected String parse(AbstractPTAExpression e,
            Map<AbstractPTAVariable, Integer> variables,
            int clockNum) throws IOException {
        String s = "";
        String booleanTarget = " = c1";// + c0";
        if(e.target instanceof PTAClock) {
            if(clockNum != -1)
                s = "reset " + parse(e.target, variables, clockNum);
        } else {
            if(e instanceof PTABinaryExpression) {
                PTABinaryExpression be = (PTABinaryExpression)e;
                BinaryExpression.Op operator =
                        CodeOpBinaryExpression.operatorCodeToTree(
                        be.operator);
                s = BinaryExpression.getOperatorName(operator);
                if(s.equals("=="))
                    s = "=";
                else if(s.equals("!="))
                    s = "<>";
                if(operator.isRelational())
                    if(clockNum == -1) {
                        s = "cond " + parse(be.left, variables, clockNum) + " " + s + " " +
                                parse(be.right, variables, clockNum);
                    } else {
                        s = parse(be.left, variables, clockNum) + " " + s + " " +
                                parse(be.right, variables, clockNum);
                    }
                else {
                    switch(operator) {
                        case PLUS:
                        case MINUS:
                        case MULTIPLY:
                        case DIVIDE:
                        case MODULUS:    
                            break;

                        default:
                            throw new IOException("operator " + s + " not allowed");

                    }
                    if(clockNum == -1)
                        s = parse(be.left, variables, clockNum) + " " + s + " " +
                                parse(be.right, variables, clockNum);
                    else
                        s = "";
                }
            } else if(e instanceof PTAUnaryExpression && clockNum == -1) {
                PTAUnaryExpression ue = (PTAUnaryExpression)e;
                if(ue.target == null) {
                    // a condition
                    s = "cond " + parse(ue.sub, variables, clockNum);
                    if(ue.operator == AbstractCodeOp.Op.UNARY_CONDITIONAL_NEGATION)
                        booleanTarget = " = c0";
                    else if(ue.operator != AbstractCodeOp.Op.NONE) {
                        s = UnaryExpression.getOperatorName(
                            CodeOpUnaryExpression.operatorCodeToTree(
                            ue.operator), null)[0];
                        throw new IOException("operator " + s + " not allowed in condition");
                    }
                } else {
                    if(ue.operator == AbstractCodeOp.Op.UNARY_CONDITIONAL_NEGATION)
                        s = "c1 - " + parse(ue.sub, variables, clockNum);
                    else if(ue.operator == AbstractCodeOp.Op.UNARY_NEGATION)
                        s = "c0 - " + parse(ue.sub, variables, clockNum);
                    else if(ue.operator == AbstractCodeOp.Op.NONE)
                        s = parse(ue.sub, variables, clockNum) + " + c0";
                    else {
                        s = UnaryExpression.getOperatorName(
                            CodeOpUnaryExpression.operatorCodeToTree(
                            ue.operator), null)[0];
                        if(s.isEmpty())
                            s = "postfix " + UnaryExpression.getOperatorName(
                                CodeOpUnaryExpression.operatorCodeToTree(
                                ue.operator), null)[1];
                        else
                            s = "prefix " + s;
                        throw new IOException("operator " + s + " not allowed");
                    }
                }
            }
            if(clockNum == -1) {
                if(e.target != null)
                    s = "assign " + s + " to " + parse(e.target, variables, clockNum);
                else if(e instanceof PTAUnaryExpression)
                    s = s + booleanTarget;
            }
        }
        if(!s.isEmpty())
            s = INDENT_STRING + s;
        return s;
    }
    /**
     * Returns a comment descriping a given list of operations.
     * 
     * @param ops                       list of operations
     * @param localVerbosityLevel       local verbosity level, meaning
     *                                  is the same as <code>verbosityLevel</code>
     * @return
     */
    static String getCodeOpListComment(List<AbstractCodeOp> ops,
            int localVerbosityLevel) {
        String s = "";
        switch(localVerbosityLevel) {
            case 0:
            {
                break;
            }
            case 1:
            {
                if(ops.size() != 0) {
                    s += "#";
                    SortedSet<String> positions = new TreeSet<>();
                    for(AbstractCodeOp op : ops)
                        positions.add("" + op.getStreamPos());
                    for(String p : positions)
                        s += " " + p;
                    s += "\n";
                }
                break;
            }
            case 2:
            {
                int count = 0;
                for(AbstractCodeOp op : ops) {
                    s += "# " + count + " " + op.toString() + " (" + op.getStreamPos() + ")\n";
                    ++count;
                }
                break;
            }
        }
        return s;
    }
    /**
     * Returns a key, that represents the contents of a given transition
     * comment. Used to find repeating comments.
     *
     * @param states                    map of states ot a PTA
     * @param tt                        transition
     * @param localVerbosityLevel       local verbosity level, meaning
     *                                  is the same as <code>verbosityLevel</code>
     * @return                          key of the transition's comment
     */
    static String getTransitionCommentKey(Map<PTAState, Integer> states,
            PTATransition tt, int localVerbosityLevel) {
        return states.get(tt.targetState).toString() + " " +
            (tt.label == null ? "" : tt.label.toString()) + " " +
            getCodeOpListComment(tt.backendOps, localVerbosityLevel);
    }
    /**
     * Generates a comment for a given state.
     *
     * @param states                    map of states ot a PTA 
     * @param state                     state to comment in the PTA
     * @param localVerbosityLevel       local verbosity level, meaning
     *                                  is the same as <code>verbosityLevel</code>
     * @param compactTargets            compact transitions to different targets,
     *                                  used only for verbosity level 1
     * @return                          comment string
     */
    static String commentState(Map<PTAState, Integer> states,
            PTAState state, int localVerbosityLevel, boolean compactTargets) {
        String s = "";
        switch(localVerbosityLevel) {
            case 0:
            {
                break;
            }
            case 1:
            {
                if(compactTargets) {
                    TreeSet<String> positions = new TreeSet<>();
                    for(Set<PTATransition> set : state.output.values())
                        for(PTATransition tt : set)
                            for(AbstractCodeOp op : tt.backendOps)
                                positions.add("" + op.getStreamPos());
                    for(String p : positions)
                        s += " " + p;
                    if(!s.isEmpty())
                        s = "#" + s + "\n";
                    break;
                }
                // fallthrough
            }
            case 2:
            {
                Map<String, Integer> repetitions = new HashMap<>();
                for(Set<PTATransition> set : state.output.values())
                    for(PTATransition tt : set) {
                        String key = getTransitionCommentKey(states, tt,
                                localVerbosityLevel);
                        int repeated;
                        if(repetitions.containsKey(key))
                            repeated = repetitions.get(key);
                        else
                            repeated = 0;
                        ++repeated;
                        repetitions.put(key, repeated);
                    }
                for(Set<PTATransition> set : state.output.values())
                    for(PTATransition tt : set) {
                        String key = getTransitionCommentKey(states, tt,
                                localVerbosityLevel);
                        int repeated;
                        if(repetitions.containsKey(key))
                            repeated = repetitions.get(key);
                        else
                            repeated = 0;
                        if(repeated != -1) {
                            repetitions.put(key, -1);
                            String repeatedStr;
                            if(repeated > 1)
                                repeatedStr = " (repeated " + repeated + " times)";
                            else
                                repeatedStr = "";
                            s += "# target " + states.get(tt.targetState) +
                                    repeatedStr + "\n";
                            if(tt.label != null)
                                s += "# " + tt.label.toString() + "\n";
                            s += getCodeOpListComment(tt.backendOps,
                                    localVerbosityLevel);
                        }
                    }
                break;
            }
        }
       return s;
    }
    /**
     * Writes a PTA compilation to a file.
     * 
     * @param file                      file to create
     * @param c                         PTA compilation
     * @return                           all model types generated
     */
    @Override
    public List<ModelType> write(File file, BackendPTACompilation c) throws IOException {
        PrintWriter out = new PrintWriter(new FileOutputStream(
                file));
        out.println("network\n");
        SortedMap<String, AbstractBackendPTAVariable> sortedVariables =
                new TreeMap<>();
        for(AbstractRuntimeContainer rc : c.variables.keySet()) {
            Map<CodeVariable, AbstractBackendPTAVariable> inContainer =
                    c.variables.get(rc);
            for(CodeVariable cv : inContainer.keySet()) {
                AbstractBackendPTAVariable tv = inContainer.get(cv);
                sortedVariables.put(tv.v().name, tv);
            }
        }
        if(options.verboseTADDVariables)
            System.out.println("warning: Verics backend does not " +
                    "support verbose names of variables");
        String s = "";
        String comment = "";
        // this map stores numbers of variables
        Map<AbstractPTAVariable, Integer> variables = new HashMap<>();
        int count = 0;
        for(AbstractBackendPTAVariable tv : sortedVariables.values())
            if(!(tv instanceof BackendPTAClock)) {
                if(tv instanceof BackendPTAScalarVariable) {
                    if(tv.v().floating)
                       throw new IOException("floating--point variables not supported by Verics format");
                    BackendPTAScalarVariable v = (BackendPTAScalarVariable)tv;
                    boolean guaranteed = tv.v().range.backendGuaranteed;
                    if(!guaranteed && v.proposal != null && v.proposal.rangeKnown())
                        if(v.proposal.isEmpty())
                            guaranteed = true;
                        else {
                            long pMin = v.proposal.min.getMaxPrecisionInteger();
                            long pMax = v.proposal.max.getMaxPrecisionInteger();
                            guaranteed = pMin >= -32768 && pMax <= 32767;
                        }
                    if(     // can it be a normal integer?
                            !guaranteed &&
                            (PTASystem.getMin(tv.v().range.range) != -32768 ||
                            PTASystem.getMax(tv.v().range.range) != 32767))
                         throw new IOException("only 16-bit signed integer " +
                                 "variables or variables not requiring range checking " +
                                 "are suported by the Verics backend");
                    variables.put(v.v(), count);
                    s += " " + (int)v.v().initValue;
                    comment += INDENT_STRING + "# z" + count + " = " +
                            v.toString() + "\n";
                    ++count;
                } else if(tv instanceof BackendPTAArrayVariable)
                     throw new IOException("arrays not supported by the Verics backend");
                 else
                     throw new RuntimeException("unknown variable type");
            }
        out.println("variables " + variables.size());
        if(options.verbosityLevel != 0)
            out.print(comment);
        if(!variables.isEmpty())
            out.println(INDENT_STRING + s.trim());
        out.println("end\n");
        // serial numbers of label names keyed with strings
        Map<String, Integer> sLabelNames = new HashMap<>();
        int nextFreeLabelNameNum = 0;
        int nextFreeClockNum = 1;
        SortedMap<String, PTA> sortedTADDs = new TreeMap<>();
        for(PTA tadd : c.ptas.values())
            sortedTADDs.put(tadd.name, tadd);
        for(PTA tadd : sortedTADDs.values()) {
            if(options.verbosityLevel != 0)
                out.println("# " + tadd.name);
            out.println("automaton");
            int clockNum = -1;
            if(tadd.clock != null) {
                out.print("clocks");
                clockNum = nextFreeClockNum++;
                out.print(" " + clockNum);
                out.println("\n");
            }
            // this map stores serial numbers of states
            Map<PTAState, Integer> states = new HashMap<>();
            Map<Integer, PTAState> statesSorted = new HashMap<>();
            count = 0;
            PTAState startState = tadd.startState;
            states.put(startState, count);
            statesSorted.put(count, startState);
            ++count;
            for(PTAState state : tadd.states.values())
                if(state != startState) {
                    states.put(state, count);
                    statesSorted.put(count, state);
                    ++count;
                }
            int statesNum = count;
            for(int i = 0; i < statesNum; ++i) {
                PTAState state = statesSorted.get(i);
                if(!options.commentVariablesOnly)
                    out.print(commentState(states, state,
                            options.verbosityLevel, false));
                out.println("location " + i);
                if(state.clockLimitHigh != null)
                    out.println(parse(state.clockLimitHigh, variables, clockNum));
                out.println("end\n");
            }
            for(PTAState tState : statesSorted.values()) {
                for(Set<PTATransition> set : tState.output.values())
                    for(PTATransition tt : new TreeSet<>(set)) {
                        int labelNum;
                        if(tt.label != null) {
                            if(sLabelNames.containsKey(tt.label.name))
                                labelNum = sLabelNames.get(tt.label.name);
                            else {
                                labelNum = nextFreeLabelNameNum++;
                                sLabelNames.put(tt.label.name, labelNum);
                                if(tt.label.broadcast)
                                    throw new IOException("broadcast labels not supported: " +
                                            tt.label.name);
                            }
                        } else {
                            labelNum = nextFreeLabelNameNum++;
                        }
                        if(!options.commentVariablesOnly) {
                            if(tt.label != null && options.verbosityLevel != 0)
                                out.println("# " + tt.label.toString());
                            out.print(getCodeOpListComment(tt.backendOps,
                                    options.verbosityLevel));
                        }
                        out.println("transition " +
                                states.get(tt.sourceState) + " " +
                                (tt.targetState == null ? "null" : states.get(tt.targetState)) + " " +
                                labelNum);
                        boolean clockReset = false;
                        if(clockNum != -1) {
                            if(tt.clockLimitLow != null)
                                out.println(parse(tt.clockLimitLow, variables, clockNum));
                            for(AbstractPTAExpression expr : tt.update) {
                                String t = parse(expr, variables, clockNum);
                                if(!t.isEmpty()) {
                                    out.println(t);
                                    clockReset = true;
                                }
                            }
                        }
                        if(!clockReset)
                            out.println(INDENT_STRING + "reset");
                        if(tt.guard != null)
                            out.println(parse(tt.guard, variables, -1));
                        for(AbstractPTAExpression expr : tt.update) {
                            String t = parse(expr, variables, -1);
                            if(!t.isEmpty())
                                out.println(t);
                        }
                        out.println("end\n");
                    }
                }
            out.println("valuation");
            boolean firstItem = true;
            int localVerbosityLevel = options.verbosityLevel;
            boolean localCompactTargets = false;
            if(localVerbosityLevel == 2)
                localVerbosityLevel = 1;
            else if(localVerbosityLevel == 1)
                localCompactTargets = true;
            for(int index = 0; index < statesNum; ++index) {
                PTAState state = statesSorted.get(index);
                Integer v = tadd.valuation.get(state);
                if(v != null) {
                    if(!firstItem && localVerbosityLevel != 0 && !localCompactTargets)
                        out.println();
                    out.print(commentState(states, state,
                            localVerbosityLevel,
                            localCompactTargets));
                    firstItem = false;
                    out.println(index + " " + v);
                }
            }
            out.println("end\n");
            out.println("end automaton\n");
        }
        out.println("end network");
        if(out.checkError())
            throw new IOException("write error to " + file.getPath());
        out.close();
        c.type = ModelType.PTA;
        return Arrays.asList(new ModelType[] {c.type});
    }
}
