/*
 * FileFormatIO.java
 *
 * Created on Jul 17, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeVariable;
import pl.gliwice.iitis.hedgeelleth.compiler.util.ModelType;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.AbstractRuntimeContainer;
import pl.gliwice.iitis.hedgeelleth.pta.PTA;

/**
 * An abstract class for saving TADD compilation to a file.<br>
 *
 * It requires the variable names to be unique, as it is allowed
 * to use these names as keys.
 * 
 * @author Artur Rataj
 */
abstract public class FileFormatIO {
    /**
     * Options.
     */
    protected FileFormatOptions options;
    /**
     * If this class uses come pta name translation, it can save the
     * mapping here for other generators to reuse. Otherwise, it is
     * null.
     */
    public Map<PTA, String> ptaNameMap;
    
    /**
     * Creates a new generator of TADD compilation to a file.
     * 
     * @param options TADD generation options
     */
    public FileFormatIO(FileFormatOptions options) {
        this.options = options;
    }
    /**
     * Determines the model type. Also checks, if the model type can be
     * narrowed. If yes, the type is modified accordingly.
     * 
     * @param c compilation
     */
    protected void checkActualModelType(BackendPTACompilation c) {
        boolean clocksFound = false;
        for(AbstractRuntimeContainer rc : c.variables.keySet()) {
            Map<CodeVariable, AbstractBackendPTAVariable> inContainer =
                    c.variables.get(rc);
            for(CodeVariable cv : inContainer.keySet()) {
                AbstractBackendPTAVariable tv = inContainer.get(cv);
                if(tv instanceof BackendPTAClock)
                    clocksFound = true;
            }
        }
        boolean nonDeterministicPTA = c.ptas.size() > 1;
        for(PTA tadd : c.ptas.values())
            if(tadd.nonDeterministic)
                nonDeterministicPTA = true;
        if(!c.playerPTAs.isEmpty()) {
            if(clocksFound)
                c.type = ModelType.TSMG;
            else
                c.type = ModelType.SMG;
        } else if(clocksFound)
            c.type = ModelType.PTA;
        else if(nonDeterministicPTA || !options.dtmcFallback)
            c.type = ModelType.MDP;
        else
            c.type = ModelType.DTMC;
        switch(c.type) {
            case DTMC:
            case MDP:
            case SMG:
                // can be needed, if the system has originally been with timed,
                // but then it turned out, that no clocks are actually employed,
                // and so, the model type has been changed to a narrower one
                for(PTA tadd : c.ptas.values())
                    tadd.clock = null;
                break;

            case PTA:
            case TSMG:
                break;

            default:
                throw new RuntimeException("unknown model type");
        }
    }
    /**
     * Writes a TADD compilation to a file.
     * 
     * @param file                      file to create
     * @param c                         TADD compilation
     * @return                           all model types generated
     */
    public abstract List<ModelType> write(File file, BackendPTACompilation c) throws IOException;
}
