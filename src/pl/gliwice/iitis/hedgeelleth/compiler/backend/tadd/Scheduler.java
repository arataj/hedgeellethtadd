/*
 * Scheduler.java
 *
 * Created on Nov 5, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.AbstractTADDGenerator;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.InternalRuntimeContainer;
import pl.gliwice.iitis.hedgeelleth.pta.*;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAConstant;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTADensity;

/**
 * Creates a scheduler definition of the generated PTAs.
 * 
 * @author Artur Rataj
 */
public class Scheduler {
    /**
     * Type of this scheduler.
     */
    public final SchedulerType type;
    /**
     * True if the scheduler should not add clock invariants to
     * the output model, but should only be used when generating
     * the model,  e. g. by the optimizer.<br>
     * 
     * Meaningless in the case of the free scheduler.
     */
    public final boolean implicit;
    /**
     * If any existing clock invariants should be overridden by this
     * scheduler.<br>
     * 
     * Meaningless in the case of the free scheduler.
     */
    public final boolean overrides;
    /**
     * Constant used when type is CONST.
     */
    protected final double c;
    /**
     * Constant used when type is NXP.
     */
    protected final double lambda;
    /**
     * Should a transition be appended after certain sleep transitions. Used only
     * by non--overriding schedulers.
     * 
     * See <code>ClockSleepGenerator</code> for details.
     */
    protected final boolean sleepAppend;
    /**
     * Should a delaying transition be prepended before a synchronising loop.
     */
    protected final boolean nonZeroSyncDelay;
    /**
     * Should all non--syncing transitions have guards, that disable them, given there is
     * at least a single synchronisation possible?
     */
    protected final boolean prioritiseSync;

    /**
     * Takes into account wait locations when completing the location guards.
     */
    public static class LocationManager {
        /**
         * Location guards added so far.
         */
        Map<String, Set<PTALocationGuard>> locationGuards;
        /**
         * Wait locations added so far.
         */
        List<PTATransition> waits;

        public LocationManager() {
            locationGuards = new HashMap<>();
            waits = new LinkedList<>();
        }
        /**
         * Adds a new location guard to a set of such guards.
         * 
         * @param x the syncing label
         * @param lg a location guard to add
         */
        public void add(PTALabel label, PTALocationGuard lg) {
            Set<PTALocationGuard> set = locationGuards.get(label.name);
            if(set == null) {
                set = new HashSet<>();
                locationGuards.put(label.name, set);
            }
            set.add(lg);
        }
        /**
         * A wait location to eventually complement any already existing guard with new
         * locations if needed.
         * 
         * @param x a transition, whose source state is the wait location
         */
        public void add(PTATransition x) {
            waits.add(x);
        }
        /**
         * Completes the location guards. Call after each guard and wait location is called.
         */
        public void complete() {
            for(PTATransition x : waits) {
                Set<PTALocationGuard> set = locationGuards.get(x.label.name);
                for(PTALocationGuard g : set) {
                    List<PTATransition> l = new LinkedList<>();
                    l.add(x);
                    g.addLocations(l);
                }
            }
        }
    }
    /**
     * Creates a new scheduler.
     * 
     * @param type type of the scheduler to create
     * @param implicit if the scheduler is to be implicit
     * @param overrides if the scheduler overrides any
     * existing clock contraints
     * @param c c parameter, used only if type is CONST
     * @param lambda lambda parameter, used only if type is NXP
     * @param sleepAppend copied to <code>speelAppend</code>;
     * must be false for an overriding scheduler
     * @param nonZeroSyncDelay copied to <code>nonZeroSyncDelay</code>
     * @param prioritiseSync copied to <code>prioritiseSync</code>
     */
    public Scheduler(SchedulerType type, boolean implicit,
            boolean overrides, double c, double lambda,
            boolean sleepAppend, boolean nonZeroSyncDelay,
            boolean prioritiseSync) {
        this.type = type;
        this.implicit = implicit;
        this.overrides = overrides;
        this.c = c;
        this.lambda = lambda;
        this.sleepAppend = sleepAppend;
        this.nonZeroSyncDelay = nonZeroSyncDelay;
        this.prioritiseSync = prioritiseSync;
    }
    /**
     * Returns the coefficient c. A runtime exception is thrown
     * for an compatible type of this scheduler.
     * 
     * @return c
     */
    public double getC() {
        if(type != SchedulerType.CONST)
            throw new RuntimeException("c coefficient is meaningless");
        else
            return c;
    }
    /**
     * Returns the coefficient k. A runtime exception is thrown
     * for an compatible type of this scheduler.
     * 
     * @return k
     */
    public double getLambda() {
        if(type != SchedulerType.NXP)
            throw new RuntimeException("lambda coefficient is meaningless");
        else
            return lambda;
    }
    /**
     * Returns the value of <code>sleepAppend</code>.
     * 
     * @return if an extra transition should be appended after a sleep transition
     */
    public boolean getSleepAppend() {
        return sleepAppend;
    }
    /**
     * Returns the value of <code>nonZeroSyncDelay</code>.
     * 
     * @return if it should not be possible for a sync delay to
     * begin from 0
     */
    public boolean getNonZeroSyncDelay() {
        return nonZeroSyncDelay;
    }
    /**
     * Returns the value of <code>prioritiseSync</code>.
     * 
     * @return if syncing transitions should have a priority, by adding
     * respective guards on non--syncing transitions
     */
    public boolean getPrioritiseSync() {
        return prioritiseSync;
    }
    /**
     * If this scheduler required a PTA to have a clock.
     * 
     * @return if clock invariants are added by this scheduler
     */
    public boolean usesClock() {
        return type != SchedulerType.FREE && !implicit;
    }
    private boolean isTimed() {
        switch(type) {
            case CONST:
            case NXP:
                return true;
                
            case FREE:
            case HIDDEN:
            case IMMEDIATE:
                return false;
                
            default:
                throw new RuntimeException("unknown scheduler type");
        }
    }
    /**
     * A set of attrubites for creating looped synchronisation.
     */
    class LoopParams {
        Set<PTAState> sEmpty = new HashSet<>();
        Set<PTATransition> xEmpty = new HashSet<>();
        // initial states of the synchronising subautomatons to add
        Set<PTAState> sInitial = new HashSet<>();
        // loops, keyed with the initial states and guards
        Map<PTAState, Map<AbstractPTAExpression, PTAState>> sLoop = new HashMap<>();
        Map<PTAState, Map<AbstractPTAExpression, PTATransition>> xLoop = new HashMap<>();
        
        PTAState s = null;
        PTATransition x;
        PTAState forwardS = null;
        PTATransition forwardX = null;
        LocationManager lm;
        boolean waitState;
        List<PTATransition> other;
        Set<String> otherOwners;

        /**
         * Resets the loop params to support a new transition.
         * 
         * @param x_
         * @param labels
         * @param labelNondeterminism 
         */
        private void newTransition(PTATransition x_,
                AbstractTADDGenerator.Labels labels,
                boolean labelNondeterminism) {
            x = x_;
            s = x.sourceState;
            forwardS = null;
            forwardX = null;
            waitState = type == SchedulerType.CONST && c == 0.0;
            if(x.label != null && x.label.type != PTALabel.Type.FREE) {
                boolean directional = x.label.isDirectional();
                if(directional) {
                    ComplementaryLabel cl = labels.complementary.get(
                            x.label.name);
    //if(cl == null)
    //    cl = cl;
                    if(isTimed() && cl.broadcast)
                        throw new RuntimeException("unexpected broadcast label in a " +
                                "timed synchronisation");
                    other = cl.getOtherTransitions(x);
                } else {
                    other = labels.getBarrierRest(x);
                }
                otherOwners = new HashSet<>();
                for(PTATransition t : other)
                    otherOwners.add(t.ownerName);
                if(isTimed() && otherOwners.size() > 1 &&
                        (x.label.type != PTALabel.Type.BARRIER ||
                            labelNondeterminism))
                    throw new RuntimeException("non--deterministic labels " +
                            "not supported by timed synchronisation if more than two " +
                            "automatons synchronise at once");
            }
        }
    }
    /**
     * Sets synchronisation labels Uppaal-style, by modifying each
     * barrier into a sender -- receiver pair.
     * 
     * @param lp loop params
     * @param x transition
     */
    private void polarise(LoopParams lp, PTATransition x)  {
        if(lp.other.size() != 1)
            throw new RuntimeException("only a pair of barriers expected");
        PTALabel peer = lp.other.iterator().next().label;
        switch(peer.type) {
            case BARRIER:
                x.label.type = PTALabel.Type.SENDER;
                break;

            case SENDER:
                x.label.type = PTALabel.Type.RECEIVER;
                break;

            default:
                throw new RuntimeException("unexpected peer type");
        }
    }
    /**
     * Sets synchronisation labels Prism-style, by adding loops if
     * required by the scheduler.
     * 
     * @param pta pta
     * @param lp loop params
     * @param x transition
     */
    private void addLoop(PTA pta, LoopParams lp) {
        if(pta instanceof TADDLock) {
            if(lp.otherOwners.size() > 1)
                throw new RuntimeException("a single lock label should synchronise " +
                        "with only a single automaton");
            // just add location guards; the common loops
            // are put directly to closed/open locations
            // acquire a respective loop or create a new one, if it does not exist
            // yet
            PTATransition loop = null;
            for(int index : lp.s.output.keySet())
                for(PTATransition tt : lp.s.output.get(index))
                    if((!lp.waitState && tt.targetState == lp.s) ||
                            (lp.waitState && tt.targetState != ((TADDLock)pta).closed &&
                            tt.targetState != ((TADDLock)pta).open)) {
                        loop = tt;
                        break;
                    }
            boolean newLoop = loop == null;
            if(newLoop) {
                loop = new PTATransition(null);
                loop.sourceState = lp.s;
                PTAState wait;
                if(lp.waitState) {
                    int index = pta.states.firstKey() - 1;
                    wait = new PTAState(index);
                    pta.states.put(index, wait);
                    loop.targetState = wait;
                } else {
                    wait = null;
                    loop.targetState = lp.s;
                }
                PTALocationGuard lg = new PTALocationGuard(lp.other,
                        PTALocationGuard.Type.NEGATED_COMMON);
                loop.guard = lg;
                loop.guard.backendEnableStop();
                pta.addTransition(loop);
            }
            // a loop which is common for several synchronisations is
            // added multiple times with different keys
            lp.lm.add(lp.x.label, (PTALocationGuard)loop.guard);
            PTA.TransitionBag xbag = pta.keyBefore(loop);
            PTALocationGuard lg = (PTALocationGuard)loop.guard;
            lg.addLocations(lp.other); 
            pta.keyAfter(xbag);
            if(lp.waitState) {
                PTATransition c = lp.x.clone();
                c.sourceState = loop.targetState;
                pta.addTransition(c);
                lp.lm.add(c);
            }
            if(lp.x.guard != null)
                throw new RuntimeException("unexpected guard");
            xbag = pta.keyBefore(lp.x);
            lg = new PTALocationGuard(lp.other, PTALocationGuard.Type.DIRECT);
            lp.x.guard = lg;
            lp.lm.add(lp.x.label, lg);
            lp.x.guard.backendEnableStop();
            pta.keyAfter(xbag);
            lp.x = loop;
            lp.xEmpty.add(lp.x);
            lp.sEmpty.add(lp.s);
       } else {
            if(lp.x.probability != null)
                throw new RuntimeException("unexpected probability in a synchronised transition");
            PTAState wait;
            PTATransition loop;
            // is there an intermediate loop state already for the guard?
            Map<AbstractPTAExpression, PTAState> sSet = lp.sLoop.get(lp.s);
            Map<AbstractPTAExpression, PTATransition> xSet = lp.xLoop.get(lp.s);
            if(sSet != null) {
                wait = sSet.get(lp.x.guard);
                loop = xSet.get(lp.x.guard);
            } else {
                wait = null;
                loop = null;
            }
            boolean newLoop = wait == null;
            if(!newLoop) {
                // complete the loop
                PTA.TransitionBag xbag = pta.keyBefore(loop);
                ((PTALocationGuard)loop.guard).addLocations(lp.other);
                pta.keyAfter(xbag);
            } else {
                // not -- add one,
                // or use a number of forwarding transitions if guards are present
                // -- it is possible because guarded forks produced by the translator
                // are never non--deterministc
                //
                PTATransition tt = new PTATransition(null);
                tt.sourceState = lp.s;
                tt.backendOps.addAll(lp.x.backendOps);
                int index = pta.states.firstKey() - 1;
                wait = new PTAState(index);
                pta.states.put(index, wait);
                tt.targetState = wait;
                pta.addTransition(tt);
                lp.sInitial.add(lp.s);
                loop = new PTATransition(null);
                loop.sourceState = wait;
                if(lp.waitState) {
                    index = pta.states.firstKey() - 1;
                    PTAState wait2 = new PTAState(index);
                    pta.states.put(index, wait2);
                    loop.targetState = wait2;
                } else
                    loop.targetState = wait;
                pta.addTransition(loop);
                // mark the initial state for decoration
                lp.forwardS = lp.s;
                // mark the forwarding transition for decoration
                lp.forwardX = tt;
                // move a possible guard backward, to the beginning of the
                // synchronising sequence
                PTA.TransitionBag xbag = pta.keyBefore(lp.forwardX);
                lp.forwardX.guard = lp.x.guard;
                pta.keyAfter(xbag);
                xbag = pta.keyBefore(loop);
                PTALocationGuard lg = new PTALocationGuard(lp.other,
                        PTALocationGuard.Type.NEGATED);
                loop.guard = lg;
                loop.guard.backendEnableStop();
                pta.keyAfter(xbag);
            }
            // a loop which is common for several synchronisations is
            // added multiple times with different keys
            lp.lm.add(lp.x.label, (PTALocationGuard)loop.guard);
            if(lp.waitState) {
                PTATransition c = lp.x.clone();
                c.sourceState = loop.targetState;
                pta.addTransition(c);
                lp.lm.add(c);
            }
            if(sSet == null) {
                sSet = new HashMap<>();
                lp.sLoop.put(lp.s, sSet);
                xSet = new HashMap<>();
                lp.xLoop.put(lp.s, xSet);
            }
            sSet.put(lp.x.guard, wait);
            xSet.put(lp.x.guard, loop);
            ((BackendPTA)pta).removeTransition(lp.x);
            PTALocationGuard lg = new PTALocationGuard(lp.other,
                    PTALocationGuard.Type.DIRECT);
            lp.x.guard = lg;
            lp.lm.add(lp.x.label, lg);
            lp.x.guard.backendEnableStop();
            // move x after the loop state
            lp.x.sourceState = wait;
            pta.addTransition(lp.x);
            loop.backendOps.addAll(lp.x.backendOps);
            // mark the synchronising subautomaton for decoration
            lp.s = wait;
            lp.sEmpty.add(lp.s);
            lp.x = loop;
            lp.xEmpty.add(lp.x);
        }
    }
    /**
     * Applies invariants. Ignores the field <code>implicit</code>, and so it should
     * typically only be called on schedulers with that field being false. Empty transitions
     * could be added by this method, optimisation might thus be needed in effect.<br>
     * 
     * This PTA modification is state formula--sensitive, see
     * <code>PTA.testFormulas()</code> for details.
     * 
     * @param pta a PTA to apply time invariants to
     * @param labels description of complementary and barrier labels, for finding
     * sets of synchronised transitions
     * @param labelNondeterminism if labels are non--deterministic UPAAL--style
     * @param compilation compilation
     * @param clockVariables clock variables, to add a new clock if missing
     * @param lm a location manager; should be completed by the called only
     * after invariants are applied to all PTAs 
     */
    public void applyInvariants(PTA pta,
            AbstractTADDGenerator.Labels labels, boolean labelNondeterminism,
            BackendPTACompilation compilation,
            InternalRuntimeContainer clockVariables,
            LocationManager lm) throws PTAException {
        LoopParams lp = new LoopParams();
        lp.lm = lm;
        for(PTAState s : pta.states.values())
            if(s.clockLimitHigh == null)
                lp.sEmpty.add(s);
        for(PTATransition x : pta.transitions) {
            if(x.clockLimitLow == null)
                lp.xEmpty.add(x);
        }
        double[] lambdaParam = { lambda };
        List<PTATransition> transitions = new LinkedList<>(pta.transitions);
        for(PTATransition x_ : transitions) {
            lp.newTransition(x_, labels, labelNondeterminism);
            if(lp.x.label != null) {
                switch(type) {
                    case FREE:
                        break;
                        
                    case CONST:
                    case NXP:
// if(x.label.toString().startsWith("notify") &&
//        x.label.toString().endsWith("Receiver_0!"))
//    x = x;
//if(x.label.name.startsWith("in") && x.label.name.contains("Receiver"))
//    x = x;
                        // if to create a short loop, or rather a transition to a wait state,
                        // from which lead a separate transition
                        if(lp.waitState && labelNondeterminism) {
                            // UPPAAL--style synchronisation, simply apply urgency
                            lp.x.label.urgent = true;
                        } else
                            addLoop(pta, lp);
                        //
                        // fallthrough
                        //
                    case IMMEDIATE:
                    case HIDDEN:
                        // locks may need a new clock
                        if(pta.clock == null)
                            pta.clock = compilation.newClock(clockVariables, pta.name).v();
                        break;
                        
                        
                    default:
                        throw new RuntimeException("unknown type");
                }
                //
            }
            if(labelNondeterminism && lp.x.label != null &&
                    lp.x.label.type == PTALabel.Type.BARRIER)
                // Uppaal--style barriers must be polarised
                polarise(lp, lp.x);
            PTA.TransitionBag xbag;
            if(lp.x != null)
                xbag = pta.keyBefore(lp.x);
            else
                xbag = null;
            PTA.StateBag sbag = pta.keyBefore(lp.s);
            switch(type) {
                case FREE:
                {
                    if(overrides) {
                        lp.s.clockLimitHigh = null;
                        if(lp.x != null)
                            lp.x.clockLimitLow = null;
                    }
                    break;
                }
                case IMMEDIATE:
                {
                    if(overrides ||
                            (lp.sEmpty.contains(lp.s) &&
                            (lp.x == null || lp.xEmpty.contains(lp.x)))) {
                        lp.s.clockLimitHigh = new PTAClockCondition(pta.clock,
                                new PTAConstant(0, false),
                                CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL);
                        lp.s.clockLimitHigh.backendEnableStop();
                        if(lp.x != null)
                            lp.x.clockLimitLow = null;
                    }
                    break;
                }
                case HIDDEN:
                {
                    if(overrides ||
                            (lp.sEmpty.contains(lp.s) &&
                            (lp.x == null || lp.xEmpty.contains(lp.x)))) {
                        lp.s.clockLimitHigh = new PTAClockCondition(pta.clock,
                                new PTAConstant(0, false),
                                CodeOpBinaryExpression.Op.BINARY_LESS);
                        lp.s.clockLimitHigh.backendEnableStop();
                        if(lp.x != null)
                            lp.x.clockLimitLow = null;
                    }
                    break;
                }
                case CONST:
                {
                    if(overrides ||
                            (lp.sEmpty.contains(lp.s) &&
                            (lp.x == null || lp.xEmpty.contains(lp.x)))) {
                        lp.s.clockLimitHigh = new PTAClockCondition(pta.clock,
                                new PTAConstant(c, false),
                                CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL);
                        lp.s.clockLimitHigh.backendEnableStop();
                        if(lp.x != null) {
                            lp.x.clockLimitLow = new PTAClockCondition(new PTAConstant(c, false),
                                    pta.clock,
                                    CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL);
                            lp.x.clockLimitLow.backendEnableStop();
                        }
                    }
                    break;
                }
                case NXP:
                {
                    if(overrides ||
                            (lp.sEmpty.contains(lp.s) &&
                            (lp.x == null || lp.xEmpty.contains(lp.x)))) {
                        lp.s.clockLimitHigh = null;
                        if(lp.x != null) {
                            lp.x.clockLimitLow = new PTAClockCondition(pta.clock,
                                    new PTADensity(PTADensity.ParameterType.NXP, lambdaParam),
                                    CodeOpBinaryExpression.Op.BINARY_EQUAL);
                            lp.x.clockLimitLow.backendEnableStop();
                        }
                    }
                    break;
                }
                default:
                    throw new RuntimeException("unknown type");
            }
            pta.keyAfter(sbag);
            if(lp.x != null) {
                //boolean repeated = xbag.outputSet.contains(lp.x);
                pta.keyAfter(xbag);
                //if(repeated)
                //    pta.transitions.remove(lp.x);
            }
            if(lp.forwardS != null && nonZeroSyncDelay) {
                // make the timing of the transition forwarding to the loop exactly as
                // that of the loop
                if(lp.s.clockLimitHigh != null && lp.forwardS != null) {
                    sbag = pta.keyBefore(lp.forwardS);
                    lp.forwardS.clockLimitHigh = lp.s.clockLimitHigh.clone();
                    pta.keyAfter(sbag);
                }
                if(lp.forwardX != null) {
                    xbag = pta.keyBefore(lp.forwardX);
if(lp.x.clockLimitLow == null)
    lp.x = lp.x;
                    lp.forwardX.clockLimitLow = lp.x.clockLimitLow.clone();
                    pta.keyAfter(xbag);
                }
            }
        }
        /*
        switch(type) {
            case FREE:
            case HIDDEN:
                break;

            case CONST:
            case NXP:
            case IMMEDIATE:
                // TADDFactory.addResets(pta);
                break;


            default:
                throw new RuntimeException("unknown type");
        }
         */
        //if(updateConnectivity)
        //    TADDFactory.checkConnectivity(pta);
    }
    /**
     * <p>If there are any transitions, that should wait for the synchronised transitions to
     * happen, mark these.</p>
     * 
     * <p>Can be called when no more new transitions are added to the PTA.</p>
     * 
     * @param pta pta to modify
     */
    public void markWaitForSync(PTA pta) {
        if(prioritiseSync)
            for(PTATransition x : pta.transitions)
                if(x.label == null)
                    x.backendWaitForSync = true;
    }
    @Override
    public String toString() {
        String s = "scheduler[ " + type.toString();
        if(implicit)
            s += ", implicit";
        if(overrides)
            s += ", overrides";
        s += ", c = " + c + (type != SchedulerType.CONST ? " (ignored)" : "");
        s += ", lambda = " + lambda + (type != SchedulerType.NXP ? " (ignored)" : "");
        s += " ]";
        return s;
    }
}
