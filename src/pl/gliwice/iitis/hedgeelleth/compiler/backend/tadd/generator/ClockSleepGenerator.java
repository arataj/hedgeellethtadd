/*
 * ClockSleepGenerator.java
 *
 * Created on Jul 23, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.pta.*;
import pl.gliwice.iitis.hedgeelleth.pta.values.*;

import hc.SleepConstants;

/**
 * A default sleep generator. Assumes, that explicit clocks can be used.
 * 
 * @author Artur Rataj
 */
public abstract class ClockSleepGenerator implements AbstractSleepGenerator {
    @Override
    public void generateSleepAnnotation(CodeOpNone n, TADDGeneratorContext context)
            throws PTAException {
        context.generator.checkConnectPrevious(context);
        // initial state
        if(context.clock == null)
            context.createClock();
        ///resetState.clockLimitHigh = new PTAClockCondition(
        ///    context.clock,
        ///    new PTAConstant(0.0, false),
        ///    CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL);
        ///int midIndex = context.factory.newStateIndex();
        // transition: reset clock
        ///PTATransition tReset = new PTATransition(null);
        ///AbstractPTAExpression resetClock = new PTAAssignment(context.clock,
        ///        new PTAConstant(0, false));
        ///tReset.update.add(resetClock);
        ///tReset.targetIndex = midIndex;
        ///context.factory.addTransition(context.currIndex, tReset);
        int sleepType = (int)((PTAConstant)context.generator.getValue(n.args.get(0).value,
                context.rm)).value;
        // the operator to use, assuming clock is the right operand
        AbstractCodeOp.Op operator;
        // if clock is the right operand
        boolean clockOnTheRight = true;
        // if to add an empty transition, so that the scheduler can
        // use it for a per--operation delay
        boolean addEmptyTransition = false;
        // null operator for sleep type--default
        switch(context.generator.options.scheduler.type) {
            case FREE:
                if(sleepType == SleepConstants.SLEEP_EXACT_MILLISECONDS ||
                        sleepType == SleepConstants.SLEEP_EXACT_SECONDS)
                    // modify the operator instead of using an additional transition
                    operator = CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL;
                else
                    operator = null;
                if(sleepType == SleepConstants.SLEEP_MAX_SECONDS)
                    addEmptyTransition = true;
                // SLEEP_MIN_SECONDS fits naturally to this scheduler
                break;

            case CONST:
                if(!context.generator.options.scheduler.overrides &&
                        context.generator.options.scheduler.getC() != 0)
                    addEmptyTransition = true;
                operator = null;
                break;

            case NXP:
                if(!context.generator.options.scheduler.overrides)
                    addEmptyTransition = true;
                operator = null;
                break;
                
            case IMMEDIATE:
            case HIDDEN:
                operator = null;
                break;

            default:
                throw new RuntimeException("unknown scheduler type");
        }
        if(!context.generator.options.scheduler.getSleepAppend())
            addEmptyTransition = false;
        PTATransition tEmpty;
        if(addEmptyTransition) {
            tEmpty = new PTATransition(null);
            CodeOpNone nEmpty = (CodeOpNone)n.clone();
            nEmpty.annotations.add(PTATransition.
                    ANNOTATION_SPECIAL_EMPTY_TRANSITION_STRING);
            AbstractTADDGenerator.addSourceInfo(tEmpty, context, nEmpty);
        } else
            tEmpty = null;            
        if(operator == null)
            switch(sleepType) {
                case SleepConstants.SLEEP_EXACT_MILLISECONDS:
                case SleepConstants.SLEEP_EXACT_SECONDS:
                    operator = CodeOpBinaryExpression.Op.BINARY_EQUAL;
                    break;
                    
                case SleepConstants.SLEEP_MIN_SECONDS:
                    operator = CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL;
                    break;
                    
                case SleepConstants.SLEEP_MAX_SECONDS:
                    operator = CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL;
                    clockOnTheRight = false;
                    break;
                    
                default:
                    throw new RuntimeException("unknown sleep type");
            }
        List<AbstractCodeOp> sourceOps = new LinkedList<>();
        sourceOps.add(n);
        PTATransition tLast;
        if(context.deferredCondition == null) {
            // no non--determinism -- single transition
            RangedCodeValue rv = n.args.get(1);
            if(rv.range != null) {
                ///context.currIndex = midIndex;
                context.generator.processPrimaryRange(rv, n, context);
                ///midIndex = context.currIndex;
            }
            AbstractCodeValue v = rv.value;
            // ((PTAConstant)context.generator.getValue(v, context.rm)).value;
            PTATransition tSleep = new PTATransition(null);
            // tSleep.update = resetClock;
            final String ARGUMENT_MESSAGE =
                "argument of @SLEEP must " +
                "be constant or density function or range of values with constant " +
                "limits";
            if(!(v instanceof CodeConstant))
                throw new PTAException(n.getStreamPos(), ARGUMENT_MESSAGE);
            else {
                AbstractPTAValue value = context.generator.getValue(v, context.rm);
                if(value instanceof PTAConstant) {
                    // scale if needed, and then convert to integer if possible
                    double seconds = ((PTAConstant)value).value;
                    if(sleepType == hc.SleepConstants.SLEEP_EXACT_MILLISECONDS)
                        seconds = seconds/1000.0;
                    // convert to integer if possible
                    value = new PTAConstant(seconds, seconds % 1 != 0);
                }
                if(clockOnTheRight)
                    tSleep.clockLimitLow = new PTAClockCondition(
                            value,
                            context.clock,
                            operator);
                else
                    tSleep.clockLimitLow = new PTAClockCondition(
                            context.clock,
                            value,
                            operator);
                context.generator.setStop(tSleep.clockLimitLow, context);
            }
            AbstractTADDGenerator.addSourceInfo(tSleep, context, sourceOps);
            context.factory.addTransition(context.currIndex, tSleep);
            if(addEmptyTransition) {
                int emptyIndex = context.factory.newStateIndex();
                tSleep.connectTargetIndex = emptyIndex;
                context.factory.addTransition(emptyIndex, tEmpty);
                tLast = tEmpty;
            } else
                tLast = tSleep;
        } else {
            //
            // an assignment from @RANDOM or @DIST_* preceedes
            //
            AbstractPTAValue v = ((PTAUnaryExpression)
                    context.deferredCondition).sub;
            context.deferredCondition = null;
            sourceOps.add(context.deferredOp);
            AbstractCodeOp tmpOp = context.deferredOp;
            context.deferredOp = null;
            if(v instanceof PTADensity) {
                // it was @DIST_*
                PTADensity d = (PTADensity)v;
                PTATransition t = new PTATransition(null);
                if(clockOnTheRight)
                    t.clockLimitLow = new PTAClockCondition(
                            v,
                            context.clock,
                            operator);
                else
                    t.clockLimitLow = new PTAClockCondition(
                            context.clock,
                            v,
                            operator);
                context.generator.setStop(t.clockLimitLow, context);
                AbstractTADDGenerator.addSourceInfo(t, context, sourceOps);
                context.factory.addTransition(context.currIndex, t);
                if(addEmptyTransition) {
                    int emptyIndex = context.factory.newStateIndex();
                    t.connectTargetIndex = emptyIndex;
                    context.factory.addTransition(emptyIndex, tEmpty);
                    tLast = tEmpty;
                } else
                    tLast = t;
            } else {
                // it was @RANDOM, so multiple
                // parallel transitions expressing non--determinism
                double num;
                if(v instanceof PTAConstant) {
                    num = ((PTAConstant)v).value;
                    if(num <= 0)
                        throw new PTAException(tmpOp.getStreamPos(),
                                "argument of @RANDOM must " +
                                "be positive");
                    // cound be Integer.MAX_VALUE, but Short.MAX_VALUE
                    // disables generation of too large automatons/files
                    if(num > Short.MAX_VALUE)
                        throw new PTAException(tmpOp.getStreamPos(),
                                "argument of @RANDOM too large for " +
                                "non--deterministic sleep");
                } else
                    throw new PTAException(tmpOp.getStreamPos(),
                            "range of random values " +
                            "used as an argument of @SLEEP must have constant " +
                            "limits");
                int endIndex = context.factory.newStateIndex();
                BranchId branchId = new BranchId(
                            BranchId.Type.NONDETERMINISTIC);
                branchId.arguments = NdUtils.getRandomNdArguments(
                        (CodeOpNone)tmpOp, context.generator, context.rm);
                for(int i = 0; i < num; ++i) {
                    PTATransition tSleep = new PTATransition(null);
                    tSleep.branchId = branchId;
                    tSleep.ndInt = new LinkedList<>(Arrays.asList(i));
                    PTAConstant c;
                    if(sleepType == hc.SleepConstants.SLEEP_EXACT_MILLISECONDS) {
                        double d = i/1000.0;
                        c = new PTAConstant(d,  d % 1 != 0);
                    } else {
                        c = new PTAConstant(i, false);
                    }
                    if(clockOnTheRight)
                        tSleep.clockLimitLow = new PTAClockCondition(
                                c,
                                context.clock,
                                operator);
                    else
                        tSleep.clockLimitLow = new PTAClockCondition(
                                context.clock,
                                c,
                                operator);
                    context.generator.setStop(tSleep.clockLimitLow, context);
                    AbstractTADDGenerator.addSourceInfo(tSleep, context, sourceOps);
                    tSleep.connectTargetIndex = endIndex;
                    context.factory.addTransition(context.currIndex, tSleep);
                }
                // non--deterministic sleep  has always an additional
                // transition anyway
                tLast = new PTATransition(null);
                AbstractTADDGenerator.addSourceInfo(tLast, context, sourceOps);
                context.factory.addTransition(endIndex, tLast);
            }
        }
        context.prevTransition = tLast;
    }
}
