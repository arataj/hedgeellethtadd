/*
 * BackendPTARange.java
 *
 * Created on Jul 20, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeConstant;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeVariable;
import pl.gliwice.iitis.hedgeelleth.compiler.code.RangedCodeValue;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeFieldDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.TypeRangeMap;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.SourcePosition;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.interpreter.AbstractInterpreter;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.pta.PTASystem;
import pl.gliwice.iitis.hedgeelleth.pta.PTAException;
import pl.gliwice.iitis.hedgeelleth.pta.PTARange;

/**
 * A PTA range, plus the functionality needed by the compiler.
 * 
 * @author Artur Rataj
 */
public class BackendPTARange implements SourcePosition {
    /**
     * Wrapped PTA range.
     */
    protected PTARange r;
    // /**
    //  * If created from a code variable, that has a source--level range.
    //  */
    // public boolean sourceLevel;
    
    /**
     * Creates a wrapper for a PTA range.
     *
     * @param pos position in the source stream
     * @param r wrapped range
     */
    public BackendPTARange(StreamPos pos, PTARange r) {
        this.r = r;
        setStreamPos(pos);
    }
    /**
     * Creates a new instance of a wrapped PTARange, using a code variable's
     * custom range or, if missing, code variable's type default range.
     * The variable must be of arithmetic types or boolean, or an array of such.<br>
     *
     * In the case of an array, the range of elements is created, just
     * like the primitive range of an array variable is the range of the
     * array's elements.<br>
     *
     * If <code>d</code> is a local, then its primitive range, if any, must
     * be evaluated to constant, otherwise an exception is thrown; this is
     * because this method does not impose any conditions on validity of
     * values of locals in <code>rm</code>, but it assumes that bound variables
     * of fields are properly initialized after the field's objects are
     * fully constructed.<br>
     * 
     * This is the only constructor, that allows for creating source--level ranges.
     * 
     * @param d                         dereference that specify the code variable,
     *                                  that in turn specify the range;
     *                                  the variable can be an array; in such
     *                                  case, the array's element range is
     *                                  taken into account
     * @param interpreter               interpreter
     * @param rm                        runtime method
     * @param defaultTypeRanges         default type ranges, usually from
     *                                  <code>getTADDDefaults()</code>
     */
    public BackendPTARange(CodeFieldDereference d, AbstractInterpreter interpreter,
            RuntimeMethod rm, TypeRangeMap defaultTypeRanges) throws PTAException {
        r = new PTARange(null, 0, 0);
        CodeVariable object = d.object;
        CodeVariable cv = d.variable;
        Type t;
        if(cv.type.isArray(null))
            t = cv.type.getElementType(null, 0);
        else
            t = cv.type;
        boolean logic = t.isOfBooleanType();
        if(!logic && !t.isOfIntegerTypes() && !t.isOfFloatingPointTypes())
            throw new RuntimeException("variable not of arithmetic types or boolean");
        if(cv.hasPrimitiveRange(false)) {
/*if(cv.type.isArray(null))            
    cv = cv;*/
            r().backendPos = cv.primitiveRange.getStreamPos();
            r().backendIsSourceLevel = cv.primitiveRange.isSourceLevel();
            Literal cMin;
            Literal cMax;
            if(cv.isLocal()) {
                if(!cv.primitiveRange.isEvaluatedAsConstant())
                    throw new PTAException(cv.getStreamPos(),
                            "primitive range not evaluated as constant");
                cMin = ((CodeConstant)cv.primitiveRange.min).value;
                cMax = ((CodeConstant)cv.primitiveRange.max).value;
            } else try {
                cMin = interpreter.getValue(new RangedCodeValue(
                        cv.primitiveRange.reconstructMin(object)), rm);
                cMax = interpreter.getValue(new RangedCodeValue(
                        cv.primitiveRange.reconstructMax(object)), rm);
            } catch(InterpreterException e) {
                throw new PTAException(e);
            }
            r().range = new Type.TypeRange(cv.type.isOfFloatingPointTypes());
            if(logic) {
                if(cMin.getMaxPrecisionInteger() == 1)
                    BackendPTACompilation.setMin(r().range, PTARange.TRUE_VALUE);
                else
                    BackendPTACompilation.setMin(r().range, PTARange.FALSE_VALUE);
                if(cMax.getMaxPrecisionInteger() == 1)
                    BackendPTACompilation.setMax(r().range, PTARange.TRUE_VALUE);
                else
                    BackendPTACompilation.setMax(r().range, PTARange.FALSE_VALUE);
            } else {
                BackendPTACompilation.setMin(r().range, cMin.getMaxPrecisionFloatingPoint());
                BackendPTACompilation.setMax(r().range, cMax.getMaxPrecisionFloatingPoint());
            }
        } else {
            r().backendPos = cv.getStreamPos();
            r().range = new Type.TypeRange(defaultTypeRanges.get(
                    t.getPrimitive()));
        }
    if(d.variable.type.isBoolean())
        // boolean types are guaranteed that they do not exceed
        // their range
        r().backendGuaranteed = true;
    r().checkValidity(d.toNameString());
    }
    /**
     * Returns the wrapped range.
     * 
     * @return a PTA variable, 
     */
    public PTARange r() {
        return r;
    }
    @Override
    public void setStreamPos(StreamPos pos) {
        r().backendPos = pos;
    }
    @Override
    public StreamPos getStreamPos() {
        return r().backendPos;
    }
    @Override
    public String toString() {
        return r().toString();
    }
}
