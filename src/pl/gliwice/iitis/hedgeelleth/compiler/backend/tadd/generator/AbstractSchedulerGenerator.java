/*
 * AbstractSchedulerGenerator.java
 *
 * Created on Jul 22, 2009, 10:37:33 AM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator;

/**
 * An abstract scheduler generator.
 *
 * @author Artur Rataj
 */
public abstract interface AbstractSchedulerGenerator {
}
