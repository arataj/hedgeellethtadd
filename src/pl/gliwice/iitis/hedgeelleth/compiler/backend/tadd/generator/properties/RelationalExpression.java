/*
 * RelationalExpression.java
 *
 * Created on Feb 19, 2010, 11:47:17 AM
 *
 * Copyright (c) 2010 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.properties;

import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAVariable;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.BinaryExpression;

/**
 * A relational expression, to be used as a part of a path property.
 *
 * @author Artur Rataj
 */
public class RelationalExpression extends AbstractConditionalExpression {
    /**
     * Left side: a variable.
     */
    public AbstractPTAVariable left;
    /**
     * Operator.
     */
    public BinaryExpression.Op r;
    /**
     * Right side: a constant value.
     */
    public double right;

    /**
     * Creates a new instance of RelationalExpression.
     *
     * @param left                      left side: a variable
     * @param r                         relational operator
     * @param right                     right side: a constant value
     */
    public RelationalExpression(AbstractPTAVariable left,
            BinaryExpression.Op r, double right) {
        super();
        this.left = left;
        if(!r.isRelational())
            throw new RuntimeException("operator must be relational");
        this.r = r;
        this.right = right;
    }
    @Override
    public String toString() {
        String operator = r.toString();
        return left.name + " " + operator + " " + right;
    }
}
