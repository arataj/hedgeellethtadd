/*
 * MirelaNode.java
 *
 * Created on Mar 26, 2018
 *
 * Copyright (c) 2018  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.mirela;

import java.util.*;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.BackendPTA;

import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeCompilation;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeObject;

/**
 * A mirela node.
 * 
 * @author Artur Rataj
 */
public class MirelaNode {
//    /**
//     * A prefix of all nodes of the type <code>Periodic</code>.
//     */
//    public final static String PERIODIC_PREFIX = "mirela.Periodic";
    /**
     * A class fo this node.
     */
    public static enum Clazz {
        // a timer, e.g. of a periodic thread
        TIMER,
        // a periodic thread
        PERIODIC_THREAD,
        // a bus
        BUS,
        // an unknown class
        UNKNOWN,
    };
    /**
     * An unique name of this node. Begins with its type.
     */
    public String name;
    /**
     * A PTA from which this node has been constructed.
     */
    public BackendPTA pta;
    /**
     * Class of this node.
     */
    public Clazz clazz;
    /**
     * A runtime object (a thread) represented by this node.
     */
    final RuntimeObject ro;
    /**
     * Producers of this node.
     */
    public final List<MirelaNode> producers;
    /**
     * Consumers of this node.
     */
    public final List<MirelaNode> consumers;
    /**
     * Size of a produced packet, -1 for none or unspecified.
     */
    int packetSize;
    /**
     * If a timer or a thread of a fixed frequency then the respective
     * period. Otherwise <code>Double.NaN</code>.
     */
    public double period;
    /**
     * A minimum transmission time (e.g. in a bus) or <code>Double.NaN</code>
     */
    public double xMin;
    /**
     * A maximum transmission time (e.g. in a bus) or <code>Double.NaN</code>
     */
    public double xMax;
    
    public MirelaNode(CodeCompilation cc, Collection<BackendPTA> ptas, RuntimeObject ro) {
        this.ro = ro;
        for(BackendPTA p : ptas)
            if(p.RO == this.ro) {
                name = p.name;
                pta = p;
                break;
            }
        if(name == null)
            throw new RuntimeException("no respective pta found");
        producers = new LinkedList<>();
        consumers = new LinkedList<>();
        if(ro.codeClass.isNarrower(cc.classes.get("mirela.Periodic")))
            clazz = Clazz.TIMER;
        else if(ro.codeClass.isNarrower(cc.classes.get("mirela.PeriodicThread"))) {
            clazz = Clazz.PERIODIC_THREAD;
        } else if(ro.codeClass.isNarrower(cc.classes.get("mirela.Bus")))
            clazz = Clazz.BUS;
        else
            clazz = Clazz.UNKNOWN;
        packetSize = -1;
        xMax = xMin = period = Double.NaN;
    }
    /**
     * If this node is of a special type, then it is returned.
     * 
     * @return special type or null if this node is not of a special type
     */
    public Clazz getClazz() {
        return clazz;
    }
    public String toString() {
        StringBuilder out = new StringBuilder();
        out.append(getClazz().toString() + " " + name);
        if(packetSize != -1)
            out.append(" PACKET_SIZE=" + packetSize);
        if(!Double.isNaN(period))
            out.append(" PERIOD=" + period);
        if(!Double.isNaN(xMin))
            out.append(" TRANSMISSION=<" + xMin + "; " + xMax + ">");
        out.append("\n");
        for(MirelaNode n : producers)
            out.append("\tp: " + n.name + "\n");
        for(MirelaNode n : consumers)
            out.append("\tc: " + n.name + "\n");
        return out.toString();
    }
}
