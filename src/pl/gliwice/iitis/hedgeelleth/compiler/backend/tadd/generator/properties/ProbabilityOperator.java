/*
 * ProbabilityOperator.java
 *
 * Created on Feb 19, 2010, 11:31:52 AM
 *
 * Copyright (c) 2010 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.properties;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.BinaryExpression;

/**
 * A simple probability operator. Specifies probability value <i>p</i>,
 * and one of the operators <i>r</i> being <, <=, >=, > or ==.
 * Together <i>p</i> and <i>r</i> represent an expression
 * <i>probability r p</i>.
 *
 * @author Artur Rataj
 */
public class ProbabilityOperator {
    /**
     * Probability.
     */
    public double p;
    /**
     * Operator.
     */
    public BinaryExpression.Op r;

    /**
     * Creates a new instance of ProbabilityOperator.
     *
     * @param p                         probability
     * @param r                         relational operator
     */
    public ProbabilityOperator(double p, BinaryExpression.Op r) {
        this.p = p;
        if(!r.isRelational())
            throw new RuntimeException("operator must be relational");
        this.r = r;
    }
    @Override
    public String toString() {
        return "P " + r.toString() + " " + p;
    }
}
