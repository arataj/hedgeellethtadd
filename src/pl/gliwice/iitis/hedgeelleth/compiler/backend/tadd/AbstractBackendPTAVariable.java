/*
 * AbstractBackendPTAVariable.java
 *
 * Created on Jul 20, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import pl.gliwice.iitis.hedgeelleth.pta.PTAException;
import pl.gliwice.iitis.hedgeelleth.compiler.sa.RangeCodeStaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.AbstractRuntimeContainer;
import pl.gliwice.iitis.hedgeelleth.pta.PTASystem;
import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAVariable;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAElementVariable;

/**
 * An abstract PTA variable, plus the functionality needed by the compiler.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractBackendPTAVariable extends AbstractBackendPTAValue
        implements Comparable {
    /**
     * Container of this variable.
     */
    public AbstractRuntimeContainer container;
    /**
     * A proposal, null for none.
     */
    public RangeCodeStaticAnalysis.Proposal proposal;
    /**
     * Original name of this variable, that is, without using the naming conventions.
     * Used only in comments. The default is to copy <code>v().name</code> into this
     * field.
     */
    public String originalVariableName;
    /**
     * If any naming convention has been applied to the name of this variable.
     */
    boolean namingConventionApplied;
    
    /**
     * Creates a wrapper for a PTA variable.
     * 
     * @param pos position in the source stream, null for none
     * @param container container of this variable
     * @param v variable to wrap
     */
    AbstractBackendPTAVariable(StreamPos pos,
            AbstractRuntimeContainer container, AbstractPTAVariable v) {
        super(pos, v);
        this.container = container;
        proposal = null;
        originalVariableName = v.name;
        namingConventionApplied = false;
    }
    @Override
    public AbstractPTAVariable v() {
        return (AbstractPTAVariable)v;
    }
    /**
     * If any initial value is not covered by <code>range</code>, then this
     * method enlarges <code>range</code> as needed.<br>
     *
     * Note that generally an error should be reported in such case. Thus,
     * this method should be used only if the range given at construction
     * time was already found to match the initial values. The need for calling
     * this method occurs generally only if <code>proposeRange()</code>
     * or, in the case of arrays, <code>computeRangeFromProposals</code>,
     * modified the range.<br>
     *
     * The method <code>PTASystem.computeFinalRanges()<code> calls
     * this method on all variables in the compilation except
     * <code>TADDClock</code>, for which this method is not applicable.<br>
     *
     * Note that TADD variables that result from locals have their init
     * values typically meaningless, as locals are typically initialized
     * within the code. These values should, though, be within the range if
     * <code>shouldAlreadyIncludeInit</code> is true.
     *
     * @param shouldAlreadyIncludeInit  true to report, that the range does
     *                                  not include some initial value,
     *                                  instead of including the value in
     *                                  the range; the report is done via
     *                                  a runtime exception
     */
    abstract public void enlargeRangeToFitInitValues(boolean shouldAlreadyIncludeInit);
    /**
     * Informs what is the maximum expected range of values, that can be assigned
     * to the code variable represented by this variable in the code. This
     * method can modify the range, and also stores the proposal to be possibly
     * reused by a backend.<br>
     *
     * Used only for scalar variables, as they have 1:1 mapping to code variables.
     * All arrays should use
     * <code>BackendPTAArrayVariable.registerVariableRangeProposal()<code>
     * instead.<br>
     *
     * If the proposal does not fully overlap the current range, then the range
     * will be reduced in this method so that unused values will be removed from
     * the current range. This reduction takes into account not only <code>p</code>,
     * but also the initial values of this variable.<br>
     *
     * If the proposal is empty, range should be set to some init value. As the init
     * values are not known in this abstract variable, an overriding method should
     * handle that case.<br>
     *
     * This method is generally used only for optimization of the output model.
     *
     * @param proposal                  range proposal, based on code analysis;
     *                                  if null, then this method does nothing
     */
    public void proposeRange(RangeCodeStaticAnalysis.Proposal proposal)
            throws PTAException {
        if(proposal != null) {
            if(proposal.rangeKnown()) {
                if(proposal.isEmpty()) {
                    // empty, so no viable assignments to this variable
                    // this should be handled in an overriding method,
                    // so report an exception
                    throw new RuntimeException("empty proposal not handled in submethod");
                } else {
                    String origRange = v().range.range.toString();
                    if(PTASystem.getMin(v().range.range) < proposal.min.getMaxPrecisionFloatingPoint())
                        BackendPTACompilation.setMin(v().range.range, proposal.min.getMaxPrecisionFloatingPoint());
                    if(PTASystem.getMax(v().range.range) > proposal.max.getMaxPrecisionFloatingPoint())
                        BackendPTACompilation.setMax(v().range.range, proposal.max.getMaxPrecisionFloatingPoint());
                    if(PTASystem.getMin(v().range.range) > PTASystem.getMax(v().range.range))
                        throw new PTAException(getStreamPos(),
                                this.v().name + " has range " + origRange + " but " +
                                " values within at most " + proposal.toString());
                }
            }
            this.proposal = proposal;
        }
    }
    @Override
    public int compareTo(Object o) {
        if(o instanceof AbstractBackendPTAVariable) {
            AbstractBackendPTAVariable other = (AbstractBackendPTAVariable)o;
            int relation = v().compareTo(other.v());
            if(relation == 0 && (this != other || this.v() != other.v()) &&
                    // element variables are allowed to be not uniquely identified
                    // by instances
                    !(this.v() instanceof PTAElementVariable))
                throw new RuntimeException("unexpected lack of uniqueness");
            return relation;
        } else
            throw new RuntimeException("invalid comparison");
    }
    @Override
    public String toString() {
        return v().toString();
    }
}
