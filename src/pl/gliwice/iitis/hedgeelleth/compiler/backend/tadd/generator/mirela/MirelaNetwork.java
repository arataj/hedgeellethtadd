/*
 * MirelaNetwork.java
 *
 * Created on Mar 26, 2018
 *
 * Copyright (c) 2018  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.mirela;

import java.util.*;
import mirela.Delay;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.BackendPTA;

import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeClass;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeCompilation;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeVariable;
import pl.gliwice.iitis.hedgeelleth.interpreter.AbstractInterpreter;
import pl.gliwice.iitis.hedgeelleth.interpreter.RuntimeThread;
import pl.gliwice.iitis.hedgeelleth.interpreter.RuntimeValue;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.ArrayStore;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeObject;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.pta.PTA;
import pl.gliwice.iitis.hedgeelleth.pta.PTAException;

/**
 * Holds a network of Mirela producer/consumer components (if any).
 * 
 * @author Artur Rataj
 */
public class MirelaNetwork {
    /**
     * Name of this network.
     */
    public final String name;
    /**
     * Nodes in this network, with a producer/consumer information.
     * Null if no Mirela classes are loaded, empty if they are loaded but
     * there is no network.
     */
    public Map<RuntimeObject, MirelaNode> nodes;
    /**
     * After the nodes are ordered, contains them in the respective order.
     * This is not a mandatory but a preferred order of listing them.
     */
    public List<MirelaNode> nodesList;
    /**
     * End-to-end flows.
     */
    List<MirelaFlow> flows;
    
    /**
     * Creates a Mirela network, if any.
     * 
     * @pram name name of this network
     * @param cc compilation
     * @param pta PTAs
     * @param interpreter interpreter
     * @param rm a main runtime method
     */
    public MirelaNetwork(String name, CodeCompilation cc, Collection<BackendPTA> ptas,
            AbstractInterpreter interpreter, RuntimeMethod rm) throws PTAException {
        this.name = name;
        constructNetwork(cc, ptas, interpreter, rm);
        if(nodes != null) {
            findFlows();
            findAttributes(cc, interpreter, rm);
            arrangeNodes();
        }
    }
    /**
     * Constructs a producer/consumer network.
     * 
     * @param cc compilation
     * @patam pta PTAs
     * @param interpreter interpreter
     * @param rm a main runtime method
     */
    private void constructNetwork(CodeCompilation cc, Collection<BackendPTA> ptas,
            AbstractInterpreter interpreter, RuntimeMethod rm) {
        List<RuntimeThread> threads = rm.pendingThreads;
        CodeClass cNode = cc.classes.get("mirela.AbstractNode");
        CodeClass cConsumer = cc.classes.get("mirela.AbstractConsumer");
        if(cNode != null && cConsumer != null) {
            CodeVariable fConsumers = cNode.fields.get("consumers");
            CodeVariable fNumConsumers = cNode.fields.get("numConsumers");
            nodes = new HashMap<>();
            for(RuntimeThread rt : threads) {
                RuntimeObject curr = rt.object;
                nodes.put(curr, new MirelaNode(cc, ptas, curr));
            }
            for(RuntimeThread rt : threads) {
                RuntimeObject curr = rt.object;
                MirelaNode node = nodes.get(curr);
                if(curr.codeClass.isNarrower(cNode)) {
                    boolean isConsumer = rt.object.codeClass.isNarrower(cConsumer);
                    int numConsumers = rt.object.values.get(fNumConsumers).getInteger();
                    for(int c = 0; c < numConsumers; ++c) {
                        try {
                            RuntimeObject sub = ((ArrayStore)rt.object.values.get(fConsumers).value).getElement(
                                    interpreter, rm, null, c).getReferencedObject();
                            if(!sub.codeClass.isNarrower(cConsumer))
                                throw new RuntimeException("consumer expected");
                            MirelaNode subNode = nodes.get(sub);
                            if(subNode == null) {
                                // a thread which is not started, e.g. a Memory component
                                nodes.put(sub, new MirelaNode(cc, ptas, sub));
                                subNode = nodes.get(sub);
                            }
//if(node == null || subNode == null || node.consumers == null || subNode.producers == null)
//    node = node;
                            if(node.consumers.contains(subNode) || subNode.producers.contains(node))
                                throw new RuntimeException("relation already registered");
                            node.consumers.add(subNode);
                            subNode.producers.add(node);
                        } catch(InterpreterException e) {
                            throw new RuntimeException("unexpected: " + e.toString());
                        }
                    }
                }
            }
        } else
            nodes = null;
    }
    /**
     * Find end-to-end flows in the producer/consumer network.
     */
    private void findFlows() {
        flows = new LinkedList<>();
        for(MirelaNode n : nodes.values())
            if(n.getClazz() != MirelaNode.Clazz.TIMER) {
                boolean source = n.producers.isEmpty();
                if(!source && n.getClazz() == MirelaNode.Clazz.PERIODIC_THREAD &&
                        n.producers.size() == 1) {
                    if(n.producers.get(0).getClazz() != MirelaNode.Clazz.TIMER)
                        throw new RuntimeException("periodic thread's timer not found");
                    source = true;
                }
                if(source) {
                    for(List<MirelaNode> nodes : findFlows(n)) {
                        flows.add(new MirelaFlow(nodes));
                    }
                }
            }
    }
    /**
     * Finds flows which originate from a given node.
     * 
     * @param root the root node
     * @return list of flows, empty for none
     */
    private List<List<MirelaNode>> findFlows(MirelaNode root) {
        List<List<MirelaNode>> flows = new LinkedList<>();
        if(root.consumers.isEmpty()) {
            List<MirelaNode> flowEnd = new LinkedList<>();
            flowEnd.add(root);
            flows.add(flowEnd);
        } else {
            for(MirelaNode c : root.consumers) {
                List<List<MirelaNode>> subFlows = findFlows(c);
                for(List<MirelaNode> subFlow : subFlows) {
                    subFlow.add(0, root);
                    flows.add(subFlow);
                }
            }
        }
        return flows;
    }
    /**
     * Tries to make some order in the order of nodes, like make nodes
     * connected by a flow to occur one after another. The flows must be
     * already found.
     */
    private void arrangeNodes() {
        nodesList = new LinkedList<>();
        // according to flows
        for(MirelaFlow f : flows)
            for(MirelaNode n : f.nodes)
                if(!nodesList.contains(n)) {
                    if(n.getClazz() == MirelaNode.Clazz.PERIODIC_THREAD) {
                        // according to timers/periodic threads
                        for(MirelaNode p : n.producers)
                            if(p.clazz == MirelaNode.Clazz.TIMER) {
                                if(!nodesList.contains(p))
                                    nodesList.add(p);
                            }
                    }
                    nodesList.add(n);
                }
        // what is left, according to lexicographic order
        SortedMap<String, MirelaNode> alphabetic = new TreeMap<>();
        for(MirelaNode n : nodes.values())
            if(!nodesList.contains(n))
                alphabetic.put(n.name, n);
        nodesList.addAll(alphabetic.values());
    }
    /**
     * Finds attributes relevant to each node class.
     * 
     * @param cc compilation
     * @param interpreter interpreter
     * @param rm a main runtime method
     */
    private void findAttributes(CodeCompilation cc, AbstractInterpreter interpreter,
            RuntimeMethod rm) throws PTAException {
        // firstly, compute the timers as they can be needed
        // elsewhere
        for(RuntimeObject ro : nodes.keySet()) {
            MirelaNode n = nodes.get(ro);
            if(n.getClazz() == MirelaNode.Clazz.TIMER) {
                RuntimeObject repeat = ro.values.get(
                        cc.classes.get("mirela.Periodic").fields.get("repeat")).getReferencedObject();
                Delay delay = getDelay(cc, repeat);
                if(delay != null && delay.min == delay.max) {
                    // a fixed frequency
                    n.period = delay.min;
                }
            }
        }
        for(RuntimeObject ro : nodes.keySet()) {
            MirelaNode n = nodes.get(ro);
            long packetSize = ro.values.get(
                    cc.classes.get("mirela.AbstractNode").fields.get("packetSize")).getLong();
            if(packetSize > Integer.MAX_VALUE)
                throw new PTAException(null, "mirela component " + n.name +
                        " has a packet size of " + packetSize+ " which " +
                        "does not fit into a 32-bit integer");
            n.packetSize = (int)packetSize;
            switch(n.getClazz()) {
                case PERIODIC_THREAD:
                {
                    for(MirelaNode p : n.producers)
                        if(p.clazz == MirelaNode.Clazz.TIMER) {
                            if(!Double.isNaN(n.period))
                                throw new PTAException(null, "periodic mirela thread " + n.name  +
                                        " has more than a single timer");
                            n.period = p.period;
                        }
                    if(Double.isNaN(n.period))
                        throw new RuntimeException("timer of a periodic thread not found");
                    break;
                }   
                case TIMER:
                    // already computed
                    break;
                    
                case BUS:
                {
                    RuntimeObject transmission = ro.values.get(
                            cc.classes.get("mirela.Bus").fields.get("transmissionTotal")).getReferencedObject();
                    Delay delay = getDelay(cc, transmission);
                    n.xMin = delay.min;
                    n.xMax = delay.max;
                    break;
                }   
                case UNKNOWN:
                    break;
                    
                default:
                    throw new RuntimeException("unknown node class");
            }
        }
    }
    private Delay getDelay(CodeCompilation cc, RuntimeObject repeat) {
        RuntimeValue minR = repeat.values.get(
                cc.classes.get("mirela.Delay").fields.get("min"));
        RuntimeValue maxR = repeat.values.get(
                cc.classes.get("mirela.Delay").fields.get("max"));
        if(minR != null && maxR != null) {
            double min = minR.getDouble();
            double max = maxR.getDouble();
            return new Delay(min, max);
        } else
            return null;
    }
    /**
     * If flow--wise, this node is a producer. Thus, e.g. timers of periodic threads
     * as absent in the flows won't be producers.
     * 
     * @param n node
     * @return if a producer
     */
    boolean isProducer(MirelaNode n) {
        for(MirelaFlow flow : flows) {
            int size = flow.nodes.size();
            for(int i = 0; i < size; ++i) {
                MirelaNode m = flow.nodes.get(i);
                if(m == n && i < size - 1)
                    return true;
            }
        }
        return false;
    }
    /**
     * If flow--wise, this node is a producer. Thus, e.g. a periodic threads won't
     * necessarily be a consumer as its timer is absent in the flows.
     * 
     * @param n node
     * @return if a producer
     */
    boolean isConsumer(MirelaNode n) {
        for(MirelaFlow flow : flows) {
            int size = flow.nodes.size();
            for(int i = 0; i < size; ++i) {
                MirelaNode m = flow.nodes.get(i);
                if(m == n && i > 0)
                    return true;
            }
        }
        return false;
    }
    public String toString() {
        StringBuilder out = new StringBuilder();
        if(nodes != null)
            for(MirelaNode n : nodesList) {
                out.append(n.toString());
            }
        for(MirelaFlow flow : flows) {
            out.append(flow.toString());
            out.append("\n");
        }
        return out.toString();
    }
}
