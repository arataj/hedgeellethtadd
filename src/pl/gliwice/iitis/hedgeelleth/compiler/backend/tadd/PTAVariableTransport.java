/*
 * PTAVariableTransport.java
 *
 * Created on Oct 29, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.AbstractTADDGenerator;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.CodeVariableTransport;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;
import pl.gliwice.iitis.hedgeelleth.pta.PTAException;
import pl.gliwice.iitis.hedgeelleth.pta.values.*;

/**
 * A PTA equivalent of <code>CodeVariableTransport</code>. Mostly unused.
 * 
 * @author Artur Rataj
 */
public class PTAVariableTransport {
    /**
     * All variables actually taken into account by this transport.
     */
    Set<PTAScalarVariable> traced;
    /**
     * An equivalent of <code>CodevariableTransport.transport</code>.
     */
    Map<Integer, Set<PTAScalarVariable>> transport;
    
    /**
     * Creates a new instance of <code>PTAVariableTransport</code>
     * for a given PTA. Copies only scalar variables.
     * 
     * @param generator a PTA generator
     * @param rm runtime method, from which the PTA is generated
     * @param codeTransport a code transport, whose traced scalar variables
     * are converted to PTA variables in this object
     */
    public PTAVariableTransport(AbstractTADDGenerator generator,
            RuntimeMethod rm, CodeVariableTransport codeTransport)
            throws PTAException {
        CodeMethod method = rm.method;
        traced = new HashSet<>();
        for(CodeVariable cv : method.variables.values())
            if(cv.type.isPrimitive()) {
                // a local and a scalar -- no setting of runtime values
                // in the interpreter required
                AbstractBackendPTAVariable v = generator.getVariable(
                        new CodeFieldDereference(cv), rm);
                /*if(v instanceof BackendPTAScalarVariable)*/
                traced.add((PTAScalarVariable)v.v());
            }
        transport = new HashMap<>();
        int maxDoubleIndex = (method.code.ops.size() - 1)*2 + 1;
        for(int doubleIndex = -1; doubleIndex <= maxDoubleIndex;
                ++doubleIndex) {
            Set<PTAScalarVariable> set = new HashSet<>();
            transport.put(doubleIndex, set);
            for(AbstractCodeDereference d : codeTransport.get(doubleIndex)) {
                Collection<CodeVariable> l = d.extractPlainLocal();
                if(!l.isEmpty()) {
                    CodeVariable local = l.iterator().next();
                    if(local.type.isPrimitive()) {
                        AbstractBackendPTAVariable v = generator.getVariable(d, rm);
                        if(v != null && v.v() instanceof PTAScalarVariable)
                            set.add((PTAScalarVariable)v.v());
                    }
                }
            }
        }
    }
    /**
     * Returns all variables actually taken into account by this transport.
     * 
     * @return a set of scalar local variables
     */
    public Set<PTAScalarVariable> getTraced() {
        return traced;
    }
    /**
     * Returns all local scalar variables, that transfer a meaningfull value
     * value from/through/to the operation at a given index.
     * 
     * @param doubleIndex double index of the instruction
     * @return a set of PTA scalars; empty for no variables
     */
    public Set<PTAScalarVariable> get(int doubleIndex) {
        return transport.get(doubleIndex);
    }
    /**
     * Returns if a given variable is known to transport a meaningfull
     * value from/through/to the operation at a given index.
     *
     * @param doubleIndex  double index of the instruction
     * @param r scalar variable
     * @return if known to be transported
     */
    public boolean isTransported(int doubleIndex, PTAScalarVariable r) {
        return get(doubleIndex).contains(r);
    }
    @Override
    public String toString() {
        String s = "";
        int maxDoubleIndex = -1;
        for(int doubleIndex : transport.keySet())
            if(maxDoubleIndex < doubleIndex)
                maxDoubleIndex = doubleIndex;
        for(int doubleIndex = 0; doubleIndex <= maxDoubleIndex;
                ++doubleIndex) {
            String l = "" + doubleIndex/2;
            if(doubleIndex%2 == 0)
                l += "_";
            else
                l += "^";
            while(l.length() < 3)
                l = " " + l;
            Set<PTAScalarVariable> set = get(doubleIndex);
            if(set != null)
                for(PTAScalarVariable r : set) {
                    if(isTransported(doubleIndex, r))
                        l += " " + r;
                }
            s += l + "\n";
        }
        return s;
    }
}
