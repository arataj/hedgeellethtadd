/*
 * BackendPTACompilation.java
 *
 * Created on Jul 20, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.Hedgeelleth;
import pl.gliwice.iitis.hedgeelleth.compiler.CommentTag;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.BarrierGenerator;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeFieldDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.interpreter.AbstractInterpreter;
import pl.gliwice.iitis.hedgeelleth.interpreter.ModelControl;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.pta.*;
import pl.gliwice.iitis.hedgeelleth.pta.PTA.TransitionBag;
import pl.gliwice.iitis.hedgeelleth.pta.values.*;

/**
 * A set of PTAs plus the functionality required by the backend.<br>
 * 
 * The last modification on this compilation should be a call to
 * <code>computeFinalRanges()</code>.
 *
 * @author Artur Rataj
 */
public class BackendPTACompilation extends PTASystem {
    /**
     * Type of clock variables.
     */
    public static final Type CLOCK_VARIABLE_TYPE =
            new Type(Type.PrimitiveOrVoid.LONG);
    /**
     * Name of the synchronization player.
     */
    public static final String SYNC_PLAYER_NAME = "synchronization";

    /**
     * Variables, keyed with their containers and
     * code variables. Arrays are not represented by
     * a code variable, and thus a respective key is null
     * in their case.
     */
    public Map<AbstractRuntimeContainer,
            Map<CodeVariable, AbstractBackendPTAVariable>> variables;
    /**
     * A helper map of scalar variables, PTA ones to code ones.
     */
    public Map<PTAScalarVariable, CodeVariable> codeScalars;
    /**
     * Lock TADDs, keyed with runtime objects.
     */
    public Map<RuntimeObject, TADDLock> locks;
    /**
     * Fully qualified name of the main class.
     */
    public String mainClassName;
    /**
     * A map of primitive fields that were replaced with constants in PTA
     * code, to these constants. No field in this map exists as a PTA
     * variable. Empty list for none.
     */
    public Map<RuntimeField, Literal> replacedPrimitiveFields;
    /**
     * Code variable representing the error.
     */
    public CodeVariable errorCodeVariable;
    /**
     * Error code, used by <code>newErrorCode()</code>. Final value of this variable
     * decides about <code>errorVariable</code>'s range.
     */
    public int currErrorCode;
    /**
     * List of used variables. Includes elements variables, if the backend does
     * not generate array variables, but the element variables instead---in such
     * a case, the array variables are still included, though.
     */
    public Set<AbstractPTAVariable> usedVariables;
    /**
     * Model control.
     */
    public ModelControl modelControl;
    /**
     * Maps PTAs made from threads to runtime objects, that represent
     * the threads.
     */
    public Map<PTA, RuntimeObject> threadPtas;
    /**
     * Names of variables, to check for duplicates. it is needed because
     * <code>ModelControl</code>'s name conventions may produce duplicates.
     */
    public Set<String> variableNames;
    /**
     * Final fields.
     */
    // protected Map<RuntimeField, CodeConstant> constantFields;
    /**
     * Static variables, that are unknown constants. Copied from
     * <code>AbstractInterpreter.unknownConstants</code>.
     */
    public Set<CodeVariable> unknownConstants;
    /**
     * <p>Returns an actual condition on the vector state in the output model for
     * a transition. Supplied by a backend, yet not all backends return this information.
     * Is a backend does not do that, this map is empty.</p>
     * 
     * <p>Requires the condition to be of a a simple form &lt;step variable&gt; = &lt;value&gt;,
     * otherwise a respective transition is mapped to null. The textual representation, if not
     * null, is guaranted to be <code>&lt;identifier&gt;=&lt;integer vale &gt;</code>.</p>
     * 
     * <p>Used now only for generating a Nd file. Special CLI options which complicate guards
     * will cause some or all values to be null.</p>
     */
    public Map<PTATransition, String> xStateFilter;
    /**
     * If a transition has a non--null state filter in <code>xStateFilter</code>,
     * this map points to the target state's filter. The filter's format rules are
     * identical to these for <code>xStateFilter</code>.
     */
    public Map<PTATransition, String> xTargetStateFilter;
    /**
     * Only nd parameters within these keys are allowed, i.e. parameters
     * listed as model constants. Tha value is a respective name in the output
     * model.
     */
    Map<String, String> allowedNdParameters;
    
    /**
     * Creates a new instance of <code>BackendPTABCompilation</code>.
     * 
     * @param modelControl model control
     * @param implicitScheduler a type of an implicit scheduler, null for an
     * explicit scheduler
     */
    public BackendPTACompilation(ModelControl modelControl,
            SchedulerType implicitScheduler) {
        super();
        variables = new HashMap<>();
        codeScalars = new HashMap<>();
        locks = new HashMap<>();
        replacedPrimitiveFields = new HashMap<>();
        currErrorCode = 0;
        this.modelControl = modelControl;
        threadPtas = new HashMap<>();
        variableNames = new HashSet<>();
        // constantFields = new HashMap<>();
        this.implicitScheduler = implicitScheduler;
        xStateFilter = new HashMap<>();
        xTargetStateFilter = new HashMap<>();
    }
    /**
     * Creates, registers and returns a new PTA variable, that is either
     * integer or an array of integers. The integers are bounded according
     * to the variable's type.<br>
     *
     * If proposals are to be used on:<br>
     *
     * -- non--array variables, either (1)
     * respective <code>RangeCodeStaticAnalysis</code>
     * needs to be constructed before calling this method, as this method
     * proposes <code>variable.proposal</code>, or (2)
     * <code>IntegerTADDVariable.proposeRange</code> should be called
     * later on the created variables;<br>
     *
     * -- array variables -- all indexing should be reported using
     * <code>BackendPTAArrayVariable.registerVariableRangeProposal</code>.
     *
     * 
     * @param container                 container of the variable
     *                                  to add, or array store that
     *                                  represents the variable to add
     * @param variable                  code variable representing
     *                                  the variable to add, null if
     *                                  <code>container</code> is an array
     *                                  store
     * @param isArray                   true for array, false for scalar
     * @param arrayLength               -1 if <code>isArray</code> is false,
     *                                  array length otherwise
     * @param initValue                 initial value, <code>Double</code>
     *                                  for no array, double[] for
     *                                  array
     * @param range                     range of allowed values, used only
     *                                  for non--array variables; to
     *                                  register ranges in array variables,
     *                                  <code>BackendPTAArrayVariable.registerVariableRange()</code>
     *                                  should be used instead; if this
     *                                  parameter is not null for array
     *                                  variable, a runtime exception is thrown
     * @param forcedName                instead of a default name, use a forced name;
     *                                  if the new variable is not an array and
     *                                  <code>variable</code> is missing, this is
     *                                  the only option to set a name, so, in such
     *                                  a case, can not be null
     * @param floating                  if a variable or its elements are of floating point type
     * @return                          created PTA variable
     */
    public AbstractBackendPTAVariable newVariable(AbstractRuntimeContainer container,
            CodeVariable variable, boolean isArray, int arrayLength, Object initValue,
            BackendPTARange range, String forcedName, boolean floating) throws PTAException {
        if(!(container instanceof ArrayStore) && variable == null)
                throw new RuntimeException("only array store can have null variable");
        if(variable != null && variable.type.isArray(null) != isArray)
            throw new RuntimeException("type mismatch");
        Map<CodeVariable, AbstractBackendPTAVariable> inContainer =
                variables.get(container);
        if(inContainer == null) {
            inContainer = new HashMap<>();
            variables.put(container, inContainer);
        }
        RuntimeObject namingConvention = null;
        StreamPos pos = null;
        Variable.Category category = null;
        AbstractBackendPTAVariable v = inContainer.get(variable);
        if(v == null) {
            String variableName;
            String originalVariableName;
            if(container instanceof ArrayStore) {
                ArrayStore store = (ArrayStore)container;
                originalVariableName = container.getUniqueName();
                if(modelControl.hasNamingConvention(store)) {
                    namingConvention = store;
                    try {
                        variableName = modelControl.getElementName(
                                namingConvention, "");
                    } catch(CompilerException e) {
                        throw new PTAException(null, e.getMessage());
                    }
                } else
                    variableName = originalVariableName;
                if(variable != null)
                    throw new RuntimeException("array store can not contain variables");
            } else {
                if(variable == null) {
                    if(forcedName != null) {
                        variableName = forcedName;
                        originalVariableName = forcedName;
                    } else
                        throw new RuntimeException("unknown name");
                } else {
                    RuntimeObject owner;
                    if(container instanceof RuntimeObject)
                        owner = (RuntimeObject)container;
                    else
                        owner = null;
                    originalVariableName = container.getRuntimeVariableName(variable);
                    if(owner != null && modelControl.hasNamingConvention(owner)) {
                        namingConvention = owner;
                        try {
                            variableName = modelControl.getElementName(
                                    namingConvention, variable.name);
                        } catch(CompilerException e) {
                            throw new PTAException(null, e.getMessage());
                        }
                    } else
                        variableName = originalVariableName;
                    pos = variable.getStreamPos();
                    category = variable.category;
                }
            }
//if(variable == null && name == null)
//    variable = variable;
            if(isArray) {
                if(range != null)
                    throw new RuntimeException("array variables do not have their ranges " +
                            "specified at compile time");
                BackendPTAArrayVariable a = new BackendPTAArrayVariable(pos, container,
                        new PTAArrayVariable(variableName, arrayLength, (double[])initValue,
                        floating));
                v = a;
            } else {
                if(arrayLength != -1)
                    throw new RuntimeException("array length specified for scalar");
                BackendPTAScalarVariable i = new BackendPTAScalarVariable(pos, container,
                        new PTAScalarVariable(variableName,
                        range != null ? range.r() : null, floating));
                i.category = category;
                i.v().backendLocal = variable.isLocal();
                i.v().backendInternal = variable.isInternal();
                i.v().initValue = ((Double)initValue).doubleValue();
                codeScalars.put(i.v(), variable);
                if(variable != null)
                    i.proposeRange(variable.proposal);
                v = i;
            }
            v.originalVariableName = originalVariableName;
            v.namingConventionApplied = namingConvention != null;
            if(variableNames.contains(v.v().name)) {
                if(namingConvention != null)
                    throw new PTAException(v.getStreamPos(), "naming convention " +
                            "for " + namingConvention.getUniqueName() + " " +
                            "caused name clash: " + v.v().name);
                else
                    throw new RuntimeException("unexpected name clash for " +
                            v.v().name);
            } else
                variableNames.add(v.v().name);
            inContainer.put(variable, v);
        } else
            throw new RuntimeException("variable already present");
        return v;
    }
    /**
     * Creates, registers and returns a new clock.
     * 
     * @param container                 container of the clock
     *                                  to add
     * @param threadName                name of the clock's thread
     * @return                          created clock
     */
    public BackendPTAClock newClock(InternalRuntimeContainer container,
            String threadName) {
        Map<CodeVariable, AbstractBackendPTAVariable> inContainer =
                variables.get(container);
        if(inContainer == null) {
            inContainer = new HashMap<>();
            variables.put(container, inContainer);
        }
        String name = "x_" + threadName;
        CodeVariable cv = container.newVariable(name,
                CLOCK_VARIABLE_TYPE);
        BackendPTAClock clock = new BackendPTAClock(container,
                new PTAClock(container.getUniqueName() + "::" + name));
        inContainer.put(cv, clock);
        return clock;
    }
    /**
     * Returns a PTA variable or null if not found.
     * 
     * @param container                 container of the variable
     * @param codeVariable              code variable representing
     *                                  the variable, null for array
     *                                  store containers
     * @return                          PTA variable or null if not
     *                                  found
     */
    public AbstractBackendPTAVariable getVariable(AbstractRuntimeContainer container,
            CodeVariable variable) {
        Map<CodeVariable, AbstractBackendPTAVariable> inContainer =
                variables.get(container);
        if(inContainer == null)
            return null;
        return inContainer.get(variable);
    }
    /**
     * Removes a PTA variable.
     * 
     * @param variable                  variable to remove
     */
    public void removeVariable(AbstractBackendPTAVariable variable) {
        Map<CodeVariable, AbstractBackendPTAVariable> inContainer =
                variables.get(variable.container);
        CodeVariable cv = null;
        for(CodeVariable v : inContainer.keySet())
            if(inContainer.get(v) == variable) {
                cv = v;
                break;
            }
        inContainer.values().remove(variable);
        if(inContainer.size() == 0)
            variables.values().remove(inContainer);
        variable.container.removeVariable(cv);
    }
    /**
     * Returns lock PTA for a given runtime object, creating
     * it and registering if needed.
     * 
     * @param object                    runtime object of the lock
     * @return                          lock PTA
     */
    public TADDLock getLock(RuntimeObject object) {
        TADDLock lock = locks.get(object);
        if(lock == null) {
            lock = new TADDLock(object, object.getUniqueName() + "_lock");
            locks.put(object, lock);
            ptas.put(lock.name, lock);
        }
        return lock;
    }
    /**
     * Verifies if names of non--compiler tags are within a given set.
     * and if the required tags are present.<br>
     * 
     * Throws a parse exception if unrecognized tag or modifier prefix
     * is found. Throws a runtime exception if a non--recognized compiler
     * modifier is found, as these should be already verified before.<br>
     * 
     * See the method <code>PTA.verifyNonCompilerTagModifiers</code>
     * for more details about the parameters.
     * 
     * @param allowed                   set to verify the names against,
     *                                  key is tag name, value is modifier's
     *                                  allowed prefix
     * @param required                  set to verify the names against,
     *                                  key is tag name, value is modifier's
     *                                  required prefix
     * @param unique                    set to verify the names against,
     *                                  key is tag name, value is modifier's
     *                                  unique prefix
     * @see PTA.verifyNonCompilerTagModifiers
     */
    public CommentTag verifyNonCompilerTagModifiers(Map<
            String, Set<String>> allowed, Map<String, Set<String>> required, Map<
            String, Set<String>> unique) throws ParseException {
        ParseException e  = new ParseException();
        for(PTA tadd : ptas.values())
            try {
                ((BackendPTA)tadd).verifyNonCompilerTagModifiers(allowed, required, unique);
            } catch(ParseException f) {
                e.addAllReports(f);
            }
        if(e.reportsExist())
            throw e;
        return null;
    }
    /**
     * Filters all tags, as defined in CodeOp.filterTags().
     * 
     * Even that <code>CodeCompilation</code> also contains a method
     * for filtering tags, the PTA backend can create its own operations.
     * This is why a PTA compilation contains its own method for
     * filtering tags.
     */
    public void filterTags() {
        for(PTA tadd : ptas.values())
            ((BackendPTA)tadd).filterTags();
    }
    /**
     * Deletes all locks that do not have transitions.
     */
    public void deleteEmptyLocks() {
        for(PTA tadd : new LinkedList<>(ptas.values()))
            if(tadd instanceof TADDLock && tadd.transitions.isEmpty()) {
                ptas.values().remove(tadd);
                locks.values().remove((TADDLock)tadd);
            }
                
    }
    /**
     * Generates standard valuation for all TADDs, overriding any
     * existing one. Use after all TADDs are generated.
     */
    public void generateStandardValuation() {
        int beginIndex = 1;
        for(PTA tadd : ptas.values())
            beginIndex = ((BackendPTA)tadd).
                    generateStandardValuation(beginIndex);
    }
    /**
     * Generates selected valuation for all TADDs, overriding any
     * existing one. Use after all TADDs are generated.
     *
     * @param label                     label required, null for none
     */
    public void generateSelectedValuation(String label) {
        int beginIndex = 1;
        for(PTA tadd : ptas.values())
            beginIndex = ((BackendPTA)tadd).generateSelectedValuation(label, beginIndex);
    }
    /**
     * Registers the mapping of replaced fields to replacement constants in
     * the PTA code, empty for none.<br>
     *
     * The mapping is used to report the field's constant values
     * in the PTA part. The report contains only primitive values,
     * as dereferences of java types are resolved while
     * building the TADDs, and even are not a part of PTA formalism.<br>
     *
     * This method tests the replaced fields against PTA variables,
     * to detect possible overlappings, which would cause a runtime
     * exception. For the test to be effective, the should be called
     * after the PTA variables are complete.
     *
     * @param replacedFields            all replaced fields, regardless
     *                                  of type
     * @param interpreter               interpreter
     */
    public void registerReplacements(Map<RuntimeField, Literal> replacedFields,
            AbstractInterpreter interpreter) {
        for(RuntimeField f : replacedFields.keySet()) {
            try {
                Map<CodeVariable, AbstractBackendPTAVariable> inContainer =
                        variables.get(f.getContainer(interpreter));
                if(inContainer != null && inContainer.containsKey(f.field))
                    throw new RuntimeException("replaced field exists as TADD variable");
            } catch(InterpreterException e) {
                // fields should already have been initialized, as the main thread
                // finished, so no interpreter exception is expected here
                throw new RuntimeException("unexpected exception: " + e.toString());
            }
// System.out.println("replacement: " + f.toString());
            if(f.field.type.isPrimitive())
                replacedPrimitiveFields.put(f, replacedFields.get(f));
        }
    }
    /**
     * Returns an unique error code for the error variable.
     *
     * @return                          error code, != 0
     */
    public int newErrorCode() {
        return ++currErrorCode;
    }
    /**
     * If <code>i</code> has a proposal with a known range, then returns
     * a range; otherwise, returns null. If the proposal is empty, returns
     * the variable's own range, which won't cause an unnecessary range checking.
     * 
     * @param i backend variable with a possible proposal
     * @return proposal or null
     */
    public static PTARange getRangeFromProposal(AbstractBackendPTAVariable i) {
        boolean knownProposal = i.proposal != null && i.proposal.rangeKnown();
        if(knownProposal) {
            double pMin;
            double pMax;
            if(i.proposal.isEmpty()) {
                // values that won't cause range checking
                pMin = i.v().range.getMin();
                pMax = i.v().range.getMax();
            } else {
                pMin = i.proposal.min.getMaxPrecisionFloatingPoint();
                pMax = i.proposal.max.getMaxPrecisionFloatingPoint();
            }
            try {
                if(i.v().floating)
                    return new PTARange(null, pMin, pMax);
                else
                    return new PTARange(null, (int)pMin, (int)pMax);
            } catch(PTAException e) {
                throw new RuntimeException("unexpected: " + e.toString());
            }
        }
        return null;
    }
    /**
     * Computes final ranges and also writes ranged of variables.
     * Can be called after all variables have registered variables ranges
     * and variable range proposals.<br>
     * 
     * A variable's write range can be larger than the variable's range,
     * what may cause range checking in the backend.<br>
     * 
     * Computing of the ranges means (1) only in the case of arrays, to call
     * <code>IntegerArrayTADDVariable.computeRangeFromProposals()</code>, and
     * then (2) for all variables that are not clocks, to call
     * <code>AbstractPTAVariable.enlargeRangeToFitInitValues()</code>.<br>
     *
     * Note that the class <code>RangeCodeStaticAnalysis</code> takes into
     * account the initial values in its proposals, so calling
     * <code>AbstractPTAVariable.enlargeRangeToFitInitValues()</code> for
     * proposals constructed using only that class is redundant.<br>
     *
     * Detects if <code>errorVariable</code> is used by checking
     * <code>currErrorCode</code>, and if not, instead of computing the variable's
     * final range, removes the variable from the container.
     *
     * @param shouldAlreadyIncludeInit  passed to
     *                                  <code>AbstractPTAVariable.enlargeRangeToFitInitValues()</code>,
     *                                  see that method docs for details
     */
    public void computeFinalRanges(boolean shouldAlreadyIncludeInit) throws PTAException {
        for(AbstractRuntimeContainer rc : variables.keySet()) {
            Map<CodeVariable, AbstractBackendPTAVariable> inContainer =
                    variables.get(rc);
            boolean removeErrorVariable = false;
            for(CodeVariable cv : inContainer.keySet()) {
                AbstractBackendPTAVariable tv = inContainer.get(cv);
                // scalar PTA variables can have their ranges computed from
                // proposals by passing respective range by
                // <code>IntegerTADDVariable.proposeRange()</code>,
                // arrays, though, need integration of possibly more than a
                // single proposal, see docs in <code>IntegerArrayTADDVariable</code>
                // for details
                if(errorCodeVariable != null && cv == errorCodeVariable) {
                    // special case of error variable
                    tv.v().range = new PTARange(null, 0, currErrorCode);
                    tv.v().range.backendGuaranteed = true;
                    if(currErrorCode == 0)
                        removeErrorVariable = true;
                } else if(tv instanceof BackendPTAArrayVariable) {
                    BackendPTAArrayVariable a = (BackendPTAArrayVariable)tv;
                    a.computeRangeFromProposals();
                } else if(cv.isLocal()) {
                    // initial values of primitive locals have no meaning, as these variables
                    // should always be initialized in the PTA thread before use; the initial
                    // values should fit in the variable's range, though
                    //
                    // dereferences or arrays are replaced with the arrays theirselves,
                    // thus if <code>cv</code> is local, <code>tv</code> should never be an
                    // array
                    if(tv instanceof BackendPTAScalarVariable) {
                        BackendPTAScalarVariable i = (BackendPTAScalarVariable)tv;
                        if(i.v().initValue < getMin(i.v().range.range))
                            i.v().initValue = getMin(i.v().range.range);
                        else if(i.v().initValue > getMax(i.v().range.range))
                            i.v().initValue = getMin(i.v().range.range);
                    } else
                        throw new RuntimeException("unknown variable's type");
                }
                if(!(tv instanceof BackendPTAClock))
                    tv.enlargeRangeToFitInitValues(shouldAlreadyIncludeInit &&
                            // proposals for arrays may not include initial values,
                            // see <code>BackendArrayPTAVariable</code> for
                            // details
                            !(tv instanceof BackendPTAArrayVariable));
                // set the write range, if known
                /*if(tv instanceof BackendPTAScalarVariable) {*/
                    /*BackendPTAScalarVariable i = (BackendPTAScalarVariable)tv;*/
                    tv.v().backendWriteRange = getRangeFromProposal(tv);
                /*}*/
            }
            if(removeErrorVariable) {
                // error variable is not used, remove it then
                inContainer.remove(errorCodeVariable);
                errorCodeVariable = null;
                errorVariable = null;
            }
        }
    }
    /**
     * Sets the minimum value of a range, in a type--independent manner.
     * 
     * @param range
     * @param min 
     */
    public static void setMin(Type.TypeRange range, double min) {
        if(range.isFloating())
            range.setMinF(min);
        else
            range.setMinI((int)min);
    }
    /**
     * Sets the minimum value of a range, in a type--independent manner.
     * 
     * @param range
     * @param min 
     */
    public static void setMax(Type.TypeRange range, double max) {
        if(range.isFloating())
            range.setMaxF(max);
        else
            range.setMaxI((int)max);
    }
    /**
     * Searches for labels in all PTAs, and saves then in <code>labels</code>.<br>
     * Omits branch id labels, as these are free, do not have semantic meaning within
     * the compilation and might otherwise interfere with optimizations. The branch
     * id labels are added later in <code>applyBranchLabels</code>.
     * 
     * If there is only a single barrier label, what is a frequent case for turn
     * games, then gives the label a clearer name.
     */
    public void setLabels() throws PTAException {
        final String BARRIER_STRING = "barrier";
        final String PAIR_BARRIER_STRING = "pair_barrier";
        labels = new TreeMap<>();
        for(PTA pta : ptas.values())
            for(PTATransition t : pta.transitions)
                if(t.label != null) {
                    if(t.label.type == PTALabel.Type.FREE && labels.containsKey(t.label.name))
                        throw new PTAException(null, "free label not unique: " + t.label);
                    labels.put(t.label.name, t.label);
                }
        if(labels.size() == 1) {
            // a special case of a single label
            String name = labels.keySet().iterator().next();
            if(!labels.get(name).isDirectional()) {
                // rename all instances of barriers
                for(PTA pta : ptas.values())
                    for(PTATransition t : new LinkedList<>(pta.transitions))
                        if(t.label != null && t.label.type != PTALabel.Type.FREE) {
                            String newName;
                            if(name.startsWith("hc.Barrier#"))
                                newName = BARRIER_STRING;
                            else if(name.startsWith("hc.PairBarrier#"))
                                newName = PAIR_BARRIER_STRING;
                            else
                                newName = null;
                            if(newName != null) {
                                PTA.TransitionBag xbag = pta.keyBefore(t);
                                labels.remove(t.label.name);
                                try {
                                    t.label = new PTALabel(newName,
                                        t.label.type, t.label.broadcast);
                                } catch(PTAException e) {
                                    throw new RuntimeException("unexpected: " + e.toString());
                                }
                                if(labels.isEmpty())
                                    labels.put(t.label.name, t.label);
                                pta.keyAfter(xbag);
                            }
                        }
            }
        }
    }
    /**
     * Adds labels from branch ids to transitions and <code>labels</code>.
     * These are added as late as possible, so as to not interfere with optimizations.
     */
    public void applyBranchLabels() throws PTAException {
        Set<String> registered = new HashSet<>();
        for(PTA pta : ptas.values())
            for(PTATransition t : new LinkedList<>(pta.transitions))
                if(t.branchId != null && !t.branchId.label.isEmpty())
                    for(Integer i : t.ndInt)
                        if(t.branchId.label.containsKey(i)) {
                            PTALabel l = t.branchId.label.get(i);
                            if(t.label != null)
                                throw new RuntimeException("unexpected");
                            TransitionBag xbag = pta.keyBefore(t);
                            t.label = l;
                            pta.keyAfter(xbag);
                            if(!registered.contains(l.name)) {
                                if(labels.containsKey(l.name))
                                    throw new PTAException(null, "branch label not unique: " + t.label);
                                labels.put(l.name, l);
                                registered.add(l.name);
                            }
                        } else
                            throw new RuntimeException("branch label expected but not found");
    }
    /**
     * Applies model control's information about players into this compilation.
     * Creates a synchronisation player if there are PTAs left or labels.
     */
    private void applyPlayers() throws PTAException {
        List<PTA> pendingPtas = new LinkedList<>(ptas.values());
        List<PTALabel> pendingLabels = new LinkedList<>(labels.values());
        Map<String, Set<RuntimeObject>> backendPlayers = modelControl.getPlayers();
        for(String name : backendPlayers.keySet()) {
            Set<RuntimeObject> participants = backendPlayers.get(name);
            for(PTA pta : threadPtas.keySet()) {
                for(RuntimeObject o : participants)
                    if(threadPtas.get(pta) == o) {
                        List<PTA> set = playerPTAs.get(name);
                        if(set == null) {
                            set = new LinkedList<>();
                            playerPTAs.put(name, set);
                            List<PTALabel> localLabels = new LinkedList<>();
                            for(PTALabel l : pta.labeled.keySet())
                                if(l != null && l.type == PTALabel.Type.FREE) {
                                    localLabels.add(l);
                                    if(!pendingLabels.contains(l))
                                        throw new RuntimeException("free label not found");
                                    pendingLabels.remove(l);
                                }
                            playerLabels.put(name, localLabels);
                        }
                        set.add(pta);
                    }
                pendingPtas.remove(pta);
            }
        }
        for(String name : modelControl.getPlayerList())
            playerList.add(name);
        if(!playerList.isEmpty() &&
                (!pendingLabels.isEmpty() || !pendingPtas.isEmpty())) {
            // this is a game, but some PTAs or labels are not players; add them into a
            // new player
            if(playerPTAs.keySet().contains(SYNC_PLAYER_NAME))
                throw new PTAException(null, "name clash with " +
                        SYNC_PLAYER_NAME + " player");
            playerPTAs.put(SYNC_PLAYER_NAME, pendingPtas);
            playerLabels.put(SYNC_PLAYER_NAME, new LinkedList<>(pendingLabels));
            playerList.add(SYNC_PLAYER_NAME);
        }
    }
    /**
     * Adds definitions of constants to the verbatim section. The constants
     * are built from static fields in non--library classes, replaced with constants.
     * Makes <code>allowedNdParameters</code>.
     * 
     * @param c code compilation
     * @param i interpreter, holding the constant's values
     */
    public void appendConstantFields(CodeCompilation c, AbstractInterpreter i)
            throws PTAException {
        Map<CodeVariable, Literal> constantFields = new HashMap<>();
        SCAN_CLASSES:
        for(CodeClass cc : c.classes.values())
            if(!cc.isFromLibrary) {
                for(CommentTag ct : cc.tags)
                    if(ct.name.equals(Hedgeelleth.INTERNAL_CONSTANTS_TAG_STRING))
                        continue SCAN_CLASSES;
                for(CodeVariable field : cc.fields.values())
                    if(field.context != Context.NON_STATIC && field.finaL &&
                            !field.isInternal()) {
                        // allow for unknown constants, as their values are discarded
                        // anyway
                        constantFields.put(field, i.getValueUnchecked(new RangedCodeValue(
                                new CodeFieldDereference(field), null), null, true));
                    }
            }
        SortedMap<String, CodeVariable> variables = new TreeMap<>();
        SortedMap<String, Literal> values = new TreeMap<>();
        allowedNdParameters = new HashMap<>();
        for(CodeVariable cv : constantFields.keySet()) {
            String name = cv.name;
            if(variables.keySet().contains(name)) {
                if(i.unknownConstants.contains(cv))
                    throw new PTAException(cv.getStreamPos(),
                            "static field that is an unknown constant " +
                            "must have an unique name: "+ name);
                // resolve a name conflict
                CodeVariable variable = variables.get(name);
                if(i.unknownConstants.contains(variable))
                    throw new PTAException(variable.getStreamPos(),
                            "static field that is an unknown constant " +
                            "must have an unique name: "+ name);
                Literal value = values.get(name);
                variables.remove(name);
                values.remove(name);
                name = ((CodeClass)variable.owner).getUnqualifiedName() +
                        "_" + name;
                variables.put(name, variable);
                values.put(name, value);
                name = ((CodeClass)cv.owner).getUnqualifiedName() +
                        "_" + cv.name;
                if(variables.keySet().contains(name))
                    throw new PTAException(cv.getStreamPos(),
                            "name clash between final static fields: " + name);
            }
            variables.put(name, cv);
            values.put(name, i.unknownConstants.contains(cv) ?
                    null : constantFields.get(cv));
            allowedNdParameters.put(cv.toNameString(), name);
        }
        StringBuilder out = new StringBuilder();
        for(String name : values.keySet()) {
            Literal value = values.get(name);
            if(variables.get(name).type.isOfFloatingPointTypes()) {
                name = "double " + name;
                if(value != null)
                    name += " = " + value.getMaxPrecisionFloatingPoint();
            } else if(value.type.isOfIntegerTypes()) {
                name = "int " + name;
                if(value != null)
                    name += " = " + value.getMaxPrecisionInteger();
            } else
                name = null;
            if(name != null)
                out.append("const " + name + ";\n");
        }
        out.append("\n");
        out.append(modelControl.getVerbatimSuffix());
        modelControl.removeVerbatim();
        modelControl.appendVerbatim(out.toString());
    }
    /**
     * Completes the system of PTAs with information in the
     * model control. Currently includes: branch labels, players.
     */
    public void applyModelControl() throws PTAException {
        applyBranchLabels();
        applyPlayers();
    }
    /**
     * Returns a PTA of a given thread object.
     * 
     * @param o thread object
     * @return PTA, null if <code>o</code> is not associated
     * with any PTA
     */
    public PTA getPTA(RuntimeObject o) {
        for(PTA pta : threadPtas.keySet())
            if(threadPtas.get(pta) == o)
                return pta;
        return null;
    }
    @Override
    public String toString() {
        String s = "";
        for(AbstractRuntimeContainer rc : variables.keySet()) {
            String c = "container " + rc.getUniqueName() + " {\n";
            Map<CodeVariable, AbstractBackendPTAVariable> inContainer =
                    variables.get(rc);
            for(CodeVariable cv : inContainer.keySet()) {
                AbstractBackendPTAVariable tv = inContainer.get(cv);
                c += "    ";
                if(cv != null)
                    c += cv.name + "=";
                c += tv.toString() + "\n";
            }
            c += "}\n";
            s += c;
        }
        for(String name : ptas.keySet()) {
            PTA tadd = ptas.get(name);
            s += "TADD " + tadd.toString();
        }
        return s;
    }
}
