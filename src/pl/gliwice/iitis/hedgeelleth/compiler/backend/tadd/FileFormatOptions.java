/*
 * FileFormatOptions.java
 *
 * Created on Jul 3, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

/**
 * Options for <code>FileFormatIO</code> implementations.
 * 
 * @author Artur Rataj
 */
public class FileFormatOptions {
    /**
     * Maximum verbosity level.
     */
    public static final int MAX_VERBOSITY_LEVEL = 2;
    /**
     * Default verbosity level.
     */
    public static final int DEFAULT_VERBOSITY_LEVEL = 1;

    /**
     * Verbosity level, 0 ... MAX_VERBOSITY_LEVEL.
     */
    public int verbosityLevel;
    /**
     * If to comment only valuation.
     */
    public boolean commentVariablesOnly;
    /**
        * If TADD names should be heuristically generated for a greater readability,
        * instead of using <code>z&lt;index&gt;</code>. If the variable is true,
        * a backend may ignore it.<br>
        */
    public boolean verboseTADDVariables;
    /**
     * If range checking should be performed on a variable or in index if its range is outside
     * proposals.
     */
    public boolean rangeChecking;
    /**
     * If model control's properties should be generated.
     */
    public boolean generateModelControlProperties;
    /**
     * If arrays should be emulated with scalar variables and constants.
     */
    public boolean emulateArrays;
    /**
     * Fall back from MDP to DTMC if no non--determinism.
     */
    public boolean dtmcFallback = false;
    /**
     * Name shortener of variables replaces instances with their position in a sorted list.
     */
    public boolean compactInstances;
    
    
    /**
     * Creates a new instance of <code>FileFormatOptions</code>.
     * 
     * @param verbosityLevel copied to the field <code>verbosityLevel</code>
     * @param commentVariablesOnly copied to the field <code>commentVariablesOnly</code>
     * @param verboseTADDVariables copied to the field <code>verboseTADDVariables</code>
     * @param rangeChecking copied to the field <code>rangeChecking</code>
     * @param generateModelControlProperties copied to the field
     * <code>generateModelControlProperties</code>
     * @param emulateArrays if arrays should be emulated
     * @param dtmcFallback fall back from MDP to DTMC if no non--determinism
     * @param compactInstances name shortener of variables replaces instances with their position
     * in a sorted list
     */
    public FileFormatOptions(int verbosityLevel, boolean commentVariablesOnly,
            boolean verboseTADDVariables, boolean rangeChecking,
            boolean generateModelControlProperties, boolean emulateArrays,
            boolean dtmcFallback, boolean compactInstances) {
        this.verbosityLevel = verbosityLevel;
        this.commentVariablesOnly = commentVariablesOnly;
        this.verboseTADDVariables = verboseTADDVariables;
        this.rangeChecking = rangeChecking;
        this.generateModelControlProperties = generateModelControlProperties;
        this.emulateArrays = emulateArrays;
        this.dtmcFallback = dtmcFallback;
        this.compactInstances = compactInstances;
    }
}
