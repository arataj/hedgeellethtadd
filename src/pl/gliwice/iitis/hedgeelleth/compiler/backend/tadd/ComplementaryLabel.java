/*
 * ComplementaryLabel.java
 *
 * Created on Jul 8, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.pta.PTALabel;
import pl.gliwice.iitis.hedgeelleth.pta.PTATransition;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.TADDGeneratorOptions;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.pta.PTAException;

/**
 * A complementary label requires transitions on both sender or "active" and
 * receiver or "passive" side. A directional label must be complementary,
 * otherwise it is deleted. Barrier labels do not use this class.<br>
 * 
 * Current complementary labels, S for sender, R for receiver:<br>
 * 
 * notify -- S notify(), notifyAll(), R wait()'s notify;
 * join -- S join(), R thread end;
 * critical sections -- S lock, R thread;
 * wait -- S lock, R thread.<br>
 *
 * Used to find pairs of synchronised transitions.
 * See also <code>AbstractTADDGenerator.purgeNotComplemented</code> for
 * details on purging non--complementary labels.
 *
 * @author Artur Rataj
 */
public class ComplementaryLabel {
    /**
     * Name of this label.
     */
    public String name;
    /**
     * If this is a broadcast label.
     */
    public boolean broadcast;
    /**
     * All active transitions having this label, empty for none.
     */
    public PriorityQueue<PTATransition> active = new PriorityQueue<>();
    /**
     * All passive transitions having this label, empty for none.
     */
    public PriorityQueue<PTATransition> passive = new PriorityQueue<>();
    /**
     * All transitions.
     */
    public PriorityQueue<PTATransition> transitions = new PriorityQueue<>();
    /**
     * Copied from <code>TADDOptions.labelNondeterminism</code>.
     * If false, forbids more than a single passive label.
     */
    boolean labelNondeterminism;
    
    /**
     * Creates a new instance of ComplementaryLabel. 
     * 
     * @param name                      name of the label
     * @param options                   TADD generator options
     */
    public ComplementaryLabel(PTALabel label, TADDGeneratorOptions options) {
        this.name = label.name;
        labelNondeterminism = options.labelNondeterminism;
        broadcast = label.broadcast;
    }
    /**
     * Reports an active transition with this label.
     * 
     * @param tt                        transition
     */
    public void reportActive(PTATransition tt) {
        if(tt.ownerName == null)
            throw new RuntimeException("reported transition needs to have " +
                    "an owner name");
        transitions.add(tt);
        active.add(tt);
        Set<String> ptaNames = new HashSet<>();
        for(PTATransition p : active)
            ptaNames.add(p.ownerName);
        if(ptaNames.size() > 1) {
            StreamPos pos;
            if(!tt.backendOps.isEmpty())
                pos = tt.backendOps.get(0).getStreamPos();
            else
                pos = null;
            throw new RuntimeException("unexpected multiple active " +
                    "labels from different automatons");
        }
    }
    /**
     * Reports a passive transition with this label.
     * 
     * @param tt                        transition
     */
    public void reportPassive(PTATransition tt) throws PTAException {
        if(tt.ownerName == null)
            throw new RuntimeException("reported transition needs to have " +
                    "an owner name");
        transitions.add(tt);
        passive.add(tt);
        if(!labelNondeterminism) {
            Set<String> ptaNames = new HashSet<>();
            for(PTATransition p : passive)
                ptaNames.add(p.ownerName);
            if(ptaNames.size() > 1) {
                StreamPos pos;
                if(!tt.backendOps.isEmpty())
                    pos = tt.backendOps.get(0).getStreamPos();
                else
                    pos = null;
                throw new PTAException(pos, "multiple passive labels from " +
                        "different automatons not supported " +
                        "if non-determinism of synchronisation is absent");
            }
        }
    }
    /**
     * If this label is complemented, that is, there are both
     * active and passive transitions labeled with this label.
     * 
     * @return                          if complemented
     */
    public boolean isComplemented() {
        return !active.isEmpty() && !passive.isEmpty();
    }
    /**
     * Returns all transitions except for a given one. More than a single transition in total is
     * required.
     * 
     * // If <code>tt</code>' label is a barrier, then labels all other transitions must be
     * // barries, too, or a runtime exception is thrown. If <code>tt</code>'s label is directional,
     * // then labels of all other transitions must be complementary to it, or a runtime exception is
     * // thrown.
     * 
     * @param tt a transition to exclude
     * @return a non--empty set of transitons
     */
    List<PTATransition> getOtherTransitions(PTATransition tt) {
        boolean mustBeBarrier = tt.label.type == PTALabel.Type.BARRIER;
        if(mustBeBarrier)
            throw new RuntimeException("unexpected barrier");
        boolean mustBeReceiver = tt.label.type == PTALabel.Type.SENDER;
        if(tt.label.type == PTALabel.Type.DIRECTIONAL)
            throw new RuntimeException("incomplete label type");
        List<PTATransition> out = new LinkedList<>();
        for(PTATransition x : transitions) {
            if(x == tt)
                continue;
            //if(mustBeBarrier) {
            //    if(x.label.type != PTALabel.Type.BARRIER)
            //        throw new RuntimeException("barrier expected");
            /*} else */if(mustBeReceiver) {
                if(x.label.type != PTALabel.Type.RECEIVER)
                    continue;
            } else {
                if(x.label.type != PTALabel.Type.SENDER)
                    continue;
            }
            out.add(x);
        }
        if(out.isEmpty())
            throw new RuntimeException("no other transition found");
        return out;
    }
}
