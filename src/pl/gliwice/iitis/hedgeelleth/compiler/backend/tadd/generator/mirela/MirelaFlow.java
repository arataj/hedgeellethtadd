/*
 * MirelaFlow.java
 *
 * Created on Mar 27, 2018
 *
 * Copyright (c) 2018  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.mirela;

import java.util.*;

/**
 *
 * @author Artur Rataj
 */
public class MirelaFlow {
    /**
     * Source, transitory nodes and a sink.
     */
    List<MirelaNode> nodes;
    
    /**
     * Creates a new flow through Mirela components.
     * 
     * @param nodes source, transitory nodes and a sink
     */
    public MirelaFlow(List<MirelaNode> nodes) {
        this.nodes = nodes;
    }
    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        out.append("FLOW ");
        boolean first = true;
        for(MirelaNode n : nodes) {
            if(!first)
                out.append(" -> ");
            out.append(n.name);
            first = false;
        }
        return out.toString();
    }
}
