/*
 * AbstractProperty.java
 *
 * Created on Feb 19, 2010, 11:28:35 AM
 *
 * Copyright (c) 2010 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator.properties;

/**
 * An abstract property. Along with its subclasses, used solely to generate
 * simple model--checking properties, created by the backend.<br>
 * 
 * Do not use outside the backend. The project <code>HedgeellethQuery</code>
 * can be used instead.
 *
 * @author Artur Rataj
 */
public class AbstractProperty {
    /**
     * Probability operator.
     */
    public ProbabilityOperator p;

    /**
     * Creates a new instance of AbstractProperty.
     *
     * @param p                         probability operator
     */
    public AbstractProperty(ProbabilityOperator p) {
        this.p = p;
    }
}
