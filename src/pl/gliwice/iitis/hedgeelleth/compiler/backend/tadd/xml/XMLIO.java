/*
 * XMLIO.java
 *
 * Created on Jun 19, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.xml;

import java.util.*;
import java.io.*;

import org.jdom.*;
import org.jdom.output.Format;
import org.jdom.output.Format.TextMode;
import org.jdom.output.XMLOutputter;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.ArrayStore;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.AbstractRuntimeContainer;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeField;
import pl.gliwice.iitis.hedgeelleth.pta.values.*;
import pl.gliwice.iitis.hedgeelleth.pta.*;
import pl.gliwice.iitis.hedgeelleth.pta.io.NameShortener;
import pl.gliwice.iitis.hedgeelleth.pta.io.PTAIO;

/**
 * Generates a text representation of a PTA compilation in either plain XML
 * or the format used by the Uppaal software.
 * 
 * @author Artur Rataj
 */
public abstract class XMLIO extends FileFormatIO {
    /**
     * Indent needed by the Uppaal profile.
     */
    static final String UPPAAL_INDENT = "    ";
    
    /**
     * Profile to use: plain XML or Uppaal.
     */
    public enum Profile {
        XML,
        UPPAAL,
    };
    
    /**
     * Committed locations of semaphores.
     */
    boolean committedLocks;
    
    /**
     * Creates a new generator of PTA compilation to file in either plain XML
     * or the format used by the Uppaal software.
     * 
     * @param options PTA generation options
     * @param committedLocks            commited locations of locks
     */
    public XMLIO(FileFormatOptions options, boolean committedLocks) {
        super(options);
        this.committedLocks = committedLocks;
        if(options.emulateArrays)
            throw new RuntimeException("emulation of arrays not supported");
    }
    /**
     * Produces a text representation of a value.
     * 
     * @param profile                   profile
     * @param v                         value
     * @param variables                 variables
     * @param clockNum                  current clock number
     */
    protected String parse(Profile profile, AbstractPTAValue e,
            Map<AbstractPTAVariable, Integer> variables,
            NameShortener shortener, int clockNum) throws IOException {
        if(e instanceof PTAConstant) {
            PTAConstant c = (PTAConstant)e;
            if(c.floating)
                return "" + c.value;
            else
                return "" + (int)c.value;
        } else if(e instanceof PTAClock) {
            return "x" + clockNum;
        } else if(e instanceof PTADensity) {
            if(profile != Profile.XML)
                throw new IOException("Uppaal format does not support densities");
            PTADensity d = (PTADensity)e;
//            if(d.constParams != null)
//                return d.toString();
//            else {
                StringBuilder s = new StringBuilder(d.typeToString() + "(");
                boolean first = true;
                for(AbstractPTAValue p : d.params) {
                    if(!first)
                        s.append(", ");
                    s.append(parse(profile, p, variables, shortener, clockNum));
                    first = false;
                }
                s.append(")");
                return s.toString();
//            }
        } else if(e instanceof PTAElementVariable) {
            PTAElementVariable ae =
                    (PTAElementVariable)e;
            return parse(profile, ae.array, variables, shortener, clockNum) + "[" +
                    parse(profile, ae.index, variables, shortener, clockNum) + "]";
        } else {
            AbstractPTAVariable v = (AbstractPTAVariable)e;
            return shortener.getName(v.name, true, variables.get(v));
        }
    }
    /**
     * Produces a text representation of an expression.
     * 
     * If clock number is -1, only assignments to non--clock variables are produced.
     * Otherwise, only assignments to clock variables are produced.
     * 
     * 
     * @param profile                   profile
     * @param e                         expression
     * @param variables                 variables
     * @param booleanResult if true, then this expression is expected to have a boolean
     * result; if false, then this expression can have any result
     * @return                          text description of the
     *                                  expression
     */
    protected String parse(Profile profile, AbstractPTAExpression e,
            Map<AbstractPTAVariable, Integer> variables,
            NameShortener shortener, int clockNum,
            boolean booleanResult) throws IOException {
        String s = "";
        String booleanTarget = "";
        if(e instanceof PTABinaryExpression) {
            PTABinaryExpression be = (PTABinaryExpression)e;
            BinaryExpression.Op operator =
                    CodeOpBinaryExpression.operatorCodeToTree(
                    be.operator);
            s = BinaryExpression.getOperatorName(operator);
            if(operator.isRelational()) {
                /*
                switch(operator) {
                    case GREATER:
                    case GREATER_OR_EQUAL:
                        s = BinaryExpression.flipOperatorName(s);
                        s = parse(be.right, variables, clockNum) + " " + s + " " +
                                parse(be.left, variables, clockNum);
                        break;
                        
                    default:
                        s = parse(be.left, variables, clockNum) + " " + s + " " +
                                parse(be.right, variables, clockNum);
                        break;
                        
                }
                */
            } else {
                switch(operator) {
                    case PLUS:
                    case MINUS:
                    case MULTIPLY:
                    case DIVIDE:
                    case MODULUS:    
                        break;

                    default:
                        throw new IOException("operator " + s + " not allowed");

                }
            }
            s = parse(profile, be.left, variables, shortener, clockNum) + " " + s + " " +
                    parse(profile, be.right, variables, shortener, clockNum);
        } else if(e instanceof PTAUnaryExpression) {
            PTAUnaryExpression ue = (PTAUnaryExpression)e;
            if(ue.target == null) {
                // a condition
                s = parse(profile, ue.sub, variables, shortener, clockNum);
                if(ue.operator == AbstractCodeOp.Op.UNARY_CONDITIONAL_NEGATION)
                    booleanTarget = " == 0";
                else if(ue.operator != AbstractCodeOp.Op.NONE) {
                    s = UnaryExpression.getOperatorName(
                        CodeOpUnaryExpression.operatorCodeToTree(
                        ue.operator), null)[0];
                    throw new IOException("operator " + s + " not allowed in condition");
                } else
                    booleanTarget = " == 1";
            } else {
                if(ue.operator == AbstractCodeOp.Op.UNARY_CONDITIONAL_NEGATION)
                    s = "1 - " + parse(profile, ue.sub, variables, shortener, clockNum);
                else if(ue.operator == AbstractCodeOp.Op.UNARY_NEGATION)
                    s = "0 - " + parse(profile, ue.sub, variables, shortener, clockNum);
                else if(ue.operator == AbstractCodeOp.Op.NONE)
                    s = parse(profile, ue.sub, variables, shortener, clockNum);
                else {
                    s = UnaryExpression.getOperatorName(
                        CodeOpUnaryExpression.operatorCodeToTree(
                        ue.operator), null)[0];
                    if(s.isEmpty())
                        s = "postfix " + UnaryExpression.getOperatorName(
                            CodeOpUnaryExpression.operatorCodeToTree(
                            ue.operator), null)[1];
                    else
                        s = "prefix " + s;
                    throw new IOException("operator " + s + " not allowed");
                }
            }
        } else
            throw new RuntimeException("XMLIO: unsupported: " + e.toString());
        if(e.target != null)
            s = parse(profile, e.target, variables, shortener, clockNum) + " = " + s;
        else if(e instanceof PTAUnaryExpression && booleanResult)
            s = s + booleanTarget;
        return s;
    }
    /**
     * Appends to a string, preceeding the addition with a separator
     * if the string was not empty.
     * 
     * @param s                         base string
     * @param a                         addition
     * @param separator                 separator or null for ", "
     * @return                          resulting string
     */
    protected static String addToString(String s, String a, String separator) {
        if(s.isEmpty())
            s = a;
        else
            s = s + (separator == null ? ", " : separator) + a;
        return s;
    }
    /**
     * Converts name of a PTA to a format supported by Uppaal.
     * 
     * @param in                        input name
     * @return                          converted name
     */
    protected static String convertTADDName(String in) {
        String out = "";
        for(int i = 0; i < in.length(); ++i) {
            char c = in.charAt(i);
            String s;
            switch(c) {
                case '_':
                    s = "__";
                    break;
                    
                case '#':
                    s = "_H";
                    break;
                    
                case '.':
                    s = "_D";
                    break;
                    
                case '<':
                    s = "_L";
                    break;
                    
                case '>':
                    s = "_G";
                    break;
                    
                default:
                    s = "" + c;
                    break;
            }
            out += s;
        }
        return out;
    }
    /**
     * Returns a comment in the form supported by a given profile.
     * 
     * @param profile                   profile
     * @param string                    comment string
     * @return                          content
     */
    protected static Content getComment(Profile profile, String string) {
        Content content = null;
        switch(profile) {
            case XML:
                content = new Comment(" " + string + " ");
                break;
                
            case UPPAAL:
                content = new Text("/* " + string + " */");
                break;
                
        }
        return content;
    }
    /**
     * Returns a string with a comment descriping a given list of operations,
     * can also add the comment to an element, according to the profile.
     *
     * @param element                   element to contain the comment,
     *                                  can be null
     * @param profile                   profile, can be null if <code>element</code>
     *                                  is null
     * @param firstComment              if it is a first comment in a
     *                                  sequence of comments
     * @param backendOps                       list of operations
     * @param miscStr                   miscelanous string to attach to the
     *                                  comment, can be null
     * @return                          a string with the comment
     */
    String getCodeOpListComment(Element element, Profile profile,
            boolean firstComment, List<AbstractCodeOp> ops,
            String miscStr) {
        String s = "";
        if(miscStr == null)
            miscStr = "";
        else if(!miscStr.isEmpty())
            miscStr = " " + miscStr;
        switch(options.verbosityLevel) {
            case 0:
            {
                break;
            }
            case 1:
            {
                if(ops.size() != 0) {
                    String comment = "";
                    TreeSet<String> positions = new TreeSet<String>();
                    for(AbstractCodeOp op : ops)
                        positions.add("" + op.getStreamPos());
                    int count = 0;
                    for(String p : positions) {
                        if(count != 0)
                            comment += " ";
                        comment += p;
                        ++count;
                    }
                    if(element != null && profile == Profile.UPPAAL && !firstComment)
                        element.addContent("\n" + UPPAAL_INDENT + "  ");
                    comment += miscStr;
                    s += comment + " ";
                    if(element != null)
                        element.addContent(getComment(profile,
                                comment));
                }
                break;
            }
            case 2:
            {
                int count = 0;
                for(AbstractCodeOp op : ops) {
                    if(element != null && profile == Profile.UPPAAL && !firstComment)
                        element.addContent("\n" + UPPAAL_INDENT + "  ");
                    String t = count + " " + op.toString() + " (" +
                            op.getStreamPos() + ")";
                    t += miscStr;
                    s += t + " ";
                    if(element != null)
                        element.addContent(getComment(profile, t));
                    firstComment = false;
                    ++count;
                }
                break;
            }   
        }
        return s.trim();
    }
    /**
     * Returns a key, that represents the contents of a given transition
     * comment. Used to find repeating comments.
     * 
     * @param states                    map of states ot a PTA 
     * @param tt                        transition
     * @return                          key of the transition's comment
     */
    String getTransitionCommentKey(Map<PTAState, Integer> states,
            PTATransition tt) {
        return states.get(tt.targetState).toString() + " " +
            (tt.label == null ? "" : tt.label.toString()) + " " +
            getCodeOpListComment(null, null, false, tt.backendOps, null);
    }
    /**
     * Generates a comment for a given state.
     *
     * @param profile                   profile to use
     * @param element                   element to contain the comment
     * @param states                    map of states ot a PTA
     * @param state                     state to comment in the PTA    
     * @return                          comment string
     */
    void commentState(Profile profile, Element element, Map<PTAState, Integer> states,
            PTAState state) {
        switch(options.verbosityLevel) {
            case 0:
            {
                break;
            }
            case 1:
            case 2:
            {
                Map<String, Integer> repetitions = new HashMap<String, Integer>();
                for(Set<PTATransition> set : state.output.values())
                    for(PTATransition tt : set) {
                        String key = getTransitionCommentKey(states, tt);
                        int repeated;
                        if(repetitions.containsKey(key))
                            repeated = repetitions.get(key);
                        else
                            repeated = 0;
                        ++repeated;
                        repetitions.put(key, repeated);
                    }
                boolean firstComment = true;
                for(Set<PTATransition> set : state.output.values()) {
                    for(PTATransition tt : set) {
                        String key = getTransitionCommentKey(states, tt);
                        int repeated;
                        if(repetitions.containsKey(key))
                            repeated = repetitions.get(key);
                        else
                            repeated = 0;
                        if(repeated != -1) {
                            repetitions.put(key, -1);
                            String repeatedStr;
                            if(repeated > 1)
                                repeatedStr = " (repeated " + repeated + " times)";
                            else
                                repeatedStr = "";
                            if(!firstComment && profile == Profile.UPPAAL)
                                    element.addContent("\n" + UPPAAL_INDENT + "  ");
                            firstComment = false;
                            element.addContent(getComment(profile,
                                    "target l" + states.get(tt.targetState) +
                                    repeatedStr));
                            if(tt.label != null) {
                                if(profile == Profile.UPPAAL)
                                        element.addContent("\n" + UPPAAL_INDENT + "  ");
                                element.addContent(getComment(profile, tt.label.toString()));
                            }
                            getCodeOpListComment(element, profile, false, tt.backendOps,
                                     null);
                        }
                    }
                }
            }
        }
    }
    protected String variableInitializerString(AbstractPTAVariable v) {
        if(v instanceof PTAScalarVariable) {
            double l = ((PTAScalarVariable)v).initValue;
            if(v.floating)
                return "" + l;
            else
                return "" + (int)l;
        } else if(v instanceof PTAArrayVariable) {
            double[] a = ((PTAArrayVariable)v).initValue;
            String s = "{";
            for(int i = 0; i < a.length; ++i) {
                if(i != 0)
                    s += ", ";
                if(v.floating)
                    s += "" + a[i];
                else
                    s += "" + (int)a[i];
            }
            s  += "}";
            return s;
        } else
            throw new RuntimeException("unrecognized variable initializer");
    }
    /**
     * Writes a PTA compilation to a file.
     * 
     * @param profile                   profile to use
     * @param file                      file to create
     * @param c                         PTA compilation
   */
    public void write(Profile profile, File file, BackendPTACompilation c) throws IOException {
        try {
            XMLOutputter outp = new XMLOutputter(Format.getPrettyFormat().
                    setTextMode(TextMode.PRESERVE));
            Document doc = new Document();
            Element eNta = new Element("nta");
            doc.addContent(eNta);
            Element eDeclaration = new Element("declaration");
            eNta.addContent(eDeclaration);
            SortedMap<String, String> sortedReplaced = new TreeMap<>();
            for(RuntimeField rf : c.replacedPrimitiveFields.keySet())
                sortedReplaced.put(rf.toString(), c.replacedPrimitiveFields.get(rf).toString());
            for(String fieldName : sortedReplaced.keySet()) {
                String fieldValue = sortedReplaced.get(fieldName);
                if(profile == Profile.UPPAAL)
                    eDeclaration.addContent("\n" + UPPAAL_INDENT);
                eDeclaration.addContent(getComment(profile,
                        fieldName + " = " + fieldValue));
            }
            SortedMap<String, AbstractBackendPTAVariable> sortedVariables =
                    new TreeMap<>();
            for(AbstractRuntimeContainer rc : c.variables.keySet()) {
                Map<CodeVariable, AbstractBackendPTAVariable> inContainer =
                        c.variables.get(rc);
                for(CodeVariable cv : inContainer.keySet()) {
                    AbstractBackendPTAVariable tv = inContainer.get(cv);
                    sortedVariables.put(tv.v().name, tv);
                }
            }
            // this map stores numbers of variables
            Map<AbstractPTAVariable, Integer> variables = new HashMap<>();
            int count = 0;
            Collection<String> allNames = new LinkedList<>();
            for(String v : sortedVariables.keySet())
                allNames.add(v);
            NameShortener shortener = new NameShortener(options.verboseTADDVariables,
                options.compactInstances, allNames, null);
            for(AbstractBackendPTAVariable tv : sortedVariables.values())
                if(!(tv instanceof BackendPTAClock)) {
                    if(profile == Profile.UPPAAL && tv.v().floating)
                       throw new IOException("floating--point variables not supported by Uppaal format");
                    variables.put(tv.v(), count);
                    if(profile == Profile.UPPAAL)
                        eDeclaration.addContent("\n" + UPPAAL_INDENT);
                    if(options.verbosityLevel != 0) {
                            eDeclaration.addContent(getComment(profile,
                                    tv.v().name));
                            if(profile == Profile.UPPAAL)
                                eDeclaration.addContent("\n" + UPPAAL_INDENT);
                    }
                    String arraySizeString;
                    if(tv instanceof BackendPTAArrayVariable) {
                        arraySizeString = "[" + ((ArrayStore)tv.container).getLength() + "]";
                    } else
                        arraySizeString = "";
                    switch(profile) {
                        case XML:
                            Element eVariable = new Element("variable").
                                    setAttribute("type", "int" + arraySizeString).
                                    setAttribute("min", "" + PTAIO.getMin(tv.v().range.range)).
                                    setAttribute("max", "" + PTAIO.getMax(tv.v().range.range)).
                                    setAttribute("rangeCheck", "" + !tv.v().range.backendGuaranteed).
                                    setAttribute("name", shortener.getName(tv.v().name, true, count)).
                                    setAttribute("initialValue", "" +
                                        variableInitializerString(tv.v()));
                            eDeclaration.addContent(eVariable);
                            break;

                        case UPPAAL:
                            eDeclaration.addContent(
                                    "int[" +
                                    PTAIO.getMin(tv.v().range.range) + "," + PTAIO.getMax(tv.v().range.range) +
                                    "] " + shortener.getName(tv.v().name, true, count) + arraySizeString + " = " +
                                    variableInitializerString(tv.v()) + ";");
                            break;

                    }
                    ++count;
                }
            // serial numbers of label names keyed with strings
            SortedMap<String, Integer> sLabelNames = new TreeMap<>();
            Set<Integer> sBroadcast = new HashSet<>();
            int nextFreeLabelNameNum = 0;
            int nextFreeClockNum = 0;
            SortedMap<String, PTA> sortedTADDs = new TreeMap<>();
            for(PTA tadd : c.ptas.values())
                sortedTADDs.put(tadd.name, tadd);
            int taddCount = 0;
            String systemString = "";
            ptaNameMap = new HashMap<>();
            for(PTA tadd : sortedTADDs.values()) {
                String taddName = convertTADDName(tadd.name);
                ptaNameMap.put(tadd, taddName);
                systemString = addToString(systemString, taddName, null);
                Element eTemplate = new Element("template");
                eNta.addContent(eTemplate);
                if(options.verbosityLevel != 0 && !options.commentVariablesOnly)
                    eTemplate.addContent(getComment(profile, tadd.name));
                Element eName = new Element("name").addContent(taddName);
                eTemplate.addContent(eName);
                int clockNum = -1;
                if(tadd.clock != null) {
                    clockNum = nextFreeClockNum++;
                    String clockName = "x" + clockNum;
                    Element eClockDeclaration = new Element("declaration").addContent(
                            "clock " + clockName + ";");
                    eTemplate.addContent(eClockDeclaration);
                }
                // this map stores serial numbers of states
                Map<PTAState, Integer> states = new HashMap<>();
                Map<Integer, PTAState> statesSorted = new HashMap<>();
                count = 0;
                PTAState startState = tadd.startState;
                states.put(startState, count);
                statesSorted.put(count, startState);
                ++count;
                for(PTAState state : tadd.states.values())
                    if(state != startState) {
                        states.put(state, count);
                        statesSorted.put(count, state);
                        ++count;
                    }
                int statesNum = count;
                for(int i = 0; i < statesNum; ++i) {
                    Element eLocation = new Element("location");
                    eTemplate.addContent(eLocation);
                    eLocation.setAttribute("id", "l" + i);
                    PTAState state = statesSorted.get(i);
                    String valuationString;
if(state == null)
    state = state;
                    Integer v = tadd.valuation.get(state);
                    if(v != null)
                        valuationString = "v" + v;
                    else
                        valuationString = "";
                    if(!options.commentVariablesOnly || v != null)
                        commentState(profile, eLocation, states, state);
                    String conditionString = "";
                    if(state.clockLimitHigh != null)
                        conditionString = addToString(conditionString,
                                parse(profile, state.clockLimitHigh, variables, shortener, clockNum, true),
                                " && ");
                    if(!conditionString.isEmpty()) {
                        Element eCondition = new Element("label").setAttribute(
                                "kind", "guard").addContent(conditionString);
                        eLocation.addContent(eCondition);
                    }
                    Element eLocationName = new Element("name").addContent(
                            valuationString);
                    eLocation.addContent(eLocationName);
                    if(committedLocks && (tadd instanceof TADDLock))
                        eLocation.addContent(new Element("committed"));
                }
                eTemplate.addContent(
                        new Element("init").setAttribute("ref", "l0"));
                if(!c.modelControl.getPlayerList().isEmpty())
                    System.out.println("warning: players not supported");
                for(PTAState tState : statesSorted.values()) {
                    for(Set<PTATransition> set : tState.output.values())
                        for(PTATransition tt : new TreeSet<>(set)) {
                            Element eTransition = new Element("transition");
                            eTemplate.addContent(eTransition);
                            int labelNum;
                            boolean synchronizationLabel;
                            if(synchronizationLabel = (tt.label != null)) {
                                if(sLabelNames.containsKey(tt.label.name))
                                    labelNum = sLabelNames.get(tt.label.name);
                                else {
                                    labelNum = nextFreeLabelNameNum++;
                                    sLabelNames.put(tt.label.name, labelNum);
                                }
                                if(tt.label.broadcast)
                                    sBroadcast.add(labelNum);
                            } else {
                                labelNum = nextFreeLabelNameNum++;
                            }
                            boolean firstComment = true;
                            if(synchronizationLabel) {
                                if(options.verbosityLevel != 0 && !options.commentVariablesOnly)
                                    eTransition.addContent(
                                        getComment(profile, tt.label.toString()));
                                firstComment = false;
                            }
                            if(!options.commentVariablesOnly)
                                getCodeOpListComment(eTransition,
                                    profile, firstComment, tt.backendOps, null);
                            Element eSource = new Element("source").setAttribute(
                                    "ref", "l" + states.get(tt.sourceState));
                            eTransition.addContent(eSource);
                            Element eTarget = new Element("target").setAttribute(
                                    "ref", "l" +
                                    (tt.targetState == null ? "null" : states.get(tt.targetState)));
                            eTransition.addContent(eTarget);
                            if(synchronizationLabel) {
                                Element eSynchronization = new Element("label").setAttribute(
                                        "kind", "synchronisation");
                                eTransition.addContent(eSynchronization);
                                /*
                                boolean isNotify = false;
                                boolean isJoin = false;
                                if(tt.ops.size() > 0 &&
                                        tt.ops.get(0) instanceof CodeOpNone) {
                                    CodeOpNone n = (CodeOpNone)tt.ops.get(0);
                                    if(n.annotations.contains(
                                            CompilerAnnotation.NOTIFY_STRING))
                                        isNotify = true;
                                    if(n.annotations.contains(
                                            CompilerAnnotation.JOIN_STRING))
                                        isJoin = true;
                                }
                                 */
                                eSynchronization.addContent("e" + labelNum + tt.label.getPostfix());
                            }
                            String conditionString = "";
                            String assignmentString = "";
                            if(clockNum != -1) {
                                /*if(false && tt.clockConditionL != null)
                                    conditionString = addToString(conditionString,
                                            parse(tt.clockConditionL, variables, clockNum),
                                            " && ");*/
                                if(tt.clockLimitLow != null)
                                    conditionString = addToString(conditionString,
                                            parse(profile, tt.clockLimitLow, variables, shortener, clockNum, true),
                                            " && ");
                            }
                            if(tt.guard != null)
                                conditionString = addToString(conditionString,
                                        parse(profile, tt.guard, variables, shortener, clockNum, true),
                                        " && ");
                            for(AbstractPTAExpression expr: tt.update)
                                assignmentString = addToString(assignmentString,
                                        parse(profile, expr, variables, shortener, clockNum, false),
                                        null);
                            if(!conditionString.isEmpty()) {
                                Element eCondition = new Element("label").setAttribute(
                                        "kind", "guard").addContent(conditionString);
                                eTransition.addContent(eCondition);
                            }
                            if(tt.probability != null) {
                                Element eProbability = new Element("label").setAttribute(
                                        "kind", profile == Profile.UPPAAL ? "weight" : "probability").
                                            addContent(parse(
                                                profile, tt.probability, variables, shortener,
                                                clockNum, false));
                                eTransition.addContent(eProbability);
                            }
                            if(!assignmentString.isEmpty()) {
                                Element eAssignment = new Element("label").setAttribute(
                                        "kind", "assignment").addContent(assignmentString);
                                eTransition.addContent(eAssignment);
                            }
                        }
                    }
                ++taddCount;
            }
            if(sLabelNames.size() != 0) {
                if(options.verbosityLevel != 0) {
                    if(profile == Profile.UPPAAL)
                        eDeclaration.addContent("\n" + UPPAAL_INDENT);
                    eDeclaration.addContent(getComment(profile,
                            "synchronisation labels"));
                }
                StringBuilder chan = new StringBuilder();
                StringBuilder bChan = new StringBuilder();
                for(int label : new TreeSet<>(sLabelNames.values())) {
                    StringBuilder s;
                    String t;
                    if(sBroadcast.contains(label)) {
                        s = bChan;
                        t = "true";
                    } else {
                        s = chan;
                        t = "false";
                    }
                    if(s.length() != 0)
                        s.append(", ");
                    s.append("e" + label);
                    if(profile == Profile.XML) {
                        Element eChannel= new Element("channel");
                        eChannel.setAttribute("broadcast", t);
                        eChannel.setAttribute("name", "e" + label);
                        eDeclaration.addContent(eChannel);
                    }
                }
                if(profile == Profile.UPPAAL) {
                    if(chan.length() != 0) {
                        eDeclaration.addContent("\n" + UPPAAL_INDENT);
                        eDeclaration.addContent("chan " + chan.toString() + ";");
                    }
                    if(bChan.length() != 0) {
                        eDeclaration.addContent("\n" + UPPAAL_INDENT);
                        eDeclaration.addContent("broadcast chan " + bChan.toString() + ";");
                    }
                    eDeclaration.addContent("\n  ");
                }
            }
            Element eSystem  = new Element("system");
            eNta.addContent(eSystem);
            eSystem.addContent("system " + systemString + ";");
            outp.output(doc, new FileOutputStream(file));
            if(options.generateModelControlProperties &&
                    !c.modelControl.getProperties().isEmpty())
                System.out.println("warning: properties not supported");
            String verbatim = c.modelControl.getVerbatimSuffix();
            if(!verbatim.isEmpty())
                System.out.println("warning: verbatim suffix not supported");
        } catch(IOException e)  {
            throw new IOException("backend XMLIO: " + e.getMessage());
        }
    }
}
