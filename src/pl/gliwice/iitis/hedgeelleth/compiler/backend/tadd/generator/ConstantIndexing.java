/*
 * ConstantIndexing.java
 *
 * Created on Sep 4, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.Unroll.ConstantIndexingInfo;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.runtime.RuntimeStaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;

/**
 * Indexing with variables is replaced by conditional jumps and indexing with
 * constants.<br>
 * 
 * An index does not have to be a local, but it may cause in turn, that
 * a field is read multiple times. That might cause undesired behaviour if another
 * thread modifies such a field in between these readings. To avoid that, any
 * code transformed by this class must eventually undergo the optimization
 * <code>PTAOptimizer.conditionalTrees()</code>, which collapses all of
 * the readings into output transitions of a single state.
 * 
 * @author Artur Rataj
 */
public class ConstantIndexing {
    /**
     * Results of static analysis at runtime.
     */
    RuntimeStaticAnalysis sa;
    /**
     * If to modify indexing of java--type elements only.
     */
    boolean onlyJavaType;
    /**
     * Do not modify reads of non--java type elements, unless a side
     * effect of modifying a respective write. Meaningless if
     * <code>onlyJavaType</code> is true, as in such a case these reads
     * would be ignored anyway.
     */
    boolean ignoreScalarReads;
    
    /**
     * A new object for removing indexing with variables from methods.<br>
     * 
     * @param sa results of static analysis at runtime; changes to methods
     * in <code>transform()</code> are reflected in this object's trace
     * @param onlyJavaType if to modify indexing of java--type elements only
     * @param ignoreScalarReads ignore reads of non--java type elements,
     * unless their modification would be a side effect of modifying a respective write
     * in the same operation; presumably to replace these reads later with
     * Prism's formulas; meaningless if <code>onlyJavaType</code> is true
     */
    public ConstantIndexing(RuntimeStaticAnalysis sa,
            boolean onlyJavaType, boolean ignoreScalarReads) {
        this.sa = sa;
        this.onlyJavaType = onlyJavaType;
        this.ignoreScalarReads = ignoreScalarReads;
    }
    /**
     * Replaces variable indexing with constant indexing in a method.
     * 
     * @param rm a method with runtime information
     * @param result if not null, description of any constant indexing added by this method
     * is put into the object; it must be empty or an exception is thrown
     * @param onlyCIInfo only create constant indexing information, do not actually
     * modify anything in <code>rm</code>; typically to supply the information to
     * <code>Unroll</code>
     * @return if any constant indexing has been introduced by this method; always
     * false if <code>onlyCIInfo</code> is true
     */
    public boolean transform(RuntimeMethod rm, ConstantIndexingInfo result,
            boolean onlyCIInfo) throws InterpreterException {
        int flagCounter = 0;
        CodeMethod cm = rm.method;
        // CodeValuePossibility possibility = sa.getTrace(rm);
        boolean totalStable = true;
        boolean stable;
        do {
            stable = true;
            // in a single run, a common flag will do for all subsequent ops
            String name;
            do {
                 name = "#b" + (flagCounter++);
            } while(cm.variables.containsKey(name));
            CodeVariable flag = new CodeVariable(cm.getStreamPos(),
                    cm, name, new Type(Type.PrimitiveOrVoid.BOOLEAN),
                    Context.NON_STATIC, false, false, false,
                    Variable.Category.INTERNAL_NOT_RANGE);
            List<AbstractCodeOp> newList = new LinkedList<>();
            int index = 0;
            for(AbstractCodeOp op : cm.code.ops) {
/*if(op.toString().indexOf("i]") != -1)
    op = op;*/
                boolean indexingReplaced = false;
                Set<CodeIndexDereference> indexing;
                if(ignoreScalarReads) {
                    indexing = new HashSet<>();
                    // only Java type reads
                    Set<CodeIndexDereference> reads = op.getIndexDereferences(false);
                    for(CodeIndexDereference i : reads)
                        if(i.getElementType().isJava())
                            indexing.add(i);
                    // all writes
                    if(op.getResult() instanceof CodeIndexDereference)
                        indexing.add((CodeIndexDereference)op.getResult());
                } else
                    indexing = op.getIndexDereferences();
                TreeMap<String, CodeIndexDereference> sortedIndexing =
                        new TreeMap<>();
                for(CodeIndexDereference d : indexing) {
                    if(!onlyJavaType || d.getElementType().isJava())
                        // clash dereferences that use the same index,
                        // as can be splitted together
                        sortedIndexing.put(d.index.value.toNameString(), d);
                }
                for(CodeIndexDereference d : sortedIndexing.values())
                    if(!(d.index.value instanceof CodeConstant)) {
                        if(!(d.index.value instanceof CodeFieldDereference))
                            throw new InterpreterException(op.getStreamPos(),
                                    "index must be a scalar");
                        // add the flag variable only when it is known that it is
                        // really needed
                        if(!cm.variables.containsKey(name))
                            cm.addLocal(flag, false);
                        CodeFieldDereference indexVariable = (CodeFieldDereference)
                                d.index.value;
                        if(result != null) {
                            if(indexVariable.isLocal()) {
                                int beginIndex = newList.size();
                                Set<CodeVariable> set = result.insertions.get(beginIndex);
                                if(set == null) {
                                    set = new HashSet<>();
                                    result.insertions.put(beginIndex, set);
                                }
                                set.add(indexVariable.variable);
// System.out.println("ci: @" + beginIndex + " " +
//         indexVariable.variable.toNameString());
                            }
                        }
                        if(!onlyCIInfo) {
                            ArrayStore store;
                            try {
                                store = sa.getArrayStore(rm, index, d);
                            } catch(InterpreterException e) {
                                // could not evaluate as a constant
                                store = null;
                            }
                            if(store != null) {
                                int length = store.getLength();
                                StreamPos pos = op.getStreamPos();
                                if(op.label != null) {
                                    CodeOpNone beginning = new CodeOpNone(pos);
                                    beginning.label = op.label;
                                    newList.add(beginning);
                                }
                                CodeOpNone sink = new CodeOpNone(pos);
                                sink.label = cm.newLabel();
                                long evaluationId = CodeOpBinaryExpression.getNewSingleEvaluationId();
                                for(int i = 0; i < length; ++i) {
                                    CodeOpBinaryExpression c = new CodeOpBinaryExpression(
                                            CodeOpBinaryExpression.Op.BINARY_EQUAL, pos);
                                    c.singleEvaluationId = evaluationId;
                                    c.left = d.index;
                                    c.right = new RangedCodeValue(new CodeConstant(
                                            pos, new Literal(i)));
                                    c.result = new CodeFieldDereference(flag);
                                    newList.add(c);
                                    CodeOpBranch b = new CodeOpBranch(pos);
                                    b.condition = new RangedCodeValue(c.result);
                                    newList.add(b);
                                    AbstractCodeOp replacement = op.clone();
                                    // a possible label is held by <code>beginning</code>
                                    replacement.label = null;
                                    if(indexVariable.isLocal()) {
                                        if(!replacement.replaceLocalSourceVariable(indexVariable.variable,
                                                c.right.value))
                                            throw new RuntimeException("no replacement");
                                    } else {
                                        if(!replacement.replaceFieldSourceVariable(indexVariable,
                                                c.right.value))
                                            throw new RuntimeException("no replacement");
                                    }
                                    newList.add(replacement);
                                    CodeOpJump j = new CodeOpJump(pos);
                                    j.gotoLabelRef = new CodeLabel(sink.label);
                                    newList.add(j);
                                    if(i == length - 1) {
                                        // but the out of range operation
                                        CodeOpThrow t = new CodeOpThrow(pos);
                                        t.label = cm.newLabel();
                                        newList.add(t);
                                        b.labelRefFalse = new CodeLabel(t.label);
                                        // put the sink
                                        newList.add(sink);
                                    } else {
                                        CodeOpNone l = new CodeOpNone(pos);
                                        l.label = cm.newLabel();
                                        newList.add(l);
                                        b.labelRefFalse = new CodeLabel(l.label);
                                    }
                                }
                                indexingReplaced = true;
                            }
                        }
                        break;
                    }
                if(indexingReplaced)
                    stable = false;
                else
                    newList.add(op);
                ++index;
            }
            if(!stable) {
                cm.code.ops = newList;
                CodeLabel.setIndices(cm.code);
                // update the trace
                sa.traceRuntimeValues(rm);
                totalStable = false;
            }
        // another pass may be needed, if some operation
        // contains more than a single indexing
        } while(!stable);
        return !totalStable;
    }
}
