/*
 * BarrierLabels.java
 *
 * Created on Nov 3, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.pta.PTATransition;

/**
 * A set of labels that form a single barrier.
 * 
 * @author Artur Rataj
 */
public class BarrierLabels {
    /**
     * Name of the labels in this set.
     */
    public final String name;
    /**
     * All transitions involved.
     */
    public final PriorityQueue<PTATransition> transitions = new PriorityQueue<>();

    /**
     * Creates a new set of barrier labels, with a common name.
     * 
     * @param name name of the labels
     */
    public BarrierLabels(String name) {
        this.name = name;
    }
}
