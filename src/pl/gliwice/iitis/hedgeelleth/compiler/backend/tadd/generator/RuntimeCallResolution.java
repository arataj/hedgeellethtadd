/*
 * RuntimeCallResolution.java
 *
 * Created on Dec 6, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.generator;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.AbstractCodeDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeFieldDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeIndexDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.interpreter.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterFatalException;
import pl.gliwice.iitis.hedgeelleth.pta.PTAException;

/**
 *
 * @author Artur Rataj
 */
public class RuntimeCallResolution {
    /**
     * If to trace allocation values, false only for testing.
     */
    public boolean TRACE_ALLOC = true;
    AbstractInterpreter interpreter;
    CodeMethod method;
    RuntimeValue thiS;
    CodeMethod processedMethod;
    /**
     * Classes which are instantiated in this model, the set is used to filters
     * overridings to the limit code size.
     */
    final Set<CodeClass> instantiated;
    /**
     * Resolved copies of the already visited methods, keyed with
     * their keys.
     */
    Map<String, CodeMethod> resolvedCopies;
    
   /**
     *  Creates a new instance of <code>RuntimeCallResolution</code>.
     * 
     * @param interpreter interpreter containing the runtime values
     * @param startMethod start method
     * @param startThis "this" of the start method
     * @param instantiated classes which are instantiated in this model,
     * the set is used to filters overridings to limit the code size
     * @param traceAlloc copied to <code>TRACE_ALLOC</code>; false
     * only for testing
     */
    public RuntimeCallResolution(AbstractInterpreter interpreter,
            CodeMethod startMethod, RuntimeValue startThis,
            Set<CodeClass> instantiated, boolean traceAlloc) throws PTAException {
        this.interpreter = interpreter;
        resolvedCopies = new HashMap<>();
        TRACE_ALLOC = traceAlloc;
        processedMethod = resolve(startMethod, startThis, 100);
        this.instantiated = instantiated;
    }
    /**
     * <p>Replacement of a given call.</p>
     * 
     * <p>This class is used if a class is not estimated well enough,
     * so a sequence of switching instanceof tests is required.</p>
     */
    static class SwitchResolution {
        /**
         * Index of the operation to replace.
         */
        int index;
        /**
         * Replacement operations.
         */
        List<AbstractCodeOp> ops;
        
        private SwitchResolution(CodeMethod cm, int index,
                RangedCodeValue thatVariable, CodeOpCall call,
                CodeMethod called) {
            ops = new LinkedList<>();
            AbstractCodeDereference result = cm.newLocal(new Type(
                    Type.PrimitiveOrVoid.BOOLEAN),
                    "q", Variable.Category.INTERNAL_NOT_RANGE);
            CodeLabel labelAfter = new CodeLabel(result.getDereferencingVariable().
                        name + "_after");
            this.index = index;
            CodeLabel prevLabelSkip = null;
            int count = 0;
            SortedMap<CodeClass, CodeMethod> overridings =
                    new TreeMap<>(new Comparator<CodeClass>() {
                        @Override
                        public int compare(CodeClass a, CodeClass b) {
                            if(a == b)
                                return 0;
                            else if(a.isEqualToOrExtending(b))
                                return -1;
                            else if(b.isEqualToOrExtending(a))
                                return 1;
                            else
                                // unrelated classes sorted by name
                                return a.name.compareTo(b.name);
                        }
                    });
            overridings.putAll(called.overridings);
            if(called.code != null)
                overridings.put(called.owner, called);
            for(Map.Entry<CodeClass, CodeMethod> entry : overridings.entrySet()) {
                CodeOpUnaryExpression condition = new CodeOpUnaryExpression(
                        CodeOpUnaryExpression.Op.UNARY_INSTANCE_OF_NOT_NULL,
                        entry.getKey().type, call.getStreamPos());
                condition.sub = thatVariable;
                condition.setResult(result);
                condition.label = prevLabelSkip;
                ops.add(condition);
                CodeLabel labelSkip = new CodeLabel(result.getDereferencingVariable().
                        name + "_" + count);
                CodeOpBranch branch = new CodeOpBranch(call.getStreamPos());
                branch.condition = new RangedCodeValue(result);
                branch.labelRefFalse = labelSkip;
                branch.canBeDead = true;
                ops.add(branch);
                AbstractCodeDereference cast = cm.newLocal(new Type(
                        entry.getKey().type),
                        "q", Variable.Category.INTERNAL_NOT_RANGE);
                CodeOpUnaryExpression castAssignment = new CodeOpUnaryExpression(
                        CodeOpUnaryExpression.Op.UNARY_CAST,
                        cast.getResultType(), call.getStreamPos());
                castAssignment.sub = call.getThisArg();
                castAssignment.setResult(cast);
                ops.add(castAssignment);
                CodeOpCall focusedCall = (CodeOpCall)call.clone();
                // the label will be relayed to a holder 
                focusedCall.label = null;
                focusedCall.args.set(0, new RangedCodeValue(
                        castAssignment.getResult()));
                focusedCall.directNonStatic = true;
                // not to be used
                focusedCall.methodReference.firstPassMethod = null;
                focusedCall.methodReference.method = entry.getValue();
                prevLabelSkip = labelSkip;
                ops.add(focusedCall);
                CodeOpJump jump = new CodeOpJump(call.getStreamPos());
                jump.gotoLabelRef = labelAfter;
                ops.add(jump);
                ++count;
            }
            CodeOpThrow error = new CodeOpThrow(call.getStreamPos());
            error.label = prevLabelSkip;
            error.staticCheckError = new CompilerException(call.getStreamPos(),
                CompilerException.Code.INVALID, "class cast exception when resolving call: " +
                "referenced object can not be cast to " + call.methodReference.method.owner);
            ops.add(error);
            CodeOpNone after = new CodeOpNone(call.getStreamPos());
            after.label = labelAfter;
            ops.add(after);
        }
    }
    /**
     * Recursively determines called methods and modifies the calls
     * so that they point to the method called at runtime. This
     * is required for making virtual calls the direct ones,
     * so that they can be inlined.<br>
     * 
     * The calls are modified in copies of the methods, and the called
     * methods are copies as well. Thus, no original methods are
     * modified.<br>
     * 
     * All fields that are references are assumed to be constant.
     * Local variables, including arguments, are ignored, except
     * for arguments "this". If called method's "this" appears to
     * be an ignored local variable, then it is traced to a field,
     * an if the process is unsuccessfull, then if (1) the method does
     * not have overridings anyway, it is not resolved, that is,
     * the runtime type of the called method's "this" is assumed
     * to be the variable's type, and if (2) the method has
     * overridings, it means that the type can not be determined
     * by this method, and thus an error is reported.<br>
     * 
     * IGNORE and MARKER annotations cause a call to be ignored and
     * left unmodified.<br>
     * 
     * @param method                    current root method
     * @param thiS                      "this" for non--static methods
     *                                  and null for static methods
     * @param nestLevelsLeft            how deep can the calls recurse,
     *                                  within <code>method</code>; the top call
     *                                  should put the maximum recursion level here
     * @return                          method copy with resolved calls,
     *                                  the calls point to method
     *                                  copies with resolved calls
     *                                  as well, or, if the method is an
     *                                  already processed copy within the
     *                                  current recursive tree of
     *                                  <code>runtimeCallResolution</code>
     *                                  instances, this method returns
     *                                  its argument <code>method</code>,
     *                                  that is, that processed copy
     */
    public final CodeMethod resolve(CodeMethod method, RuntimeValue thiS,
            int nestLevelsLeft) throws PTAException {
        CodeMethod cm = new CodeMethod(method);
        RuntimeMethod rm = new RuntimeMethod(cm);
//if(cm.owner.toString().contains("Margin") && cm.signature.toString().contains("sleep"))
//    cm = cm;
        Map<CodeVariable, AbstractCodeValue> args =
                new HashMap<>();
        args.put(cm.getThisArg(), new CodeConstant(null, thiS));
        // trace only if really needed
        CodeValuePossibility possibility = null;
        CodeValuePossibility possibilityAllocation = null;
        if(thiS != null)
            interpreter.setValueUnchecked(new CodeFieldDereference(cm.getThisArg()),
                    thiS, rm);
        Map<Integer, SwitchResolution> switches = new HashMap<>();
if(cm.code == null)
    cm = cm;
        SCAN_OPS:
        for(int index = 0; index < cm.code.ops.size(); ++index) {
/*if(cm.signature.toString().indexOf("run") != -1)
    cm = cm;*/
            AbstractCodeOp op = cm.code.ops.get(index);
            if(op instanceof CodeOpCall) {
                CodeOpCall c = (CodeOpCall)op;
                CodeMethod called = null;
                RuntimeValue that;
/*if(c.toString().indexOf("Closure") != -1)
    c = c;*/
                if(!c.isVirtual())
                    that = null;
                else {
                    RangedCodeValue thisArg = c.getThisArg();
                    RangedCodeValue thatVariable = thisArg;
                    // dereference a possible array recursively, until a scalar
                    // value is found -- semantic check assures, that the scalar
                    // is the method's "this" argument
                    while(thatVariable.value instanceof CodeIndexDereference) {
                        CodeIndexDereference ti = (CodeIndexDereference)thatVariable.value;
                        CodeVariable object = ti.object;
                        if(object.isLocal()) {
                            // a local has an unknown runtime type when this method is called,
                            // so try to trace it to a field
                            if(possibility == null)
                                // no need for a complete trace
                                possibility = TraceValues.traceValuesWithoutCasts(cm, args, true);
                            AbstractCodeValue v = possibility.getSingleM1(
                                    index - 1, new CodeFieldDereference(object));
                            if(v != null)
                                thatVariable = new RangedCodeValue(v);
                            else
                                thatVariable = null;
                        } else
                            thatVariable = new RangedCodeValue(new CodeFieldDereference(object));
                    }
                    try {
                        if(thatVariable == null)
                            throw new InterpreterFatalException(
                                    InterpreterFatalException.Category.UNINITIALIZED_VARIABLE,
                                    "call resolution: could not trace a local to a field");
                        that = interpreter.getValueUnchecked(thatVariable, rm);
                    } catch(InterpreterFatalException e) {
                        if(e.category != InterpreterFatalException.Category.
                                UNINITIALIZED_VARIABLE)
                            throw new RuntimeException(e.toString());
                        if(possibilityAllocation == null)
                            // no need for a complete trace
                            possibilityAllocation = TraceValues.getUniqueAllocationTraceRecursive(
                                    cm, interpreter);
                        if(TRACE_ALLOC) {
                            AbstractCodeValue av = possibilityAllocation.getSingleM1(
                                    index - 1, (AbstractCodeDereference)(thatVariable.value));
                            if(av instanceof CodeConstant && ((CodeConstant)av).getResultType().
                                    isOfIntegerTypes()) {
                                int allocIndex = ((CodeConstant)av).value.getInteger();
                                CodeOpAllocation a = ((CodeOpAllocation)cm.code.ops.get(allocIndex));
                                called = c.getCalled(interpreter.getCodeClass(a.getInstantiatedType()));
                            } else
                                called = c.methodReference.method;
//                            AbstractCodeValue av = possibility.getSingleM1(
//                                    index - 1, (AbstractCodeDereference)(thatVariable.value));
//                            if(av instanceof CodeFieldDereference) {
//                                CodeFieldDereference f = (CodeFieldDereference)av;
//                                if(interpreter.getCodeClass(f.getResultType()).isNarrower(
//                                        interpreter.getCodeClass(thisArg.value.getResultType())))
//                                    thisArg = new RangedCodeValue(f);
//                            }
//                            called = c.getCalled(interpreter.getCodeClass(thisArg.value.getResultType()));
                        } else
                            called = c.methodReference.method;
                        if(/*!c.methodReference.method.overridings.
                                keySet().isEmpty() &&*/ !called.overridings.isEmpty()) {
                            switches.put(index, new SwitchResolution(cm, index,
                                thisArg, c, called));
//                            throw new PTAException(c.methodReference.firstPassMethod.getStreamPos(),
//                                    "type of \"this\" of a non-static method " + c.
//                                    methodReference.firstPassMethod.getDeclarationDescription() +
//                                    " required to avoid ambiguity, but " +
//                                    "local dereferences not traced during virtual call resolution");
                            continue SCAN_OPS;
                        }
                        that = null;
                    }
                }
                if(that != null) {
                    if(that.isNull()) {
                        throw new PTAException(op.getStreamPos(),
                                "within thread " + thiS.getReferencedObject().getUniqueName() + ": " +
                                "can not resolve virtual method call " +
                                "as runtime value of \"this\" is null: " + op.toString(cm));
                    }
                    CodeClass clazz;
                    if(that.type.isArray(null))
                        clazz = interpreter.getCodeClass(that.type.getElementType(null, 0));
                    else
                        clazz = interpreter.getCodeClass(that);
                    called = c.getCalled(clazz);
                    for(CodeMethod o : called.overridings.values()) {
                        if(that.type.isArray(null)) {
                            switches.put(index, new SwitchResolution(cm, index,
                                c.getThisArg(), c, c.methodReference.method));
//                            throw new PTAException(op.getStreamPos(),
//                                    "within thread " + thiS.getReferencedObject().getUniqueName() + ": " +
//                                    "can not resolve virtual method call " +
//                                    "as the type " + clazz + " is only estimated, and not exactly enough " +
//                                    "to exclude possible overridings");
                            continue SCAN_OPS;
                        }
                    }
                } else {
                    if(called == null)
                        called = c.methodReference.method;
                }
                boolean execute = true;
                for(String s : called.annotations) {
                    switch(CompilerAnnotation.parse(s)) {
                        case IGNORE:
                        case MARKER:
                            execute = false;
                            break;
                            
                    }
                }
                if(execute) {
                    if(c.directNonStatic)
                        try {
                            that = interpreter.getValueUnchecked(c.getThisArg(), rm);
                        } catch(InterpreterFatalException e) {
                            if(e.category != InterpreterFatalException.Category.
                                    UNINITIALIZED_VARIABLE)
                                throw new RuntimeException(e.toString());
                        }
                    if(nestLevelsLeft == 0)
                        throw new PTAException(c.getStreamPos(), "call stack overflow");
                    CodeMethod resolved;
                    if(resolvedCopies.containsKey(called.getKey()))
                        resolved = resolvedCopies.get(called.getKey());
                    else
                        resolved = resolve(
                            called, that, nestLevelsLeft - 1);
                    CodeOpCall call = (CodeOpCall)c.clone();
                    call.methodReference = new CodeMethodReference(
                            call.methodReference.getStreamPos(),
                            null);
//                    log(2, 
//                            "method call " + c.methodReference.method.
//                            getDeclarationDescription() +
//                            " in method runtime " + rm + " resolved to be " +
//                            resolved.getDeclarationDescription());
                    call.methodReference.method = resolved;
                    if(call.isVirtual())
                        // the call is resolved, so it is no longer virtual
                        call.directNonStatic = true;
                    cm.code.replace(index, call, false);
                }
            }
        }
//if(cm.owner.toString().contains("Margin") && cm.signature.toString().contains("sleep"))
//    cm = cm;
        boolean callsAdded = !switches.isEmpty();
        if(callsAdded) {
            for(int index = cm.code.ops.size() - 1; index >= 0; --index) {
                SwitchResolution r = switches.get(index);
                if(r != null) {
//if(index == 7)
//    index = index;
                    AbstractCodeOp replaced = cm.code.ops.get(index);
                    CodeOpNone labelHolder = new CodeOpNone(
                            replaced.getStreamPos());
                    labelHolder.label = replaced.label;
                    cm.code.ops.remove(index);
                    cm.code.ops.add(index, labelHolder);
                    cm.code.ops.addAll(index + 1, r.ops);
                }
//for(AbstractCodeOp op : cm.code.ops) {
//    if(op.label != null)
//        op.label.codeIndex = -1;
//    for(CodeLabel l : op.getLabelRefs())
//        l.codeIndex = -1;
//}
//CodeLabel.setIndices(cm.code);
            }
            CodeLabel.setIndices(cm.code);
        }
        resolvedCopies.put(cm.getKey(), cm);
        if(callsAdded)
            // as new calls have been added, rerun the resolution on this method
            cm = resolve(cm, thiS, nestLevelsLeft);
        return cm;
    }
    /**
     * Returns a method with the calls resolved or splitted.
     * 
     * @return a new method, independent from the old ones
     */
    CodeMethod getMethod() {
        return processedMethod;
    }
}
