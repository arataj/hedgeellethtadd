/*
 * BackendPTAArrayVariable.java
 *
 * Created on Jul 20, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd;

import pl.gliwice.iitis.hedgeelleth.pta.PTAException;
import pl.gliwice.iitis.hedgeelleth.compiler.sa.RangeCodeStaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.sa.RangeCodeStaticAnalysis.Proposal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.AbstractRuntimeContainer;
import pl.gliwice.iitis.hedgeelleth.pta.*;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAArrayVariable;

/**
 * An array PTA variable, plus the functionality needed by the compiler.
 * 
 * @author Artur Rataj
 */
public class BackendPTAArrayVariable extends AbstractBackendPTAVariable {
    /**
     * Sum of all proposals, used by <code>registerVariableRangeProposal</code>
     * and <code>computeFinalRange</code>.
     */
    RangeCodeStaticAnalysis.Proposal proposalSum;
    
    /**
     * Creates a wrapper for an array PTA variable.
     * 
     * @param pos position in the source stream, null for none
     * @param container container of this variable
     * @param v variable to wrap
     */
    public BackendPTAArrayVariable(StreamPos pos,
            AbstractRuntimeContainer container, PTAArrayVariable v) {
        super(pos, container, v);
        proposalSum = null;
    }
    @Override
    public PTAArrayVariable v() {
        return (PTAArrayVariable)v;
    }
    @Override
    public void enlargeRangeToFitInitValues(boolean shouldAlreadyIncludeInit) {
        for(int i = 0; i < this.v().length; ++i) {
            double v = v().initValue[i];
            if(PTASystem.getMin(v().range.range) > v) {
                if(shouldAlreadyIncludeInit)
                    throw new RuntimeException("does not include some input value");
                BackendPTACompilation.setMin(v().range.range, v);
            } if(PTASystem.getMax(v().range.range) < v) {
                if(shouldAlreadyIncludeInit)
                    throw new RuntimeException("does not include some input value");
                BackendPTACompilation.setMax(v().range.range, v);
            }
        }
    }
    @Override
    public void proposeRange(RangeCodeStaticAnalysis.Proposal proposal) {
        throw new RuntimeException("not applicable to arrays");
    }
    /**
     * Registers a variable range of a code variable that represents
     * this variable in an indexing dereference. Every indexing
     * of this variable in the code translated to transitions should
     * be registered using this method.<br>
     *
     * This method replaces <code>proposeRange()</code>. The reason for
     * this replacement is, that there can be multiple code variables
     * for one array TADD variable.<br>
     *
     * The registered variable ranges are checked against each other
     * for equality. If they are equal, range of this TADD variable
     * is determined. If they are not equal, an error is reported.<br>
     *
     * //Note that possible variable ranges of non--indexing dereferences
     * //of this variable are meaningless -- this is because only actual
     * //indexing of arrays is translated into the transitions. Thus,
     * //variable ranges of non--indexing dereferences should not be
     * //registered using this method.
     *
     * @param newRange                  range to register
     */
    public void registerVariableRange(BackendPTARange newRange) throws PTAException {
        if(v().range != null) {
            if(!v().range.range.arithmeticEqualTo(newRange.r().range)) {
                throw new PTAException(null,
                        "range " + newRange.toString() + " of a variable at " +
                        newRange.getStreamPos() + " does not match range " +
                        v().range.toString() + " of a variable at " +
                        v().range.backendPos + ", but both variables are indexed when referring " +
                        "to the same array");
            }
            if(newRange.r().backendPos != null && v().range.backendPos == null)
                v().range.backendPos = newRange.r().backendPos;
        } else {
            v().range = new PTARange(newRange.r());
            for(int i = 0; i < v().length; ++i) {
                double v = v().initValue[i];
                if(v < PTASystem.getMin(v().range.range) ||
                        v > PTASystem.getMax(v().range.range))
                    throw new PTAException(null,
                            "initial value " +
                            (v().range.range.isFloating() ? "" + v : "" + (int)v) +
                            " is outside the range " +
                            v().range.toString() + " of runtime array" +
                            (v().range.backendPos != null ? " referenced by variable at " +
                                    v().range.backendPos :
                                ""));
            }
        }
    }
    /**
     * Registers a variable range proposal of a code variable that represents
     * this variable in an indexing dereference. Every indexing
     * of this variable in the code translated to transitions should
     * be registered using this method.<br>
     *
     * After all proposals are registered, call <code>computeFinalRange</code>
     * to compute the range on basis of the proposals.<br>
     *
     * This implementation enlarges a single, internal proposal
     * <code>proposalSum</code> to cover
     * all input proposals, and in <code>computeRangeFromProposals</code> only
     * the internal proposal is taken into account.<br>
     *
     * Note that possible variable ranges of non--indexing dereferences
     * of this variable are meaningless -- this is because only actual
     * indexing of arrays is translated into the transitions. Thus,
     * variable ranges of non--indexing dereferences should not be
     * registered using this method.
     *
     * @param newRange                  range to register
     */
    public void registerVariableRangeProposal(RangeCodeStaticAnalysis.Proposal proposal)
            throws PTAException {
        if(proposalSum == null)
            proposalSum = new RangeCodeStaticAnalysis.Proposal(proposal);
        else {
            if(!proposal.rangeKnown() || proposal.isEmpty())
                proposalSum = new RangeCodeStaticAnalysis.Proposal(proposal);
            else {
                if(!proposalSum.min.arithmeticLessThan(proposal.min))
                    proposalSum.min = proposal.min.clone();
                if(proposalSum.max.arithmeticLessThan(proposal.max))
                    proposalSum.max = proposal.max.clone();
            }
        }
    }
    /**
     * Computes final range.<br>
     *
     * All this implementation needs to do, is to possibly shrink <code>range</code>
     * to <code>proposalSum</code>.<br>
     *
     * This method should be called:<br>
     *
     * (1) <i>after</i> <code>registerVariableRange()</code>, so that the range to
     * shrink has a valid value;<br>
     *
     * (2) <i>before</i> <code>enlargeRangeToFitInitValues()</code>,
     * as the proposals do not take into account initial values.
     */
    public void computeRangeFromProposals() {
        boolean onlyInitial = false;
        boolean guaranteed = false;
//        if(v().range == null)
//            if(v().floating)
//                v().range = new PTARange(new Type(Type.PrimitiveOrVoid.INT));
//            else
//                v().range = new PTARange(new Type(Type.PrimitiveOrVoid.DOUBLE));
        if(proposalSum != null) {
            if(proposalSum.rangeKnown()) {
                if(proposalSum.isEmpty()) {
                    // no viable assignments in the code
                    onlyInitial = true;
                    guaranteed = true;
                } else {
                    guaranteed = true;
                    if(PTASystem.getMin(v().range.range) <=
                            proposalSum.min.getMaxPrecisionFloatingPoint())
                        BackendPTACompilation.setMin(v().range.range,
                                proposalSum.min.getMaxPrecisionFloatingPoint());
                    else
                        guaranteed = false;
                    if(PTASystem.getMax(v().range.range) >=
                            proposalSum.max.getMaxPrecisionFloatingPoint())
                        BackendPTACompilation.setMax(v().range.range,
                                proposalSum.max.getMaxPrecisionFloatingPoint());
                    else
                        guaranteed = false;
                }
            }
            this.proposal = proposalSum;
            // not needed, proposal is enough
            // v().range.backendGuaranteed = guaranteed;
        } else
            // no assignments in the code
            onlyInitial = true;
        if(onlyInitial) {
            if(v().length != 0) {
                this.proposal = new Proposal();
                this.proposal.min = new Literal(v().initValue[0]);
                this.proposal.max = new Literal(v().initValue[0]);
                for(int i = 1; i < v().length; ++i)
                    this.proposal = this.proposal.newExtendedToInclude(new Literal(v().initValue[i]));
                BackendPTACompilation.setMin(v().range.range, this.proposal.min.getDouble());
                BackendPTACompilation.setMax(v().range.range, this.proposal.max.getDouble());
            }
        }
    }
}
