/*
 * PrismSubFilter.java
 *
 * Created on Jul 13, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.prism;

import pl.gliwice.iitis.hedgeelleth.pta.io.AbstractSubFilter;
import pl.gliwice.iitis.hedgeelleth.pta.io.PTAIO;

/**
 * A PrismIO--specific sub--filter.
 * 
 * @author Artur Rataj
 */
public class PrismSubFilter extends AbstractSubFilter {
    public PrismSubFilter() {
    }
    @Override
    public String accept(String sub) {
        String out = sub;
        int delete = -1;
        String[] prefixes = {
            "active", "in", "notify", "out", "wait",
        };
        for(String p : prefixes) {
            if(sub.startsWith(PTAIO.BACKEND_CHANNEL_PREFIX + p)) {
                delete = (PTAIO.BACKEND_CHANNEL_PREFIX + p).length() + 1;
                break;
            }
        }
        if(delete == -1 && sub.startsWith(PTAIO.MODULE_PREFIX))
            delete = PTAIO.MODULE_PREFIX.length();
        if(delete != -1)
            out = out.substring(delete);
        return out;
    }
}
